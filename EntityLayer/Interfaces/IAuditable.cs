﻿using EntityLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EntityLayer.Interfaces
{
    public interface IAuditable
    {
        string UsuarioAlterando { get; set; }
        List<Auditoria> Auditorias {get; set;}
    }
}
