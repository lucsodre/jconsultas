﻿using System;
using System.Collections.Generic;

namespace EntityLayer.Models
{
    public partial class IpAddress
    {
        public int IpAddressId { get; set; }
        public int? ProfileId { get; set; }
        public string IpAddress1 { get; set; }
    }
}
