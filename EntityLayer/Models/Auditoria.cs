﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityLayer.Models
{
    public class Auditoria
    {
        public int IdAuditoria { get; set; }
        public string NomeTabela { get; set; }
        public DateTime DataHora { get; set; }
        public string Usuario { get; set; }
        public string ValorAnterior { get; set; }
        public string ValorNovo { get; set; }
        public string Descricao { get; set; }
        public int IdRegistro { get; set; }
    }
}
