﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityLayer.Models
{
    public class ConsultaAvancadaVeiculo
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public bool DadosVeic { get; set; }
        public bool DadosProp { get; set; }
        public bool DetalhamentoMultas { get; set; }
        public bool DetalhamentoDPVAT { get; set; }
        public bool DetalhamentoIPVA { get; set; }
        public bool DetalhamentoDebitos { get; set; }
        public bool BloqueiosDetran { get; set; }
        public bool InspecaoVeicular { get; set; }
        public bool Restricoes { get; set; }
        public bool IntencaoGravame { get; set; }
        public bool ArrendarioFinanceira { get; set; }
    }

    public class ConsultaAvancadaVeiculoWS
    {
        public bool AGREGADO { get; set; }
        public bool VEICULAR { get; set; }
        public bool DEBITOS_360 { get; set; }
        public bool NOTIFICACAO_MULTA { get; set; }
        public bool HISTORICO_MULTA { get; set; }
        public bool BLOQUEIO_RENAJUD { get; set; }
        public bool BLOQUEIO_DETRAN { get; set; }
        public bool GRAVAME { get; set; }
        public bool DADOS_PROPRIETARIO { get; set; }
    }
}
