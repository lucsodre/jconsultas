﻿using EntityLayer.Interfaces;
using EntityLayer.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EntityLayer.Models
{
    public partial class JConsultasContext : IdentityDbContext<ApplicationUser>
    {
        public virtual DbSet<AccountMaster> AccountMaster { get; set; }
        public virtual DbSet<IpAddress> IpAddress { get; set; }
        public virtual DbSet<MultipleData> MultipleData { get; set; }
        public virtual DbSet<MultipleMasterData> MultipleMasterData { get; set; }
        public virtual DbSet<ProfileMaster> ProfileMaster { get; set; }
        public virtual DbSet<QueryResult> QueryResult { get; set; }
        public virtual DbSet<UserMaster> UserMaster { get; set; }
        public virtual DbSet<VerificationCode> VerificationCode { get; set; }
        public virtual DbSet<Auditoria> Auditoria { get; set; }
        public virtual DbSet<ConsultaAvancadaVeiculo> ConsultaAvancadaVeiculo { get; set; }
        public static IConfigurationRoot Configuration { get; set; }

        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //            //optionsBuilder.UseSqlServer(@"Server=ASPDEVELOPERPC\SQLEXP2014DEV1;Initial Catalog=JConsultas;User ID=sa;Password=connectus@123;Trusted_Connection=True;");
        //            //optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["BloggingDatabase"].ConnectionString);
        //            //optionsBuilder.UseSqlServer(stringConexaoBanco);
        //            string stringConexaoBanco = Configuration.GetConnectionString("DefaultConnection");
        //            optionsBuilder.UseSqlServer(stringConexaoBanco);
        //        }

        public JConsultasContext(DbContextOptions<JConsultasContext> options) : base(options)
        {
        }

        public override int SaveChanges()
        {
            var auditEntries = OnBeforeSaveChanges();
            var r = base.SaveChanges();
            OnAfterSaveChanges(auditEntries);
            return r;
        }

        private List<AuditEntry> OnBeforeSaveChanges()
        {
            JObject traducao = null;

            try
            {
                traducao = JObject.Parse(File.ReadAllText(@"wwwroot\Localization\locale-pt_BR.json"));
            }
            catch (Exception)
            {
                traducao = null;
            }

            ChangeTracker.DetectChanges();
            var auditEntries = new List<AuditEntry>();
            foreach (var entry in ChangeTracker.Entries())
            {
                if (!(entry.Entity is IAuditable) || entry.State == EntityState.Detached || entry.State == EntityState.Unchanged)
                    continue;

                var auditEntry = new AuditEntry(entry);
                auditEntry.TableName = entry.Metadata.Relational().TableName;

                if (entry.Entity is IAuditable)
                {
                    var entidade = entry.Entity as IAuditable;

                    auditEntry.Usuario = entidade.UsuarioAlterando;
                }

                if (auditEntry.Usuario == null)
                {
                    continue;
                }

                auditEntries.Add(auditEntry);

                List<DetalheAuditoria> detalhesAuditoria = new List<DetalheAuditoria>();

                foreach (var property in entry.Properties)
                {
                    string propertyName;

                    if (traducao != null)
                    {
                        var nomePropriedadeTraduzido = (string)traducao[property.Metadata.Name];

                        propertyName = (nomePropriedadeTraduzido != null) && (nomePropriedadeTraduzido != string.Empty) ? nomePropriedadeTraduzido : property.Metadata.Name;
                    }
                    else
                    {
                        propertyName = property.Metadata.Name;
                    }
                    var detalhe = new DetalheAuditoria();

                    if (property.IsTemporary)
                    {
                        auditEntry.TemporaryProperties.Add(property);
                        continue;
                    }

                    if (property.Metadata.IsPrimaryKey())
                    {
                        auditEntry.IdRegistro = int.Parse(property.CurrentValue.ToString());
                        continue;
                    }

                    switch (entry.State)
                    {
                        case EntityState.Added:
                            detalhe.ValorNovo = property.CurrentValue != null ? property.CurrentValue.ToString() : "nulo";
                            detalhe.ValorAnterior = "nulo";
                            detalhe.Descricao = "Cadastro criado com valor inicial: " + detalhe.ValorNovo + " para a propriedade " + propertyName;
                            detalhesAuditoria.Add(detalhe);
                            break;

                        case EntityState.Deleted:
                            detalhe.ValorNovo = "Excluído";
                            detalhe.ValorAnterior = property.OriginalValue.ToString();
                            detalhe.Descricao = "Cadastro excluído";
                            detalhesAuditoria.Add(detalhe);
                            break;

                        case EntityState.Modified:
                            if (property.IsModified)
                            {
                                if (propertyName != "ModifiedBy" && propertyName != "ModifiedAt" && propertyName != "CreatedBy" && propertyName != "CreatedAt"
                                    && propertyName != "Última atualização feita por" && propertyName != "Última atualização" && propertyName != "Criado por" && propertyName != "Data de criação")
                                { 
                                    var valorAnterior = string.Format("{0}", property.OriginalValue);
                                    var valorNovo = string.Format("{0}", property.CurrentValue);

                                    if (valorNovo != valorAnterior)
                                    {
                                        if (traducao != null)
                                        {
                                            var valorAnteriorTraduzido = (string)traducao[valorAnterior];
                                            var valorNovoTraduzido = (string)traducao[valorNovo];

                                            valorAnterior = (valorAnteriorTraduzido != null) && (valorAnteriorTraduzido != string.Empty) ? valorAnteriorTraduzido : valorAnterior;
                                            valorNovo = (valorNovoTraduzido != null) && (valorNovoTraduzido != string.Empty) ? valorNovoTraduzido : valorNovo; 
                                        }

                                        detalhe.ValorNovo = valorNovo == string.Empty ? "nulo" : valorNovo;
                                        detalhe.ValorAnterior = valorAnterior == string.Empty ? "nulo" : valorAnterior;
                                        detalhe.Descricao = "O valor da propriedade " + propertyName + " foi alterado de " + detalhe.ValorAnterior + " para " + detalhe.ValorNovo;
                                        detalhesAuditoria.Add(detalhe);
                                    }
                                }
                            }
                            break;
                    }
                }

                auditEntry.Detalhes = detalhesAuditoria;
            }
            
            return auditEntries.ToList();
        }

        private void OnAfterSaveChanges(List<AuditEntry> auditEntries)
        {
            if (auditEntries == null || auditEntries.Count == 0)
                return;
    
            foreach (var auditEntry in auditEntries)
            {
                foreach (var audit in auditEntry.ToAudit())
                {
                    foreach (var prop in auditEntry.TemporaryProperties)
                    {
                        if (prop.Metadata.IsPrimaryKey())
                        {
                            audit.IdRegistro = int.Parse(prop.CurrentValue.ToString());
                        }
                    }
                    Auditoria.Add(audit);
                }                
            }

            SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AccountMaster>(entity =>
            {
                entity.HasKey(e => e.AccountId)
                    .HasName("PK_AccountMaster");

                entity.Property(e => e.AccountId).HasColumnName("AccountID");

                entity.Property(e => e.AccountName).HasMaxLength(250);

                entity.Property(e => e.ApiCode).HasMaxLength(50);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(450);

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(450);
            });

            //modelBuilder.Entity<ConsultaAvancadaVeiculo>(entity =>
            //{
            //    entity.Property(e => e.DetalhamentoDPVAT).HasColumnName("DetalhamentoDPVAP");
            //    entity.Property(e => e.InspecaoVeicular).HasColumnName("InspesaoVeicular");
            //});

            modelBuilder.Entity<Auditoria>(entity =>
            {
                entity.HasKey(e => e.IdAuditoria)
                    .HasName("PK_Auditoria");

                entity.Property(e => e.IdAuditoria).HasColumnName("IdAuditoria");

                entity.Property(e => e.IdRegistro);

                entity.Property(e => e.NomeTabela).HasMaxLength(40);

                entity.Property(e => e.Usuario).HasMaxLength(300);

                entity.Property(e => e.Descricao).HasMaxLength(2000);

                entity.Property(e => e.DataHora).HasColumnType("datetime");

                entity.Property(e => e.ValorAnterior).HasMaxLength(100);

                entity.Property(e => e.ValorNovo).HasMaxLength(100);
            });
            
            //modelBuilder.Entity<AspNetRoleClaims>(entity =>
            //{
            //    entity.Property(e => e.RoleId)
            //        .IsRequired()
            //        .HasMaxLength(450);

            //    entity.HasOne(d => d.Role)
            //        .WithMany(p => p.AspNetRoleClaims)
            //        .HasForeignKey(d => d.RoleId);
            //});

            //modelBuilder.Entity<AspNetRoles>(entity =>
            //{
            //    entity.Property(e => e.Id).HasMaxLength(450);

            //    entity.Property(e => e.Name).HasMaxLength(256);

            //    entity.Property(e => e.NormalizedName).HasMaxLength(256);
            //});

            //modelBuilder.Entity<AspNetUserClaims>(entity =>
            //{
            //    entity.Property(e => e.UserId)
            //        .IsRequired()
            //        .HasMaxLength(450);

            //    entity.HasOne(d => d.User)
            //        .WithMany(p => p.AspNetUserClaims)
            //        .HasForeignKey(d => d.UserId);
            //});

            //modelBuilder.Entity<AspNetUserLogins>(entity =>
            //{
            //    entity.HasKey(e => new { e.LoginProvider, e.ProviderKey })
            //        .HasName("PK_AspNetUserLogins");

            //    entity.Property(e => e.LoginProvider).HasMaxLength(450);

            //    entity.Property(e => e.ProviderKey).HasMaxLength(450);

            //    entity.Property(e => e.UserId)
            //        .IsRequired()
            //        .HasMaxLength(450);

            //    entity.HasOne(d => d.User)
            //        .WithMany(p => p.AspNetUserLogins)
            //        .HasForeignKey(d => d.UserId);
            //});

            //modelBuilder.Entity<AspNetUserRoles>(entity =>
            //{
            //    entity.HasKey(e => new { e.UserId, e.RoleId })
            //        .HasName("PK_AspNetUserRoles");

            //    entity.Property(e => e.UserId).HasMaxLength(450);

            //    entity.Property(e => e.RoleId).HasMaxLength(450);

            //    entity.HasOne(d => d.Role)
            //        .WithMany(p => p.AspNetUserRoles)
            //        .HasForeignKey(d => d.RoleId);

            //    entity.HasOne(d => d.User)
            //        .WithMany(p => p.AspNetUserRoles)
            //        .HasForeignKey(d => d.UserId);
            //});

            //modelBuilder.Entity<AspNetUserTokens>(entity =>
            //{
            //    entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name })
            //        .HasName("PK_AspNetUserTokens");

            //    entity.Property(e => e.UserId).HasMaxLength(450);

            //    entity.Property(e => e.LoginProvider).HasMaxLength(450);

            //    entity.Property(e => e.Name).HasMaxLength(450);
            //});

            //modelBuilder.Entity<AspNetUsers>(entity =>
            //{
            //    entity.Property(e => e.Id).HasMaxLength(450);

            //    entity.Property(e => e.Email).HasMaxLength(256);

            //    entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

            //    entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

            //    entity.Property(e => e.UserName).HasMaxLength(256);
            //});

            modelBuilder.Entity<IpAddress>(entity =>
            {
                entity.Property(e => e.IpAddress1)
                    .HasColumnName("IpAddress")
                    .HasMaxLength(50);

                entity.Property(e => e.ProfileId).HasColumnName("ProfileID");
            });

            modelBuilder.Entity<MultipleData>(entity =>
            {
                entity.Property(e => e.Chassi)
                    .HasColumnName("chassi")
                    .HasMaxLength(50);

                entity.Property(e => e.Placa)
                    .HasColumnName("placa")
                    .HasMaxLength(50);

                entity.Property(e => e.UF)
                    .HasColumnName("UF")
                    .HasMaxLength(2);

                entity.Property(e => e.Nome)
                    .HasColumnName("Nome")
                    .HasMaxLength(350);

                entity.Property(e => e.CPF)
                    .HasColumnName("CPF")
                    .HasMaxLength(11);

                entity.Property(e => e.NumRegistro)
                    .HasColumnName("NumRegistro")
                    .HasMaxLength(11);

                entity.Property(e => e.NumRenach)
                    .HasColumnName("NumRenach")
                    .HasMaxLength(30);

                entity.Property(e => e.ValidadeCNH)
                    .HasColumnName("ValidadeCNH");

                entity.Property(e => e.RG)
                    .HasColumnName("RG")
                    .HasMaxLength(9);

                entity.Property(e => e.DataNascimento)
                    .HasColumnName("DataNascimento");

                entity.Property(e => e.DataHabilitacao)
                    .HasColumnName("DataHabilitacao");

                entity.Property(e => e.MunicipioNascimento)
                    .HasColumnName("MunicipioNascimento")
                    .HasMaxLength(350);

                entity.Property(e => e.NumCedulaEspelho)
                    .HasColumnName("NumCedulaEspelho")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<MultipleMasterData>(entity =>
            {
                entity.HasKey(e => e.MultipleMasterDataid)
                    .HasName("PK_MultipleMasterData");

                entity.Property(e => e.CreatedBy).HasColumnType("datetime");

                entity.Property(e => e.FileName).HasMaxLength(50);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(450);
            });

            modelBuilder.Entity<ProfileMaster>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(450);

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(450);

                entity.Property(e => e.ProfileName).HasMaxLength(50);
            });

            modelBuilder.Entity<QueryResult>(entity =>
            {
                entity.HasKey(e => e.QueryResultid)
                    .HasName("PK_QueryResult");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.DocumentCode).HasMaxLength(50);

                entity.Property(e => e.Error).HasMaxLength(500);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(450);

                entity.Property(e => e.TipoDeConsulta)
                    .HasColumnName("TipoDeConsulta");

                entity.Property(e => e.Xml).HasColumnName("XML");
            });

            modelBuilder.Entity<UserMaster>(entity =>
            {
                entity.HasKey(e => e.ProfileId)
                    .HasName("PK_UserMaster_1");

                entity.Property(e => e.ProfileId).HasColumnName("ProfileID");

                entity.Property(e => e.AccessToken).HasMaxLength(50);

                entity.Property(e => e.AccountId).HasColumnName("AccountID");

                entity.Property(e => e.ApiPassword).HasMaxLength(250);

                entity.Property(e => e.ApiSenha)
                    .HasColumnName("apiSenha")
                    .HasMaxLength(250);

                entity.Property(e => e.ApiUser).HasMaxLength(250);

                entity.Property(e => e.ApiUsuario)
                    .HasColumnName("apiUsuario")
                    .HasMaxLength(250);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(450);

                entity.Property(e => e.Id).HasMaxLength(450);

                entity.Property(e => e.LastLogin).HasColumnType("datetime");

                entity.Property(e => e.LastName).HasMaxLength(250);

                entity.Property(e => e.MobileNo).HasMaxLength(20);

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(450);

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.Picture).HasMaxLength(250);

                entity.Property(e => e.UseDoubleVerification).HasMaxLength(50);
            });

            modelBuilder.Entity<VerificationCode>(entity =>
            {
                entity.HasKey(e => e.VerificationCodeId)
                    .HasName("PK_VerificationCode");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ExpDateTime).HasColumnType("datetime");

                entity.Property(e => e.Id).HasMaxLength(450);

                entity.Property(e => e.ProfileId).HasColumnName("ProfileID");

                entity.Property(e => e.VerificationCode1)
                    .HasColumnName("VerificationCode")
                    .HasMaxLength(50);
            });
        }
    }
}


public class AuditEntry
{
    public AuditEntry(EntityEntry entry)
    {
        Entry = entry;
    }
    public List<PropertyEntry> TemporaryProperties { get; } = new List<PropertyEntry>();

    public EntityEntry Entry { get; }
    public string TableName { get; set; }

    public string Usuario { get; set; }
    public string Descricao { get; set; }

    public int IdRegistro { get; set; }

    public List<DetalheAuditoria> Detalhes { get; set; }

    private List<Auditoria> Auditorias = new List<Auditoria>();

    public List<Auditoria> ToAudit()
    {
        foreach (var detalhe in Detalhes)
        {
            var audit = new Auditoria();
            audit.NomeTabela = TableName;
            audit.DataHora = DateTime.UtcNow;
            audit.Usuario = Usuario;
            audit.ValorAnterior = detalhe.ValorAnterior;
            audit.ValorNovo = detalhe.ValorNovo;
            audit.Descricao = detalhe.Descricao;
            audit.IdRegistro = IdRegistro;
            Auditorias.Add(audit);
        }    

        return Auditorias;
    }
}



public class DetalheAuditoria
{
    public string ValorAnterior { get; set; }
    public string ValorNovo { get; set; }
    public string Descricao { get; set; }
}