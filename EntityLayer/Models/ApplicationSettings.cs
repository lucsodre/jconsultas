﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityLayer.Models
{
    public class ApplicationSettings
    {
        public string sSiteURL { get; set; }
        public string ServiceURL { get; set; }
        public string PathLogErro { get; set; }
        
        public string SMTPName { get; set; }
        public int SMTPPortNo { get; set; }
        public string EmailUserName { get; set; }
        public string EmailUserPassword { get; set; }
        public string AWSAccessKey { get; set; }
        public string AWSSecretKey { get; set; }

        public string ApiUsuario { get; set; }
        public string ApiSenha { get; set; }
        public string ApiCodigo { get; set; }
    }
}
