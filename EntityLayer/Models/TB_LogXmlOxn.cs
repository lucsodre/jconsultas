﻿using System;
using System.Collections.Generic;

namespace EntityLayer.Models
{
    public partial class TB_LogXmlOxn
    {
        public int ID_LogXmlOxn { get; set; }
        public string XmlRetorno { get; set; }
        public Boolean? reuso { get; set; }
        public DateTime DataLcto { get; set; }
        public DateTime? data_ini_consulta { get; set; }
        public int ID_Consulta { get; set; }
       
    }
}
