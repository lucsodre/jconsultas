﻿using System;
using System.Collections.Generic;

namespace EntityLayer.Models
{
    public partial class VerificationCode
    {
        public int VerificationCodeId { get; set; }
        public string VerificationCode1 { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ExpDateTime { get; set; }
        public int? ProfileId { get; set; }
        public string Id { get; set; }
    }
}
