﻿using System;
using System.Collections.Generic;

namespace EntityLayer.Models
{
    public partial class QueryResult
    {
        public int QueryResultid { get; set; }
        public string DocumentCode { get; set; }
        public string Xml { get; set; }
        public string ConsultaSolicitada { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string Id { get; set; }
        public int? MultipleDataId { get; set; }
        public bool? IsError { get; set; }
        public string Error { get; set; }
        public int TipoDeConsulta { get; set; }
        public bool IsConsultaApi { get; set; }
    }
}
