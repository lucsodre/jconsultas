﻿using EntityLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityLayer.Models
{
    public partial class UserProfile : IAuditable
    {
        public int UserProfileId { get; set; }
        public int? ProfileId { get; set; }
        public bool? UserMaster { get; set; }
        public bool? Account { get; set; }
        public bool? SingleSearch { get; set; }
        public bool? BatchSearch { get; set; }
        public bool? IsActive { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }

        [NotMapped]
        public string UsuarioAlterando { get; set; }

        [NotMapped]
        public List<Auditoria> Auditorias { get; set; }
    }
}
