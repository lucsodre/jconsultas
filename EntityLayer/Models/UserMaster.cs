﻿using EntityLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityLayer.Models
{
    public partial class UserMaster : IAuditable
    {
        [DisplayName("Nome")]
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Picture { get; set; }
        public string ApiUser { get; set; }
        public string ApiPassword { get; set; }
        public string ApiUsuario { get; set; }
        public string ApiSenha { get; set; }
        public string MobileNo { get; set; }
        public int? ProfileMasterId { get; set; }
        public int? AccountId { get; set; }
        public int ProfileId { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public bool? IsActive { get; set; }
        public string Id { get; set; }
        public DateTime? LastLogin { get; set; }
        public string UseDoubleVerification { get; set; }
        public bool? IsEmailVerifeyed { get; set; }
        public bool? IsPhonelVerifeyed { get; set; }
        public string AccessToken { get; set; }
        
        [NotMapped]
        public string UsuarioAlterando { get; set; }

        [NotMapped]
        public List<Auditoria> Auditorias { get; set; }
    }
}
