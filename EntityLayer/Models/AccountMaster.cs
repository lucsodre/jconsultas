﻿using EntityLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityLayer.Models
{
    public partial class AccountMaster : IAuditable
    {
       
        public int AccountId { get; set; }

        [DisplayName("Nome")]
        public string AccountName { get; set; }

        public string ApiCode { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public bool? IsActive { get; set; }

        [NotMapped]
        public string UsuarioAlterando { get; set; }

        [NotMapped]
        public List<Auditoria> Auditorias { get; set; }
    }
}
