﻿using System;
using System.Collections.Generic;

namespace EntityLayer.Models
{
    public partial class MultipleMasterData
    {
        public int MultipleMasterDataid { get; set; }
        public string FileName { get; set; }
        public string ConsultaSolicitada { get; set; }
        public DateTime? CreatedBy { get; set; }
        public bool? IsProceedDone { get; set; }
        public bool? IsRetryProcess { get; set; }
        public int? TotalFile { get; set; }
        public int? TotalProceed { get; set; }
        public int? TotlaFailed { get; set; }
        public int? TipoDeConsulta { get; set; }
        public string Id { get; set; }
    }
}
