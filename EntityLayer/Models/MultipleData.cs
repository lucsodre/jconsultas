﻿using System;
using System.Collections.Generic;

namespace EntityLayer.Models
{
    public partial class MultipleData
    {
        public int MultipleDataId { get; set; }
        public int? MultipleMasterDataid { get; set; }
        public string Placa { get; set; }
        public string Chassi { get; set; }

        public string UF { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string NumRegistro { get; set; }
        public string NumRenach { get; set; }
        public DateTime? ValidadeCNH { get; set; }
        public string RG { get; set; }
        public DateTime? DataNascimento { get; set; }
        public DateTime? DataHabilitacao { get; set; }
        public string MunicipioNascimento { get; set; }
        public string NumCedulaEspelho { get; set; }
    }
}
