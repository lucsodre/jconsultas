﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace EntityLayer.Models
{
    public partial class SGDExpressContext : IdentityDbContext<ApplicationUser>
    {
        public virtual DbSet<TB_LogXmlOxn> TB_LogXmlOxn { get; set; }
        public static IConfigurationRoot Configuration { get; set; }

        public SGDExpressContext(DbContextOptions<SGDExpressContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TB_LogXmlOxn>(entity =>
            {
                entity.HasKey(e => e.ID_Consulta)
                    .HasName("PK_TB_LogXmlOxn");
            });

            //modelBuilder.Entity<AspNetRoleClaims>(entity =>
            //{
            //    entity.Property(e => e.RoleId)
            //        .IsRequired()
            //        .HasMaxLength(450);

            //    entity.HasOne(d => d.Role)
            //        .WithMany(p => p.AspNetRoleClaims)
            //        .HasForeignKey(d => d.RoleId);
            //});

            //modelBuilder.Entity<AspNetRoles>(entity =>
            //{
            //    entity.Property(e => e.Id).HasMaxLength(450);

            //    entity.Property(e => e.Name).HasMaxLength(256);

            //    entity.Property(e => e.NormalizedName).HasMaxLength(256);
            //});

            //modelBuilder.Entity<AspNetUserClaims>(entity =>
            //{
            //    entity.Property(e => e.UserId)
            //        .IsRequired()
            //        .HasMaxLength(450);

            //    entity.HasOne(d => d.User)
            //        .WithMany(p => p.AspNetUserClaims)
            //        .HasForeignKey(d => d.UserId);
            //});

            //modelBuilder.Entity<AspNetUserLogins>(entity =>
            //{
            //    entity.HasKey(e => new { e.LoginProvider, e.ProviderKey })
            //        .HasName("PK_AspNetUserLogins");

            //    entity.Property(e => e.LoginProvider).HasMaxLength(450);

            //    entity.Property(e => e.ProviderKey).HasMaxLength(450);

            //    entity.Property(e => e.UserId)
            //        .IsRequired()
            //        .HasMaxLength(450);

            //    entity.HasOne(d => d.User)
            //        .WithMany(p => p.AspNetUserLogins)
            //        .HasForeignKey(d => d.UserId);
            //});

            //modelBuilder.Entity<AspNetUserRoles>(entity =>
            //{
            //    entity.HasKey(e => new { e.UserId, e.RoleId })
            //        .HasName("PK_AspNetUserRoles");

            //    entity.Property(e => e.UserId).HasMaxLength(450);

            //    entity.Property(e => e.RoleId).HasMaxLength(450);

            //    entity.HasOne(d => d.Role)
            //        .WithMany(p => p.AspNetUserRoles)
            //        .HasForeignKey(d => d.RoleId);

            //    entity.HasOne(d => d.User)
            //        .WithMany(p => p.AspNetUserRoles)
            //        .HasForeignKey(d => d.UserId);
            //});

            //modelBuilder.Entity<AspNetUserTokens>(entity =>
            //{
            //    entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name })
            //        .HasName("PK_AspNetUserTokens");

            //    entity.Property(e => e.UserId).HasMaxLength(450);

            //    entity.Property(e => e.LoginProvider).HasMaxLength(450);

            //    entity.Property(e => e.Name).HasMaxLength(450);
            //});

            //modelBuilder.Entity<AspNetUsers>(entity =>
            //{
            //    entity.Property(e => e.Id).HasMaxLength(450);

            //    entity.Property(e => e.Email).HasMaxLength(256);

            //    entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

            //    entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

            //    entity.Property(e => e.UserName).HasMaxLength(256);
            //});
            /*
            modelBuilder.Entity<IpAddress>(entity =>
            {
                entity.Property(e => e.IpAddress1)
                    .HasColumnName("IpAddress")
                    .HasMaxLength(50);

                entity.Property(e => e.ProfileId).HasColumnName("ProfileID");
            });

            modelBuilder.Entity<MultipleData>(entity =>
            {
                entity.Property(e => e.Chassi)
                    .HasColumnName("chassi")
                    .HasMaxLength(50);

                entity.Property(e => e.Placa)
                    .HasColumnName("placa")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MultipleMasterData>(entity =>
            {
                entity.HasKey(e => e.MultipleMasterDataid)
                    .HasName("PK_MultipleMasterData");

                entity.Property(e => e.CreatedBy).HasColumnType("datetime");

                entity.Property(e => e.FileName).HasMaxLength(50);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(450);
            });

            modelBuilder.Entity<ProfileMaster>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(450);

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(450);

                entity.Property(e => e.ProfileName).HasMaxLength(50);
            });

            modelBuilder.Entity<QueryResult>(entity =>
            {
                entity.HasKey(e => e.QueryResultid)
                    .HasName("PK_QueryResult");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.DocumentCode).HasMaxLength(50);

                entity.Property(e => e.Error).HasMaxLength(500);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(450);

                entity.Property(e => e.Xml).HasColumnName("XML");
            });

            modelBuilder.Entity<UserMaster>(entity =>
            {
                entity.HasKey(e => e.ProfileId)
                    .HasName("PK_UserMaster_1");

                entity.Property(e => e.ProfileId).HasColumnName("ProfileID");

                entity.Property(e => e.AccessToken).HasMaxLength(50);

                entity.Property(e => e.AccountId).HasColumnName("AccountID");

                entity.Property(e => e.ApiPassword).HasMaxLength(250);

                entity.Property(e => e.ApiSenha)
                    .HasColumnName("apiSenha")
                    .HasMaxLength(250);

                entity.Property(e => e.ApiUser).HasMaxLength(250);

                entity.Property(e => e.ApiUsuario)
                    .HasColumnName("apiUsuario")
                    .HasMaxLength(250);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(450);

                entity.Property(e => e.Id).HasMaxLength(450);

                entity.Property(e => e.LastLogin).HasColumnType("datetime");

                entity.Property(e => e.LastName).HasMaxLength(250);

                entity.Property(e => e.MobileNo).HasMaxLength(20);

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(450);

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.Picture).HasMaxLength(250);

                entity.Property(e => e.UseDoubleVerification).HasMaxLength(50);
            });

            modelBuilder.Entity<VerificationCode>(entity =>
            {
                entity.HasKey(e => e.VerificationCodeId)
                    .HasName("PK_VerificationCode");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ExpDateTime).HasColumnType("datetime");

                entity.Property(e => e.Id).HasMaxLength(450);

                entity.Property(e => e.ProfileId).HasColumnName("ProfileID");

                entity.Property(e => e.VerificationCode1)
                    .HasColumnName("VerificationCode")
                    .HasMaxLength(50);
            });*/
        }
    }
}