﻿using EntityLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityLayer.Models
{
    public partial class ProfileMaster : IAuditable
    {
        public int ProfileMasterId { get; set; }
        public string ProfileName { get; set; }

        [NotMapped]
        public string ProfileNameTraduzido
        {
            get
            {
                switch (ProfileName)
                {
                    case "Admin User":
                        return "Usuário administrador";
                    case "Account batch query":
                        return "Usuário consulta lote";
                    case "Account single query":
                        return "Usuário consulta veiculo";
                    case "Account Administrator":
                        return "Usuário administrador de clientes";
                    default:
                        return ProfileName;
                }
            }
        }

        public bool? UserMaster { get; set; }
        public bool? Account { get; set; }
        public bool? SingleSearch { get; set; }
        public bool? BatchSearch { get; set; }
        public bool? ConsultaMotorista { get; set; }
        public bool? ConsultaMotoristaLote { get; set; }
        public bool? IsActive { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }

        [NotMapped]
        public string UsuarioAlterando { get; set; }

        [NotMapped]
        public List<Auditoria> Auditorias { get; set; }
    }
}
