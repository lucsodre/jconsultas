﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using EntityLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;

namespace BusinessLogicLayer
{
    public class UserBLL
    {
        private readonly JConsultasContext _context;
       
        public UserBLL(JConsultasContext context)
        {
            _context = context;
        }

        public IEnumerable<ProfileMaster> GetProfile()
        {
            return _context.ProfileMaster; 
        }

        public string GetUserRoleOnId(string id)
        {
            var result = _context.Roles.SingleOrDefault(b => b.Id == id);
            return result.Name;
        }

        public UserMaster UserDetailsOnEmailId(string emailid)
        {
            var result = _context.UserMaster.SingleOrDefault(b => b.ApiUser == emailid);
            if (result != null)
            {
                return result;
            }
            else
                return null;
        }

        public UserMaster UserDetailsOnId(string ProfileID)
        {
            int iProfileID = 0;
            int.TryParse(ProfileID, out iProfileID);

            var result = _context.UserMaster.SingleOrDefault(b => b.ProfileId == iProfileID);
            if (result != null)
            {
                return result;
            }
            else
                return null;
        }

        public UserMaster UserDetailsOnMasterId(string id)
        {
            var result = _context.UserMaster.SingleOrDefault(b => b.Id == id);
            if (result != null)
            {
                return result;
            }
            else
                return null;
        }

        //public string UserIsUserAthentication(string emailid)
        //{
        //    var result = _context.UserMaster.SingleOrDefault(b => b.ApiUser == emailid && b.IsActive == true);
        //    if (result != null)
        //        return result.UseDoubleVerification.ToString();
        //    else
        //        return null;
        //}

        public bool CheckIpAddressIsExistOrNot(int iProfileId, string IpAddress)
        {
            //int iProfileId = 0;
            //int.TryParse(sProfileId, out iProfileId);

            var user = (from r in _context.IpAddress where r.ProfileId == iProfileId && r.IpAddress1 == IpAddress select r).ToList();
            if (user != null && user.Count > 0)
                return true;
            else
                return false;
        }

        public bool UserSetLastLogin(string emailid)
        {
            var result = _context.UserMaster.SingleOrDefault(b => b.ApiUser == emailid && b.IsActive == true);
            if (result != null)
            {
                result.LastLogin = DateTime.Now;
                _context.SaveChanges();

                return true;
            }
            else
                return false;
        }

        public bool UserSetLastLoginWithIpAddress(string emailid, string IpAddress)
        {
            var result = _context.UserMaster.SingleOrDefault(b => b.ApiUser == emailid && b.IsActive == true);
            if (result != null)
            {
                result.LastLogin = DateTime.Now;
                _context.SaveChanges();

                // now update in IpAddress
                IpAddress _IpAddress = new EntityLayer.Models.IpAddress();
                _IpAddress.IpAddress1 = IpAddress;
                _IpAddress.ProfileId = result.ProfileId;

                _context.IpAddress.Add(_IpAddress);
                _context.SaveChanges();

                return true;
            }
            else
                return false;

        }

        public IEnumerable<UserList> GetUserList(string userId)
        {
            try
            {
                UserRequest _UserRequest = new UserRequest();
                _UserRequest = IsAdminRequest(userId);
                if (_UserRequest.IsAdminRequest)
                {
                    try
                    {


                        var res = (from u in _context.UserMaster
                                   from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                                   join au in _context.Users on u.Id equals au.Id
                                   join cu in _context.Users on u.CreatedBy equals cu.Id
                                   join pm in _context.ProfileMaster on u.ProfileMasterId equals pm.ProfileMasterId
                                   //join ur in _context.UserRoles on u.Id equals ur.UserId
                                   //join r in _context.Roles on ur.RoleId equals r.Id
                                   from mu in _context.Users.Where(gruppe => gruppe.Id == u.ModifiedBy).DefaultIfEmpty()
                                   select new UserList
                                   {
                                       ApiPassword = u.ApiPassword
                                       ,
                                       ApiUser = u.ApiUser
                                       ,
                                       IsActive = u.IsActive
                                       ,
                                       LastName = u.LastName
                                       ,
                                       Name = u.Name
                                       ,
                                       ProfileId = u.ProfileId
                                       ,
                                       Picture = u.Picture
                                       ,
                                       AccountId = a == null ? 0 : a.AccountId
                                       ,
                                       AccountName = a == null ? "" : a.AccountName
                                       //,
                                       //AccountName = "123"
                                       ,
                                       Id = au.Id
                                       ,
                                       ProfileMasterId = u.ProfileMasterId
                                       ,
                                       MobileNo = u.MobileNo
                                       ,
                                       CreatedByName = cu.UserName
                                       ,
                                       CreatedBy = u.CreatedBy
                                       ,
                                       ModifiedBy = u.ModifiedBy
                                       ,
                                       ModifiedByName = Convert.ToString(mu.UserName)
                                        ,
                                       CreatedAt = u.CreatedAt.Value.ToString("dd/MM/yyyy HH:mm")
                                       ,
                                       ModifiedAt = string.IsNullOrEmpty(Convert.ToString(u.ModifiedAt)) ? null : u.ModifiedAt.Value.ToString("dd/MM/yyyy HH:mm")
                                       ,
                                       LastLogin = string.IsNullOrEmpty(Convert.ToString(u.LastLogin)) ? null : u.LastLogin.Value.ToString("dd/MM/yyyy HH:mm")
                                       ,
                                       apiSenha = u.ApiSenha
                                       ,
                                       apiUsuario = u.ApiUsuario
                                       ,
                                       RoleName = pm.ProfileName
                                       ,
                                       UseDoubleVerification = u.UseDoubleVerification
                                       ,
                                       IsEmailVerifeyed = u.IsEmailVerifeyed
                                       ,
                                       IsPhonelVerifeyed = u.IsPhonelVerifeyed
                                        ,
                                       IpAddress = null
                                       ,
                                       AccessToken = u.AccessToken

                                   });

                        return res;
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
                else
                {
                    var res = (from u in _context.UserMaster
                                   // join a in _context.AccountMaster on u.AccountId equals a.AccountId
                               from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                               join au in _context.Users on u.Id equals au.Id
                               join cu in _context.Users on u.CreatedBy equals cu.Id
                               join pm in _context.ProfileMaster on u.ProfileMasterId equals pm.ProfileMasterId
                               join aud in _context.Auditoria on u.AccountId equals aud.IdRegistro orderby aud.DataHora descending
                               //join ur in _context.UserRoles on u.Id equals ur.UserId
                               //join r in _context.Roles on ur.RoleId equals r.Id
                               from mu in _context.Users.Where(gruppe => gruppe.Id == u.ModifiedBy).DefaultIfEmpty()
                               where u.AccountId == _UserRequest.AccountID && aud.NomeTabela == "UserMaster"
                               select new UserList
                               {
                                   ApiPassword = u.ApiPassword
                                   ,
                                   ApiUser = u.ApiUser
                                   ,
                                   IsActive = u.IsActive
                                   ,
                                   LastName = u.LastName
                                   ,
                                   Name = u.Name
                                   ,
                                   ProfileId = u.ProfileId
                                   ,
                                   Picture = u.Picture
                                   ,
                                   AccountId = a == null ? 0 : a.AccountId
                                   ,
                                   AccountName = a == null ? "" : a.AccountName
                                   ,
                                   Id = au.Id
                                   ,
                                   ProfileMasterId = u.ProfileMasterId
                                   ,
                                   MobileNo = u.MobileNo
                                   ,
                                   CreatedByName = cu.UserName
                                   ,
                                   CreatedBy = u.CreatedBy
                                   ,
                                   ModifiedBy = u.ModifiedBy
                                   ,
                                   ModifiedByName = Convert.ToString(mu.UserName)
                                    ,
                                   CreatedAt = u.CreatedAt.Value.ToString()
                                   ,
                                   ModifiedAt = string.IsNullOrEmpty(Convert.ToString(u.ModifiedAt)) ? null : u.ModifiedAt.Value.ToString()
                                   ,
                                   LastLogin = string.IsNullOrEmpty(Convert.ToString(u.LastLogin)) ? null : u.LastLogin.Value.ToString()
                                   ,
                                   apiSenha = u.ApiSenha
                                   ,
                                   apiUsuario = u.ApiUsuario
                                   ,
                                   RoleName = pm.ProfileName
                                   ,
                                   UseDoubleVerification = u.UseDoubleVerification
                                  ,
                                   IsEmailVerifeyed = u.IsEmailVerifeyed
                                       ,
                                   IsPhonelVerifeyed = u.IsPhonelVerifeyed
                                    ,
                                   IpAddress = null
                                   ,
                                   AccessToken = u.AccessToken

                               });

                    return res;
                }


            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<UserList> GetAllUserList()
        {
            try
            {
                var res = (from u in _context.UserMaster
                           join a in _context.AccountMaster on u.AccountId equals a.AccountId
                           // from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                           join au in _context.Users on u.Id equals au.Id
                           join cu in _context.Users on u.CreatedBy equals cu.Id
                           join pm in _context.ProfileMaster on u.ProfileMasterId equals pm.ProfileMasterId
                           //join ur in _context.UserRoles on u.Id equals ur.UserId
                           //join r in _context.Roles on ur.RoleId equals r.Id
                           from mu in _context.Users.Where(gruppe => gruppe.Id == u.ModifiedBy).DefaultIfEmpty()
                           select new UserList
                           {
                               ApiPassword = u.ApiPassword
                               ,
                               ApiUser = u.ApiUser
                               ,
                               IsActive = u.IsActive
                               ,
                               LastName = u.LastName
                               ,
                               Name = u.Name
                               ,
                               ProfileId = u.ProfileId
                               ,
                               Picture = u.Picture
                               ,
                               AccountId = a == null ? 0 : a.AccountId
                                       ,
                               AccountName = a == null ? "" : a.AccountName
                               ,
                               Id = au.Id
                               ,
                               ProfileMasterId = u.ProfileMasterId
                               ,
                               MobileNo = u.MobileNo
                               ,
                               CreatedByName = cu.UserName
                               ,
                               CreatedBy = u.CreatedBy
                               ,
                               ModifiedBy = u.ModifiedBy
                               ,
                               ModifiedByName = Convert.ToString(mu.UserName)
                                ,
                               CreatedAt = u.CreatedAt.Value.ToString()
                               ,
                               ModifiedAt = string.IsNullOrEmpty(Convert.ToString(u.ModifiedAt)) ? null : u.ModifiedAt.Value.ToString()
                               ,
                               LastLogin = string.IsNullOrEmpty(Convert.ToString(u.LastLogin)) ? null : u.LastLogin.Value.ToString()
                               ,
                               apiSenha = u.ApiSenha
                               ,
                               apiUsuario = u.ApiUsuario
                               ,
                               RoleName = pm.ProfileName
                               ,
                               UseDoubleVerification = u.UseDoubleVerification
                               ,
                               IsEmailVerifeyed = u.IsEmailVerifeyed
                                       ,
                               IsPhonelVerifeyed = u.IsPhonelVerifeyed
                               ,
                               IpAddress = null
                               ,
                               AccessToken = u.AccessToken

                           });

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<UserList> GetAuditorias(List<UserList> users)
        {
            foreach (var item in users)
            {
                item.Auditorias = new List<Auditoria>();
                item.Auditorias = (from p in _context.Auditoria
                                   join u in _context.UserMaster on p.Usuario equals u.Id
                                   where p.NomeTabela == "UserMaster" && p.IdRegistro == item.ProfileId
                                   orderby p.DataHora descending
                                   select new Auditoria
                                   {
                                       DataHora = p.DataHora,
                                       Descricao = p.Descricao,
                                       Usuario = u.Name
                                   }).Take(5).ToList();
            }
            return users;
        }

        public UserResponse SaveUsers(UserDetails objusermaster)
        {
            UserResponse _userrespones = new UserResponse();

            UserMaster _usermaster = new UserMaster();
            _usermaster.Name = objusermaster.Name;
            _usermaster.LastName = objusermaster.LastName;
            _usermaster.Picture = objusermaster.Picture;
            _usermaster.ApiUser = objusermaster.ApiUser;
            _usermaster.ApiPassword = objusermaster.ApiPassword;
            _usermaster.AccountId = objusermaster.AccountId;
            _usermaster.ProfileId = objusermaster.ProfileId;
            _usermaster.ModifiedBy = objusermaster.ModifiedBy;
            _usermaster.ModifiedAt = objusermaster.ModifiedAt;
            _usermaster.CreatedBy = objusermaster.CreatedBy;
            _usermaster.CreatedAt = objusermaster.CreatedAt;
            _usermaster.IsActive = objusermaster.IsActive;
            _usermaster.Id = objusermaster.Id;
            _usermaster.ProfileMasterId = objusermaster.ProfileMasterId;
            _usermaster.MobileNo = objusermaster.MobileNo;
            _usermaster.ApiUsuario = objusermaster.apiUsuario;
            _usermaster.ApiSenha = objusermaster.apiSenha;
            _usermaster.UseDoubleVerification = objusermaster.UseDoubleVerification;
            _usermaster.IsEmailVerifeyed = false;
            _usermaster.IsPhonelVerifeyed = false;
            _usermaster.AccessToken = objusermaster.AccessToken;
            _usermaster.UsuarioAlterando = objusermaster.ModifiedBy == null ? objusermaster.CreatedBy : objusermaster.ModifiedBy;

            if (objusermaster.ProfileId == 0)
            {
                //_usermaster.IpAddress = objusermaster.IpAddress;

                _context.UserMaster.Add(_usermaster);
                _context.SaveChanges();

                if (objusermaster.UseDoubleVerification != "No")
                {
                    _userrespones.AuthRequired = true;
                }

                //// now update in IpAddress
                //IpAddress _IpAddress = new EntityLayer.Models.IpAddress();
                //_IpAddress.IpAddress1 = objusermaster.IpAddress;
                //_IpAddress.ProfileId = _usermaster.ProfileId;

                //_context.IpAddress.Add(_IpAddress);
                //_context.SaveChanges();

                _userrespones.Respones = true;
                _userrespones.id = _usermaster.ProfileId;
                _userrespones.Error = null;
            }
            else
            {
                var result = _context.Users.SingleOrDefault(b => b.Id == objusermaster.Id);
                if (result != null)
                {
                    try
                    {
                        result.UserName = objusermaster.ApiUser;
                        result.Email = objusermaster.ApiUser;
                        result.NormalizedUserName = objusermaster.ApiUser.ToUpper().ToString();
                        result.NormalizedEmail = objusermaster.ApiUser.ToUpper().ToString();
                        result.PasswordHash = objusermaster.ApiPassword;
                        _context.SaveChanges();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                var oldrkd = _context.UserMaster.SingleOrDefault(b => b.Id == objusermaster.Id);
                if (oldrkd != null)
                {
                    if (objusermaster.UseDoubleVerification == "By_email")
                    {
                        if (oldrkd.ApiUser != objusermaster.ApiUser)
                        {
                            objusermaster.IsEmailVerifeyed = false;
                            _userrespones.AuthRequired = true;
                        }
                    }

                    if (objusermaster.UseDoubleVerification == "By_SMS")
                    {
                        if (oldrkd.MobileNo != objusermaster.MobileNo)
                        {
                            objusermaster.IsPhonelVerifeyed = false;
                            _userrespones.AuthRequired = true;
                        }
                    }
                }

                _context.UserMaster.Attach(_usermaster);
                _context.Entry(_usermaster).State = EntityState.Modified;
                _context.SaveChanges();

                _userrespones.Respones = true;
                _userrespones.id = _usermaster.ProfileId;
                _userrespones.Error = null;
            }

            return _userrespones;
        }

        public UserResponse UpdateUsers(UserDetails objusermaster)
        {

            UserResponse _userrespones = new UserResponse();

            UserDetails obj = _context.UserMaster.Where(z => z.Id == objusermaster.Id).Select(z => new UserDetails { CreatedAt = z.CreatedAt, CreatedBy = z.CreatedBy, ApiPassword = z.ApiPassword }).SingleOrDefault();//  .SingleOrDefault(b => b.Id == objusermaster.Id);

            UserMaster _usermaster = new UserMaster();
            _usermaster.Name = objusermaster.Name;
            _usermaster.LastName = objusermaster.LastName;
            _usermaster.Picture = objusermaster.Picture;
            _usermaster.ApiUser = objusermaster.ApiUser;
            //_usermaster.ApiPassword = objusermaster.ApiPassword;
            _usermaster.ApiPassword = obj.ApiPassword;
            _usermaster.AccountId = objusermaster.AccountId;
            _usermaster.ProfileId = objusermaster.ProfileId;
            _usermaster.ModifiedBy = objusermaster.ModifiedBy;
            _usermaster.ModifiedAt = objusermaster.ModifiedAt;
            _usermaster.CreatedBy = obj.CreatedBy;
            _usermaster.CreatedAt = obj.CreatedAt;
            _usermaster.IsActive = objusermaster.IsActive;
            _usermaster.Id = objusermaster.Id;
            _usermaster.ProfileMasterId = objusermaster.ProfileMasterId;
            _usermaster.MobileNo = objusermaster.MobileNo;
            _usermaster.ApiUsuario = objusermaster.apiUsuario;
            _usermaster.ApiSenha = objusermaster.apiSenha;
            _usermaster.UseDoubleVerification = objusermaster.UseDoubleVerification;
            _usermaster.IsPhonelVerifeyed = objusermaster.IsPhonelVerifeyed;
            _usermaster.IsEmailVerifeyed = objusermaster.IsEmailVerifeyed;
            _usermaster.AccessToken = objusermaster.AccessToken;
            _usermaster.UsuarioAlterando = objusermaster.ModifiedBy == null ? objusermaster.CreatedBy : objusermaster.ModifiedBy;
            //_usermaster.IpAddress = objusermaster.IpAddress;

            //objUser = null;

            if (objusermaster.ProfileId == 0)
            {
                _context.UserMaster.Add(_usermaster);
                _context.SaveChanges();

                if (objusermaster.UseDoubleVerification != "No")
                {
                    _userrespones.AuthRequired = true;
                }


                _userrespones.Respones = true;
                _userrespones.id = _usermaster.ProfileId;
                _userrespones.Error = null;
            }
            else
            {
                var result = _context.Users.SingleOrDefault(b => b.Id == objusermaster.Id);
                if (result != null)
                {
                    try
                    {
                        result.UserName = objusermaster.ApiUser;
                        result.Email = objusermaster.ApiUser;
                        result.NormalizedUserName = objusermaster.ApiUser.ToUpper().ToString();
                        result.NormalizedEmail = objusermaster.ApiUser.ToUpper().ToString();
                        result.PasswordHash = objusermaster.ApiPassword;
                        _context.SaveChanges();
                    }
                    catch (Exception ex)
                    {

                    }
                }

                var oldrkd = _context.UserMaster.SingleOrDefault(b => b.Id == objusermaster.Id);
                if (oldrkd != null)
                {
                    if (oldrkd.UseDoubleVerification != objusermaster.UseDoubleVerification)
                    {
                        if (oldrkd.UseDoubleVerification == "no")
                        {
                            if (objusermaster.UseDoubleVerification == "By_email")
                            {
                                objusermaster.IsEmailVerifeyed = false;
                                _userrespones.AuthRequired = true;
                            }
                            if (objusermaster.UseDoubleVerification == "By_SMS")
                            {
                                objusermaster.IsPhonelVerifeyed = false;
                                _userrespones.AuthRequired = true;
                            }
                        }
                        else
                        {
                            if (objusermaster.UseDoubleVerification == "By_email")
                            {
                                if (oldrkd.ApiUser != objusermaster.ApiUser)
                                {
                                    objusermaster.IsEmailVerifeyed = false;
                                    _userrespones.AuthRequired = true;
                                }
                            }

                            if (objusermaster.UseDoubleVerification == "By_SMS")
                            {
                                if (oldrkd.MobileNo != objusermaster.MobileNo)
                                {
                                    objusermaster.IsPhonelVerifeyed = false;
                                    _userrespones.AuthRequired = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (objusermaster.UseDoubleVerification == "By_email")
                        {
                            if (oldrkd.ApiUser != objusermaster.ApiUser)
                            {
                                objusermaster.IsEmailVerifeyed = false;
                                _userrespones.AuthRequired = true;
                            }
                        }

                        if (objusermaster.UseDoubleVerification == "By_SMS")
                        {
                            if (oldrkd.MobileNo != objusermaster.MobileNo)
                            {
                                objusermaster.IsPhonelVerifeyed = false;
                                _userrespones.AuthRequired = true;
                            }
                        }
                    }


                }

                UserMaster _UM = _context.UserMaster.First(i => i.ProfileId == _usermaster.ProfileId);
                _UM.Name = objusermaster.Name;
                _UM.LastName = objusermaster.LastName;
                _UM.Picture = objusermaster.Picture;
                _UM.ApiUser = objusermaster.ApiUser;
                //_usermaster.ApiPassword = objusermaster.ApiPassword;
                _UM.ApiPassword = obj.ApiPassword;
                _UM.AccountId = objusermaster.AccountId;
                _UM.ProfileId = objusermaster.ProfileId;
                _UM.ModifiedBy = objusermaster.ModifiedBy;
                _UM.ModifiedAt = objusermaster.ModifiedAt;
                _UM.CreatedBy = obj.CreatedBy;
                _UM.CreatedAt = obj.CreatedAt;
                _UM.IsActive = objusermaster.IsActive;
                _UM.Id = objusermaster.Id;
                _UM.ProfileMasterId = objusermaster.ProfileMasterId;
                _UM.MobileNo = objusermaster.MobileNo;
                _UM.ApiUsuario = objusermaster.apiUsuario;
                _UM.ApiSenha = objusermaster.apiSenha;
                _UM.UseDoubleVerification = objusermaster.UseDoubleVerification;
                _UM.IsPhonelVerifeyed = objusermaster.IsPhonelVerifeyed;
                _UM.IsEmailVerifeyed = objusermaster.IsEmailVerifeyed;
                _UM.AccessToken = objusermaster.AccessToken;
                _UM.UsuarioAlterando = objusermaster.ModifiedBy == null ? objusermaster.CreatedBy : objusermaster.ModifiedBy;

                _context.SaveChanges();

                _userrespones.Respones = true;
                _userrespones.id = _usermaster.ProfileId;
                _userrespones.Error = null;
            }

            return _userrespones;
        }

        public IEnumerable<UserMaster> CheckEmailIdExistOrNot(string emailid)
        {
            var user = (from r in _context.UserMaster where r.ApiUser == emailid && r.IsActive == true select new UserMaster { ApiUser = r.ApiUser, ProfileId = r.ProfileId });
            return user;
        }

        public PasswordResponseResult ResetPassword(string id, string NewPassword, string idUser)
        {
            PasswordResponseResult _passwordresponse = new PasswordResponseResult();

            int iId = 0;
            int.TryParse(id, out iId);
            if (iId == 0)
            {
                _passwordresponse.Resposne = false;
                _passwordresponse.Msg = "Invalid";
            }
            else
            {
                var UserMaster = _context.UserMaster.SingleOrDefault(b => b.ProfileId == iId);
                if (UserMaster != null)
                {
                    UserMaster.ApiPassword = NewPassword;
                    UserMaster.UsuarioAlterando = idUser;
                    _context.SaveChanges();

                    var aspUser = _context.Users.SingleOrDefault(b => b.Id == UserMaster.Id);
                    if (aspUser != null)
                    {
                        aspUser.PasswordHash = NewPassword;
                        _context.SaveChanges();

                        _passwordresponse.Resposne = true;
                    }
                    else
                        _passwordresponse.Resposne = false;
                }
                else
                {
                    _passwordresponse.Resposne = false;
                    _passwordresponse.Msg = "NotExist";
                }
            }

            return _passwordresponse;
        }

        public bool SaveExternalUserData(string emailId, string id, string idUser)
        {
            UserMaster _objUserMaster = new UserMaster();
            _objUserMaster.AccountId = 1;
            _objUserMaster.ApiUser = emailId;
            _objUserMaster.Id = id;
            _objUserMaster.UsuarioAlterando = idUser;

            _context.UserMaster.Add(_objUserMaster);
            _context.SaveChanges();

            return true;
        }

        public UserAuthenticationData GetUsreAuthenticationData(string sid)
        {
            UserAuthenticationData objUserData = new UserAuthenticationData();

            var res = (from u in _context.UserMaster
                       join a in _context.AccountMaster on u.AccountId equals a.AccountId
                       where u.Id == sid
                       select new
                       {
                           u.ApiSenha
                           ,
                           u.ApiUsuario
                           ,
                           a.ApiCode
                       }).ToList();

            if (res != null && res.Count > 0)
            {
                objUserData.usuario = res[0].ApiUsuario.ToString();
                objUserData.senha = res[0].ApiSenha.ToString();

                Int64 codigo = 0;
                Int64.TryParse(res[0].ApiCode.ToString(), out codigo);

                objUserData.codigo = codigo;

                return objUserData;
            }
            else
                return null;
        }

        // Send Verification Code an
        public bool SaveVerificationCode(string id, int ProfileID, string sCode)
        {

            VerificationCode _VerificationCode = new VerificationCode();
            _VerificationCode.CreatedDate = DateTime.Now;
            _VerificationCode.ExpDateTime = DateTime.Now.AddMinutes(5);
            _VerificationCode.Id = id;
            _VerificationCode.ProfileId = ProfileID;
            _VerificationCode.VerificationCode1 = sCode;


            _context.VerificationCode.Add(_VerificationCode);
            _context.SaveChanges();

            return true;
        }

        public bool CheckVerificationCode(string ProfileID, string code, string sType)
        {
            int iProfileId = 0;
            int.TryParse(ProfileID, out iProfileId);

            var q = (from v in _context.VerificationCode where v.ProfileId == iProfileId && v.ExpDateTime >= DateTime.Now orderby v.VerificationCodeId descending select v).FirstOrDefault();
            if (q != null)
            {
                if (q.VerificationCode1 == code)
                {
                    if (!string.IsNullOrEmpty(sType))
                    {
                        // Update status
                        var result = _context.UserMaster.SingleOrDefault(b => b.Id == q.Id);
                        if (result != null)
                        {
                            if (sType == "Email")
                                result.IsEmailVerifeyed = true;
                            if (sType == "Phone")
                                result.IsPhonelVerifeyed = true;
                            _context.SaveChanges();
                        }

                        return true;
                    }
                    else
                        return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public bool SendVerificationCode(string Type, string Email, string Browser, string OperatingSystem)
        {
            Random generator = new Random();
            String Code = generator.Next(0, 1000000).ToString("D6");

            UserMaster _user = UserDetailsOnEmailId(Email);
            if (_user != null)
            {
                string sUseDoubleVerification = _user.UseDoubleVerification.ToString();
                if (sUseDoubleVerification == "By_email")
                {


                    return true;
                }
                else
                {
                    return true;
                }
            }
            else
                return false;

        }

        public UserRequest IsAdminRequest(string id)
        {
            UserRequest _UserRequest = new UserRequest();
            var result = _context.UserMaster.SingleOrDefault(b => b.Id == id);
            if (result != null)
            {
                if (result.ProfileMasterId == 1)
                    _UserRequest.IsAdminRequest = true;
                else
                    _UserRequest.IsAdminRequest = false;

                _UserRequest.AccountID = result.AccountId.Value;
            }
            else
                _UserRequest.IsAdminRequest = false;

            return _UserRequest;
        }

    }

    public class UserResponse
    {
        public bool Respones { get; set; }
        public bool AuthRequired { get; set; }
        public int id { get; set; }
        public IEnumerable<IdentityError> Error { get; set; }
    }

    public class PasswordResponseResult
    {
        public bool Resposne { get; set; }
        public string Msg { get; set; }
    }

    public class UserAuthenticationData
    {
        public string usuario { get; set; }
        public string senha { get; set; }
        public Int64 codigo { get; set; }
    }

    public class UserDetails
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Picture { get; set; }
        public string ApiUser { get; set; }
        public string ApiPassword { get; set; }
        public int AccountId { get; set; }
        public int ProfileId { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public bool? IsActive { get; set; }
        public string Id { get; set; }
        public string RoleID { get; set; }
        public bool? SendPasswordEmail { get; set; }
        public string MobileNo { get; set; }
        public int? ProfileMasterId { get; set; }
        public string apiUsuario { get; set; }
        public string apiSenha { get; set; }
        public string UseDoubleVerification { get; set; }
        public string IpAddress { get; set; }
        public string AccessToken { get; set; }
        public Boolean IsEmailVerifeyed { get; set; }
        public Boolean IsPhonelVerifeyed { get; set; }


    }

    public class UserList
    {
        public List<Auditoria> Auditorias { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Picture { get; set; }
        public string ApiUser { get; set; }
        public string ApiPassword { get; set; }
        public int AccountId { get; set; }
        public int ProfileId { get; set; }
        public string AccountName { get; set; }
        public bool? IsActive { get; set; }
        public string Id { get; set; }
        public string MobileNo { get; set; }
        public int? ProfileMasterId { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public string ModifiedAt { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedAt { get; set; }
        public string LastLogin { get; set; }
        public string apiUsuario { get; set; }
        public string apiSenha { get; set; }
        public string RoleName { get; set; }
        public string UseDoubleVerification { get; set; }
        public string IpAddress { get; set; }
        public bool? IsEmailVerifeyed { get; set; }
        public bool? IsPhonelVerifeyed { get; set; }
        public string AccessToken { get; set; }

    }

    public class UserRoleList
    {
        public string Id { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
    }

    public class UserRequest
    {
        public bool IsAdminRequest { get; set; }
        public int AccountID { get; set; }
    }


}

