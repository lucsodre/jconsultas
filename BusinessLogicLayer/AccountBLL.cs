﻿using System;
using System.Collections.Generic;
using System.Text;
using EntityLayer.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Claims;

namespace BusinessLogicLayer
{
    public class AccountBLL
    {
        private readonly JConsultasContext _context;
        private readonly UserBLL _UserBLL;

        public AccountBLL(JConsultasContext context)
        {
            _context = context;
            _UserBLL = new UserBLL(context);
        }

        public IEnumerable<AccountMaster> GetAccountList(string userId)
        {
            UserRequest _UserRequest = new UserRequest();
            _UserRequest = _UserBLL.IsAdminRequest(userId);

            if (_UserRequest.IsAdminRequest)
                return _context.AccountMaster;
            else
                return _context.AccountMaster.Where(a => a.AccountId == _UserRequest.AccountID);
        }

        public IEnumerable<AccountMaster> GetActiveAccount(string userId)
        {

            UserRequest _UserRequest = new UserRequest();
            _UserRequest = _UserBLL.IsAdminRequest(userId);
            if (_UserRequest.IsAdminRequest)
                return _context.AccountMaster.Where(o => o.IsActive == true);
            else
                return _context.AccountMaster.Where(o => o.IsActive == true && o.AccountId == _UserRequest.AccountID);
        }

        public void GetAuditorias(List<AccountMaster> accounts)
        {
            foreach (var item in accounts)
            {
                item.Auditorias = new List<Auditoria>();
                item.Auditorias = (from p in _context.Auditoria
                                  join u in _context.Users on p.Usuario equals u.Id
                                  where p.NomeTabela == "AccountMaster" && p.IdRegistro == item.AccountId
                                  orderby p.DataHora descending
                                   select new Auditoria
                                   {
                                       DataHora = p.DataHora,
                                       Descricao = p.Descricao,
                                       Usuario = u.UserName
                                   }).Take(5).ToList();
            }
        }


        public bool AddUpdateAccount(AccountMaster objaccountmaster)
        {
            if (objaccountmaster.AccountId == 0)
            {
                _context.AccountMaster.Add(objaccountmaster);
                _context.SaveChanges();
            }
            else
            {
                var account = _context.AccountMaster.FirstOrDefault(x => x.AccountId == objaccountmaster.AccountId);
                if (account != null)
                {
                    account.AccountName = objaccountmaster.AccountName;
                    account.ApiCode = objaccountmaster.ApiCode;
                    account.CreatedAt = objaccountmaster.CreatedAt;
                    account.CreatedBy = objaccountmaster.CreatedBy;
                    account.IsActive = objaccountmaster.IsActive;
                    account.ModifiedAt = objaccountmaster.ModifiedAt;
                    account.ModifiedBy = objaccountmaster.ModifiedBy;
                    account.UsuarioAlterando = objaccountmaster.ModifiedBy == null ? account.CreatedBy : account.ModifiedBy;
                    _context.SaveChanges();
                }
                else
                {
                    _context.AccountMaster.Attach(objaccountmaster);
                    _context.Entry(objaccountmaster).State = EntityState.Modified;
                    _context.SaveChanges();
                }
            }

            return true;
        }
        
        public bool DeleteAccount(int id, string usuario)
        {
            var account = new AccountMaster { AccountId = id };
            account.UsuarioAlterando = usuario;
            _context.AccountMaster.Attach(account);
            _context.AccountMaster.Remove(account);
            _context.SaveChanges();

            return true;
        }
    }
}
