﻿using EntityLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace BusinessLogicLayer
{
    public class ConsultaCNHBLL
    {
        private readonly JConsultasContext _context;

        public ConsultaCNHBLL(JConsultasContext context)
        {
            _context = context;
        }

        public dynamic CheckExistsInDatabase(string sid, string UF, string CPF, string NumRegistro, string NumRenach,
                                            DateTime ValidadeCNH, string RG, DateTime DataNascimento, DateTime DataHabilitacao,
                                            string MunicipioNascimento, string NumCedulaEspelho)
        {
            try
            {
                double validHours = 24;
                var latest = DateTime.UtcNow.AddHours(-validHours);

                DateTime now = DateTime.Now;
                var lstRecord = (from a in _context.QueryResult where a.Id.Trim() == sid.Trim() && a.DocumentCode.Trim() == CPF.Trim() 
                                 && a.CreatedAt > now.AddHours(-24) && a.CreatedAt <= now && a.TipoDeConsulta == 2 select a).ToList();
                if (lstRecord != null && lstRecord.Count > 0)
                    return new { status = true, date = lstRecord[0].CreatedAt };
                else
                    return new { status = false, date = "" };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public string GetDocumentFromService(string sid, string UF, string CPF, string NumRegistro, string NumRenach,
                                            DateTime ValidadeCNH, string RG, DateTime DataNascimento, DateTime DataHabilitacao,
                                            string MunicipioNascimento, string NumCedulaEspelho, string apiUsuario, string apiSenha, string apiCodigo)
        {
            try
            {
                int codigo = 0;
                int.TryParse(apiCodigo, out codigo);
                ConsultasStringService.ConsultasStringSoapClient c = new ConsultasStringService.ConsultasStringSoapClient(new ConsultasStringService.ConsultasStringSoapClient.EndpointConfiguration());

                var r = c.ConsultaCNHAsync(CriarStringXmlEnvioConsultaCNH(CPF, NumRegistro, UF, NumRenach, DataHabilitacao, DataNascimento,
                    DataHabilitacao, RG, "", NumCedulaEspelho, codigo, apiUsuario, apiSenha)).Result;

                string sXML = r.Consulta_CNH_TODOS_ESTADOSResult;
                bool bIsError = false;

                var xml = new XmlDocument();
                xml.LoadXml(sXML);
                string RETORNOEXECUCAO = "";
                string DESCRICAO = "";
                var node = xml.DocumentElement.SelectSingleNode("/CONSULTA1650/RETORNO");
                if (node != null)
                {
                    RETORNOEXECUCAO = node.InnerText.ToString();
                    if (RETORNOEXECUCAO == "999")
                    {
                        bIsError = true;
                        DESCRICAO = xml.DocumentElement.SelectSingleNode("/CONSULTA1650/DESCRICAO").InnerText.ToString();
                    }
                }
                else if (xml.DocumentElement.InnerXml.Contains("Usuário ou senha inválida."))
                {
                    bIsError = true;
                    DESCRICAO = "Usuário e senha invalidos";
                }
                else
                {
                    if (!xml.DocumentElement.InnerXml.Contains("TRANSACAO EFETUADA COM SUCESSO!"))
                    {
                        throw new Exception("Ocorreu um erro interno, por favor contacte o suporte!");
                    }
                }

                QueryResult objQueryResult = new QueryResult();
                objQueryResult.DocumentCode = CPF;
                objQueryResult.CreatedAt = DateTime.Now;
                objQueryResult.Id = sid;
                objQueryResult.Xml = sXML;
                objQueryResult.IsError = bIsError;
                objQueryResult.Error = DESCRICAO;
                objQueryResult.TipoDeConsulta = 2;

                _context.QueryResult.Add(objQueryResult);
                _context.SaveChanges();

                return sXML;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public string GetDocumentFromServiceAutentication(string UF, string CPF, string NumRegistro, string NumRenach,
                                            DateTime ValidadeCNH, string RG, DateTime DataNascimento, DateTime DataHabilitacao,
                                            string MunicipioNascimento, string NumCedulaEspelho, string apiUsuario, string apiSenha, string apiCodigo,
                                            string sUsuario, string sSenha)
        {
            try
            {
                Int32 codigo = 0;
                bool isApi = true;
                ServiceResponse objResponse = new ServiceResponse();

                var res = (from u in _context.UserMaster
                           join a in _context.AccountMaster on u.AccountId equals a.AccountId
                           where u.ApiUser == sUsuario && u.ApiPassword == sSenha
                           select new
                           {
                               u.Id
                           }).ToList();

                if (res == null || res.Count == 0)
                {
                    return "Credenciais de usuário inválidas";
                }
                else if (!Int32.TryParse(apiCodigo, out codigo) || String.IsNullOrEmpty(apiUsuario) || String.IsNullOrEmpty(apiSenha))
                {
                    return "Credenciais para consulta API inválidas";
                }
                else
                {
                    string sid = res[0].Id.ToString();
                    int.TryParse(apiCodigo, out codigo);
                    ConsultasStringService.ConsultasStringSoapClient c = new ConsultasStringService.ConsultasStringSoapClient(new ConsultasStringService.ConsultasStringSoapClient.EndpointConfiguration());

                    var r = c.ConsultaCNHAsync(CriarStringXmlEnvioConsultaCNH(CPF, NumRegistro, UF, NumRenach, DataHabilitacao, DataNascimento,
                        DataHabilitacao, RG, "", NumCedulaEspelho, codigo, apiUsuario, apiSenha)).Result;

                    string sXML = r.Consulta_CNH_TODOS_ESTADOSResult;
                    bool bIsError = false;

                    var xml = new XmlDocument();
                    xml.LoadXml(sXML);
                    string RETORNOEXECUCAO = "";
                    string DESCRICAO = "";
                    var node = xml.DocumentElement.SelectSingleNode("/CONSULTA1650/RETORNO");
                    if (node != null)
                    {
                        RETORNOEXECUCAO = node.InnerText.ToString();
                        if (RETORNOEXECUCAO == "999")
                        {
                            bIsError = true;
                            DESCRICAO = xml.DocumentElement.SelectSingleNode("/CONSULTA1650/DESCRICAO").InnerText.ToString();
                        }
                    }
                    else if (xml.DocumentElement.InnerXml.Contains("Usuário ou senha inválida."))
                    {
                        bIsError = true;
                        DESCRICAO = "Usuário e senha invalidos";
                    }
                    else
                    {
                        if (!xml.DocumentElement.InnerXml.Contains("TRANSACAO EFETUADA COM SUCESSO!"))
                        {
                            throw new Exception("Ocorreu um erro interno, por favor contacte o suporte!");
                        }
                    }

                    QueryResult objQueryResult = new QueryResult();
                    objQueryResult.DocumentCode = CPF;
                    objQueryResult.CreatedAt = DateTime.Now;
                    objQueryResult.Id = sid;
                    objQueryResult.Xml = sXML;
                    objQueryResult.IsError = bIsError;
                    objQueryResult.Error = DESCRICAO;
                    objQueryResult.TipoDeConsulta = 2;
                    objQueryResult.IsConsultaApi = isApi;

                    _context.QueryResult.Add(objQueryResult);
                    _context.SaveChanges();

                    return sXML;
                } 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public static string CriarStringXmlEnvioConsultaCNH(string cpf, string cnh, string uf, string renach, DateTime vencimentoCnh,
            DateTime nascimento, DateTime Data1aHabilitacao, string rg, string nomeMotorista, string espelho, int codigoUsuario,
            string usuario, string senha)
        {
            DateTime auxvencimentoCnh = vencimentoCnh;
            DateTime auxnascimento = nascimento;
            DateTime auxData1aHabilitacao = Data1aHabilitacao;

            string envioxml = "<CONSULTA1650><LOGIN IDCLIENTE='" + codigoUsuario + "' USUARIO='" + usuario + "' SENHA='" + senha + "'/>";

            envioxml += "<CONSULTAS> <CONSULTA NUM='" + DateTime.Now.ToString("ddMMyyyyhhmms") + "' UF='" + uf +
                "' TIPODOCCONDUTOR='1'" + " DOCUMENTO='" + cpf;

            envioxml += "' NRENACH='";
            envioxml += ((renach != "undefined" && renach != null) ? renach : "");

            if (auxvencimentoCnh != DateTime.MinValue)
            {
                envioxml += "' DATA_VALIDADECNH='" + auxvencimentoCnh.ToString("dd/MM/yyyy");
            }
            else
            {
                envioxml += "' DATA_VALIDADECNH='" + "";
            }

            if (auxnascimento != DateTime.MinValue)
            {
                envioxml += "' DATA_NASCIMENTO='" + auxnascimento.ToString("dd/MM/yyyy");
            }
            else
            {
                envioxml += "' DATA_NASCIMENTO='" + "";
            }

            if (auxData1aHabilitacao != DateTime.MinValue)
            {
                envioxml += "' DATA_1_HABILITACAO='" + auxData1aHabilitacao.ToString("dd/MM/yyyy");
            }
            else
            {
                envioxml += "' DATA_1_HABILITACAO='" + "";
            }

            envioxml += "' RG='";
            envioxml += ((rg != "undefined" && rg != null) ? rg : "");

            envioxml += "' NOME='";
            envioxml += ((nomeMotorista != "undefined" && nomeMotorista != null) ? nomeMotorista : "");
            envioxml += "' NUMERO_ESPELHO='";
            envioxml += ((espelho != "undefined" && espelho != null) ? espelho : "");
            envioxml += "' NREGISTRO='";
            envioxml += ((cnh != "undefined" && cnh != null) ? cnh : "");
            envioxml += "' DESPACHANTE='" + "' CONSULTAORIGEM='" + 5 + "'/>";
            envioxml += "</CONSULTAS></CONSULTA1650>";

            return envioxml;
        }

        public DocumentXMLResponse GetDocumentFromDB(string sDocumentCode, string id)
        {
            try
            {
                DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();

                //Check Record is exist or not
                double validHours = 24;
                var latest = DateTime.UtcNow.AddHours(-validHours);

                DateTime now = DateTime.Now;
                var lstRecord = (from a in _context.QueryResult where a.Id.Trim() == id.Trim() && a.DocumentCode.Trim() == sDocumentCode.Trim() 
                                 && a.CreatedAt > now.AddHours(-24) && a.CreatedAt <= now
                                 && a.TipoDeConsulta == 2
                                 select a).ToList();

                if (lstRecord != null && lstRecord.Count > 0)
                {
                    _DocumentXMLResponse.Result = true;
                    _DocumentXMLResponse.xml = lstRecord[lstRecord.Count - 1].Xml.ToString();
                    _DocumentXMLResponse.QueryResultid = lstRecord[lstRecord.Count - 1].QueryResultid;

                    return _DocumentXMLResponse;
                }
                else
                {
                    _DocumentXMLResponse.Result = false;
                    return _DocumentXMLResponse;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public DocumentXMLResponse GetDocumentFromDB(string sDocumentCode)
        {
            try
            {
                DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();

                //Check Record is exist or not
                double validHours = 24;
                var latest = DateTime.UtcNow.AddHours(-validHours);

                DateTime now = DateTime.Now;
                var lstRecord = (from a in _context.QueryResult where a.DocumentCode.Trim() == sDocumentCode.Trim() && a.TipoDeConsulta == 2 select a).ToList();
                if (lstRecord != null && lstRecord.Count > 0)
                {
                    _DocumentXMLResponse.Result = true;
                    _DocumentXMLResponse.DocumentCode = sDocumentCode;
                    _DocumentXMLResponse.xml = lstRecord[lstRecord.Count - 1].Xml.ToString();
                    _DocumentXMLResponse.QueryResultid = lstRecord[lstRecord.Count - 1].QueryResultid;

                    return _DocumentXMLResponse;
                }
                else
                {
                    _DocumentXMLResponse.Result = false;
                    return _DocumentXMLResponse;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public dynamic GetDocumentFromDBById(int QId)
        {
            try
            {
                DateTime now = DateTime.Now;
                var lstRecord = (from a in _context.QueryResult where a.QueryResultid == QId select a).ToList();
                if (lstRecord != null && lstRecord.Count > 0)
                {
                    return lstRecord[0].Xml.ToString();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}



//public ServiceResponse GetDocumentFromServiceOnAuthentication(string sDocumentCode, string sUsuario, string sSenha)
//{

//    string placa = "", chassi = null, usuario = null, senha = null;
//    Int64 codigo = 0;
//    string sid = "";

//    ServiceResponse objResponse = new ServiceResponse();

//    var res = (from u in _context.UserMaster
//               join a in _context.AccountMaster on u.AccountId equals a.AccountId
//               where u.ApiUser == sUsuario && u.ApiPassword == sSenha
//               select new
//               {
//                   u.ApiSenha
//                   ,
//                   u.ApiUsuario
//                   ,
//                   a.ApiCode
//                   ,
//                   u.Id
//               }).ToList();

//    if (res != null && res.Count > 0)
//    {
//        usuario = res[0].ApiUsuario.ToString();
//        senha = res[0].ApiSenha.ToString();
//        Int64.TryParse(res[0].ApiCode.ToString(), out codigo);
//        sid = res[0].Id.ToString();

//        objResponse.status = true;
//        //objResponse.xml = GetDocumentFromService(sDocumentCode, sid);
//        return objResponse;
//    }
//    else
//    {
//        objResponse.status = false;
//        objResponse.Message = "invalid credentials";
//        return objResponse;
//    }
//}



//public dynamic GetDocumentFromDBById(int QId)
//{
//    try
//    {
//        //Check Record is exist or not
//        //double validHours = 24;
//        //var latest = DateTime.UtcNow.AddHours(-validHours);

//        DateTime now = DateTime.Now;
//        var lstRecord = (from a in _context.QueryResult where a.QueryResultid == QId select a).ToList();
//        if (lstRecord != null && lstRecord.Count > 0)
//        {
//            return lstRecord[0].Xml.ToString();
//        }
//        else
//        {
//            return null;
//        }
//    }
//    catch (Exception ex)
//    {
//        throw new Exception(ex.ToString());
//    }
//}

//public DocumentXMLResponse GetDocumentFromDB(string sDocumentCode)
//{
//    try
//    {
//        DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();

//        //Check Record is exist or not
//        double validHours = 24;
//        var latest = DateTime.UtcNow.AddHours(-validHours);

//        DateTime now = DateTime.Now;
//        var lstRecord = (from a in _context.QueryResult where a.DocumentCode.Trim() == sDocumentCode.Trim() select a).ToList();
//        if (lstRecord != null && lstRecord.Count > 0)
//        {
//            _DocumentXMLResponse.Result = true;
//            _DocumentXMLResponse.DocumentCode = sDocumentCode;
//            _DocumentXMLResponse.xml = lstRecord[lstRecord.Count - 1].Xml.ToString();
//            _DocumentXMLResponse.QueryResultid = lstRecord[lstRecord.Count - 1].QueryResultid;

//            return _DocumentXMLResponse;
//        }
//        else
//        {
//            _DocumentXMLResponse.Result = false;
//            return _DocumentXMLResponse;
//        }
//    }
//    catch (Exception ex)
//    {
//        throw new Exception(ex.ToString());
//    }
//}