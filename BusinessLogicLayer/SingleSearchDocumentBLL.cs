﻿using System;
using System.Collections.Generic;
using System.Text;
using EntityLayer.Models;
using System.Net;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using BusinessLogicLayer.Helpers;

namespace BusinessLogicLayer
{
    public class ServiceResponse
    {
        public bool status { get; set; }
        public string Message { get; set; }
        public string xml { get; set; }
    }

    public class SingleSearchDocumentBLL
    {
        private readonly JConsultasContext _context;

        public SingleSearchDocumentBLL(JConsultasContext context)
        {
            _context = context;
        }

        public dynamic CheckExistsInDatabase(string sDocumentCode, string sid)
        {
            try
            {
                //Check Record is exist or not
                double validHours = 24;
                var latest = DateTime.UtcNow.AddHours(-validHours);

                DateTime now = DateTime.Now;
                var lstRecord = (from a in _context.QueryResult where a.Id.Trim() == sid.Trim() && a.DocumentCode.Trim() == sDocumentCode.Trim() && a.TipoDeConsulta == 1 && a.CreatedAt > now.AddHours(-24) && a.CreatedAt <= now  select a).ToList();
                if (lstRecord != null && lstRecord.Count > 0)
                    return new { status = true, date = lstRecord[0].CreatedAt };
                else
                    return new { status = false, date = "" };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public string GetDocumentFromService(string sDocumentCode, string sid, string apiUsuario, string apiSenha, string apiCodigo,
                                                bool isApi = false)
        {
            try
            {
                string placa = "", chassi = null;

                if (sDocumentCode.Length <= 7)
                    placa = sDocumentCode.Trim();
                else
                    chassi = sDocumentCode.Trim();

                /*var res = (from u in _context.UserMaster
                           join a in _context.AccountMaster on u.AccountId equals a.AccountId
                           where u.Id == sid
                           select new
                           {
                               u.ApiSenha
                               ,
                               u.ApiUsuario
                               ,
                               a.ApiCode
                           }).ToList();

                if (res != null && res.Count > 0)
                {
                    usuario = res[0].ApiUsuario.ToString();
                    senha = res[0].ApiSenha.ToString();
                    
                }*/
                Int64 codigo = 0;
                Int64.TryParse(apiCodigo, out codigo);
                ConsultasStringService.ConsultasStringSoapClient c = new ConsultasStringService.ConsultasStringSoapClient(new ConsultasStringService.ConsultasStringSoapClient.EndpointConfiguration());
                ConsultasStringService.ConsultaVeiculoNovoResponse r = new ConsultasStringService.ConsultaVeiculoNovoResponse();

                r = c.ConsultaVeiculoNovoAsync(placa, chassi, codigo, apiUsuario.Trim(), apiSenha.Trim(), 5, "N").Result;

                string sXML = r.Body.ConsultaVeiculoNovoResult.ToString();
                bool bIsError = false;

                var xml = new XmlDocument();
                xml.LoadXml(sXML);
                string RETORNOEXECUCAO = "";
                string DESCRICAO = "";
                var node = xml.DocumentElement.SelectSingleNode("/CONSULTA2300/CONSULTAS/CONSULTA/RETORNOEXECUCAO");
                if (node != null)
                {
                    RETORNOEXECUCAO = node.InnerText.ToString();
                    if (RETORNOEXECUCAO == "999")
                    {
                        bIsError = true;
                        DESCRICAO = xml.DocumentElement.SelectSingleNode("/CONSULTA2300/CONSULTAS/CONSULTA/DESCRICAO").InnerText.ToString();
                    }
                }
                else if (xml.DocumentElement.InnerXml.Contains("Usuário ou senha inválida."))
                {
                    bIsError = true;
                    DESCRICAO = "Usuário e senha invalidos";
                }
                else
                {
                    throw new Exception("Ocorreu um erro interno, por favor contacte o suporte!");
                }


                // Now Insert Into database 
                QueryResult objQueryResult = new QueryResult();
                objQueryResult.DocumentCode = sDocumentCode;
                objQueryResult.CreatedAt = DateTime.Now;
                objQueryResult.Id = sid;
                objQueryResult.Xml = sXML;
                objQueryResult.IsError = bIsError;
                objQueryResult.Error = DESCRICAO;
                objQueryResult.TipoDeConsulta = 1;
                objQueryResult.IsConsultaApi = isApi;
                _context.QueryResult.Add(objQueryResult);
                _context.SaveChanges();

                return sXML;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public string GetDocumentFromService(string sDocumentCode, string sid, string apiUsuario, string apiSenha, string apiCodigo,
                                                ConsultaAvancadaVeiculo consultaAvancada, bool isApi = false)
        {
            try
            {
                string placa = "", chassi = null;

                if (sDocumentCode.Length <= 7)
                    placa = sDocumentCode.Trim();
                else
                    chassi = sDocumentCode.Trim();

                /*var res = (from u in _context.UserMaster
                           join a in _context.AccountMaster on u.AccountId equals a.AccountId
                           where u.Id == sid
                           select new
                           {
                               u.ApiSenha
                               ,
                               u.ApiUsuario
                               ,
                               a.ApiCode
                           }).ToList();

                if (res != null && res.Count > 0)
                {
                    usuario = res[0].ApiUsuario.ToString();
                    senha = res[0].ApiSenha.ToString();
                    
                }*/

                var consultaDePara = ObterConsultaDePara(consultaAvancada);

                Int64 codigo = 0;
                Int64.TryParse(apiCodigo, out codigo);
                ConsultasStringService.ConsultasStringSoapClient c = new ConsultasStringService.ConsultasStringSoapClient(new ConsultasStringService.ConsultasStringSoapClient.EndpointConfiguration());
                ConsultasStringService.ConsultaVeiculoV2Response r = new ConsultasStringService.ConsultaVeiculoV2Response();

                r = c.ConsultaVeiculoV2Async(placa, chassi, codigo, apiUsuario.Trim(), apiSenha.Trim(), 5, "N", consultaDePara.AGREGADO, consultaDePara.VEICULAR, consultaDePara.DEBITOS_360, consultaDePara.NOTIFICACAO_MULTA,
                    consultaDePara.HISTORICO_MULTA, consultaDePara.BLOQUEIO_RENAJUD, consultaDePara.BLOQUEIO_DETRAN, consultaDePara.GRAVAME, consultaDePara.DADOS_PROPRIETARIO).Result;

                string sXML = r.Body.ConsultaVeiculoV2Result.ToString();
                bool bIsError = false;

                var xml = new XmlDocument();
                xml.LoadXml(sXML);
                string RETORNOEXECUCAO = "";
                string DESCRICAO = "";
                var node = xml.DocumentElement.SelectSingleNode("/CONSULTA2300/CONSULTAS/CONSULTA/RETORNOEXECUCAO");
                if (node != null)
                {
                    RETORNOEXECUCAO = node.InnerText.ToString();
                    if (RETORNOEXECUCAO == "999")
                    {
                        bIsError = true;
                        DESCRICAO = xml.DocumentElement.SelectSingleNode("/CONSULTA2300/CONSULTAS/CONSULTA/DESCRICAO").InnerText.ToString();
                    }
                }
                else if (xml.DocumentElement.InnerXml.Contains("Usuário ou senha inválida."))
                {
                    bIsError = true;
                    DESCRICAO = "Usuário e senha invalidos";
                }
                else
                {
                    throw new Exception("Ocorreu um erro interno, por favor contacte o suporte!");
                }
                // Now Insert Into database 
                QueryResult objQueryResult = new QueryResult();
                objQueryResult.DocumentCode = sDocumentCode;
                objQueryResult.CreatedAt = DateTime.Now;
                objQueryResult.Id = sid;
                objQueryResult.Xml = sXML;
                objQueryResult.ConsultaSolicitada = SerializadorXML.Serializar(consultaAvancada);
                objQueryResult.IsError = bIsError;
                objQueryResult.Error = DESCRICAO;
                objQueryResult.TipoDeConsulta = 1;
                objQueryResult.IsConsultaApi = isApi;
                _context.QueryResult.Add(objQueryResult);
                _context.SaveChanges();

                return sXML;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private ConsultaAvancadaVeiculoWS ObterConsultaDePara(ConsultaAvancadaVeiculo consultaAvancada)
        {
            ConsultaAvancadaVeiculoWS consultaWS = new ConsultaAvancadaVeiculoWS();

            consultaWS.AGREGADO = consultaAvancada.DadosVeic;

            if (consultaAvancada.DadosProp || consultaAvancada.IntencaoGravame || consultaAvancada.ArrendarioFinanceira || consultaAvancada.Restricoes
                || consultaAvancada.InspecaoVeicular)
            {
                consultaWS.VEICULAR = true;
            }
            else
            {
                consultaWS.VEICULAR = false;
            }

            if (consultaAvancada.DetalhamentoDPVAT || consultaAvancada.DetalhamentoIPVA || consultaAvancada.DetalhamentoMultas || consultaAvancada.DetalhamentoDebitos)
            {
                consultaWS.DEBITOS_360 = true;
            }
            else
            {
                consultaWS.DEBITOS_360 = false;
            }

            if (consultaAvancada.DetalhamentoMultas)
            {
                consultaWS.NOTIFICACAO_MULTA = true;
                consultaWS.HISTORICO_MULTA = true;
            }
            else
            {
                consultaWS.NOTIFICACAO_MULTA = false;
                consultaWS.HISTORICO_MULTA = false;
            }

            if (consultaAvancada.BloqueiosDetran)
            {
                consultaWS.BLOQUEIO_DETRAN = true;
                consultaWS.BLOQUEIO_RENAJUD = true;
            }
            else
            {
                consultaWS.BLOQUEIO_DETRAN = false;
                consultaWS.BLOQUEIO_RENAJUD = false;
            }

            if (consultaAvancada.IntencaoGravame || consultaAvancada.ArrendarioFinanceira)
            {
                consultaWS.GRAVAME = true;
            }
            else
            {
                consultaWS.GRAVAME = false;
            }

            if (consultaAvancada.DadosProp)
            {
                consultaWS.DADOS_PROPRIETARIO = true;
            }
            else
            {
                consultaWS.DADOS_PROPRIETARIO = false;
            }

            return consultaWS;
        }

        public ServiceResponse GetDocumentFromServiceOnAuthentication(string sDocumentCode, string sUsuario, string sSenha, string apiUsuario, string apiSenha, string apiCodigo)
        {
            Int32 codigo = 0;
            string sid = "";
            bool isApi = true;
            ServiceResponse objResponse = new ServiceResponse();

            var res = (from u in _context.UserMaster
                       join a in _context.AccountMaster on u.AccountId equals a.AccountId
                       where u.ApiUser == sUsuario && u.ApiPassword == sSenha
                       select new
                       {
                           u.Id
                       }).ToList();

            if(res == null || res.Count == 0)
            {
                objResponse.status = false;
                objResponse.Message = "Credenciais de usuário inválidas";
                return objResponse;
            }
            else if (!Int32.TryParse(apiCodigo, out codigo) || String.IsNullOrEmpty(apiUsuario) || String.IsNullOrEmpty(apiSenha))
            {
                objResponse.status = false;
                objResponse.Message = "Credenciais para consulta API inválidas";
                return objResponse;
            }
            else
            {
                sid = res[0].Id.ToString();

                objResponse.status = true;
                objResponse.xml = GetDocumentFromService(sDocumentCode, sid, apiUsuario, apiSenha, codigo.ToString(), isApi);
                return objResponse;
            }
        }

        public DocumentXMLResponse GetDocumentFromDB(string sDocumentCode, string id)
        {
            try
            {
                DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();

                //Check Record is exist or not
                double validHours = 24;
                var latest = DateTime.UtcNow.AddHours(-validHours);

                DateTime now = DateTime.Now;
                var lstRecord = (from a in _context.QueryResult where a.Id.Trim() == id.Trim() && a.DocumentCode.Trim() == sDocumentCode.Trim() && a.TipoDeConsulta == 1 && a.CreatedAt > now.AddHours(-24) && a.CreatedAt <= now  select a).ToList();
                if (lstRecord != null && lstRecord.Count > 0)
                {
                    _DocumentXMLResponse.Result = true;
                    _DocumentXMLResponse.xml = lstRecord[lstRecord.Count - 1].Xml.ToString();
                    _DocumentXMLResponse.QueryResultid = lstRecord[lstRecord.Count - 1].QueryResultid;
                    _DocumentXMLResponse.ConsultaSolicitada = lstRecord[lstRecord.Count - 1].ConsultaSolicitada;
                    return _DocumentXMLResponse;
                }
                else
                {
                    _DocumentXMLResponse.Result = false;
                    return _DocumentXMLResponse;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public DocumentXMLResponse GetDocumentFromDBById(int QId)
        {
            try
            {
                DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();

                DateTime now = DateTime.Now;
                var lstRecord = (from a in _context.QueryResult where a.QueryResultid == QId && a.TipoDeConsulta == 1 select a).ToList();
                if (lstRecord != null && lstRecord.Count > 0)
                {
                    _DocumentXMLResponse.Result = true;
                    _DocumentXMLResponse.xml = lstRecord[0].Xml.ToString();
                    _DocumentXMLResponse.QueryResultid = lstRecord[0].QueryResultid;
                    _DocumentXMLResponse.ConsultaSolicitada = lstRecord[0].ConsultaSolicitada;
                    return _DocumentXMLResponse;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public DocumentXMLResponse GetDocumentFromDB(string sDocumentCode)
        {
            try
            {
                DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();

                //Check Record is exist or not
                double validHours = 24;
                var latest = DateTime.UtcNow.AddHours(-validHours);

                DateTime now = DateTime.Now;
                var lstRecord = (from a in _context.QueryResult where a.DocumentCode.Trim() == sDocumentCode.Trim() && a.TipoDeConsulta == 1 select a).ToList();
                if (lstRecord != null && lstRecord.Count > 0)
                {
                    _DocumentXMLResponse.Result = true;
                    _DocumentXMLResponse.DocumentCode = sDocumentCode;
                    _DocumentXMLResponse.xml = lstRecord[lstRecord.Count - 1].Xml.ToString();
                    _DocumentXMLResponse.QueryResultid = lstRecord[lstRecord.Count - 1].QueryResultid;
                    _DocumentXMLResponse.ConsultaSolicitada = lstRecord[lstRecord.Count - 1].ConsultaSolicitada;
                    return _DocumentXMLResponse;
                }
                else
                {
                    _DocumentXMLResponse.Result = false;
                    return _DocumentXMLResponse;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }

    public class DocumentXMLResponse
    {
        public bool Result { get; set; }
        public string xml { get; set; }
        public string ConsultaSolicitada { get; set; }
        public string DocumentCode { get; set; }
        public int QueryResultid { get; set; }
    }

}
