﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace BusinessLogicLayer.Helpers
{
    public static class SerializadorXML
    {
        public static string SerializarSemCabecalhos(object obj)
        {
            var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(obj.GetType());
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            using (var stream = new StringWriter())
            using (var writer = XmlWriter.Create(stream, settings))
            {
                serializer.Serialize(writer, obj, emptyNamepsaces);
                return stream.ToString();
            }
        }

        public static string Serializar(object obj)
        {
            var serializer = new XmlSerializer(obj.GetType());
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            using (var stream = new StringWriter())
            using (var writer = XmlWriter.Create(stream, settings))
            {
                serializer.Serialize(writer, obj);
                return stream.ToString();
            }
        }

        public static T Deserializar<T>(string xml)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                var retorno = (T)serializer.Deserialize(reader);
                return retorno;
            }
            catch (Exception ex)
            {
                return default(T);
                //throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }
    }
}
