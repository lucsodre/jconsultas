﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer
{
    public class ImportChildData
    {
        public string placa { get; set; }
        public string chassi { get; set; }

        public string UF { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string NumRegistro { get; set; }
        public string NumRenach { get; set; }
        public DateTime? ValidadeCNH { get; set; }
        public string RG { get; set; }
        public DateTime? DataNascimento { get; set; }
        public DateTime? DataHabilitacao { get; set; }
        public string MunicipioNascimento { get; set; }
        public string NumCedulaEspelho { get; set; }
    }

    public class ImportMasterData
    {
        public string FileName { get; set; }
        public int TotalFile { get; set; }
        public int TotalProceed { get; set; }
        public int TotlaFailed { get; set; }
    }

    public class ImpoerExcel
    {
        public bool Status { get; set; }
        public string Msg { get; set; }
        public List<ImportChildData> ImportChildData { get; set; }
        public ImportMasterData ImportMasterData { get; set; }

    }
}
