﻿using EntityLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class ListaConsultaCNHBLL
    {
        private readonly JConsultasContext _context;
        private readonly UserBLL _UserBLL;

        public ListaConsultaCNHBLL(JConsultasContext context)
        {
            _context = context;
            _UserBLL = new UserBLL(context);
        }

        public IEnumerable<UserMaster> GetUserList(string UserId)
        {
            try
            {
                var accountId = (from u in _context.UserMaster where u.Id == UserId select u).First().AccountId;
                var res = (from u in _context.UserMaster where u.AccountId == accountId select u).ToList();
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<dynamic> GetListOfSearchedDocs(string Id, string DocumentCode, string StartDate, string EndDate, string userId, string AccountID)
        {
            try
            {

                UserRequest _UserRequest = new UserRequest();
                _UserRequest = _UserBLL.IsAdminRequest(userId);
                if (_UserRequest.IsAdminRequest && !string.IsNullOrEmpty(AccountID))
                {
                    int iAccountID = 0;
                    int.TryParse(AccountID, out iAccountID);


                    DateTime sDate = Convert.ToDateTime(StartDate);
                    DateTime eDate = Convert.ToDateTime(EndDate);
                    var res = (from o in _context.QueryResult
                               join u in _context.UserMaster on o.Id equals u.Id
                               from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                               where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                               && o.DocumentCode == (string.IsNullOrEmpty(DocumentCode) ? o.DocumentCode : DocumentCode)
                               && o.CreatedAt.Value.Date >= sDate.Date && o.CreatedAt.Value.Date <= eDate.Date
                               && u.AccountId == iAccountID
                               && o.TipoDeConsulta == 2
                               select new
                               {
                                   o.Id,
                                   o.QueryResultid,
                                   o.DocumentCode,
                                   o.CreatedAt,
                                   o.Xml,
                                   o.IsError,
                                   o.Error,
                                   o.IsConsultaApi,
                                   UserName = u.Name + " " + u.LastName
                                   ,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(z => z.CreatedAt).ToList();
                    return res;
                }
                else if (_UserRequest.IsAdminRequest)
                {
                    DateTime sDate = Convert.ToDateTime(StartDate);
                    DateTime eDate = Convert.ToDateTime(EndDate);
                    //var res = (from o in _context.QueryResult where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id) && o.DocumentCode == (string.IsNullOrEmpty(DocumentCode) ? o.DocumentCode : DocumentCode) && o.CreatedAt.Value.Date >= sDate.Date && o.CreatedAt.Value.Date <= eDate.Date select o).ToList();
                    var res = (from o in _context.QueryResult
                               join u in _context.UserMaster on o.Id equals u.Id
                               from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                               where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                               && o.DocumentCode == (string.IsNullOrEmpty(DocumentCode) ? o.DocumentCode : DocumentCode)
                               && o.CreatedAt.Value.Date >= sDate.Date && o.CreatedAt.Value.Date <= eDate.Date
                               && o.TipoDeConsulta == 2
                               select new
                               {
                                   o.Id,
                                   o.QueryResultid,
                                   o.DocumentCode,
                                   o.CreatedAt,
                                   o.Xml,
                                   o.IsError,
                                   o.Error,
                                   o.IsConsultaApi,
                                   UserName = u.Name + " " + u.LastName
                                   ,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(z => z.CreatedAt).ToList();
                    return res;
                }
                else
                {
                    DateTime sDate = Convert.ToDateTime(StartDate);
                    DateTime eDate = Convert.ToDateTime(EndDate);
                    var res = (from o in _context.QueryResult
                               join u in _context.UserMaster on o.Id equals u.Id
                               from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                               where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                               && o.DocumentCode == (string.IsNullOrEmpty(DocumentCode) ? o.DocumentCode : DocumentCode)
                               && o.CreatedAt.Value.Date >= sDate.Date && o.CreatedAt.Value.Date <= eDate.Date
                               && u.AccountId == _UserRequest.AccountID
                               && o.TipoDeConsulta == 2
                               select new
                               {
                                   o.Id,
                                   o.QueryResultid,
                                   o.DocumentCode,
                                   o.CreatedAt,
                                   o.Xml,
                                   o.IsError,
                                   o.Error,
                                   o.IsConsultaApi,
                                   UserName = u.Name + " " + u.LastName
                                    ,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(z => z.CreatedAt).ToList();
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<dynamic> GetListOfSearchedDocs(string Id, string DocumentCode, string StartDate, string EndDate, string userId, string AccountID, string ConsultaAPI)
        {
            try
            {

                UserRequest _UserRequest = new UserRequest();
                _UserRequest = _UserBLL.IsAdminRequest(userId);
                if (_UserRequest.IsAdminRequest && !string.IsNullOrEmpty(AccountID))
                {
                    int iAccountID = 0;
                    int.TryParse(AccountID, out iAccountID);


                    DateTime sDate = Convert.ToDateTime(StartDate);
                    DateTime eDate = Convert.ToDateTime(EndDate);
                    var res = (from o in _context.QueryResult
                               join u in _context.UserMaster on o.Id equals u.Id
                               from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                               where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                               && o.DocumentCode == (string.IsNullOrEmpty(DocumentCode) ? o.DocumentCode : DocumentCode)
                               && o.CreatedAt.Value.Date >= sDate.Date && o.CreatedAt.Value.Date <= eDate.Date
                               && u.AccountId == iAccountID
                               && o.TipoDeConsulta == 2
                               && o.IsConsultaApi == (ConsultaAPI == "1") ? true : false
                               select new
                               {
                                   o.Id,
                                   o.QueryResultid,
                                   o.DocumentCode,
                                   o.CreatedAt,
                                   o.Xml,
                                   o.IsError,
                                   o.Error,
                                   o.IsConsultaApi,
                                   UserName = u.Name + " " + u.LastName
                                   ,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(z => z.CreatedAt).ToList();
                    return res;
                }
                else if (_UserRequest.IsAdminRequest)
                {
                    DateTime sDate = Convert.ToDateTime(StartDate);
                    DateTime eDate = Convert.ToDateTime(EndDate);
                    //var res = (from o in _context.QueryResult where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id) && o.DocumentCode == (string.IsNullOrEmpty(DocumentCode) ? o.DocumentCode : DocumentCode) && o.CreatedAt.Value.Date >= sDate.Date && o.CreatedAt.Value.Date <= eDate.Date select o).ToList();
                    var res = (from o in _context.QueryResult
                               join u in _context.UserMaster on o.Id equals u.Id
                               from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                               where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                               && o.DocumentCode == (string.IsNullOrEmpty(DocumentCode) ? o.DocumentCode : DocumentCode)
                               && o.CreatedAt.Value.Date >= sDate.Date && o.CreatedAt.Value.Date <= eDate.Date
                               && o.TipoDeConsulta == 2
                               && o.IsConsultaApi == (ConsultaAPI == "1") ? true : false
                               select new
                               {
                                   o.Id,
                                   o.QueryResultid,
                                   o.DocumentCode,
                                   o.CreatedAt,
                                   o.Xml,
                                   o.IsError,
                                   o.Error,
                                   o.IsConsultaApi,
                                   UserName = u.Name + " " + u.LastName
                                   ,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(z => z.CreatedAt).ToList();
                    return res;
                }
                else
                {
                    DateTime sDate = Convert.ToDateTime(StartDate);
                    DateTime eDate = Convert.ToDateTime(EndDate);
                    var res = (from o in _context.QueryResult
                               join u in _context.UserMaster on o.Id equals u.Id
                               from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                               where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                               && o.DocumentCode == (string.IsNullOrEmpty(DocumentCode) ? o.DocumentCode : DocumentCode)
                               && o.CreatedAt.Value.Date >= sDate.Date && o.CreatedAt.Value.Date <= eDate.Date
                               && u.AccountId == _UserRequest.AccountID
                               && o.TipoDeConsulta == 2
                               && o.IsConsultaApi == (ConsultaAPI == "1") ? true : false
                               select new
                               {
                                   o.Id,
                                   o.QueryResultid,
                                   o.DocumentCode,
                                   o.CreatedAt,
                                   o.Xml,
                                   o.IsError,
                                   o.Error,
                                   o.IsConsultaApi,
                                   UserName = u.Name + " " + u.LastName
                                    ,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(z => z.CreatedAt).ToList();
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<dynamic> GetListOfMultipleSearchedDocs(string Id, string StartDate, string EndDate, bool? IsProcessed, string userId, string AccountID)
        {
            try
            {

                UserRequest _UserRequest = new UserRequest();
                _UserRequest = _UserBLL.IsAdminRequest(userId);
                if (_UserRequest.IsAdminRequest && !string.IsNullOrEmpty(AccountID))
                {
                    int iAccountID = 0;
                    int.TryParse(AccountID, out iAccountID);

                    DateTime sDate = Convert.ToDateTime(StartDate);
                    DateTime eDate = Convert.ToDateTime(EndDate);

                    var res = (from o in _context.MultipleMasterData
                               join u in _context.UserMaster on o.Id equals u.Id
                               from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                               where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                               && o.IsProceedDone == (string.IsNullOrEmpty(Convert.ToString(IsProcessed)) ? o.IsProceedDone : IsProcessed)
                               && o.CreatedBy.Value.Date >= sDate.Date && o.CreatedBy.Value.Date <= eDate.Date
                               && o.TipoDeConsulta == 2
                               && u.AccountId == iAccountID
                               select new
                               {
                                   o.MultipleMasterDataid,
                                   o.FileName,
                                   IsProceedDone = o.IsProceedDone == true ? "Finalizado" : "Pending",
                                   o.IsRetryProcess,
                                   o.TotalFile,
                                   o.TotalProceed,
                                   o.TotlaFailed,
                                   o.CreatedBy,
                                   UserName = u.Name + " " + u.LastName
                                    ,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(o => o.CreatedBy).ToList();
                    return res;
                }
                else if (_UserRequest.IsAdminRequest)
                {
                    int iAccountID = 0;
                    int.TryParse(AccountID, out iAccountID);

                    DateTime sDate = Convert.ToDateTime(StartDate);
                    DateTime eDate = Convert.ToDateTime(EndDate);

                    var res = (from o in _context.MultipleMasterData
                               join u in _context.UserMaster on o.Id equals u.Id
                               from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                               where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                               && o.IsProceedDone == (string.IsNullOrEmpty(Convert.ToString(IsProcessed)) ? o.IsProceedDone : IsProcessed)
                               && o.TipoDeConsulta == 2
                               && o.CreatedBy.Value.Date >= sDate.Date && o.CreatedBy.Value.Date <= eDate.Date
                               select new
                               {
                                   o.MultipleMasterDataid,
                                   o.FileName,
                                   IsProceedDone = o.IsProceedDone == true ? "Finalizado" : "Pending",
                                   o.IsRetryProcess,
                                   o.TotalFile,
                                   o.TotalProceed,
                                   o.TotlaFailed,
                                   o.CreatedBy,
                                   UserName = u.Name + " " + u.LastName
                                    ,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(o => o.CreatedBy).ToList();
                    return res;
                }

                else
                {
                    DateTime sDate = Convert.ToDateTime(StartDate);
                    DateTime eDate = Convert.ToDateTime(EndDate);

                    var res = (from o in _context.MultipleMasterData
                               join u in _context.UserMaster on o.Id equals u.Id
                               join a in _context.AccountMaster on u.AccountId equals a.AccountId
                               where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                               && o.IsProceedDone == (string.IsNullOrEmpty(Convert.ToString(IsProcessed)) ? o.IsProceedDone : IsProcessed)
                               && o.CreatedBy.Value.Date >= sDate.Date && o.CreatedBy.Value.Date <= eDate.Date
                               && o.TipoDeConsulta == 2
                               && u.AccountId == _UserRequest.AccountID
                               select new
                               {
                                   o.MultipleMasterDataid,
                                   o.FileName,
                                   IsProceedDone = o.IsProceedDone == true ? "Finalizado" : "Pending",
                                   o.IsRetryProcess,
                                   o.TotalFile,
                                   o.TotalProceed,
                                   o.TotlaFailed,
                                   o.CreatedBy,
                                   UserName = u.Name + " " + u.LastName
                                    ,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(o => o.CreatedBy).ToList();
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<dynamic> GetListaBuscaEmLoteCNH(string Id, string StartDate, string EndDate, bool? IsProcessed, string userId, string AccountID)
        {
            try
            {

                UserRequest _UserRequest = new UserRequest();
                _UserRequest = _UserBLL.IsAdminRequest(userId);
                if (_UserRequest.IsAdminRequest && !string.IsNullOrEmpty(AccountID))
                {
                    int iAccountID = 0;
                    int.TryParse(AccountID, out iAccountID);

                    DateTime sDate = Convert.ToDateTime(StartDate);
                    DateTime eDate = Convert.ToDateTime(EndDate);

                    var res = (from o in _context.MultipleMasterData
                               join u in _context.UserMaster on o.Id equals u.Id
                               from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                               where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                               && o.IsProceedDone == (string.IsNullOrEmpty(Convert.ToString(IsProcessed)) ? o.IsProceedDone : IsProcessed)
                               && o.CreatedBy.Value.Date >= sDate.Date && o.CreatedBy.Value.Date <= eDate.Date
                               && o.TipoDeConsulta == 2
                               && u.AccountId == iAccountID
                               select new
                               {
                                   o.MultipleMasterDataid,
                                   o.FileName,
                                   IsProceedDone = o.IsProceedDone == true ? "Finalizado" : "Pending",
                                   o.IsRetryProcess,
                                   o.TotalFile,
                                   o.TotalProceed,
                                   o.TotlaFailed,
                                   o.CreatedBy,
                                   UserName = u.Name + " " + u.LastName,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(o => o.CreatedBy).ToList();
                    return res;
                }
                else if (_UserRequest.IsAdminRequest)
                {
                    int iAccountID = 0;
                    int.TryParse(AccountID, out iAccountID);

                    DateTime sDate = Convert.ToDateTime(StartDate);
                    DateTime eDate = Convert.ToDateTime(EndDate);

                    var res = (from o in _context.MultipleMasterData
                               join u in _context.UserMaster on o.Id equals u.Id
                               from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                               where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                               && o.IsProceedDone == (string.IsNullOrEmpty(Convert.ToString(IsProcessed)) ? o.IsProceedDone : IsProcessed)
                               && o.CreatedBy.Value.Date >= sDate.Date && o.CreatedBy.Value.Date <= eDate.Date
                               && o.TipoDeConsulta == 2
                               select new
                               {
                                   o.MultipleMasterDataid,
                                   o.FileName,
                                   IsProceedDone = o.IsProceedDone == true ? "Finalizado" : "Pending",
                                   o.IsRetryProcess,
                                   o.TotalFile,
                                   o.TotalProceed,
                                   o.TotlaFailed,
                                   o.CreatedBy,
                                   UserName = u.Name + " " + u.LastName
                                    ,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(o => o.CreatedBy).ToList();
                    return res;
                }

                else
                {
                    DateTime sDate = Convert.ToDateTime(StartDate);
                    DateTime eDate = Convert.ToDateTime(EndDate);

                    var res = (from o in _context.MultipleMasterData
                               join u in _context.UserMaster on o.Id equals u.Id
                               join a in _context.AccountMaster on u.AccountId equals a.AccountId
                               where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                               && o.IsProceedDone == (string.IsNullOrEmpty(Convert.ToString(IsProcessed)) ? o.IsProceedDone : IsProcessed)
                               && o.CreatedBy.Value.Date >= sDate.Date && o.CreatedBy.Value.Date <= eDate.Date
                               && u.AccountId == _UserRequest.AccountID
                               && o.TipoDeConsulta == 2
                               select new
                               {
                                   o.MultipleMasterDataid,
                                   o.FileName,
                                   IsProceedDone = o.IsProceedDone == true ? "Finalizado" : "Pending",
                                   o.IsRetryProcess,
                                   o.TotalFile,
                                   o.TotalProceed,
                                   o.TotlaFailed,
                                   o.CreatedBy,
                                   UserName = u.Name + " " + u.LastName
                                    ,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(o => o.CreatedBy).ToList();
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<dynamic> GetListOfMultipleSearchedPendingDocs(string Id, string StartDate, string EndDate, bool? IsProcessed)
        {
            try
            {
                DateTime sDate = Convert.ToDateTime(StartDate);
                DateTime eDate = Convert.ToDateTime(EndDate);

                var res = (from o in _context.MultipleMasterData
                           join u in _context.UserMaster on o.Id equals u.Id
                           where o.Id == (string.IsNullOrEmpty(Id) ? o.Id : Id)
                           && o.IsProceedDone == (string.IsNullOrEmpty(Convert.ToString(IsProcessed)) ? o.IsProceedDone : IsProcessed)
                           && o.CreatedBy.Value.Date >= sDate.Date && o.CreatedBy.Value.Date <= eDate.Date
                           && o.TipoDeConsulta == 2

                           select new { o.MultipleMasterDataid, o.FileName, IsProceedDone = o.IsProceedDone == true ? "Finalizado" : "Pending", o.IsRetryProcess, o.TotalFile, o.TotalProceed, o.TotlaFailed, o.CreatedBy }).OrderByDescending(o => o.CreatedBy).ToList();
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<dynamic> GetLastSearchedDocs(string userId)
        {
            try
            {
                UserRequest _UserRequest = new UserRequest();
                _UserRequest = _UserBLL.IsAdminRequest(userId);
                if (_UserRequest.IsAdminRequest)
                {
                    var res = (from o in _context.QueryResult
                               join u in _context.UserMaster on o.Id equals u.Id
                               from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                               where o.TipoDeConsulta == 2
                               orderby o.Id descending
                               select new
                               {
                                   o.Id,
                                   o.QueryResultid,
                                   o.DocumentCode,
                                   o.CreatedAt,
                                   o.Xml,
                                   o.IsError,
                                   o.Error,
                                   o.IsConsultaApi,
                                   UserName = u.Name + " " + u.LastName
                                    ,
                                   AccountName = a == null ? "" : a.AccountName
                               }).OrderByDescending(z => z.CreatedAt).ToList().Take(10);
                    return res;
                }
                else
                {
                    var res = (from o in _context.QueryResult
                               join d in _context.MultipleData on o.MultipleDataId equals d.MultipleDataId
                               join m in _context.MultipleMasterData on d.MultipleMasterDataid equals m.MultipleMasterDataid
                               join u in _context.UserMaster on o.Id equals u.Id
                               join a in _context.AccountMaster on u.AccountId equals a.AccountId
                               where u.AccountId == _UserRequest.AccountID && o.TipoDeConsulta == 2
                               orderby o.Id descending
                               select new
                               {
                                   o.Id,
                                   o.QueryResultid,
                                   o.DocumentCode,
                                   o.CreatedAt,
                                   o.Xml,
                                   o.IsError,
                                   o.Error,
                                   o.IsConsultaApi,
                                   UserName = u.Name + " " + u.LastName
                                     ,
                                   AccountName = a.AccountName
                               }).OrderByDescending(z => z.CreatedAt).ToList().Take(10);
                    return res;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public IEnumerable<dynamic> GetDocCodeListOnMaster(string mid)
        //{

        //    var res = (from m in _context.MultipleData
        //               join q in _context.QueryResult on m.MultipleDataId equals q.MultipleDataId
        //               join u in _context.UserMaster on q.Id equals u.Id
        //               where m.MultipleMasterDataid == iMid && data.QueryResultid.Contains(q.QueryResultid)
        //               orderby q.Id descending
        //               select new { q.Id, q.QueryResultid, q.DocumentCode, q.CreatedAt, q.Xml, UserName = u.Name + " " + u.LastName }
        //               ).OrderByDescending(z => z.CreatedAt).ToList();

        //    return res;
        //}

        public IEnumerable<dynamic> GetDataOnMultipleData(MultipleSearch data)
        {
            try
            {
                int iMid = 0;
                int.TryParse(data.id, out iMid);

                if (data.QueryResultid == null)
                {
                    if (data.iserror == "true")
                    {
                        var res = (from m in _context.MultipleData
                                   join d in _context.MultipleMasterData on m.MultipleMasterDataid equals d.MultipleMasterDataid
                                   join q in _context.QueryResult on m.MultipleDataId equals q.MultipleDataId
                                   join u in _context.UserMaster on q.Id equals u.Id
                                   from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                                   where m.MultipleMasterDataid == iMid && q.IsError == true && q.TipoDeConsulta == 2
                                   orderby q.Id descending
                                   select new
                                   {
                                       q.Id,
                                       q.QueryResultid,
                                       q.DocumentCode,
                                       q.CreatedAt,
                                       q.Xml,
                                       q.IsError,
                                       q.Error,
                                       q.IsConsultaApi,
                                       UserName = u.Name + " " + u.LastName,
                                       AccountName = a == null ? "" : a.AccountName
                                   }
                               ).OrderByDescending(z => z.CreatedAt).ToList();

                        return res;
                    }
                    else
                    {
                        var res = (from m in _context.MultipleData
                                   join d in _context.MultipleMasterData on m.MultipleMasterDataid equals d.MultipleMasterDataid
                                   join q in _context.QueryResult on m.MultipleDataId equals q.MultipleDataId
                                   join u in _context.UserMaster on q.Id equals u.Id
                                   from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                                   where m.MultipleMasterDataid == iMid && q.TipoDeConsulta == 2
                                   orderby q.Id descending
                                   select new
                                   {
                                       q.Id,
                                       q.QueryResultid,
                                       q.DocumentCode,
                                       q.CreatedAt,
                                       q.Xml,
                                       q.IsError,
                                       q.Error,
                                       q.IsConsultaApi,
                                       UserName = u.Name + " " + u.LastName,
                                       AccountName = a == null ? "" : a.AccountName
                                   }
                               ).OrderByDescending(z => z.CreatedAt).ToList();

                        return res;
                    }
                }
                else
                {
                    if (data.QueryResultid.Length == 0)
                    {
                        if (data.iserror == "true")
                        {
                            var res = (from m in _context.MultipleData
                                       join d in _context.MultipleMasterData on m.MultipleMasterDataid equals d.MultipleMasterDataid
                                       join q in _context.QueryResult on m.MultipleDataId equals q.MultipleDataId
                                       join u in _context.UserMaster on q.Id equals u.Id
                                       from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                                       where m.MultipleMasterDataid == iMid && q.IsError == true && q.TipoDeConsulta == 2
                                       orderby q.Id descending
                                       select new
                                       {
                                           q.Id,
                                           q.QueryResultid,
                                           q.DocumentCode,
                                           q.CreatedAt,
                                           q.Xml,
                                           q.IsError,
                                           q.Error,
                                           q.IsConsultaApi,
                                           UserName = u.Name + " " + u.LastName,
                                           AccountName = a == null ? "" : a.AccountName
                                       }
                                   ).OrderByDescending(z => z.CreatedAt).ToList();

                            return res;
                        }
                        else
                        {
                            var res = (from m in _context.MultipleData
                                       join d in _context.MultipleMasterData on m.MultipleMasterDataid equals d.MultipleMasterDataid
                                       join q in _context.QueryResult on m.MultipleDataId equals q.MultipleDataId
                                       join u in _context.UserMaster on q.Id equals u.Id
                                       from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                                       where m.MultipleMasterDataid == iMid && q.TipoDeConsulta == 2
                                       orderby q.Id descending
                                       select new
                                       {
                                           q.Id,
                                           q.QueryResultid,
                                           q.DocumentCode,
                                           q.CreatedAt,
                                           q.Xml,
                                           q.IsError,
                                           q.Error,
                                           q.IsConsultaApi,
                                           UserName = u.Name + " " + u.LastName,
                                           AccountName = a == null ? "" : a.AccountName
                                       }
                                   ).OrderByDescending(z => z.CreatedAt).ToList();

                            return res;
                        }
                    }
                    else
                    {


                        if (data.iserror == "true")
                        {
                            var res = (from m in _context.MultipleData
                                       join d in _context.MultipleMasterData on m.MultipleMasterDataid equals d.MultipleMasterDataid
                                       join q in _context.QueryResult on m.MultipleDataId equals q.MultipleDataId
                                       join u in _context.UserMaster on q.Id equals u.Id
                                       from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                                       where m.MultipleMasterDataid == iMid && data.QueryResultid.Contains(q.QueryResultid) && q.IsError == true && q.TipoDeConsulta == 2
                                       orderby q.Id descending
                                       select new
                                       {
                                           q.Id,
                                           q.QueryResultid,
                                           q.DocumentCode,
                                           q.CreatedAt,
                                           q.Xml,
                                           q.IsError,
                                           q.Error,
                                           q.IsConsultaApi,
                                           UserName = u.Name + " " + u.LastName,
                                           AccountName = a == null ? "" : a.AccountName
                                       }
                               ).OrderByDescending(z => z.CreatedAt).ToList();

                            return res;
                        }
                        else
                        {
                            var res = (from m in _context.MultipleData
                                       join d in _context.MultipleMasterData on m.MultipleMasterDataid equals d.MultipleMasterDataid
                                       join q in _context.QueryResult on m.MultipleDataId equals q.MultipleDataId
                                       join u in _context.UserMaster on q.Id equals u.Id
                                       from a in _context.AccountMaster.Where(mapping => mapping.AccountId == u.AccountId).DefaultIfEmpty()
                                       where m.MultipleMasterDataid == iMid && data.QueryResultid.Contains(q.QueryResultid) && q.TipoDeConsulta == 2
                                       orderby q.Id descending
                                       select new
                                       {
                                           q.Id,
                                           q.QueryResultid,
                                           q.DocumentCode,
                                           q.CreatedAt,
                                           q.Xml,
                                           q.IsError,
                                           q.Error,
                                           q.IsConsultaApi,
                                           UserName = u.Name + " " + u.LastName,
                                           AccountName = a == null ? "" : a.AccountName
                                       }
                               ).OrderByDescending(z => z.CreatedAt).ToList();

                            return res;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCodeOnId(string QueryResultid)
        {
            int iQueryResultid = 0;
            int.TryParse(QueryResultid, out iQueryResultid);

            var res = (from q in _context.QueryResult
                       where q.QueryResultid == iQueryResultid
                       select new
                       {
                           q.DocumentCode
                       }).ToList();

            return res[0].DocumentCode.ToString();
        }
    }
}
