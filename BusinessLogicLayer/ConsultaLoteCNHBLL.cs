﻿using EntityLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class ConsultaLoteCNHBLL
    {
        private readonly JConsultasContext _context;

        public ConsultaLoteCNHBLL(JConsultasContext context)
        {
            _context = context;
        }

        public bool SaveMultipledata(ImpoerExcel data, string UserID)
        {
            // Insert into master data
            MultipleMasterData objMultipleMasterData = new MultipleMasterData();
            objMultipleMasterData.CreatedBy = DateTime.Now;
            objMultipleMasterData.FileName = data.ImportMasterData.FileName.ToString();
            objMultipleMasterData.TotalFile = data.ImportMasterData.TotalFile;
            objMultipleMasterData.TotalProceed = 0;
            objMultipleMasterData.TotlaFailed = 0;
            objMultipleMasterData.IsRetryProcess = false;
            objMultipleMasterData.Id = UserID;
            objMultipleMasterData.IsProceedDone = false;
            objMultipleMasterData.TipoDeConsulta = 2;

            _context.MultipleMasterData.Add(objMultipleMasterData);
            _context.SaveChanges();

            if (objMultipleMasterData.MultipleMasterDataid > 0)
            {
                for (int i = 0; i < data.ImportChildData.Count; i++)
                {
                    MultipleData objMultipleData = new MultipleData();
                    objMultipleData.MultipleMasterDataid = objMultipleMasterData.MultipleMasterDataid;
                    objMultipleData.Chassi = Convert.ToString(data.ImportChildData[i].chassi);
                    objMultipleData.Placa = Convert.ToString(data.ImportChildData[i].placa);

                    objMultipleData.CPF = Convert.ToString(data.ImportChildData[i].CPF);
                    objMultipleData.Nome = Convert.ToString(data.ImportChildData[i].Nome);
                    objMultipleData.DataHabilitacao = data.ImportChildData[i].DataHabilitacao;
                    objMultipleData.DataNascimento = data.ImportChildData[i].DataNascimento;

                    objMultipleData.MunicipioNascimento = Convert.ToString(data.ImportChildData[i].MunicipioNascimento);
                    objMultipleData.NumCedulaEspelho = Convert.ToString(data.ImportChildData[i].NumCedulaEspelho);
                    objMultipleData.NumRegistro = Convert.ToString(data.ImportChildData[i].NumRegistro);
                    objMultipleData.NumRenach = Convert.ToString(data.ImportChildData[i].NumRenach);
                    objMultipleData.RG = Convert.ToString(data.ImportChildData[i].RG);
                    objMultipleData.UF = Convert.ToString(data.ImportChildData[i].UF);
                    objMultipleData.ValidadeCNH = data.ImportChildData[i].ValidadeCNH;

                    _context.MultipleData.Add(objMultipleData);
                    _context.SaveChanges();
                }
            }

            return true;
        }

        public bool RetryProcess(string sid)
        {
            int id = 0;
            int.TryParse(sid, out id);
            if (id > 0)
            {
                var result = _context.MultipleMasterData.SingleOrDefault(b => b.MultipleMasterDataid == id);
                if (result != null)
                {
                    result.IsProceedDone = false;
                    result.IsRetryProcess = true;
                    _context.SaveChanges();

                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public List<MultipleData> GetMultipleData(string MultipleMasterDataid)
        {
            int iMultipleMasterDataid = 0;
            int.TryParse(MultipleMasterDataid, out iMultipleMasterDataid);

            return _context.MultipleData.Where(i => i.MultipleMasterDataid == iMultipleMasterDataid).ToList();
        }
    }
}
