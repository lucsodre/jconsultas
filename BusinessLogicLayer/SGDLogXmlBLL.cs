﻿using System;
using System.Collections.Generic;
using System.Text;
using EntityLayer.Models;
using System.Net;
using System.Linq;
using System.Xml;

namespace BusinessLogicLayer
{

    public class SGDLogXmlBLL
    {
        private readonly SGDExpressContext _context;

        public SGDLogXmlBLL(SGDExpressContext context)
        {
            _context = context;
        }


        public string  GetxmlId(Int64 QId)
        {
            try
            {
                var lstRecord = (from a in _context.TB_LogXmlOxn where a.ID_Consulta == QId select a).FirstOrDefault< TB_LogXmlOxn>();
               

                if (lstRecord != null)
                {
                    return lstRecord.XmlRetorno;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}
