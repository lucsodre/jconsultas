﻿using System;
using System.Collections.Generic;
using System.Text;
using EntityLayer.Models;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogicLayer.Helpers;

namespace BusinessLogicLayer
{
    public class MultipleDocs
    {
        public List<string> DocumentCodes { get; set; }
    }

    public class MultipleDocSearchBLL
    {
        private readonly JConsultasContext _context;

        public MultipleDocSearchBLL(JConsultasContext context)
        {
            _context = context;
        }

        public bool SaveMultipledata(ImpoerExcel data, string UserID, ConsultaAvancadaVeiculo consultaAvancada)
        {
            // Insert into master data
            MultipleMasterData objMultipleMasterData = new MultipleMasterData();
            objMultipleMasterData.CreatedBy = DateTime.Now;
            objMultipleMasterData.FileName = data.ImportMasterData.FileName.ToString();
            objMultipleMasterData.TotalFile = data.ImportMasterData.TotalFile;
            objMultipleMasterData.TotalProceed = 0;
            objMultipleMasterData.TotlaFailed = 0;
            objMultipleMasterData.IsRetryProcess = false;
            objMultipleMasterData.Id = UserID;
            objMultipleMasterData.IsProceedDone = false;
            objMultipleMasterData.TipoDeConsulta = 1;
            objMultipleMasterData.ConsultaSolicitada = SerializadorXML.Serializar(consultaAvancada);

            _context.MultipleMasterData.Add(objMultipleMasterData);
            _context.SaveChanges();

            if (objMultipleMasterData.MultipleMasterDataid > 0)
            {
                for (int i = 0; i < data.ImportChildData.Count; i++)
                {
                    MultipleData objMultipleData = new MultipleData();
                    objMultipleData.MultipleMasterDataid = objMultipleMasterData.MultipleMasterDataid;
                    objMultipleData.Chassi = Convert.ToString(data.ImportChildData[i].chassi);
                    objMultipleData.Placa = Convert.ToString(data.ImportChildData[i].placa);

                    objMultipleData.CPF = Convert.ToString(data.ImportChildData[i].CPF);
                    objMultipleData.Nome = Convert.ToString(data.ImportChildData[i].Nome);
                    objMultipleData.DataHabilitacao = data.ImportChildData[i].DataHabilitacao;
                    objMultipleData.DataNascimento = data.ImportChildData[i].DataNascimento;

                    objMultipleData.MunicipioNascimento = Convert.ToString(data.ImportChildData[i].MunicipioNascimento);
                    objMultipleData.NumCedulaEspelho = Convert.ToString(data.ImportChildData[i].NumCedulaEspelho);
                    objMultipleData.NumRegistro = Convert.ToString(data.ImportChildData[i].NumRegistro);
                    objMultipleData.NumRenach = Convert.ToString(data.ImportChildData[i].NumRenach);
                    objMultipleData.RG = Convert.ToString(data.ImportChildData[i].RG);
                    objMultipleData.UF = Convert.ToString(data.ImportChildData[i].UF);
                    objMultipleData.ValidadeCNH = data.ImportChildData[i].ValidadeCNH;

                    _context.MultipleData.Add(objMultipleData);
                    _context.SaveChanges();
                }
            }

            return true;
        }

        public bool SaveMultipleRecord(MultipleDocs data, string UserID, ConsultaAvancadaVeiculo consultaAvancada)
        {
            string FileName = "";
            if (data.DocumentCodes.Count > 1)
                FileName = data.DocumentCodes[0].ToString() + " -" + data.DocumentCodes[data.DocumentCodes.Count - 1].ToString();
            else
                FileName = data.DocumentCodes[0].ToString();

            MultipleMasterData objMultipleMasterData = new MultipleMasterData();
            objMultipleMasterData.CreatedBy = DateTime.Now;
            objMultipleMasterData.FileName = FileName;
            objMultipleMasterData.TotalFile = data.DocumentCodes.Count;
            objMultipleMasterData.TotalProceed = 0;
            objMultipleMasterData.TotlaFailed = 0;
            objMultipleMasterData.Id = UserID;
            objMultipleMasterData.IsProceedDone = false;
            objMultipleMasterData.IsRetryProcess = false;
            objMultipleMasterData.TipoDeConsulta = 1;
            objMultipleMasterData.ConsultaSolicitada = SerializadorXML.Serializar(consultaAvancada);

            _context.MultipleMasterData.Add(objMultipleMasterData);
            _context.SaveChanges();

            if (objMultipleMasterData.MultipleMasterDataid > 0)
            {
                for (int i = 0; i < data.DocumentCodes.Count; i++)
                {
                    data.DocumentCodes[i] = data.DocumentCodes[i].Trim();
                    string chassi = "", placa = "";
                    if (data.DocumentCodes[i].Length == 7)
                        chassi = data.DocumentCodes[i].ToString();
                    if (data.DocumentCodes[i].Length == 17)
                        placa = data.DocumentCodes[i].ToString();

                    MultipleData objMultipleData = new MultipleData();
                    objMultipleData.MultipleMasterDataid = objMultipleMasterData.MultipleMasterDataid;
                    objMultipleData.Chassi = Convert.ToString(chassi);
                    objMultipleData.Placa = Convert.ToString(placa);

                    _context.MultipleData.Add(objMultipleData);
                    _context.SaveChanges();
                }
            }

            return true;
        }

        public bool RetryProcess(string sid)
        {
            int id = 0;
            int.TryParse(sid, out id);
            if (id > 0)
            {
                var result = _context.MultipleMasterData.SingleOrDefault(b => b.MultipleMasterDataid == id);
                if (result != null)
                {
                    result.IsProceedDone = false;
                    result.IsRetryProcess = true;
                    _context.SaveChanges();

                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public List<MultipleData> GetMultipleData(string MultipleMasterDataid)
        {
            int iMultipleMasterDataid = 0;
            int.TryParse(MultipleMasterDataid, out iMultipleMasterDataid);

            return _context.MultipleData.Where(i => i.MultipleMasterDataid == iMultipleMasterDataid).ToList();
        }
    }
}




//public async Task<dynamic> GetDocumentFromService(string sDocumentCode, string sid, string apiUsuario, string apiSenha, string apiCodigo)
//{
//    try
//    {
//        string placa = "", chassi = null;

//        if (sDocumentCode.Length <= 7)
//            placa = sDocumentCode.Trim();
//        else
//            chassi = sDocumentCode.Trim();

//        /*var res = (from u in _context.UserMaster
//                   join a in _context.AccountMaster on u.AccountId equals a.AccountId
//                   where u.Id == sid
//                   select new
//                   {
//                       u.ApiSenha
//                       ,
//                       u.ApiUsuario
//                       ,
//                       a.ApiCode
//                   }).ToList(); 

//        if (res != null && res.Count > 0)
//        {
//            usuario = res[0].ApiUsuario.ToString();
//            senha = res[0].ApiSenha.ToString();
//            Int64.TryParse(res[0].ApiCode.ToString(), out codigo);
//        }*/
//        Int64 codigo = 0;
//        Int64.TryParse(apiCodigo, out codigo);

//        ConsultasStringService.ConsultasStringSoapClient c = new ConsultasStringService.ConsultasStringSoapClient(new ConsultasStringService.ConsultasStringSoapClient.EndpointConfiguration());
//        ConsultasStringService.ConsultaVeiculoNovoResponse r = new ConsultasStringService.ConsultaVeiculoNovoResponse();

//        //var task = c.ConsultaVeiculoNovoAsync(placa, chassi, codigo, apiUsuario.Trim(), apiSenha.Trim(), 1, "");
//        var task = c.ConsultaVeiculoNovoAsync(placa, chassi, codigo, apiUsuario.Trim(), apiSenha.Trim(), 5, "N");
//        //r = c.ConsultaVeiculoNovoAsync(placa, chassi, 70911, "india", "123mudar", 1, null).Result;

//        // return null;

//        r = await task;

//        //Old Method
//        //string sXML = r.Body.ConsultaVeiculoResult.ToString();
//        //New Method
//        string sXML = r.Body.ConsultaVeiculoNovoResult.ToString();

//        // Now Insert Into database 
//        //QueryResult objQueryResult = new QueryResult();
//        //objQueryResult.DocumentCode = sDocumentCode;
//        //objQueryResult.CreatedAt = DateTime.Now;
//        //objQueryResult.Id = sid;
//        //objQueryResult.Xml = sXML;

//        //_context.QueryResult.Add(objQueryResult);
//        //_context.SaveChanges();

//        return sXML;

//    }
//    catch (Exception ex)
//    {
//        throw new Exception(ex.ToString());
//    }
//}