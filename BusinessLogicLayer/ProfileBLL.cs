﻿using EntityLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer
{
    public class ProfileBLL
    {
        private readonly JConsultasContext _context;

        public ProfileBLL(JConsultasContext context)
        {
            _context = context;
        }

        public IEnumerable<ProfileMaster> GetUserProfileList()
        {
            return _context.ProfileMaster;
        }

        public IEnumerable<UserProfileList> GetUserProfileListOnProfile(int iProfileID)
        {
            var res = (from u in _context.ProfileMaster
                       join a in _context.UserMaster on u.ProfileMasterId equals a.ProfileMasterId
                       where a.ProfileId == iProfileID
                       select new UserProfileList
                       {
                           ProfileName = u.ProfileName
                           ,
                           UserMaster = (bool)u.UserMaster
                           ,
                           Account = (bool)u.Account
                           ,
                           SingleSearch = (bool)u.SingleSearch
                           ,
                           BatchSearch = (bool)u.BatchSearch
                           ,
                           ConsultaMotorista = (u.ConsultaMotorista != null) ? (bool)u.ConsultaMotorista :  false,
                           ConsultaMotoristaLote = (u.ConsultaMotoristaLote != null) ? (bool)u.ConsultaMotoristaLote : false,
                           UserName = a.Name + ' ' + a.LastName
                       });

            return res;
        }

        public List<ProfileMaster> GetAuditorias(List<ProfileMaster> profiles)
        {
            foreach (var item in profiles)
            {
                item.Auditorias = new List<Auditoria>();
                item.Auditorias = (from p in _context.Auditoria
                                   join u in _context.UserMaster on p.Usuario equals u.Id
                                   where p.NomeTabela == "ProfileMaster" && p.IdRegistro == item.ProfileMasterId
                                   orderby p.DataHora descending
                                   select new Auditoria
                                   {
                                       DataHora = p.DataHora,
                                       Descricao = p.Descricao,
                                       Usuario = u.Name
                                   }).Take(5).ToList();
            }
            return profiles;
        }

        public bool AddUpdateUserProfile(ProfileMaster objprofilemaster)
        {

            if (string.IsNullOrEmpty(Convert.ToString(objprofilemaster.Account)))
                objprofilemaster.Account = false;
            if (string.IsNullOrEmpty(Convert.ToString(objprofilemaster.SingleSearch)))
                objprofilemaster.SingleSearch = false;
            if (string.IsNullOrEmpty(Convert.ToString(objprofilemaster.BatchSearch)))
                objprofilemaster.BatchSearch = false;


            if (objprofilemaster.ProfileMasterId == 0)
            {
                _context.ProfileMaster.Add(objprofilemaster);
                _context.SaveChanges();
            }
            else
            {
                //_context.ProfileMaster.Attach(objprofilemaster);
                //_context.Entry(objprofilemaster).State = EntityState.Modified;
                //_context.SaveChanges();

                var profileMaster = _context.ProfileMaster.FirstOrDefault(x => x.ProfileMasterId == objprofilemaster.ProfileMasterId);
                if (profileMaster != null)
                {
                    profileMaster.Account = objprofilemaster.Account;
                    profileMaster.BatchSearch = objprofilemaster.BatchSearch;
                    profileMaster.CreatedAt = objprofilemaster.CreatedAt;
                    profileMaster.CreatedBy = objprofilemaster.CreatedBy;
                    profileMaster.IsActive = objprofilemaster.IsActive;
                    profileMaster.ModifiedAt = objprofilemaster.ModifiedAt;
                    profileMaster.ModifiedBy = objprofilemaster.ModifiedBy;
                    profileMaster.ProfileName = objprofilemaster.ProfileName;
                    profileMaster.SingleSearch = objprofilemaster.SingleSearch;
                    profileMaster.UserMaster = objprofilemaster.UserMaster;
                    profileMaster.ConsultaMotorista = objprofilemaster.ConsultaMotorista;
                    profileMaster.ConsultaMotoristaLote = objprofilemaster.ConsultaMotoristaLote;
                    profileMaster.UsuarioAlterando = objprofilemaster.ModifiedBy == null ? objprofilemaster.CreatedBy : objprofilemaster.ModifiedBy;
                    _context.SaveChanges();
                }
                else
                {
                    _context.ProfileMaster.Attach(objprofilemaster);
                    _context.Entry(objprofilemaster).State = EntityState.Modified;
                    _context.SaveChanges();
                }
            }

            return true;
        }
    }

    public class UserProfileList
    {
        public string ProfileName { get; set; }
        public bool UserMaster { get; set; }
        public bool Account { get; set; }
        public bool SingleSearch { get; set; }
        public bool BatchSearch { get; set; }
        public bool ConsultaMotorista { get; set; }
        public bool ConsultaMotoristaLote { get; set; }
        public string UserName { get; set; }
    }
}
