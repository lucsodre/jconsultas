using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using BusinessLogicLayer;
using JConsultas.Models;
using System.Xml.Serialization;
using System.IO;
using System.IO.Compression;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using Microsoft.AspNetCore.Hosting;
using Amazon.S3;
using Amazon.S3.Transfer;
using EntityLayer.Models;
using Microsoft.Extensions.Options;
using BusinessLogicLayer.Helpers;

namespace JConsultas.Controllers
{
    [Produces("application/json")]
    [Route("api/VeiculoAPI")]
    public class VeiculoAPIController : Controller
    {
        private readonly SGDLogXmlBLL _SGDLogXmlBLL;
        private IHostingEnvironment _env;
        private static IOptions<ApplicationSettings> _settings;
        public VeiculoAPIController(IHostingEnvironment env, EntityLayer.Models.SGDExpressContext context, IOptions<ApplicationSettings> settings)
        {
            _SGDLogXmlBLL = new SGDLogXmlBLL(context);
            this._env = env; _settings = settings;
            _settings = settings;
        }
        // GET: api/VeiculoAPI
        [HttpGet]
        [Route("Get")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1Get", "value2Get" };
        }

        [HttpGet]
        [Route("ImpressaoUltimaConsulta")]
        public dynamic GetImpressaoUltimaConsulta(string strOrgID, List<String> listaId)
        {
            try
            {
                if (!(String.IsNullOrEmpty(strOrgID)) && listaId.Count>0)
                {
                    Document document = new Document(PageSize.A4);
                    document.Open();
                    string dirZip = _env.WebRootPath + $@"\uploadpdf\" + DateTime.Now.Ticks.ToString();
                    string dir = dirZip + "\\" + strOrgID;

                    string pdfName;
                    string filepath;
                    string strXml;
                    CONSULTA2300 objCONSULTA2300;
                    string sData;
                    Helper.GravarLog("VeiculoAPIController -> GetImpressaoUltimaConsulta()" + strOrgID + listaId.ToString(), _env.WebRootPath + @"\ErrorLog");
                    foreach (string itemListaId in listaId)
                    {
                        try
                        {
                            strXml = _SGDLogXmlBLL.GetxmlId(Convert.ToInt64(itemListaId));
                            objCONSULTA2300 = DeserializarXMLRetorno(strXml);
                            sData = UtilPDF.GenerateHtmlViewPDF(objCONSULTA2300, _settings.Value.sSiteURL);
                            pdfName = objCONSULTA2300.CONSULTAS.CONSULTA.VEICULO.PLACA + "_" + DateTime.Now.Ticks.ToString() + ".pdf";
                            filepath = dir + "\\" + pdfName;

                            if (!System.IO.Directory.Exists(dir))
                                System.IO.Directory.CreateDirectory(dir);

                            using (FileStream fs = new FileStream(filepath, FileMode.Create))
                            {
                                PdfWriter pdfWriter = PdfWriter.GetInstance(document, fs);
                                pdfWriter.PageEvent = new PdfEventHandler();

                                using (StringReader stringReader = new StringReader(sData))
                                {
                                    StyleSheet styles = new StyleSheet();
                                    styles.LoadTagStyle("th", "size", "10px");
                                    styles.LoadTagStyle("th", "face", "helvetica");
                                    styles.LoadTagStyle("td", "size", "8px");
                                    styles.LoadTagStyle("td", "face", "helvetica");

                                    System.Collections.ArrayList parsedList = iTextSharp.text.html.simpleparser.HtmlWorker.ParseToList(stringReader, styles);
                                    document.Open();
                                    foreach (object item in parsedList)
                                    {
                                        document.Add((IElement)item);
                                    }
                                    document.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                            Helper.GravarLog("VeiculoAPIController -> GetImpressaoUltimaConsulta()" + ex.ToString() + itemListaId, _env.WebRootPath + @"\ErrorLog");
                        }

                    }

                    Helper.GravarLog("VeiculoAPIController -> GetImpressaoUltimaConsulta() Zipando", _env.WebRootPath + @"\ErrorLog");
                    ZipFile.CreateFromDirectory(dir, dirZip + "\\arquivo.zip");

                    AmazonS3Client client = new AmazonS3Client(_settings.Value.AWSAccessKey, _settings.Value.AWSSecretKey
                           , Amazon.RegionEndpoint.SAEast1);

                    System.IO.FileStream stream = System.IO.File.OpenRead(dirZip + "\\arquivo.zip");
                    System.IO.MemoryStream data = new System.IO.MemoryStream();
                    stream.CopyTo(data);
                    byte[] buf = new byte[data.Length];
                    data.Read(buf, 0, buf.Length);

                    TransferUtility fileTransferUtility = new
                       TransferUtility(client);
                    string caminhoAws = "pdfconsultaveiculo/" + strOrgID + "/" + DateTime.Now.Ticks.ToString() + ".zip";
                    Helper.GravarLog("VeiculoAPIController -> GetImpressaoUltimaConsulta() Enviando para AWS", _env.WebRootPath + @"\ErrorLog");
                    fileTransferUtility.Upload(data, "frotacontrol", caminhoAws);



                    return new { status = "Sucesso", filepathAWS = caminhoAws };
                }else
                    return new { status = "Erro", Erro = "Parametros invalidos" };
            }
            catch (Exception ex)
            {
                Helper.GravarLog("VeiculoAPIController -> GetImpressaoUltimaConsulta()" + ex.ToString(), _env.WebRootPath + @"\ErrorLog");

                return new { status = "Erro", Erro= ex.ToString() }; 
            }
        }

        // GET: api/VeiculoAPI/5
        [HttpGet("{id}")]
        [Route("Get/{id}")]
        public string Get(int id)
        {
            return "valueGetid";
        }
        
        // POST: api/VeiculoAPI
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/VeiculoAPI/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        public CONSULTA2300 DeserializarXMLRetorno(string xml)
        {
            try
            {
                CONSULTA2300 retorno = null;
                XmlSerializer serializer = new XmlSerializer(typeof(CONSULTA2300));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                retorno = (CONSULTA2300)serializer.Deserialize(reader);
                //reader.Close();
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }

        private void clearFolder(string FolderName)
        {
            DirectoryInfo dir = new DirectoryInfo(FolderName);

            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.Delete();
            }

            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                clearFolder(di.FullName);
                di.Delete();
            }
        }
    }
}
