using BusinessLogicLayer;
using EntityLayer.Models;
using JConsultas.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Xml.Serialization;

namespace JConsultas.Controllers
{

    public class ListaCNHConsultadasController : Controller
    {
        private readonly ListaConsultaCNHBLL _ListaConsultaCNHBLL;
        private readonly ConsultaCNHBLL _ConsultaLoteCNHBLL;
        private readonly UserBLL _UserBLL;
        private IHostingEnvironment _env;
        private JConsultasContext _context;

        public ListaCNHConsultadasController(IHostingEnvironment env, EntityLayer.Models.JConsultasContext context)
        {
            _ListaConsultaCNHBLL = new ListaConsultaCNHBLL(context);
            _UserBLL = new UserBLL(context);
            _ConsultaLoteCNHBLL = new ConsultaCNHBLL(context);
            this._env = env;
            this._context = context;
        }

       
        [HttpGet]
        public dynamic GetUserList()
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                IEnumerable<UserList> objUserList = _UserBLL.GetUserList(userId.ToString());

                return objUserList;
            }
            catch (Exception ex)
            {
                Helper.LogError("ListaCNHConsultadasController.CS -> GetUserList()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }
        }


        [HttpPost]
        public dynamic GetListOfSearchedDocs([FromBody]SearchDocs data)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                IEnumerable<dynamic> queryResult;

                if (data.ConsultaID != string.Empty)
                {
                    queryResult = _ListaConsultaCNHBLL.GetListOfSearchedDocs(data.Id, data.DocumentCode, data.StartDate, data.EndDate, userId, data.AccountID, data.ConsultaID);
                    return queryResult;
                }

                queryResult = _ListaConsultaCNHBLL.GetListOfSearchedDocs(data.Id, data.DocumentCode, data.StartDate, data.EndDate, userId, data.AccountID);
                return queryResult;
            }
            catch (Exception ex)
            {
                Helper.LogError("ListaCNHConsultadasController.CS -> GetListOfSearchedDocs()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }
        }

        [HttpPost]
        public dynamic GetListOfMultipleSearchedDocs([FromBody]SearchMultipleDocs data)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                bool? IsProcessed = null;
                if (data.Status == "true")
                    IsProcessed = true;
                if (data.Status == "false")
                    IsProcessed = false;

                var queryResult = _ListaConsultaCNHBLL.GetListOfMultipleSearchedDocs(data.Id, data.StartDate, data.EndDate, IsProcessed, userId, data.AccountID);
                return queryResult;
            }
            catch (Exception ex)
            {
                Helper.LogError("ListaCNHConsultadasController.CS -> GetListOfMultipleSearchedDocs()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }
        }

        [HttpPost]
        public dynamic GetListOfMultipleSearchedPendingDocs([FromBody]SearchMultipleDocs data)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                bool? IsProcessed = null;
                if (data.Status == "true")
                    IsProcessed = true;
                if (data.Status == "false")
                    IsProcessed = false;

                var queryResult = _ListaConsultaCNHBLL.GetListOfMultipleSearchedPendingDocs(data.Id, data.StartDate, data.EndDate, IsProcessed);
                return queryResult;
            }
            catch (Exception ex)
            {
                Helper.LogError("ListaCNHConsultadasController.CS -> GetListOfMultipleSearchedDocs()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }
        }

        [HttpPost]
        public dynamic GetLastSearchedDocs()
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                var queryResult = _ListaConsultaCNHBLL.GetLastSearchedDocs(userId);
                return queryResult;
            }
            catch (Exception ex)
            {
                Helper.LogError("ListaCNHConsultadasController.CS -> GetLastSearchedDocs()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }
        }

        [HttpPost]
        public dynamic GetDataOnMultipleData([FromBody]MultipleSearch data)
        {
            try
            {
                return _ListaConsultaCNHBLL.GetDataOnMultipleData(data);
            }
            catch (Exception ex)
            {

                Helper.LogError("ListaCNHConsultadasController.CS -> GetDataOnMultipleData()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }
        }

        [HttpPost]
        public dynamic GetMultipleFile([FromBody]MultipleDocumentCode data)
        {
            try
            {
                for (int i = 0; i < data.QueryResultid.Length; i++)
                {
                    string pdfName = "ConsultaMotorista_" + data.QueryResultid[i] + ".pdf";
                    string filepath = _env.WebRootPath + $@"\uploadpdf\" + pdfName;
                    string dir = _env.WebRootPath + $@"\uploadpdf";

                    bool exists = System.IO.File.Exists(filepath);
                    if (!exists)
                    {
                        int id = 0;
                        int.TryParse(Convert.ToString(data.QueryResultid[i]), out id);
                        DetalheConsultaMotoristaController objdoc = new DetalheConsultaMotoristaController(this._env, _context);
                        bool s = objdoc.CreatePdfOnId(id);
                    }
                }

                string root = _env.WebRootPath + $@"\uploadpdf\Receipt_636300200830409088.pdf";
                string zipName = "ConsultaMotorista_Lote_" + DateTime.Now.Ticks.ToString() + ".zip";
                string zipPath = _env.WebRootPath + $@"\uploadpdf\" + zipName;

                using (var memoryStream = new MemoryStream())
                {
                    using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        for (int i = 0; i < data.QueryResultid.Length; i++)
                        { 
                            string pdfName = "ConsultaMotorista_" + data.QueryResultid[i] + ".pdf";
                            string filepath = _env.WebRootPath + $@"\uploadpdf\" + pdfName;
                            string dir = _env.WebRootPath + $@"\uploadpdf";

                            bool exists = System.IO.File.Exists(filepath);
                            if (exists)
                            {
                                ziparchive.CreateEntryFromFile(filepath, pdfName);
                            }
                        }
                    }

                    using (var fileStream = new FileStream(zipPath, FileMode.Create))
                    {
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        memoryStream.CopyTo(fileStream);
                    }
                }

                return new { status = 1, filename = zipName, filepath = _env.WebRootPath + $@"\uploadpdf\" + zipName, fullFilePath = $@"..\..\uploadpdf\" + zipName };
            }
            catch (Exception ex)
            {
                return new { status = 0, message = ex.Message };
            }
        }


        public static CONSULTA1650 DeserializarXMLRetorno(string xml)
        {
            try
            {
                CONSULTA1650 retorno = null;
                XmlSerializer serializer = new XmlSerializer(typeof(CONSULTA1650));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                retorno = (CONSULTA1650)serializer.Deserialize(reader);
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }

        [HttpPost]
        public dynamic GetMultipleExcelFile([FromBody]MultipleDocumentCode data)
        {
            try
            {
                string sFileName = "";
                DocumentXMLResponselst _DocumentXMLResponselst = new DocumentXMLResponselst();
                List<DocumentXMLResponse> _lstDocumentXMLResponse = new List<DocumentXMLResponse>();

                for (int i = 0; i < data.QueryResultid.Length; i++)
                {
                    string DocumentCode = _ListaConsultaCNHBLL.GetCodeOnId(data.QueryResultid[i].ToString());

                    DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();
                    _DocumentXMLResponse = _ConsultaLoteCNHBLL.GetDocumentFromDB(DocumentCode);
                    _lstDocumentXMLResponse.Add(_DocumentXMLResponse);
                }

                if (_lstDocumentXMLResponse.Count > 0)
                {
                    sFileName = "Excel_" + DateTime.Now.ToString("dd_MM_yyyy_H_mm_ss") + ".xlsx";
                    string sWebRootFolder = _env.WebRootPath + $@"\uploadExcel\";
                    string dir = _env.WebRootPath + $@"\uploadExcel";

                    string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
                    FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

                    if (file.Exists)
                    {
                        file.Delete();
                        file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                    }

                    List<ExcelWorksheet> works = new List<ExcelWorksheet>();
                    bool cabecalhoCriado = false;

                    using (ExcelPackage package = new ExcelPackage(file))
                    {
                        for (int v = 0; v < _lstDocumentXMLResponse.Count; v++)
                        {
                            var r = _lstDocumentXMLResponse[v].xml != null ? DeserializarXMLRetorno(_lstDocumentXMLResponse[v].xml) : null;

                            RESULTADO resultado = new RESULTADO();

                            if (r != null)
                            {
                                if (r.CONSULTAS != null)
                                {
                                    resultado = r.CONSULTAS.CONSULTA.RESULTADO;
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            else
                            {
                                continue;
                            }

                            if (!cabecalhoCriado)
                            {
                                PropertyInfo[] properties = resultado.GetType().GetProperties();

                                foreach (var propertyInfo in properties)
                                {
                                    var sheet = package.Workbook.Worksheets.Add(propertyInfo.Name);
                                    works.Add(sheet);

                                    var detalhe = propertyInfo.PropertyType.GetProperties();

                                    int coluna = 1;

                                    foreach (var prop in detalhe)
                                    {
                                        int colunaLista = 1;

                                        if (prop.PropertyType.Name.Contains("List"))
                                        {
                                            if (prop.Name == "RECURSO")
                                            {
                                                var propertyRecursos = GetProperties("JConsultas.Models", prop.Name);
                                                foreach (var item in propertyRecursos)
                                                {
                                                    var a = GetProperties("JConsultas.Models", item.Name);
                                                    foreach (var b in a)
                                                    {
                                                        if (b.Name == "EVENTO")
                                                        {
                                                            var c = GetProperties("JConsultas.Models", "EVENTO");
                                                            foreach (var d in c)
                                                            {
                                                                sheet.Cells[1, colunaLista].Value = "EVENTO_" + d.Name;
                                                                colunaLista++;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            sheet.Cells[1, colunaLista].Value = item.Name + "_" + b.Name;
                                                            colunaLista++;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var propertyInfos = GetProperties("JConsultas.Models", prop.Name);
                                                foreach (var item in propertyInfos)
                                                {
                                                    sheet.Cells[1, colunaLista].Value = item.Name;
                                                    colunaLista++;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            sheet.Cells[1, coluna].Value = prop.Name;
                                            coluna++;
                                        }
                                    }
                                }
                                cabecalhoCriado = true;
                            }

                            var planilha = works.Find(x => x.Name == "INFRACOES");
                            PreenchePlanilha<INFRACAO>(resultado.INFRACOES.INFRACAO, planilha);

                            planilha = works.Find(x => x.Name == "EXAMES");
                            PreenchePlanilha<EXAME>(resultado.EXAMES.EXAME, planilha);

                            planilha = works.Find(x => x.Name == "CURSOS");
                            PreenchePlanilha<CURSO>(resultado.CURSOS.CURSO, planilha);

                            planilha = works.Find(x => x.Name == "BLOQUEIOS");
                            PreenchePlanilha<BLOQUEIO>(resultado.BLOQUEIOS.BLOQUEIO, planilha);

                            planilha = works.Find(x => x.Name == "DADOSCONDUTOR");
                            PreenchePlanilha<DADOSCONDUTOR>(resultado.DADOSCONDUTOR, planilha);

                            planilha = works.Find(x => x.Name == "DADOSCNH");
                            PreenchePlanilha<DADOSCNH>(resultado.DADOSCNH, planilha);

                            planilha = works.Find(x => x.Name == "PONTUACAO");
                            PreenchePlanilha<PONTUACAO>(resultado.PONTUACAO, planilha);

                            planilha = works.Find(x => x.Name == "RECURSOS");
                            PreencheRecursoPlanilha(resultado.RECURSOS?.RECURSO, planilha);

                            package.Save();
                        }
                    }
                }

                return new { status = 1, filename = sFileName, filepath = _env.WebRootPath + $@"\uploadExcel\" + sFileName, fullFilePath = $@"..\..\uploadExcel\" + sFileName };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = 0,
                    Msg = ex.Message.ToString()
                };
            }
        }

        public static void PreenchePlanilha<T>(List<T> resultado, ExcelWorksheet planilha)
        {
            if (resultado != null)
            {
                int linha = 2;
                foreach (var objetos in resultado)
                {
                    var propInfracao = objetos.GetType().GetProperties();
                    int col = 1;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(objetos, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }
                    linha++;
                }
            }
        }

        public static void PreenchePlanilha<T>(T resultado, ExcelWorksheet planilha)
        {
            if (resultado != null)
            {
                int linha = 2;

                var propInfracao = resultado.GetType().GetProperties();
                int col = 1;
                foreach (var item in propInfracao)
                {
                    var val = GetPropValue(resultado, item.Name);
                    if (val != null)
                    {
                        planilha.Cells[linha, col].Value = val.ToString();
                    }
                    col++;
                }
                linha++;
            }
        }

        public static void PreencheRecursoPlanilha(List<RECURSO> resultado, ExcelWorksheet planilha)
        {
            if (resultado != null)
            {
                int linha = 2;
                foreach (var objetos in resultado)
                {
                    var veic = objetos.VEICULO;
                    var infracao = objetos.INFRACAO_RECURSO;
                    var dados = objetos.DADOS_PROCESSO;
                    var eventos = objetos.EVENTOS?.EVENTO;

                    var propInfracao = veic.GetType().GetProperties();
                    int col = 1;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(veic, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }

                    propInfracao = infracao.GetType().GetProperties();
                    col = 5;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(infracao, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }

                    propInfracao = dados.GetType().GetProperties();
                    col = 12;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(dados, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }


                    if (eventos != null)
                    {
                        foreach (var ev in eventos)
                        {
                            propInfracao = ev.GetType().GetProperties();
                            col = 18;
                            foreach (var item in propInfracao)
                            {
                                var val = GetPropValue(ev, item.Name);
                                if (val != null)
                                {
                                    planilha.Cells[linha, col].Value = val.ToString();
                                }
                                col++;
                            }
                            linha++;
                        }
                    }

                    linha++;
                }
            }
        }

        public static object GetPropValue(object source, string propertyName)
        {
            var property = source.GetType().GetRuntimeProperties().FirstOrDefault(p => string.Equals(p.Name, propertyName, StringComparison.OrdinalIgnoreCase));
            if (property != null)
            {
                return property.GetValue(source);
            }
            return null;
        }

        private static PropertyInfo[] GetProperties(string nameSpace, string className)
        {
            return Type.GetType(nameSpace + "." + className).GetProperties();
        }
    }
}