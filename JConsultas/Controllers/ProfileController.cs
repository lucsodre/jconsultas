using BusinessLogicLayer;
using EntityLayer.Models;
using JConsultas.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace JConsultas.Controllers
{
    [Produces("application/json")]
    //[Route("api/Profile")]
    public class ProfileController : Controller
    {
        private readonly ProfileBLL _profilebll;
        private IHostingEnvironment _env;
        public ProfileController(IHostingEnvironment env, EntityLayer.Models.JConsultasContext context)
        {
            this._env = env;
            _profilebll = new ProfileBLL(context);
        }

        [HttpGet]
        public IEnumerable<ProfileMaster> GetUserProfileList()
        {
            try
            {
                var r = _profilebll.GetUserProfileList();
                r = _profilebll.GetAuditorias(r.ToList());
                return r;
            }
            catch (Exception ex)
            {
                Helper.LogError("ProfileController.CS -> GetUserProfileList()", ex.Message, _env);
                return null;
            }
        }

        [HttpPost]
        public bool AddUpdateUserProfile([FromBody]ProfileMaster data)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                if (string.IsNullOrEmpty(Convert.ToString(data.IsActive)))
                {
                    data.IsActive = false;
                }

                if (data.ProfileMasterId == 0)
                {
                    data.CreatedBy = userId;
                    data.CreatedAt = DateTime.Now;
                }
                else
                {
                    data.ModifiedAt = DateTime.Now;
                    data.ModifiedBy = userId;
                }

                return _profilebll.AddUpdateUserProfile(data);
            }
            catch (Exception ex)
            {
                Helper.LogError("ProfileController.CS -> AddUpdateUserProfile()", ex.Message, _env);
                return false;
            }
        }

    }
}