﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MailKit.Net.Smtp;
using MimeKit;
using MailKit.Security;
using Microsoft.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using EntityLayer.Models;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using System.Text;
using BusinessLogicLayer;
using System.Net;
using System.Security.Claims;
using JConsultas.Models;
using Amazon.SimpleNotificationService.Model;
using Amazon.SimpleNotificationService;

namespace JConsultas.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private IHostingEnvironment _env;
        private static IOptions<ApplicationSettings> _settings;

        static string sSMTPName = "";
        public HomeController(IHostingEnvironment env, SignInManager<ApplicationUser> signInManager, IOptions<ApplicationSettings> settings)
        {
            this._env = env;
            _signInManager = signInManager;
            _settings = settings;
            sSMTPName = settings.Value.SMTPName;
        }

        public async Task<IActionResult> Index()
        {
            //SMSHelper _onj = new SMSHelper();
            //PublishResponse response = await _onj.send("", "");

            //AmazonSimpleNotificationServiceClient smsClient = new AmazonSimpleNotificationServiceClient("AKIAJPIVQ35DJSVW5CCQ", "KMbfNmpHa1S/E5fGOgKqwlRHz5b/pqIPfO0Lvk+b", RegionEndpoint.USEast1);
            //PublishRequest publishRequest = new PublishRequest();
            //publishRequest.Message = "My SMS message";
            //publishRequest.PhoneNumber = "+5519998415094";

            //PublishResponse result = await smsClient.PublishAsync(publishRequest);

            //WebApplication1.Controllers.HomeController hm = new WebApplication1.Controllers.HomeController();
            //var res = hm.Send();

            //string root = _env.WebRootPath + $@"\uploadpdf\Receipt_636300200830409088.pdf";
            //string sSavePath = _env.WebRootPath + $@"\uploadpdf\test.zip";
            //using (var memoryStream = new MemoryStream())
            //{
            //    using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
            //    {
            //        //ziparchive.CreateEntryFromFile(@"D:\style.css", "style.css");
            //        ziparchive.CreateEntryFromFile(root, "Receipt.pdf");
            //    }
            //    //var file = File(memoryStream.ToArray(), "application/zip", "Attachments.zip");
            //    //using (var fileStream = new FileStream(@"D:\test.zip", FileMode.Create))
            //    using (var fileStream = new FileStream(sSavePath, FileMode.Create))
            //    {
            //        memoryStream.Seek(0, SeekOrigin.Begin);
            //        memoryStream.CopyTo(fileStream);
            //    }
            //}

            return View();
        }

        public IActionResult Account()
        {
            return View();
        }

        public IActionResult User()
        {
            return View();
        }

        public IActionResult AdminUser()
        {
            return View();
        }

        public IActionResult EditProfile()
        {
            return View();
        }

        public IActionResult UploadDocuments()
        {
            return View();
        }

        public IActionResult Profile()
        {
            return View();
        }

        public IActionResult SingleDocumentSearch()
        {
            return View();
        }

        public IActionResult ConsultaCNH()
        {
            return View();
        }

        public IActionResult ListOfSearchedDocs()
        {
            return View();
        }

        public IActionResult ListaCNHBuscadas()
        {
            return View();
        }

        public IActionResult MultipleDocSearch()
        {
            return View();
        }

        public IActionResult BuscaEmLoteCNH()
        {
            return View();
        }

        public IActionResult ListOfMultipleSearch()
        {
            return View();
        }

        public IActionResult ListaBuscaEmLoteCNH()
        {
            return View();
        }

        public IActionResult Help()
        {
            return View();
        }

        public IActionResult HelpMotorista()
        {
            return View();
        }

        public IActionResult DocDetailPage(int qid, string docid)
        {
            ViewBag.QId = qid;
            ViewBag.DocId = docid;
            return View();
        }

        public IActionResult DetalheConsultaMotorista(int qid, string docid)
        {
            ViewBag.QId = qid;
            ViewBag.DocId = docid;
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        //public string Send()
        //public static string Send(string to, string subject, string content)
        [HttpPost]
        public static bool Send(string to, string subject, string content)
        {
            try
            {
                string from = "";
                using (var client = new SmtpClient())
                {
                    MimeMessage mes = new MimeMessage();
                    mes.From.Add(new MailboxAddress(from));
                    mes.To.Add(new MailboxAddress(to));
                    mes.Subject = subject;
                    mes.Body = new TextPart("html")
                    {
                        Text = content
                    };

                    client.Connect(_settings.Value.SMTPName, _settings.Value.SMTPPortNo, SecureSocketOptions.StartTls);
                    client.Authenticate(_settings.Value.EmailUserName, _settings.Value.EmailUserPassword);
                    client.Send(mes);
                    client.Disconnect(true);
                }

                return true;
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message.ToString());
                return false;
            }
        }

        public static bool Send(string to, string subject, string content, IOptions<ApplicationSettings> settings)
        {
            try
            {
                string from = "";
                using (var client = new SmtpClient())
                {
                    MimeMessage mes = new MimeMessage();
                    mes.From.Add(new MailboxAddress(from));
                    mes.To.Add(new MailboxAddress(to));
                    mes.Subject = subject;
                    mes.Body = new TextPart("html")
                    {
                        Text = content
                    };

                    if (_settings == null)
                        _settings = settings;

                    client.Connect(_settings.Value.SMTPName, _settings.Value.SMTPPortNo, SecureSocketOptions.StartTls);
                    client.Authenticate(_settings.Value.EmailUserName, _settings.Value.EmailUserPassword);
                    client.Send(mes);
                    client.Disconnect(true);
                }

                return true;
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message.ToString());
                return false;
            }
        }

        public static void ErrorLog(string error)
        {
            //var rootPath = this._env.WebRootPath.ToString() + @"\ImportExcel\error.txt";
            try
            {
                var rootPath = _settings.Value.PathLogErro.ToString() + "\\ErrorLog_" + DateTime.Now.ToString("ddMMyyyyHHMMSs") + ".txt";
                //var rootPath =  @"D:\WEBSERVER\JConsultas\ErrorLog.txt";
                var logWriter = System.IO.File.CreateText(rootPath);
                logWriter.WriteLine(error);
                logWriter.Dispose();
            }
            catch { }
        }

        public ActionResult SingleSearchPdfDownload(string fileDownloadName, string filePath)
        {
            try
            {
                var bytes = System.IO.File.ReadAllBytes(filePath);
                return File(bytes, "application/octet-stream", fileDownloadName);
            }
            catch (Exception)
            {
                return null;
            }
        }
        //End

        [HttpPost]
        public bool UploadFilesAjax(string sImage)
        {
            try
            {
                // delete old image
                if (!string.IsNullOrEmpty(sImage))
                {
                    var deleteFileName = _env.WebRootPath + $@"\ProfilePhoto\{sImage}";
                    if (System.IO.File.Exists(deleteFileName))
                    {
                        try
                        {
                            System.IO.File.Delete(deleteFileName);
                        }
                        catch { }
                    }
                }

                long size = 0;
                var files = Request.Form.Files;
                foreach (var file in files)
                {
                    var filename = ContentDispositionHeaderValue
                                    .Parse(file.ContentDisposition)
                                    .FileName
                                    .Trim('"');

                    var FileName = Path.GetFileName(file.FileName);
                    string extension = Path.GetExtension(FileName).ToUpper();

                    //var filename = file.Name.Trim('"');
                    filename = _env.WebRootPath + $@"\ProfilePhoto\{filename}";
                    size += file.Length;
                    using (FileStream fs = System.IO.File.Create(filename))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        public ImpoerExcel ImportExcel()
        {
            try
            {
                long size = 0;
                var files = Request.Form.Files;
                foreach (var file in files)
                {
                    //var filename = ContentDispositionHeaderValue
                    //                .Parse(file.ContentDisposition)
                    //                .FileName
                    //                .Trim('"');

                    var FileName = Path.GetFileName(file.FileName);
                    string extension = Path.GetExtension(FileName);

                    var fnm = DateTime.Now.ToString("d_M_yyyy_hh_mm_ss_tt") + extension;
                    //var fnm = FileName + extension;

                    //var filename = file.Name.Trim('"');
                    var rootPath = _env.WebRootPath.ToString() + @"\ImportExcel\" + fnm.ToString();
                    // var filename = Path.Combine(rootPath, @"\ImportExcel\", fnm);
                    size += file.Length;
                    using (FileStream fs = System.IO.File.Create(rootPath))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }

                    List<ImportChildData> lstExcel = new List<ImportChildData>();

                    //FileInfo fi = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                    FileInfo fi = new FileInfo(rootPath);
                    try
                    {
                        using (ExcelPackage package = new ExcelPackage(fi))
                        {
                            StringBuilder sb = new StringBuilder();
                            string sdata = "";
                            //ExcelWorksheet worksheet = package.Workbook.Worksheets["Sheet1"];
                            ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                            //ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                            int rowCount = worksheet.Dimension.Rows;
                            int ColCount = worksheet.Dimension.Columns;
                            bool bHeaderRow = true;
                            for (int row = 1; row <= rowCount; row++)
                            {
                                ImportChildData obj = new ImportChildData();

                                for (int col = 1; col <= ColCount; col++)
                                {
                                    string sData = Convert.ToString(worksheet.Cells[row, col].Value);
                                    if (!string.IsNullOrEmpty(sData))
                                    {
                                        if (sData.Length <= 7)
                                            obj.placa = sData;
                                        else
                                            obj.chassi = sData;
                                    }
                                }

                                lstExcel.Add(obj);
                            }

                            ImportMasterData objMasterdata = new ImportMasterData();
                            //objMasterdata.FileName = fnm.ToString();
                            objMasterdata.FileName = FileName.ToString();
                            objMasterdata.TotalFile = lstExcel.Count;

                            ImpoerExcel objImportdata = new ImpoerExcel();
                            objImportdata.ImportChildData = lstExcel;
                            objImportdata.ImportMasterData = objMasterdata;
                            objImportdata.Status = true;

                            return objImportdata;
                        }
                    }
                    catch (Exception ex)
                    {
                        ImpoerExcel objImportdata = new ImpoerExcel();
                        objImportdata.Status = false;
                        objImportdata.Msg = ex.Message.ToString();

                        return objImportdata;
                    }
                }

                ImpoerExcel response = new ImpoerExcel();
                response.Status = false;
                response.Msg = "Arquivo não encontrado.";

                return response;

            }
            catch (Exception ex)
            {
                ImpoerExcel objImportdata = new ImpoerExcel();
                objImportdata.Status = false;
                objImportdata.Msg = ex.Message.ToString();

                return objImportdata;
            }
        }

        [HttpPost]
        public ImpoerExcel ImportExcelCNH()
        {
            try
            {
                long size = 0;
                var files = Request.Form.Files;
                foreach (var file in files)
                {
                    var FileName = Path.GetFileName(file.FileName);
                    string extension = Path.GetExtension(FileName);

                    var fnm = DateTime.Now.ToString("d_M_yyyy_hh_mm_ss_tt") + extension;
                    
                    var rootPath = _env.WebRootPath.ToString() + @"\ImportExcel\" + fnm.ToString();

                    size += file.Length;
                    using (FileStream fs = System.IO.File.Create(rootPath))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }

                    List<ImportChildData> lstExcel = new List<ImportChildData>();
                    
                    FileInfo fi = new FileInfo(rootPath);
                    try
                    {
                        using (ExcelPackage package = new ExcelPackage(fi))
                        {
                            StringBuilder sb = new StringBuilder();
                            ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                            int rowCount = worksheet.Dimension.Rows;
                            int ColCount = worksheet.Dimension.Columns;
                            int rowInicial = 1;
                            int linhasErro = 0;

                            if (Convert.ToString(worksheet.Cells[1, 1].Value) == "UF")
                            {
                                rowInicial = 2;
                            }

                            for (int row = rowInicial; row <= rowCount; row++)
                            {
                                ImportChildData obj = new ImportChildData();
                                
                                obj.UF = Convert.ToString(worksheet.Cells[row, 1].Value);
                                if (obj.UF == string.Empty)
                                {
                                    linhasErro++;
                                    if (linhasErro < 3)
                                    {
                                        continue;
                                    }
                                    else if(lstExcel.Count < 1)
                                    {
                                        throw new Exception("Erro na linha " + row + ": A coluna UF é obrigatória para consulta de CNH.");
                                    }
                                    else if (lstExcel.Count > 0)
                                    {
                                        break;
                                    }
                                }
                                obj.Nome = Convert.ToString(worksheet.Cells[row, 2].Value);
                                if (obj.Nome == string.Empty)
                                {
                                    throw new Exception("Erro na linha " + row + ": A coluna Nome é obrigatória para consulta de CNH");
                                }
                                obj.CPF = Convert.ToString(worksheet.Cells[row, 3].Value);
                                if (obj.CPF == string.Empty)
                                {
                                    throw new Exception("Erro na linha " + row + ": A coluna CPF é obrigatória para consulta de CNH");
                                }
                                obj.NumRegistro = Convert.ToString(worksheet.Cells[row, 4].Value);
                                if (obj.NumRegistro == string.Empty)
                                {
                                    throw new Exception("Erro na linha " + row + ": A coluna número de registro é obrigatória para consulta de CNH");
                                }

                                obj.NumRenach = Convert.ToString(worksheet.Cells[row, 5].Value);
                                if (obj.UF == "TO" && (obj.NumRenach == null || obj.NumRenach == string.Empty))
                                {
                                    throw new Exception("Erro na linha " + row + ": O número do renach deve ser informado para pesquisa quando UF = TO");
                                }

                                try
                                {
                                    obj.ValidadeCNH = DateTime.Parse(Convert.ToString(worksheet.Cells[row, 6].Value));
                                }
                                catch (Exception)
                                {
                                    if (obj.UF == "PR")
                                    {
                                        throw new Exception("Erro na linha " + row + ": A válidade da CNH deve ser informada para pesquisa quando UF = PR");
                                    }
                                }

                                obj.RG = Convert.ToString(worksheet.Cells[row, 7].Value);
                                if ((obj.UF == "RO" || obj.UF == "RS") && (obj.RG == null || obj.RG == string.Empty))
                                {
                                    throw new Exception("Erro na linha " + row + ": O número do RG deve ser informado para pesquisa quando UF = RO ou RS");
                                }


                                try
                                {
                                    obj.DataNascimento = DateTime.Parse(Convert.ToString(worksheet.Cells[row, 8].Value));
                                }
                                catch (Exception)
                                {
                                    if (obj.UF == "AP" || obj.UF == "DF" || obj.UF == "RO" || obj.UF == "RR")
                                    {
                                        throw new Exception("Erro na linha " + row + ": A data de nascimento deve ser informada para pesquisa quando UF = AP, DF, RO, RR");
                                    }
                                }

                                try
                                {
                                    obj.DataHabilitacao = DateTime.Parse(Convert.ToString(worksheet.Cells[row, 9].Value));
                                }
                                catch (Exception)
                                {
                                    if (obj.UF == "MG")
                                    {
                                        throw new Exception("Erro na linha " + row + ": A data da habilitação deve ser informada para pesquisa quando UF = MG");
                                    }
                                }
                                
                                obj.MunicipioNascimento = Convert.ToString(worksheet.Cells[row, 10].Value);
                                if (obj.UF == "AL" && (obj.MunicipioNascimento == null || obj.MunicipioNascimento == string.Empty))
                                {
                                    throw new Exception("Erro na linha " + row + ": O Município de nascimento deve ser informado para pesquisa quando UF = AL");
                                }

                                obj.NumCedulaEspelho = Convert.ToString(worksheet.Cells[row, 11].Value);
                                if (obj.UF == "ES" && (obj.NumCedulaEspelho == null || obj.NumCedulaEspelho == string.Empty))
                                {
                                    throw new Exception("Erro na linha " + row + ": O número da cédula espelho deve ser informado para pesquisa quando UF igual a ES");
                                }

                                lstExcel.Add(obj);
                            }

                            ImportMasterData objMasterdata = new ImportMasterData();
                            objMasterdata.FileName = FileName.ToString();
                            objMasterdata.TotalFile = lstExcel.Count;

                            ImpoerExcel objImportdata = new ImpoerExcel();
                            objImportdata.ImportChildData = lstExcel;
                            objImportdata.ImportMasterData = objMasterdata;
                            objImportdata.Status = true;

                            return objImportdata;
                        }
                    }
                    catch (Exception ex)
                    {
                        ImpoerExcel objImportdata = new ImpoerExcel();
                        objImportdata.Status = false;
                        objImportdata.Msg = ex.Message.ToString();

                        return objImportdata;
                    }
                }

                ImpoerExcel response = new ImpoerExcel();
                response.Status = false;
                response.Msg = "Arquivo não encontrado.";

                return response;

            }
            catch (Exception ex)
            {
                ImpoerExcel objImportdata = new ImpoerExcel();
                objImportdata.Status = false;
                objImportdata.Msg = ex.Message.ToString();

                return objImportdata;
            }
        }
    }
}
