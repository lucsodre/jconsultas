using BusinessLogicLayer;
using BusinessLogicLayer.Helpers;
using EntityLayer.Models;
using JConsultas.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Xml.Serialization;

namespace JConsultas.Controllers
{
    public class MultipleDocsResponse
    {
        public string DocNo { get; set; }
        public bool Response { get; set; }
        public CONSULTA2300 xml { get; set; }
    }

    public class DocumentXMLResponselst
    {
        public List<DocumentXMLResponse> DocumentXMLResponse { get; set; }
    }

    [Produces("application/json")]
    //[Route("api/MultipleDocSearch")]
    public class MultipleDocSearchController : Controller
    {
        private readonly MultipleDocSearchBLL _MultipleDocSearchBLL;
        private readonly SingleSearchDocumentBLL _SingleSearchDocumentBLL;
        private JConsultasContext _context;
        private IHostingEnvironment _env;

        public MultipleDocSearchController(IHostingEnvironment env, EntityLayer.Models.JConsultasContext context)
        {
            this._env = env;

            _context = context;
            _MultipleDocSearchBLL = new MultipleDocSearchBLL(context);
            _SingleSearchDocumentBLL = new SingleSearchDocumentBLL(context);
        }

        [HttpPost]
        public bool GetMultipleDocSearch([FromBody]MultipleDocs data)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                var param = _context.ConsultaAvancadaVeiculo.Where(x => x.UserId == userId);
                ConsultaAvancadaVeiculo consultaAvancada;
                if (param == null || param.Count() == 0)
                {
                    consultaAvancada = new ConsultaAvancadaVeiculo();
                    consultaAvancada.ArrendarioFinanceira = true;
                    consultaAvancada.BloqueiosDetran = true;
                    consultaAvancada.DadosProp = true;
                    consultaAvancada.DadosVeic = true;
                    consultaAvancada.DetalhamentoDebitos = true;
                    consultaAvancada.DetalhamentoDPVAT = true;
                    consultaAvancada.DetalhamentoIPVA = true;
                    consultaAvancada.DetalhamentoMultas = true;
                    consultaAvancada.InspecaoVeicular = true;
                    consultaAvancada.IntencaoGravame = true;
                    consultaAvancada.Restricoes = true;
                }
                else
                {
                    consultaAvancada = param.First();
                }

                return _MultipleDocSearchBLL.SaveMultipleRecord(data, userId, consultaAvancada);
            }
            catch (Exception ex)
            {
                Helper.LogError("SingleSearchController.CS -> GetSigleSearch()", ex.Message, _env);
                //return new { status = 0, message = ex.Message };
                return false;
            }

        }

        //private async void Start_Button_Click(object sender, RoutedEventArgs e)
        //{
        //    int uploads = await UploadPicturesAsync(GenerateTestImages(),
        //        new Progress<int[]>(percents => ... do something...));
        //}

        public static CONSULTA2300 DeserializarXMLRetorno(string xml)
        {
            try
            {
                CONSULTA2300 retorno = null;
                XmlSerializer serializer = new XmlSerializer(typeof(CONSULTA2300));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                retorno = (CONSULTA2300)serializer.Deserialize(reader);
                //reader.Close();
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }

        public static CONSULTA2300ERR DeserializarXMLRetornoErr(string xml)
        {
            try
            {
                CONSULTA2300ERR retorno = null;
                XmlSerializer serializer = new XmlSerializer(typeof(CONSULTA2300ERR));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                retorno = (CONSULTA2300ERR)serializer.Deserialize(reader);
                //reader.Close();
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }

        [HttpPost]
        public bool SaveExcelData([FromBody]ImpoerExcel data)
        {
            var claimsIdentity = (ClaimsIdentity)this.User.Identity;
            var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
            var userId = claim.Value;

            var param = _context.ConsultaAvancadaVeiculo.Where(x => x.UserId == userId);
            ConsultaAvancadaVeiculo consultaAvancada;
            if (param == null || param.Count() == 0)
            {
                consultaAvancada = new ConsultaAvancadaVeiculo();
                consultaAvancada.ArrendarioFinanceira = true;
                consultaAvancada.BloqueiosDetran = true;
                consultaAvancada.DadosProp = true;
                consultaAvancada.DadosVeic = true;
                consultaAvancada.DetalhamentoDebitos = true;
                consultaAvancada.DetalhamentoDPVAT = true;
                consultaAvancada.DetalhamentoIPVA = true;
                consultaAvancada.DetalhamentoMultas = true;
                consultaAvancada.InspecaoVeicular = true;
                consultaAvancada.IntencaoGravame = true;
                consultaAvancada.Restricoes = true;
            }
            else
            {
                consultaAvancada = param.First();
            }

            return _MultipleDocSearchBLL.SaveMultipledata(data, userId, consultaAvancada);
        }

        [HttpPost]
        public dynamic RetryProcess(string Id)
        {
            try
            {
                if (_MultipleDocSearchBLL.RetryProcess(Id))
                    return new { status = 1 };
                else
                    return new { status = 0 };
            }
            catch (Exception ex)
            {
                return new { status = 0, Msg = ex.Message.ToString() };
            };
        }

        [HttpPost]
        public dynamic ExportExcel(string QId)
        {
            try
            {
                string sFileName = "";
                DocumentXMLResponselst _DocumentXMLResponselst = new DocumentXMLResponselst();
                List<DocumentXMLResponse> _lstDocumentXMLResponse = new List<DocumentXMLResponse>();

                List<MultipleData> _lst = new List<MultipleData>();
                _lst = _MultipleDocSearchBLL.GetMultipleData(QId);
                if (_lst.Count > 0 && _lst != null)
                {
                    for (int i = 0; i < _lst.Count; i++)
                    {
                        string sDocCode = "";
                        if (string.IsNullOrEmpty(Convert.ToString(_lst[i].Placa)))
                            sDocCode = Convert.ToString(_lst[i].Chassi);
                        else
                            sDocCode = Convert.ToString(_lst[i].Placa);

                        DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();
                        _DocumentXMLResponse = _SingleSearchDocumentBLL.GetDocumentFromDB(sDocCode);
                        _lstDocumentXMLResponse.Add(_DocumentXMLResponse);
                    }
                }

                if (_lstDocumentXMLResponse.Count > 0)
                {
                    sFileName = "Excel_" + DateTime.Now.ToString("dd_MM_yyyy_H_mm_ss") + ".xlsx";
                    string sWebRootFolder = _env.WebRootPath + $@"\uploadExcel\";
                    string dir = _env.WebRootPath + $@"\uploadExcel";

                    //string sWebRootFolder = _env.WebRootPath;
                    //string sFileName = @"demo2.xlsx";
                    string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
                    FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                    if (file.Exists)
                    {
                        file.Delete();
                        file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                    }

                    using (ExcelPackage package = new ExcelPackage(file))
                    {
                        ExcelWorksheet VEICULO = package.Workbook.Worksheets.Add("VEICULO");
                        ExcelWorksheet _TAXAS_LICENCIAMENTO = package.Workbook.Worksheets.Add("TAXAS_LICENCIAMENTO");
                        ExcelWorksheet _DPVATS = package.Workbook.Worksheets.Add("DPVATS");
                        ExcelWorksheet _IPVAS = package.Workbook.Worksheets.Add("IPVAS");
                        ExcelWorksheet _MULTAS = package.Workbook.Worksheets.Add("MULTAS");
                        ExcelWorksheet _MULTASRENAINF = package.Workbook.Worksheets.Add("MULTASRENAINF");
                        ExcelWorksheet _BLOQUEIOSRENAJUD = package.Workbook.Worksheets.Add("BLOQUEIOSRENAJUD");
                        ExcelWorksheet _BLOQUEIOSDETRAN = package.Workbook.Worksheets.Add("BLOQUEIOSDETRAN");
                        ExcelWorksheet parametrosConsultaSheet = package.Workbook.Worksheets.Add("PAR�METROS DA CONSULTA");

                        for (int v = 0; v < _lstDocumentXMLResponse.Count; v++)
                        {
                            var no = v + 2;
                            if (v == 0)
                            {
                                #region  1. VEICULO
                                // 1. VEICULO
                                VEICULO.Cells[1, 1].Value = "PLACA";
                                VEICULO.Cells[1, 2].Value = "CHASSI";
                                VEICULO.Cells[1, 3].Value = "RENAVAM";
                                VEICULO.Cells[1, 4].Value = "MUNICIPIO";
                                VEICULO.Cells[1, 5].Value = "MUNICIPIODESC";
                                VEICULO.Cells[1, 6].Value = "UF";
                                VEICULO.Cells[1, 7].Value = "PROCEDENCIA";
                                VEICULO.Cells[1, 8].Value = "PROCEDENCIADESC";
                                VEICULO.Cells[1, 9].Value = "MARCA";
                                VEICULO.Cells[1, 10].Value = "MARCADESC";
                                VEICULO.Cells[1, 11].Value = "ANOFABRICACAO";
                                VEICULO.Cells[1, 12].Value = "ANOMODELO";
                                VEICULO.Cells[1, 13].Value = "TIPO";
                                VEICULO.Cells[1, 14].Value = "TIPODESC";
                                VEICULO.Cells[1, 15].Value = "CARROCERIA";
                                VEICULO.Cells[1, 16].Value = "CARROCERIADESC";
                                VEICULO.Cells[1, 17].Value = "COR";
                                VEICULO.Cells[1, 18].Value = "CORDESC";
                                VEICULO.Cells[1, 19].Value = "CATEGORIA";
                                VEICULO.Cells[1, 20].Value = "CATEGORIADESC";
                                VEICULO.Cells[1, 21].Value = "COMBUSTIVEL";
                                VEICULO.Cells[1, 22].Value = "COMBUSTIVELDESC";
                                VEICULO.Cells[1, 23].Value = "ESPECIE";
                                VEICULO.Cells[1, 24].Value = "ESPECIEDESC";
                                VEICULO.Cells[1, 25].Value = "CAPACIDADEPASSAGEIRO";
                                VEICULO.Cells[1, 26].Value = "CAPACIDADECARGA";
                                VEICULO.Cells[1, 27].Value = "POTENCIA";
                                VEICULO.Cells[1, 28].Value = "CILINDRADA";
                                VEICULO.Cells[1, 29].Value = "CMT";
                                VEICULO.Cells[1, 30].Value = "PBT";
                                VEICULO.Cells[1, 31].Value = "EIXOS";
                                VEICULO.Cells[1, 32].Value = "NUMEROMOTOR";
                                VEICULO.Cells[1, 33].Value = "DATAALTERACAO";
                                VEICULO.Cells[1, 34].Value = "NUMEROCAMBIO";
                                VEICULO.Cells[1, 35].Value = "TIPOMONTAGEM";
                                VEICULO.Cells[1, 36].Value = "SITUACAOVEICULO";
                                VEICULO.Cells[1, 37].Value = "TIPOREMARCACAOCHASSI";
                                VEICULO.Cells[1, 38].Value = "DATAEMISSAOCRV";
                                VEICULO.Cells[1, 39].Value = "DATALICENCIAMENTO";
                                VEICULO.Cells[1, 40].Value = "EXERCICIOLICENCIAMENTO";
                                VEICULO.Cells[1, 41].Value = "VALORNF";
                                VEICULO.Cells[1, 42].Value = "NUMERONF";
                                VEICULO.Cells[1, 43].Value = "DATANF";

                                // 1.  Add PROPRIETARIO data
                                VEICULO.Cells[1, 44].Value = "TIPOCPFPROPRIETARIO";
                                VEICULO.Cells[1, 45].Value = "CPFPROPRIETARIO";
                                VEICULO.Cells[1, 46].Value = "NOMEPROPRIETARIO";
                                VEICULO.Cells[1, 47].Value = "RG";
                                VEICULO.Cells[1, 48].Value = "ORGEXP";
                                VEICULO.Cells[1, 49].Value = "ENDERECO";
                                VEICULO.Cells[1, 50].Value = "COMPLEMENTO";
                                VEICULO.Cells[1, 51].Value = "BAIRRO";
                                VEICULO.Cells[1, 52].Value = "CIDADE";
                                VEICULO.Cells[1, 53].Value = "CEP";
                                VEICULO.Cells[1, 54].Value = "NUMERO";
                                VEICULO.Cells[1, 55].Value = "DDD";
                                VEICULO.Cells[1, 56].Value = "TELEFONE";
                                VEICULO.Cells[1, 57].Value = "NOMEPROPRIETARIOANTERIOR";

                                // 1.  Add GRAVAME data
                                VEICULO.Cells[1, 58].Value = "CNPJFINANCIADO";
                                VEICULO.Cells[1, 59].Value = "ARRENDATARIOFINANCIADO";
                                VEICULO.Cells[1, 60].Value = "TIPOTRANSACAO";
                                VEICULO.Cells[1, 61].Value = "TIPOTRANSACAODESC";
                                VEICULO.Cells[1, 62].Value = "TIPORESTRICAOFINANCEIRA";
                                VEICULO.Cells[1, 63].Value = "TIPORESTRICAOFINANCEIRADESC";
                                VEICULO.Cells[1, 64].Value = "CNPJFINANCEIRA";
                                VEICULO.Cells[1, 65].Value = "AGENTEFINANCEIRO";
                                VEICULO.Cells[1, 66].Value = "AGENTEFINANCEIRODESC";
                                VEICULO.Cells[1, 67].Value = "NUMEROCONTRATO";
                                VEICULO.Cells[1, 68].Value = "DATAVIGENCIACONTRATO";
                                VEICULO.Cells[1, 69].Value = "DATAINCLUSAOFINANCIAMENTO";
                                VEICULO.Cells[1, 70].Value = "DATACONTRATO";
                                VEICULO.Cells[1, 71].Value = "UF_GRAVAME";
                                VEICULO.Cells[1, 72].Value = "NUMERORESTRICAO";
                                VEICULO.Cells[1, 73].Value = "SITUACAO";

                                // 1.  Add GRAVAME_INTENCAO
                                VEICULO.Cells[1, 74].Value = "CNPJFINANCEIRA";
                                VEICULO.Cells[1, 75].Value = "INFORMANTEFINANCIAMENTO";
                                VEICULO.Cells[1, 76].Value = "TIPOTRANSACAO";
                                VEICULO.Cells[1, 77].Value = "TIPOTRANSACAODESC";
                                VEICULO.Cells[1, 78].Value = "TIPORESTRICAOFINANCEIRA";
                                VEICULO.Cells[1, 79].Value = "TIPORESTRICAOFINANCEIRADESC";
                                VEICULO.Cells[1, 80].Value = "AGENTEFINANCEIRO";
                                VEICULO.Cells[1, 81].Value = "AGENTEFINANCEIRODESC";
                                VEICULO.Cells[1, 82].Value = "CNPJFINANCIADO";
                                VEICULO.Cells[1, 83].Value = "NOMEFINANCIADO";
                                VEICULO.Cells[1, 84].Value = "DATAINCLUSAOINTENCAOTROCAFINANC";
                                VEICULO.Cells[1, 85].Value = "NUMEROCONTRATOFINANC";
                                VEICULO.Cells[1, 86].Value = "DATAVIGENCIACONTRATOFINANC";
                                VEICULO.Cells[1, 87].Value = "UF_GRAVAME";
                                VEICULO.Cells[1, 88].Value = "NUMERORESTRICAO";
                                VEICULO.Cells[1, 89].Value = "SITUACAO";

                                // 1.  Add RESTRICOES
                                VEICULO.Cells[1, 90].Value = "RESTRICOES";

                                // 1 Add TOTAL_DEBITOS
                                VEICULO.Cells[1, 91].Value = "DEBITOIPVALICENCIAMENTO_DESC";
                                VEICULO.Cells[1, 92].Value = "DEBITOSMULTAS_DESC";
                                VEICULO.Cells[1, 93].Value = "DEBITOSDIVIDAATIVA_DESC";
                                VEICULO.Cells[1, 94].Value = "IPVA";
                                VEICULO.Cells[1, 95].Value = "LICENCIAMENTO";
                                VEICULO.Cells[1, 96].Value = "MULTAS";
                                VEICULO.Cells[1, 97].Value = "DPVAT";
                                VEICULO.Cells[1, 98].Value = "IPVA_DIVIDAATIVA";

                                // 1 Add COMUNICACAO_VENDA
                                VEICULO.Cells[1, 99].Value = "COMUNICACAOVENDA";
                                VEICULO.Cells[1, 100].Value = "COMUNICACAOVENDADESC";
                                VEICULO.Cells[1, 101].Value = "DATAINCLUSAOCOMUNICACAOVENDAS";
                                VEICULO.Cells[1, 102].Value = "DATAVENDA";
                                VEICULO.Cells[1, 103].Value = "TIPODOCTOCOMPRADOR";
                                VEICULO.Cells[1, 104].Value = "CNPJCPFCOMPRADOR";
                                VEICULO.Cells[1, 105].Value = "DATANOTAFISCAL";
                                VEICULO.Cells[1, 106].Value = "DATAPROTOCOLODETRAN";

                                // 1 Add INSPECAO_VEICULAR
                                VEICULO.Cells[1, 107].Value = "INSPECAOVEICULAR";
                                VEICULO.Cells[1, 108].Value = "INSPECAOVEICULARDESC";
                                VEICULO.Cells[1, 109].Value = "DATAINSPECAOVEICULAR";
                                VEICULO.Cells[1, 110].Value = "DATAINCLUSACAOINSPECAOVEICULAR";

                                #endregion

                                #region DPVATS
                                _DPVATS.Cells[1, 1].Value = "PLACA";
                                _DPVATS.Cells[1, 2].Value = "ORIGEM";
                                _DPVATS.Cells[1, 3].Value = "ANOREF";
                                _DPVATS.Cells[1, 4].Value = "VALDPVAT";
                                _DPVATS.Cells[1, 5].Value = "NUMPARCELA";
                                _DPVATS.Cells[1, 6].Value = "DTVENCTODPVAT";
                                _DPVATS.Cells[1, 7].Value = "CATEGORIA";
                                #endregion

                                #region IPVAS
                                _IPVAS.Cells[1, 1].Value = "PLACA";
                                _IPVAS.Cells[1, 2].Value = "ORIGEM";
                                _IPVAS.Cells[1, 3].Value = "ANOREF";
                                _IPVAS.Cells[1, 4].Value = "VALIPVA";
                                _IPVAS.Cells[1, 5].Value = "COTAIPVA";
                                _IPVAS.Cells[1, 6].Value = "COTAIPVADES";
                                _IPVAS.Cells[1, 7].Value = "DTVENCTOIPVA";
                                #endregion

                                #region MULTAS
                                _MULTAS.Cells[1, 1].Value = "PLACA";
                                _MULTAS.Cells[1, 2].Value = "ORIGEM";
                                _MULTAS.Cells[1, 3].Value = "NUMGUIA";
                                _MULTAS.Cells[1, 4].Value = "NUMAIIP";
                                _MULTAS.Cells[1, 5].Value = "CODREC";
                                _MULTAS.Cells[1, 6].Value = "DTINFRA";
                                _MULTAS.Cells[1, 7].Value = "LOCALINFRA";
                                _MULTAS.Cells[1, 8].Value = "HORAINFRA";
                                _MULTAS.Cells[1, 9].Value = "MUNINFRA";
                                _MULTAS.Cells[1, 10].Value = "ENQUADRAM";
                                _MULTAS.Cells[1, 11].Value = "ENQUADRAMDESC";
                                _MULTAS.Cells[1, 12].Value = "ENQUADRAMLEG";
                                _MULTAS.Cells[1, 13].Value = "DTVENCTOINFRA";
                                _MULTAS.Cells[1, 14].Value = "VALMULTA";
                                _MULTAS.Cells[1, 15].Value = "ENTTRANSITANT";
                                _MULTAS.Cells[1, 16].Value = "NOMEENTTRANSITANT";
                                _MULTAS.Cells[1, 17].Value = "MUNINFRADESC";
                                _MULTAS.Cells[1, 18].Value = "EMPRESA";
                                _MULTAS.Cells[1, 19].Value = "TIPO";
                                #endregion

                                #region BLOQUEIOSDETRAN
                                _BLOQUEIOSDETRAN.Cells[1, 1].Value = "PLACA";
                                _BLOQUEIOSDETRAN.Cells[1, 2].Value = "TIPO";
                                _BLOQUEIOSDETRAN.Cells[1, 3].Value = "MOTIVO";
                                _BLOQUEIOSDETRAN.Cells[1, 4].Value = "DATAINCLUSAO";
                                _BLOQUEIOSDETRAN.Cells[1, 5].Value = "PROTOCOLO_NUMERO";
                                _BLOQUEIOSDETRAN.Cells[1, 6].Value = "PROTOCOLO_ANO";
                                _BLOQUEIOSDETRAN.Cells[1, 7].Value = "PROCESSO_NUMERO";
                                _BLOQUEIOSDETRAN.Cells[1, 8].Value = "PROCESSO_ANO";
                                _BLOQUEIOSDETRAN.Cells[1, 9].Value = "OFICIO_NUMERO";
                                _BLOQUEIOSDETRAN.Cells[1, 10].Value = "OFICIO_ANO";
                                _BLOQUEIOSDETRAN.Cells[1, 11].Value = "IDCIDADE";
                                _BLOQUEIOSDETRAN.Cells[1, 12].Value = "MUNICIPIO";
                                #endregion

                                #region Parametros
                                parametrosConsultaSheet.Cells[1, 1].Value = "Placa";
                                parametrosConsultaSheet.Cells[1, 2].Value = "Dados do ve�culo";
                                parametrosConsultaSheet.Cells[1, 3].Value = "Gravame";
                                parametrosConsultaSheet.Cells[1, 4].Value = "Bloqueios Detran";
                                parametrosConsultaSheet.Cells[1, 5].Value = "Dados do propriet�rio";
                                parametrosConsultaSheet.Cells[1, 6].Value = "Detalhamento dos d�bitos";
                                parametrosConsultaSheet.Cells[1, 7].Value = "Detalhamento DPVAT";
                                parametrosConsultaSheet.Cells[1, 8].Value = "Detalhamento IPVA";
                                parametrosConsultaSheet.Cells[1, 9].Value = "Detalhamento das multas";
                                parametrosConsultaSheet.Cells[1, 10].Value = "Inspe��o veicular";
                                parametrosConsultaSheet.Cells[1, 11].Value = "Inten��o de gravame";
                                parametrosConsultaSheet.Cells[1, 12].Value = "Restri��es";
                                #endregion
                            }

                            if (!string.IsNullOrEmpty(_lstDocumentXMLResponse[v].xml))
                            {
                                CONSULTA2300 r = DeserializarXMLRetorno(_lstDocumentXMLResponse[v].xml);

                                var consultaAvancada = _lstDocumentXMLResponse[v].ConsultaSolicitada;

                                var consultaAvancadaVeic = SerializadorXML.Deserializar<ConsultaAvancadaVeiculo>(_lstDocumentXMLResponse[v].ConsultaSolicitada);

                                var veiculoData = r.CONSULTAS.CONSULTA.VEICULO;
                                var proprietarioData = r.CONSULTAS.CONSULTA.PROPRIETARIO;
                                var gravameData = r.CONSULTAS.CONSULTA.GRAVAME;
                                var gravamE_INTENCAOData = r.CONSULTAS.CONSULTA.GRAVAME_INTENCAO;
                                var debitosData = r.CONSULTAS.CONSULTA.TOTAL_DEBITOS;
                                var restricoesData = r.CONSULTAS.CONSULTA.RESTRICOES;
                                var multasData = r.CONSULTAS.CONSULTA.MULTAS;
                                var COMUNICACAO_VENDAData = r.CONSULTAS.CONSULTA.COMUNICACAO_VENDA;
                                var INSPECAO_VEICULARData = r.CONSULTAS.CONSULTA.INSPECAO_VEICULAR;

                                var TAXAS_LICENCIAMENTO = r.CONSULTAS.CONSULTA.TAXAS_LICENCIAMENTO;
                                var DPVATS = r.CONSULTAS.CONSULTA.DPVATS;
                                var IPVAS = r.CONSULTAS.CONSULTA.IPVAS;
                                var NOTIFICACOES = r.CONSULTAS.CONSULTA.NOTIFICACOES;
                                var MULTAS = r.CONSULTAS.CONSULTA.MULTAS;
                                var MULTASRENAINF = r.CONSULTAS.CONSULTA.MULTASRENAINF;
                                var BLOQUEIOSRENAJUD = r.CONSULTAS.CONSULTA.BLOQUEIOSRENAJUD;
                                var BLOQUEIOSDETRAN = r.CONSULTAS.CONSULTA.BLOQUEIOSDETRAN;

                                if (veiculoData == null)
                                {
                                    continue;
                                }

                                #region Condi��es da tela
                                parametrosConsultaSheet.Cells[no, 1].Value = Convert.ToString(veiculoData.PLACA);

                                if (consultaAvancadaVeic != null)
                                {
                                    if (!consultaAvancadaVeic.DadosVeic)
                                    {
                                        veiculoData = null;
                                        parametrosConsultaSheet.Cells[no, 2].Value = "N";
                                    }
                                    else
                                    {
                                        parametrosConsultaSheet.Cells[no, 2].Value = "S";
                                    }

                                    if (!consultaAvancadaVeic.ArrendarioFinanceira)
                                    {
                                        gravameData = null;
                                        parametrosConsultaSheet.Cells[no, 3].Value = "N";
                                    }
                                    else
                                    {
                                        parametrosConsultaSheet.Cells[no, 3].Value = "S";
                                    }

                                    if (!consultaAvancadaVeic.BloqueiosDetran)
                                    {
                                        BLOQUEIOSDETRAN = null;
                                        parametrosConsultaSheet.Cells[no, 4].Value = "N";
                                    }
                                    else
                                    {
                                        parametrosConsultaSheet.Cells[no, 4].Value = "S";
                                    }

                                    if (!consultaAvancadaVeic.DadosProp)
                                    {
                                        proprietarioData = null;
                                        parametrosConsultaSheet.Cells[no, 5].Value = "N";
                                    }
                                    else
                                    {
                                        parametrosConsultaSheet.Cells[no, 5].Value = "S";
                                    }

                                    if (!consultaAvancadaVeic.DetalhamentoDebitos)
                                    {
                                        debitosData = null;
                                        parametrosConsultaSheet.Cells[no, 6].Value = "N";
                                    }
                                    else
                                    {
                                        parametrosConsultaSheet.Cells[no, 6].Value = "S";
                                    }

                                    if (!consultaAvancadaVeic.DetalhamentoDPVAT)
                                    {
                                        DPVATS = null;
                                        parametrosConsultaSheet.Cells[no, 7].Value = "N";
                                    }
                                    else
                                    {
                                        parametrosConsultaSheet.Cells[no, 7].Value = "S";
                                    }

                                    if (!consultaAvancadaVeic.DetalhamentoIPVA)
                                    {
                                        IPVAS = null;
                                        parametrosConsultaSheet.Cells[no, 8].Value = "N";
                                    }
                                    else
                                    {
                                        parametrosConsultaSheet.Cells[no, 8].Value = "S";
                                    }

                                    if (!consultaAvancadaVeic.DetalhamentoMultas)
                                    {
                                        MULTAS = null;
                                        parametrosConsultaSheet.Cells[no, 9].Value = "N";
                                    }
                                    else
                                    {
                                        parametrosConsultaSheet.Cells[no, 9].Value = "S";
                                    }

                                    if (!consultaAvancadaVeic.InspecaoVeicular)
                                    {
                                        INSPECAO_VEICULARData = null;
                                        parametrosConsultaSheet.Cells[no, 10].Value = "N";
                                    }
                                    else
                                    {
                                        parametrosConsultaSheet.Cells[no, 10].Value = "S";
                                    }

                                    if (!consultaAvancadaVeic.IntencaoGravame)
                                    {
                                        gravamE_INTENCAOData = null;
                                        parametrosConsultaSheet.Cells[no, 11].Value = "N";
                                    }
                                    else
                                    {
                                        parametrosConsultaSheet.Cells[no, 11].Value = "S";
                                    }

                                    if (!consultaAvancadaVeic.Restricoes)
                                    {
                                        restricoesData = null;
                                        parametrosConsultaSheet.Cells[no, 12].Value = "N";
                                    }
                                    else
                                    {
                                        parametrosConsultaSheet.Cells[no, 12].Value = "S";
                                    }
                                }
                                else
                                {
                                    parametrosConsultaSheet.Cells[no, 2].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 3].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 4].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 5].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 6].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 7].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 8].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 9].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 10].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 11].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 12].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 13].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 14].Value = "S";
                                    parametrosConsultaSheet.Cells[no, 15].Value = "S";
                                }
                                #endregion


                                #region VEICULO
                                if (veiculoData != null)
                                {
                                    VEICULO.Cells[no, 1].Value = Convert.ToString(veiculoData.PLACA);
                                    VEICULO.Cells[no, 2].Value = Convert.ToString(veiculoData.CHASSI);
                                    VEICULO.Cells[no, 3].Value = Convert.ToString(veiculoData.RENAVAM);
                                    VEICULO.Cells[no, 4].Value = Convert.ToString(veiculoData.MUNICIPIO);
                                    VEICULO.Cells[no, 5].Value = Convert.ToString(veiculoData.MUNICIPIODESC);
                                    VEICULO.Cells[no, 6].Value = Convert.ToString(veiculoData.UF);
                                    VEICULO.Cells[no, 7].Value = Convert.ToString(veiculoData.PROCEDENCIA);
                                    VEICULO.Cells[no, 8].Value = Convert.ToString(veiculoData.PROCEDENCIADESC);
                                    VEICULO.Cells[no, 9].Value = Convert.ToString(veiculoData.MARCA);
                                    VEICULO.Cells[no, 10].Value = Convert.ToString(veiculoData.MARCADESC);
                                    VEICULO.Cells[no, 11].Value = Convert.ToString(veiculoData.ANOFABRICACAO);
                                    VEICULO.Cells[no, 12].Value = Convert.ToString(veiculoData.ANOMODELO);
                                    VEICULO.Cells[no, 13].Value = Convert.ToString(veiculoData.TIPO);
                                    VEICULO.Cells[no, 14].Value = Convert.ToString(veiculoData.TIPODESC);
                                    VEICULO.Cells[no, 15].Value = Convert.ToString(veiculoData.CARROCERIA);
                                    VEICULO.Cells[no, 16].Value = Convert.ToString(veiculoData.CARROCERIADESC);
                                    VEICULO.Cells[no, 17].Value = Convert.ToString(veiculoData.COR);
                                    VEICULO.Cells[no, 18].Value = Convert.ToString(veiculoData.CORDESC);
                                    VEICULO.Cells[no, 19].Value = Convert.ToString(veiculoData.CATEGORIA);
                                    VEICULO.Cells[no, 20].Value = Convert.ToString(veiculoData.CATEGORIADESC);
                                    VEICULO.Cells[no, 21].Value = Convert.ToString(veiculoData.COMBUSTIVEL);
                                    VEICULO.Cells[no, 22].Value = Convert.ToString(veiculoData.COMBUSTIVELDESC);
                                    VEICULO.Cells[no, 23].Value = Convert.ToString(veiculoData.ESPECIE);
                                    VEICULO.Cells[no, 24].Value = Convert.ToString(veiculoData.ESPECIEDESC);
                                    VEICULO.Cells[no, 25].Value = Convert.ToString(veiculoData.CAPACIDADEPASSAGEIRO);
                                    VEICULO.Cells[no, 26].Value = Convert.ToString(veiculoData.CAPACIDADECARGA);
                                    VEICULO.Cells[no, 27].Value = Convert.ToString(veiculoData.POTENCIA);
                                    VEICULO.Cells[no, 28].Value = Convert.ToString(veiculoData.CILINDRADA);
                                    VEICULO.Cells[no, 29].Value = Convert.ToString(veiculoData.CMT);
                                    VEICULO.Cells[no, 30].Value = Convert.ToString(veiculoData.PBT);
                                    VEICULO.Cells[no, 31].Value = Convert.ToString(veiculoData.EIXOS);
                                    VEICULO.Cells[no, 32].Value = Convert.ToString(veiculoData.NUMEROMOTOR);
                                    VEICULO.Cells[no, 33].Value = Convert.ToString(veiculoData.DATAALTERACAO);
                                    VEICULO.Cells[no, 34].Value = Convert.ToString(veiculoData.NUMEROCAMBIO);
                                    VEICULO.Cells[no, 35].Value = Convert.ToString(veiculoData.TIPOMONTAGEM);
                                    VEICULO.Cells[no, 36].Value = Convert.ToString(veiculoData.SITUACAOVEICULO);
                                    VEICULO.Cells[no, 37].Value = Convert.ToString(veiculoData.TIPOREMARCACAOCHASSI);
                                    VEICULO.Cells[no, 38].Value = Convert.ToString(veiculoData.DATAEMISSAOCRV);
                                    VEICULO.Cells[no, 39].Value = Convert.ToString(veiculoData.DATALICENCIAMENTO);
                                    VEICULO.Cells[no, 40].Value = Convert.ToString(veiculoData.EXERCICIOLICENCIAMENTO);
                                    VEICULO.Cells[no, 41].Value = Convert.ToString(veiculoData.VALORNF);
                                    VEICULO.Cells[no, 42].Value = Convert.ToString(veiculoData.NUMERONF);
                                    VEICULO.Cells[no, 43].Value = Convert.ToString(veiculoData.DATANF);
                                }

                                // 1.  Add PROPRIETARIO data
                                if (proprietarioData != null)
                                {
                                    VEICULO.Cells[no, 44].Value = Convert.ToString(proprietarioData.TIPOCPFPROPRIETARIO);
                                    VEICULO.Cells[no, 45].Value = Convert.ToString(proprietarioData.CPFPROPRIETARIO);
                                    VEICULO.Cells[no, 46].Value = Convert.ToString(proprietarioData.NOMEPROPRIETARIO);
                                    VEICULO.Cells[no, 47].Value = Convert.ToString(proprietarioData.RG);
                                    VEICULO.Cells[no, 48].Value = Convert.ToString(proprietarioData.ORGEXP);
                                    VEICULO.Cells[no, 49].Value = Convert.ToString(proprietarioData.ENDERECO);
                                    VEICULO.Cells[no, 50].Value = Convert.ToString(proprietarioData.COMPLEMENTO);
                                    VEICULO.Cells[no, 51].Value = Convert.ToString(proprietarioData.BAIRRO);
                                    VEICULO.Cells[no, 52].Value = Convert.ToString(proprietarioData.CIDADE);
                                    VEICULO.Cells[no, 53].Value = Convert.ToString(proprietarioData.CEP);
                                    VEICULO.Cells[no, 54].Value = Convert.ToString(proprietarioData.NUMERO);
                                    VEICULO.Cells[no, 55].Value = Convert.ToString(proprietarioData.DDD);
                                    VEICULO.Cells[no, 56].Value = Convert.ToString(proprietarioData.TELEFONE);
                                    VEICULO.Cells[no, 57].Value = Convert.ToString(proprietarioData.NOMEPROPRIETARIOANTERIOR);
                                }

                                // 1.  Add GRAVAME data
                                if (gravameData != null)
                                {
                                    VEICULO.Cells[no, 58].Value = Convert.ToString(gravameData.CNPJFINANCIADO);
                                    VEICULO.Cells[no, 59].Value = Convert.ToString(gravameData.ARRENDATARIOFINANCIADO);
                                    VEICULO.Cells[no, 60].Value = Convert.ToString(gravameData.TIPOTRANSACAO);
                                    VEICULO.Cells[no, 61].Value = Convert.ToString(gravameData.TIPOTRANSACAODESC);
                                    VEICULO.Cells[no, 62].Value = Convert.ToString(gravameData.TIPORESTRICAOFINANCEIRA);
                                    VEICULO.Cells[no, 63].Value = Convert.ToString(gravameData.TIPORESTRICAOFINANCEIRADESC);
                                    VEICULO.Cells[no, 64].Value = Convert.ToString(gravameData.CNPJFINANCEIRA);
                                    VEICULO.Cells[no, 65].Value = Convert.ToString(gravameData.AGENTEFINANCEIRO);
                                    VEICULO.Cells[no, 66].Value = Convert.ToString(gravameData.AGENTEFINANCEIRODESC);
                                    VEICULO.Cells[no, 67].Value = Convert.ToString(gravameData.NUMEROCONTRATO);
                                    VEICULO.Cells[no, 68].Value = Convert.ToString(gravameData.DATAVIGENCIACONTRATO);
                                    VEICULO.Cells[no, 69].Value = Convert.ToString(gravameData.DATAINCLUSAOFINANCIAMENTO);
                                    VEICULO.Cells[no, 70].Value = Convert.ToString(gravameData.DATACONTRATO);
                                    VEICULO.Cells[no, 71].Value = Convert.ToString(gravameData.UF_GRAVAME);
                                    VEICULO.Cells[no, 72].Value = Convert.ToString(gravameData.NUMERORESTRICAO);
                                    VEICULO.Cells[no, 73].Value = Convert.ToString(gravameData.SITUACAO);
                                }

                                // 1.  Add GRAVAME_INTENCAO
                                if (gravamE_INTENCAOData != null)
                                {
                                    VEICULO.Cells[no, 74].Value = Convert.ToString(gravamE_INTENCAOData.CNPJFINANCEIRA);
                                    VEICULO.Cells[no, 75].Value = Convert.ToString(gravamE_INTENCAOData.INFORMANTEFINANCIAMENTO);
                                    VEICULO.Cells[no, 76].Value = Convert.ToString(gravamE_INTENCAOData.TIPOTRANSACAO);
                                    VEICULO.Cells[no, 77].Value = Convert.ToString(gravamE_INTENCAOData.TIPOTRANSACAODESC);
                                    VEICULO.Cells[no, 78].Value = Convert.ToString(gravamE_INTENCAOData.TIPORESTRICAOFINANCEIRA);
                                    VEICULO.Cells[no, 79].Value = Convert.ToString(gravamE_INTENCAOData.TIPORESTRICAOFINANCEIRADESC);
                                    VEICULO.Cells[no, 80].Value = Convert.ToString(gravamE_INTENCAOData.AGENTEFINANCEIRO);
                                    VEICULO.Cells[no, 81].Value = Convert.ToString(gravamE_INTENCAOData.AGENTEFINANCEIRODESC);
                                    VEICULO.Cells[no, 82].Value = Convert.ToString(gravamE_INTENCAOData.CNPJFINANCIADO);
                                    VEICULO.Cells[no, 83].Value = Convert.ToString(gravamE_INTENCAOData.NOMEFINANCIADO);
                                    VEICULO.Cells[no, 84].Value = Convert.ToString(gravamE_INTENCAOData.DATAINCLUSAOINTENCAOTROCAFINANC);
                                    VEICULO.Cells[no, 85].Value = Convert.ToString(gravamE_INTENCAOData.NUMEROCONTRATOFINANC);
                                    VEICULO.Cells[no, 86].Value = Convert.ToString(gravamE_INTENCAOData.DATAVIGENCIACONTRATOFINANC);
                                    VEICULO.Cells[no, 87].Value = Convert.ToString(gravamE_INTENCAOData.UF_GRAVAME);
                                    VEICULO.Cells[no, 88].Value = Convert.ToString(gravamE_INTENCAOData.NUMERORESTRICAO);
                                    VEICULO.Cells[no, 89].Value = Convert.ToString(gravamE_INTENCAOData.SITUACAO);
                                }

                                // 1.  Add RESTRICOES
                                if (restricoesData != null)
                                {
                                    VEICULO.Cells[no, 90].Value = Convert.ToString(restricoesData);
                                }

                                // 1 Add TOTAL_DEBITOS
                                if (debitosData != null)
                                {
                                    VEICULO.Cells[no, 91].Value = Convert.ToString(debitosData.DEBITOIPVALICENCIAMENTO_DESC);
                                    VEICULO.Cells[no, 92].Value = Convert.ToString(debitosData.DEBITOSMULTAS_DESC);
                                    VEICULO.Cells[no, 93].Value = Convert.ToString(debitosData.DEBITOSDIVIDAATIVA_DESC);
                                    VEICULO.Cells[no, 94].Value = Convert.ToString(debitosData.IPVA);
                                    VEICULO.Cells[no, 95].Value = Convert.ToString(debitosData.LICENCIAMENTO);
                                    VEICULO.Cells[no, 96].Value = Convert.ToString(debitosData.MULTAS);
                                    VEICULO.Cells[no, 97].Value = Convert.ToString(debitosData.DPVAT);
                                    VEICULO.Cells[no, 98].Value = Convert.ToString(debitosData.IPVA_DIVIDAATIVA);
                                }

                                // 1 Add COMUNICACAO_VENDA
                                if (COMUNICACAO_VENDAData != null)
                                {
                                    VEICULO.Cells[no, 99].Value = Convert.ToString(COMUNICACAO_VENDAData.COMUNICACAOVENDA);
                                    VEICULO.Cells[no, 100].Value = Convert.ToString(COMUNICACAO_VENDAData.COMUNICACAOVENDADESC);
                                    VEICULO.Cells[no, 101].Value = Convert.ToString(COMUNICACAO_VENDAData.DATAINCLUSAOCOMUNICACAOVENDAS);
                                    VEICULO.Cells[no, 102].Value = Convert.ToString(COMUNICACAO_VENDAData.DATAVENDA);
                                    VEICULO.Cells[no, 103].Value = Convert.ToString(COMUNICACAO_VENDAData.TIPODOCTOCOMPRADOR);
                                    VEICULO.Cells[no, 104].Value = Convert.ToString(COMUNICACAO_VENDAData.CNPJCPFCOMPRADOR);
                                    VEICULO.Cells[no, 105].Value = Convert.ToString(COMUNICACAO_VENDAData.DATANOTAFISCAL);
                                    VEICULO.Cells[no, 106].Value = Convert.ToString(COMUNICACAO_VENDAData.DATAPROTOCOLODETRAN);
                                }

                                // 1 Add INSPECAO_VEICULAR
                                if (INSPECAO_VEICULARData != null)
                                {
                                    VEICULO.Cells[no, 107].Value = Convert.ToString(INSPECAO_VEICULARData.INSPECAOVEICULAR);
                                    VEICULO.Cells[no, 108].Value = Convert.ToString(INSPECAO_VEICULARData.INSPECAOVEICULARDESC);
                                    VEICULO.Cells[no, 109].Value = Convert.ToString(INSPECAO_VEICULARData.DATAINSPECAOVEICULAR);
                                    VEICULO.Cells[no, 110].Value = Convert.ToString(INSPECAO_VEICULARData.DATAINCLUSACAOINSPECAOVEICULAR);
                                }

                                #endregion

                                // 2. TAXAS_LICENCIAMENTO


                                #region DPVATS

                                // 3. DPVATS
                                //ExcelWorksheet _DPVATS = package.Workbook.Worksheets.Add("DPVATS");
                                if (DPVATS != null)
                                {
                                    for (int i = 0; i < DPVATS.Length; i++)
                                    {
                                        var dpvtno = i + 2;
                                        _DPVATS.Cells[dpvtno, 1].Value = Convert.ToString(_lstDocumentXMLResponse[v].DocumentCode);
                                        _DPVATS.Cells[dpvtno, 2].Value = Convert.ToString(DPVATS[i].ORIGEM);
                                        _DPVATS.Cells[dpvtno, 3].Value = Convert.ToString(DPVATS[i].ANOREF);
                                        _DPVATS.Cells[dpvtno, 4].Value = Convert.ToString(DPVATS[i].VALDPVAT);
                                        _DPVATS.Cells[dpvtno, 5].Value = Convert.ToString(DPVATS[i].NUMPARCELA);
                                        _DPVATS.Cells[dpvtno, 6].Value = Convert.ToString(DPVATS[i].DTVENCTODPVAT);
                                        _DPVATS.Cells[dpvtno, 7].Value = Convert.ToString(DPVATS[i].CATEGORIA);
                                    }
                                }
                                #endregion

                                #region  4. IPVAS

                                // 4. IPVAS
                                if (IPVAS != null)
                                {
                                    //ExcelWorksheet _IPVAS = package.Workbook.Worksheets.Add("IPVAS");
                                    for (int i = 0; i < IPVAS.Length; i++)
                                    {
                                        var IPVASno = i + 2;

                                        _IPVAS.Cells[IPVASno, 1].Value = Convert.ToString(_lstDocumentXMLResponse[v].DocumentCode);
                                        _IPVAS.Cells[IPVASno, 2].Value = Convert.ToString(IPVAS[i].ORIGEM);
                                        _IPVAS.Cells[IPVASno, 3].Value = Convert.ToString(IPVAS[i].ANOREF);
                                        _IPVAS.Cells[IPVASno, 4].Value = Convert.ToString(IPVAS[i].VALIPVA);
                                        _IPVAS.Cells[IPVASno, 5].Value = Convert.ToString(IPVAS[i].COTAIPVA);
                                        _IPVAS.Cells[IPVASno, 6].Value = Convert.ToString(IPVAS[i].COTAIPVADES);
                                        _IPVAS.Cells[IPVASno, 7].Value = Convert.ToString(IPVAS[i].DTVENCTOIPVA);
                                    }
                                }

                                #endregion

                                #region 5. MULTAS 

                                // 5. MULTAS 
                                if (MULTAS != null)
                                {
                                    //ExcelWorksheet _MULTAS = package.Workbook.Worksheets.Add("MULTAS");
                                    for (int i = 0; i < MULTAS.Length; i++)
                                    {
                                        var MULTASno = i + 2;

                                        _MULTAS.Cells[MULTASno, 1].Value = Convert.ToString(_lstDocumentXMLResponse[v].DocumentCode);
                                        _MULTAS.Cells[MULTASno, 2].Value = Convert.ToString(MULTAS[i].ORIGEM);
                                        _MULTAS.Cells[MULTASno, 3].Value = Convert.ToString(MULTAS[i].NUMGUIA);
                                        _MULTAS.Cells[MULTASno, 4].Value = Convert.ToString(MULTAS[i].NUMAIIP);
                                        _MULTAS.Cells[MULTASno, 5].Value = Convert.ToString(MULTAS[i].CODREC);
                                        _MULTAS.Cells[MULTASno, 6].Value = Convert.ToString(MULTAS[i].DTINFRA);
                                        _MULTAS.Cells[MULTASno, 7].Value = Convert.ToString(MULTAS[i].LOCALINFRA);
                                        _MULTAS.Cells[MULTASno, 8].Value = Convert.ToString(MULTAS[i].HORAINFRA);
                                        _MULTAS.Cells[MULTASno, 9].Value = Convert.ToString(MULTAS[i].MUNINFRA);
                                        _MULTAS.Cells[MULTASno, 10].Value = Convert.ToString(MULTAS[i].ENQUADRAM);
                                        _MULTAS.Cells[MULTASno, 11].Value = Convert.ToString(MULTAS[i].ENQUADRAMDESC);
                                        _MULTAS.Cells[MULTASno, 12].Value = Convert.ToString(MULTAS[i].ENQUADRAMLEG);
                                        _MULTAS.Cells[MULTASno, 13].Value = Convert.ToString(MULTAS[i].DTVENCTOINFRA);
                                        _MULTAS.Cells[MULTASno, 14].Value = Convert.ToString(MULTAS[i].VALMULTA);
                                        _MULTAS.Cells[MULTASno, 15].Value = Convert.ToString(MULTAS[i].ENTTRANSITANT);
                                        _MULTAS.Cells[MULTASno, 16].Value = Convert.ToString(MULTAS[i].NOMEENTTRANSITANT);
                                        _MULTAS.Cells[MULTASno, 17].Value = Convert.ToString(MULTAS[i].MUNINFRADESC);
                                        _MULTAS.Cells[MULTASno, 18].Value = Convert.ToString(MULTAS[i].EMPRESA);
                                        _MULTAS.Cells[MULTASno, 19].Value = (MULTAS[i].NOTIFICACAO == "0" ? "Infra��o" : "Notifica��o");
                                    }
                                }

                                #endregion

                                // 6. MULTASRENAINF
                                // ExcelWorksheet _MULTASRENAINF = package.Workbook.Worksheets.Add("MULTASRENAINF");

                                // 7. BLOQUEIOSRENAJUD
                                //ExcelWorksheet _BLOQUEIOSRENAJUD = package.Workbook.Worksheets.Add("BLOQUEIOSRENAJUD");

                                // 8. BLOQUEIOSDETRAN
                                //ExcelWorksheet _BLOQUEIOSDETRAN = package.Workbook.Worksheets.Add("BLOQUEIOSDETRAN");
                                if (BLOQUEIOSDETRAN != null)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN)))
                                    {
                                        _BLOQUEIOSDETRAN.Cells[no, 1].Value = Convert.ToString(_lstDocumentXMLResponse[v].DocumentCode);
                                        _BLOQUEIOSDETRAN.Cells[no, 2].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.TIPO);
                                        _BLOQUEIOSDETRAN.Cells[no, 3].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.MOTIVO);
                                        _BLOQUEIOSDETRAN.Cells[no, 4].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.DATAINCLUSAO);
                                        _BLOQUEIOSDETRAN.Cells[no, 5].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.PROTOCOLO_NUMERO);
                                        _BLOQUEIOSDETRAN.Cells[no, 6].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.PROTOCOLO_ANO);
                                        _BLOQUEIOSDETRAN.Cells[no, 7].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.PROCESSO_NUMERO);
                                        _BLOQUEIOSDETRAN.Cells[no, 8].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.PROCESSO_ANO);
                                        _BLOQUEIOSDETRAN.Cells[no, 9].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.OFICIO_NUMERO);
                                        _BLOQUEIOSDETRAN.Cells[no, 10].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.OFICIO_ANO);
                                        _BLOQUEIOSDETRAN.Cells[no, 11].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.IDCIDADE);
                                        _BLOQUEIOSDETRAN.Cells[no, 12].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.MUNICIPIO);
                                    }
                                }
                            }
                            else
                            {
                                VEICULO.Cells[no, 1].Value = Convert.ToString(_lstDocumentXMLResponse[v].DocumentCode);
                            }
                        }
                        package.Save();
                    }
                }

                return new { status = 1, filename = sFileName, filepath = _env.WebRootPath + $@"\uploadExcel\" + sFileName, fullFilePath = $@"..\..\uploadExcel\" + sFileName };

            }
            catch (Exception ex)
            {
                return new
                {
                    status = 0,
                    Msg = ex.Message.ToString()
                };
            }
        }

    }

    //[HttpGet]
    //public dynamic Get
}
