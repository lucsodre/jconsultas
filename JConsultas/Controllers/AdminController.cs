using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EntityLayer.Models;
using BusinessLogicLayer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Hosting;

namespace JConsultas.Controllers
{
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private IHostingEnvironment _env;
        private readonly string _externalCookieScheme;

        private readonly UserBLL _UserBLL;
        private IOptions<ApplicationSettings> _settings;

        public AdminController(UserManager<ApplicationUser> userManager,
           SignInManager<ApplicationUser> signInManager,
           IOptions<ApplicationSettings> settings, IHostingEnvironment env, EntityLayer.Models.JConsultasContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _UserBLL = new UserBLL(context);
            _settings = settings;
            _env = env;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Index(string returnUrl = null)
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(AccountController.Index), "Account");

            LoginViewModel model = new LoginViewModel();
            if (Request.Cookies["Identity.External"] != null)
            {
                Response.Cookies.Delete("Identity.External");
            }

            if (!string.IsNullOrEmpty(_externalCookieScheme))
            {
                await HttpContext.Authentication.SignOutAsync(_externalCookieScheme);
            }
            if (Request.Cookies["UserName"] != null && Request.Cookies["Password"] != null)
            {
                model.Email = Request.Cookies["UserName"].ToString();
                model.Password = Request.Cookies["Password"].ToString();
                model.RememberMe = true;
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(model);

            //await _signInManager.SignOutAsync();
            //return RedirectToAction(nameof(AccountController.Index), "Account");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    bool IsActive;
                    string sUseDoubleVerification = "";

                    UserMaster _usermaster = _UserBLL.UserDetailsOnEmailId(model.Email.ToString());
                    if (_usermaster != null)
                    {
                        IsActive = _usermaster.IsActive.Value;
                        sUseDoubleVerification = Convert.ToString(_usermaster.UseDoubleVerification);
                    }
                    else
                        IsActive = false;

                    if (IsActive)
                    {
                        if (model.RememberMe)
                        {
                            Response.Cookies.Append("UserName", model.Email);
                            Response.Cookies.Append("Password", model.Password);
                        }
                        else
                        {
                            Response.Cookies.Delete("UserName");
                            Response.Cookies.Delete("Password");
                        }
                        _UserBLL.UserSetLastLogin(model.Email.ToString());
                        return RedirectToLocal(returnUrl);

                        if (!string.IsNullOrEmpty(sUseDoubleVerification))
                        {
                            if (sUseDoubleVerification == "No")
                            {
                                if (model.RememberMe)
                                {
                                    Response.Cookies.Append("UserName", model.Email);
                                    Response.Cookies.Append("Password", model.Password);
                                }
                                else
                                {
                                    Response.Cookies.Delete("UserName");
                                    Response.Cookies.Delete("Password");
                                }
                                _UserBLL.UserSetLastLogin(model.Email.ToString());
                                return RedirectToLocal(returnUrl);
                            }
                            else
                            {
                                string userAgent = Request.Headers["User-Agent"];
                                UserAgent.UserAgent ua = new UserAgent.UserAgent(userAgent);

                                string Browser = ua.Browser.Name.ToString();
                                string OperatingSystem = ua.OS.Name.ToString() + "-" + ua.OS.Version.ToString();

                                Random generator = new Random();
                                String Code = generator.Next(0, 1000000).ToString("D6");

                                if (_UserBLL.SaveVerificationCode(_usermaster.Id, _usermaster.ProfileId, Code))
                                {
                                    if (sUseDoubleVerification == "By_email")
                                    {
                                        await _signInManager.SignOutAsync();

                                        string sPath = _env.WebRootPath + @"\EmailTemplte\VerificationCode.html";
                                        string sHtml = System.IO.File.ReadAllText(sPath);
                                        sHtml = sHtml.Replace("#Browser ", Browser);
                                        sHtml = sHtml.Replace("#os", OperatingSystem);
                                        sHtml = sHtml.Replace("#Username", _usermaster.ApiUser);
                                        sHtml = sHtml.Replace("#code1", Code.Substring(0, 3));
                                        sHtml = sHtml.Replace("#code2", Code.Substring(3, 3));

                                        bool bIsSend = false;
                                        // bIsSend = HomeController.Send(_usermaster.ApiUser, "Verify your identity in Jconsultas", sHtml);

                                        return RedirectToAction("verifyCode", new { ProfileId = _usermaster.ProfileId });
                                    }
                                    else
                                    {
                                        ModelState.AddModelError(string.Empty, "Usu�rio e /ou senha inv�lidos.");
                                        return View(model);
                                    }
                                }
                                else
                                {
                                    ModelState.AddModelError(string.Empty, "Usu�rio e /ou senha inv�lidos.");
                                    return View(model);
                                }
                            }
                        }
                        else
                        {
                            if (model.RememberMe)
                            {
                                Response.Cookies.Append("UserName", model.Email);
                                Response.Cookies.Append("Password", model.Password);
                            }
                            else
                            {
                                Response.Cookies.Delete("UserName");
                                Response.Cookies.Delete("Password");
                            }
                            _UserBLL.UserSetLastLogin(model.Email.ToString());
                            return RedirectToLocal(returnUrl);
                        }
                    }
                    else
                    {
                        await _signInManager.SignOutAsync();

                        ModelState.AddModelError(string.Empty, "Usu�rio e /ou senha inv�lidos.");
                        return View(model);
                    }


                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Usu�rio e /ou senha inv�lidos.");
                    return View(model);
                }
            }
            return View(model);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
    }
}