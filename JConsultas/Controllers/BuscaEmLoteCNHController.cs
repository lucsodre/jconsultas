using BusinessLogicLayer;
using EntityLayer.Models;
using JConsultas.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Xml.Serialization;

namespace JConsultas.Controllers
{
    [Produces("application/json")]
    public class BuscaEmLoteCNHController : Controller
    {
        private readonly ConsultaLoteCNHBLL _ConsultaLoteCNHBLL;
        private readonly ConsultaCNHBLL _ConsultaCNHBLL;
        private IHostingEnvironment _env;

        public BuscaEmLoteCNHController(IHostingEnvironment env, EntityLayer.Models.JConsultasContext context)
        {
            this._env = env;
            _ConsultaLoteCNHBLL = new ConsultaLoteCNHBLL(context);
            _ConsultaCNHBLL = new ConsultaCNHBLL(context);
        }

        [HttpPost]
        public bool SaveExcelData([FromBody]ImpoerExcel data)
        {
            var claimsIdentity = (ClaimsIdentity)this.User.Identity;
            var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
            var userId = claim.Value;

            return _ConsultaLoteCNHBLL.SaveMultipledata(data, userId);
        }

        [HttpPost]
        public dynamic RetryProcess(string Id)
        {
            try
            {
                if (_ConsultaLoteCNHBLL.RetryProcess(Id))
                    return new { status = 1 };
                else
                    return new { status = 0 };
            }
            catch (Exception ex)
            {
                return new { status = 0, Msg = ex.Message.ToString() };
            };
        }

        public static CONSULTA1650 DeserializarXMLRetorno(string xml)
        {
            try
            {
                CONSULTA1650 retorno = null;
                XmlSerializer serializer = new XmlSerializer(typeof(CONSULTA1650));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                retorno = (CONSULTA1650)serializer.Deserialize(reader);
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }

        [HttpPost]
        public dynamic ExportExcel(string QId)
        {
            try
            {
                string sFileName = "";
                DocumentXMLResponselst _DocumentXMLResponselst = new DocumentXMLResponselst();
                List<DocumentXMLResponse> _lstDocumentXMLResponse = new List<DocumentXMLResponse>();

                List<MultipleData> _lst = new List<MultipleData>();
                _lst = _ConsultaLoteCNHBLL.GetMultipleData(QId);
                if (_lst.Count > 0 && _lst != null)
                {
                    for (int i = 0; i < _lst.Count; i++)
                    {
                        string sDocCode = Convert.ToString(_lst[i].CPF);

                        DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();
                        _DocumentXMLResponse = _ConsultaCNHBLL.GetDocumentFromDB(sDocCode);
                        _lstDocumentXMLResponse.Add(_DocumentXMLResponse);
                    }
                }

                if (_lstDocumentXMLResponse.Count > 0)
                {
                    sFileName = "Excel_" + DateTime.Now.ToString("dd_MM_yyyy_H_mm_ss") + ".xlsx";
                    string sWebRootFolder = _env.WebRootPath + $@"\uploadExcel\";
                    string dir = _env.WebRootPath + $@"\uploadExcel";
                    
                    string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
                    FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

                    if (file.Exists)
                    {
                        file.Delete();
                        file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                    }                    

                    List<ExcelWorksheet> works = new List<ExcelWorksheet>();

                    using (ExcelPackage package = new ExcelPackage(file))
                    {
                        for (int v = 0; v < _lstDocumentXMLResponse.Count; v++)
                        {
                            var r = _lstDocumentXMLResponse[v].xml != null ? DeserializarXMLRetorno(_lstDocumentXMLResponse[v].xml) : null;

                            RESULTADO resultado = new RESULTADO();

                            if (r != null)
                            {
                                resultado = r.CONSULTAS.CONSULTA.RESULTADO;
                            }
                            else
                            {
                                continue;
                            }

                            if (v == 0)
                            {
                                PropertyInfo[] properties = resultado.GetType().GetProperties();

                                foreach (var propertyInfo in properties)
                                {
                                    var sheet = package.Workbook.Worksheets.Add(propertyInfo.Name);
                                    works.Add(sheet);

                                    var detalhe = propertyInfo.PropertyType.GetProperties();

                                    int coluna = 1;

                                    foreach (var prop in detalhe)
                                    {
                                        int colunaLista = 1;

                                        if (prop.PropertyType.Name.Contains("List"))
                                        {
                                            if (prop.Name == "RECURSO")
                                            {
                                                var propertyRecursos = GetProperties("JConsultas.Models", prop.Name);
                                                foreach (var item in propertyRecursos)
                                                {
                                                    var a = GetProperties("JConsultas.Models", item.Name);
                                                    foreach (var b in a)
                                                    {
                                                        if (b.Name == "EVENTO")
                                                        {
                                                            var c = GetProperties("JConsultas.Models", "EVENTO");
                                                            foreach (var d in c)
                                                            {
                                                                sheet.Cells[1, colunaLista].Value = "EVENTO_" + d.Name;
                                                                colunaLista++;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            sheet.Cells[1, colunaLista].Value = item.Name + "_" + b.Name;
                                                            colunaLista++;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var propertyInfos = GetProperties("JConsultas.Models", prop.Name);
                                                foreach (var item in propertyInfos)
                                                {
                                                    sheet.Cells[1, colunaLista].Value = item.Name;
                                                    colunaLista++;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            sheet.Cells[1, coluna].Value = prop.Name;
                                            coluna++;
                                        }
                                    }
                                }
                            }                           

                            var planilha = works.Find(x => x.Name == "INFRACOES");
                            PreenchePlanilha<INFRACAO>(resultado.INFRACOES.INFRACAO, planilha);

                            planilha = works.Find(x => x.Name == "EXAMES");
                            PreenchePlanilha<EXAME>(resultado.EXAMES.EXAME, planilha);

                            planilha = works.Find(x => x.Name == "CURSOS");
                            PreenchePlanilha<CURSO>(resultado.CURSOS.CURSO, planilha);

                            planilha = works.Find(x => x.Name == "BLOQUEIOS");
                            PreenchePlanilha<BLOQUEIO>(resultado.BLOQUEIOS.BLOQUEIO, planilha);

                            planilha = works.Find(x => x.Name == "DADOSCONDUTOR");
                            PreenchePlanilha<DADOSCONDUTOR>(resultado.DADOSCONDUTOR, planilha);

                            planilha = works.Find(x => x.Name == "DADOSCNH");
                            PreenchePlanilha<DADOSCNH>(resultado.DADOSCNH, planilha);

                            planilha = works.Find(x => x.Name == "PONTUACAO");
                            PreenchePlanilha<PONTUACAO>(resultado.PONTUACAO, planilha);

                            planilha = works.Find(x => x.Name == "RECURSOS");
                            PreencheRecursoPlanilha(resultado.RECURSOS?.RECURSO, planilha);

                            package.Save();
                        }
                    }
                }

                return new { status = 1, filename = sFileName, filepath = _env.WebRootPath + $@"\uploadExcel\" + sFileName, fullFilePath = $@"..\..\uploadExcel\" + sFileName };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = 0,
                    Msg = ex.Message.ToString()
                };
            }
        }

        public static void PreenchePlanilha<T>(List<T> resultado, ExcelWorksheet planilha)
        {
            if (resultado != null)
            {
                int linha = 2;
                foreach (var objetos in resultado)
                {
                    var propInfracao = objetos.GetType().GetProperties();
                    int col = 1;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(objetos, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }
                    linha++;
                }
            }
        }

        public static void PreenchePlanilha<T>(T resultado, ExcelWorksheet planilha)
        {
            if (resultado != null)
            {
                int linha = 2;

                var propInfracao = resultado.GetType().GetProperties();
                int col = 1;
                foreach (var item in propInfracao)
                {
                    var val = GetPropValue(resultado, item.Name);
                    if (val != null)
                    {
                        planilha.Cells[linha, col].Value = val.ToString();
                    }
                    col++;
                }
                linha++;
            }
        }

        public static void PreencheRecursoPlanilha(List<RECURSO> resultado, ExcelWorksheet planilha)
        {
            if (resultado != null)
            {
                int linha = 2;
                foreach (var objetos in resultado)
                {
                    var veic = objetos.VEICULO;
                    var infracao = objetos.INFRACAO_RECURSO;
                    var dados = objetos.DADOS_PROCESSO;
                    var eventos = objetos.EVENTOS?.EVENTO;

                    var propInfracao = veic.GetType().GetProperties();
                    int col = 1;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(veic, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }

                    propInfracao = infracao.GetType().GetProperties();
                    col = 5;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(infracao, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }

                    propInfracao = dados.GetType().GetProperties();
                    col = 12;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(dados, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }


                    if (eventos != null)
                    {
                        foreach (var ev in eventos)
                        {
                            propInfracao = ev.GetType().GetProperties();
                            col = 18;
                            foreach (var item in propInfracao)
                            {
                                var val = GetPropValue(ev, item.Name);
                                if (val != null)
                                {
                                    planilha.Cells[linha, col].Value = val.ToString();
                                }
                                col++;
                            }
                            linha++;
                        }
                    }

                    linha++;
                }
            }
        }

        public static object GetPropValue(object source, string propertyName)
        {
            var property = source.GetType().GetRuntimeProperties().FirstOrDefault(p => string.Equals(p.Name, propertyName, StringComparison.OrdinalIgnoreCase));
            if (property != null)
            {
                return property.GetValue(source);
            }
            return null;
        }

        private static PropertyInfo[] GetProperties(string nameSpace, string className)
        {
            return Type.GetType(nameSpace + "." + className).GetProperties();
        }
    }
}
