using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EntityLayer.Models;
using JConsultas.Models;
using BusinessLogicLayer;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Options;

namespace JConsultas.Controllers
{
    [Produces("application/json")]
    [Route("api/ConsultaVeiculo")]
    public class ConsultaVeiculoController : Controller
    {
        private readonly SingleSearchDocumentBLL _SingleSearchDocumentBLL;
        private IHostingEnvironment _env;
        private static IOptions<ApplicationSettings> _settings;

        public ConsultaVeiculoController(IHostingEnvironment env, EntityLayer.Models.JConsultasContext _context, IOptions<ApplicationSettings> settings)
        {
            this._env = env;
            _SingleSearchDocumentBLL = new SingleSearchDocumentBLL(_context);
            _settings = settings;
        }

        [HttpGet]
        public dynamic GetData()
        {
            var Usuario = Convert.ToString(Request.Headers["Usuario"]);
            //var Senha = Convert.ToString(Request.Headers["Senha"]);
            //var codigo = Convert.ToString(Request.Headers["Senha"]);

            //var Output = "Output:" + Usuario.ToString() + Senha.ToString();

            return true;
        }

        [HttpGet("{DocumentCode}")]
        [Produces("application/json", "application/xml")]
        public dynamic GetData(string DocumentCode)
        {
            //var id = Convert.ToString(Request.Headers["id"]);
            var Usuario = Convert.ToString(Request.Headers["Usuario"]);
            var Senha = Convert.ToString(Request.Headers["Senha"]);

            //var result = _SingleSearchDocumentBLL.GetDocumentFromService(DocumentCode, apiUsuario, apiSenha, codigo);
            ServiceResponse objResponse = new ServiceResponse();
            objResponse = _SingleSearchDocumentBLL.GetDocumentFromServiceOnAuthentication(DocumentCode, Usuario, Senha, _settings.Value.ApiUsuario, _settings.Value.ApiSenha, _settings.Value.ApiCodigo);

            WebServiceResponse _res = new WebServiceResponse();
            _res.status = objResponse.status;
            _res.Message = objResponse.Message;
            if (objResponse.status)
            {
                CONSULTA2300 r = DeserializarXML.DeserializarXMLRetorno(objResponse.xml);
                _res.xml = r;

                //string json = JsonConvert.SerializeObject(r, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
                //_res.xml = json;
            }
            return _res;
        }
    }

    public class WebServiceResponse
    {
        public bool status { get; set; }
        public string Message { get; set; }
        public CONSULTA2300 xml { get; set; }
    }

}