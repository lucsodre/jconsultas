using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EntityLayer.Models;
using BusinessLogicLayer;
using JConsultas.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using System.IO;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using EntityLayer.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Amazon.SimpleNotificationService.Model;

namespace JConsultas.Controllers
{
    [Produces("application/json")]
    //[Route("api/User")]
    public class UserController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly string _externalCookieScheme;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private IOptions<ApplicationSettings> _settings;
        private IHostingEnvironment _env;
        private readonly UserBLL _userbll;
        private readonly ProfileBLL _profilebll;
        private SMSHelper _smsHelper;

        public UserController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IHostingEnvironment hostingEnvironment,
            IOptions<ApplicationSettings> setting, IHostingEnvironment env, EntityLayer.Models.JConsultasContext context, SMSHelper smsHelper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _userbll = new UserBLL(context);
            _profilebll = new ProfileBLL(context);
            _hostingEnvironment = hostingEnvironment;
            _settings = setting;
            this._env = env;
            _smsHelper = smsHelper;
        }

        [HttpGet]
        public IEnumerable<UserList> GetUserList()
        {
            try
            {

                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                var r = _userbll.GetUserList(userId);

                r = _userbll.GetAuditorias(r.ToList());

                return r;
            }
            catch (Exception ex)
            {
                Helper.LogError("UserController.CS -> GetUserList()", ex.Message, _env);
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<UserList> GetActiveUserList()
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                var r = _userbll.GetUserList(userId).Where(u => u.IsActive == true).ToList();

                r = _userbll.GetAuditorias(r.ToList());
                return r;
            }
            catch (Exception ex)
            {
                Helper.LogError("UserController.CS -> GetActiveUserList()", ex.Message, _env);
                return null;
            }
        }

        [HttpGet]
        public dynamic GetUserLoginUserDetails()
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                var userdata = _userbll.GetAllUserList().ToList().Where(p => p.Id == userId).ToList();
                //var userdata = _userbll.GetAllUserList().ToList();
                int iProfileID = 0;
                int.TryParse(Convert.ToString(userdata[0].ProfileId), out iProfileID);
                var profiledata = _profilebll.GetUserProfileListOnProfile(iProfileID);

                return new { profiledata = profiledata, userdata = userdata };
            }
            catch (Exception ex)
            {
                Helper.LogError("UserController.CS -> GetUserList()", ex.Message, _env);
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<ProfileMaster> GetProfile()
        {
            try
            {
                var r = _userbll.GetProfile();
                return r;
            }
            catch (Exception ex)
            {
                Helper.LogError("UserController.CS -> GetProfile()", ex.Message, _env);
                return null;
            }
        }

        public async Task<UserResponse> SaveUser([FromBody]UserDetails data)
        {
           try
            {
                UserResponse _userresponse = new UserResponse();
                if (string.IsNullOrEmpty(data.Id))
                {
                    var user = new ApplicationUser { UserName = data.ApiUser, Email = data.ApiUser };
                    var result = await _userManager.CreateAsync(user, "--");
                    if (result.Succeeded)
                    {
                        data.Id = user.Id;
                        var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                        var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                        var userId = claim.Value;

                        if (string.IsNullOrEmpty(Convert.ToString(data.IsActive)))
                        {
                            data.IsActive = false;
                        }

                        if (data.ProfileId == 0)
                        {
                            data.CreatedBy = userId;
                            data.CreatedAt = DateTime.Now;

                            var CurrentIdAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                            data.IpAddress = CurrentIdAddress;

                        }
                        else
                        {
                            data.ModifiedAt = DateTime.Now;
                            data.ModifiedBy = userId;
                        }                        

                        _userresponse = _userbll.SaveUsers(data);
                        if (_userresponse.Respones)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(data.SendPasswordEmail)))
                            {
                                if (data.SendPasswordEmail.Value)
                                {
                                    string sURL = _settings.Value.sSiteURL.ToString();
                                    EncryptController e = new EncryptController();
                                    var EncID = e.EncryptValue(_userresponse.id.ToString());
                                    var dnm = e.Decrypt(EncID);
                                    string sContent = ObterTextoEmail(data.ApiUser, sURL, EncID);

                                    bool bIsSend = false;
                                    bIsSend = HomeController.Send(data.ApiUser, "SGDExpress - Redefini��o de senha", sContent);

                                    return _userresponse;
                                }
                                else
                                    return _userresponse;
                            }
                            else
                                return _userresponse;
                        }
                        else
                            return _userresponse;
                    }
                    else
                    {
                        _userresponse.Respones = false;
                        _userresponse.Error = result.Errors.ToList();
                        return _userresponse;
                    }
                }
                else
                {
                    var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                    var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                    var userId = claim.Value;

                    if (string.IsNullOrEmpty(Convert.ToString(data.IsActive)))
                    {
                        data.IsActive = false;
                    }

                    if (data.ProfileId == 0)
                    {
                        data.CreatedBy = userId;
                        data.CreatedAt = DateTime.Now;
                    }
                    else
                    {
                        data.ModifiedAt = DateTime.Now;
                        data.ModifiedBy = userId;
                    }

                    _userresponse = _userbll.UpdateUsers(data);
                    if (_userresponse.Respones)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(data.SendPasswordEmail)))
                        {
                            if (data.SendPasswordEmail.Value)
                            {
                                string sURL = _settings.Value.sSiteURL.ToString();
                                EncryptController e = new EncryptController();
                                var EncID = e.EncryptValue(_userresponse.id.ToString());
                                var dnm = e.Decrypt(EncID);

                                string sContent = ObterTextoEmail(data.ApiUser, sURL, EncID);

                                bool bIsSend = false;
                                bIsSend = HomeController.Send(data.ApiUser, "SGDExpress - Redefini��o de senha", sContent);

                                return _userresponse;
                            }
                            else
                                return _userresponse;
                        }
                        else
                            return _userresponse;
                    }
                    else
                        return _userresponse;
                }


            }
            catch (Exception ex)
            {
                Helper.LogError("AccountMasterController.CS -> SaveUser()", ex.Message, _env);
                return null;
            }
        }

        private static string ObterTextoEmail(string userName, string sURL, string EncID)
        {
            string sContent = "";
            sContent = sContent + "Ol� ," + userName;
            sContent = sContent + " <br><br> Bem-vindo ao SGDExpress!";
            sContent = sContent + " <br><br> Clique no link abaixo para redefinir sua senha e entrar no sistema.";
            sContent = sContent + " <br><br>" + sURL + "/Account/ForgetPassword?id=" + EncID + "&type=create";
            sContent = sContent + " <br><br> ";

            sContent = sContent + " <br><br>  Atenciosamente,<br> ";
            sContent = sContent + " Equipe SGDExpress<br> ";
            return sContent;
        }

        [HttpGet]
        public bool AuthenticationEmail(string id, string Email, string UserName)
        {
            try
            {
                string sURL = _settings.Value.sSiteURL.ToString();
                // Password creation email
                EncryptController e = new EncryptController();
                var EncID = e.EncryptValue(id.ToString());

                var dnm = e.Decrypt(EncID);

                string sContent = ObterTextoEmail(UserName, sURL,EncID);

                return HomeController.Send(Email, "SGDExpress - Redefini��o de senha", sContent);
            }
            catch (Exception ex)
            {
                Helper.LogError("UserController.CS -> AuthenticationEmail()", ex.Message, _env);
                return false;
            }
        }

        [HttpPost]
        public dynamic SendEmailVarification(int id)
        {
            List<UserList> _lstUser = _userbll.GetAllUserList().Where(x => x.ProfileId == id).ToList();
            if (_lstUser.Count > 0)
            {
                Random generator = new Random();
                String Code = generator.Next(0, 1000000).ToString("D6");

                if (_userbll.SaveVerificationCode(_lstUser[0].Id, _lstUser[0].ProfileId, Code))
                {
                    string userAgent = Request.Headers["User-Agent"];
                    UserAgent.UserAgent ua = new UserAgent.UserAgent(userAgent);

                    string Browser = ua.Browser.Name.ToString();
                    string OperatingSystem = ua.OS.Name.ToString() + "-" + ua.OS.Version.ToString();

                    string sPath = _env.WebRootPath + @"\EmailTemplte\EmailVarfication.html";
                    string sHtml = System.IO.File.ReadAllText(sPath);
                    sHtml = sHtml.Replace("#Username", _lstUser[0].ApiUser);
                    sHtml = sHtml.Replace("#code1", Code.Substring(0, 3));
                    sHtml = sHtml.Replace("#code2", Code.Substring(3, 3));
                    sHtml = sHtml.Replace("#Browser ", Browser);
                    sHtml = sHtml.Replace("#os", OperatingSystem);

                    bool bIsSend = false;
                    bIsSend = HomeController.Send(_lstUser[0].ApiUser, "Valide sua identifica��o no SGDExpress", sHtml);
                    if (bIsSend)
                        return new { Result = 1, Msg = "Opera��o inv�lida" };
                    else
                        return new { Result = 0, Msg = "Falha no envio do email de verifica��o." };
                }
                else
                    return new { Result = 0, Msg = "Falha na gera��o e envio do c�digo de verifica��o." };

            }
            else
                return new { Result = 0, Msg = "Opera��o inv�lida" };
        }

        [HttpPost]
        public dynamic CheckEmailVerficationCode(int id, string code)
        {
            return _userbll.CheckVerificationCode(id.ToString(), code, "Email");
        }

        [HttpPost]
        public async Task<dynamic> SendPhoneVarification(int id)
        {
            List<UserList> _lstUser = _userbll.GetAllUserList().Where(x => x.ProfileId == id).ToList();
            if (_lstUser.Count > 0)
            {
                Random generator = new Random();
                String Code = generator.Next(0, 1000000).ToString("D6");

                if (_userbll.SaveVerificationCode(_lstUser[0].Id, _lstUser[0].ProfileId, Code))
                {
                    //string sPath = _env.WebRootPath + @"\EmailTemplte\EmailVarfication.html";
                    //string sHtml = System.IO.File.ReadAllText(sPath);
                    //sHtml = sHtml.Replace("#Username", _lstUser[0].ApiUser);
                    //sHtml = sHtml.Replace("#code1", Code.Substring(0, 3));
                    //sHtml = sHtml.Replace("#code2", Code.Substring(3, 3));

                    // Now sms send to user
                    string sSMSTemplate = "Seu c�digo de verifica��o para SGDExpress �: " + Code;
                    string sMobileNo = _lstUser[0].MobileNo.ToString();
                    PublishResponse response = await _smsHelper.Send(sSMSTemplate, sMobileNo);
                    //PublishResponse response = await _onj.send(sSMSTemplate, "");

                    if (!string.IsNullOrEmpty(response.MessageId))
                        return new { Result = 1, Msg = "Opera��o inv�lida" };
                    else
                        return new { Result = 0, Msg = "Falha no envio de SMS de verifica��o." };
                }
                else
                    return new { Result = 0, Msg = "Falha na gera��o e envio do c�digo de verifica��o." };

            }
            else
                return new { Result = 0, Msg = "Opera��o inv�lida" };
        }

        [HttpPost]
        public dynamic CheckPhoneVerficationCode(int id, string code)
        {
            return _userbll.CheckVerificationCode(id.ToString(), code, "Phone");
        }
    }
}