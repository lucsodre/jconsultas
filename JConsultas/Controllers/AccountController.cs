using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EntityLayer.Models;
using BusinessLogicLayer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Hosting;
using JConsultas.Models;
using Amazon.SimpleNotificationService.Model;

namespace JConsultas.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private IHostingEnvironment _env;
        private readonly string _externalCookieScheme;

        private SMSHelper _smsHelper;
        EncryptController objEncryptController = new EncryptController();

        private readonly UserBLL _UserBLL;

        private IOptions<ApplicationSettings> _settings;

        public AccountController(
           UserManager<ApplicationUser> userManager,
           SignInManager<ApplicationUser> signInManager,
           IOptions<ApplicationSettings> settings, IHostingEnvironment env,
           EntityLayer.Models.JConsultasContext _context, SMSHelper smsHelper
        )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _UserBLL = new UserBLL(_context);
            _settings = settings;
            _env = env;
            _smsHelper = smsHelper;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Index(string returnUrl = null)
        {
            //var httpConnectionFeature = HttpContext.Features.Get<IHttpConnectionFeature>();
            //var localIpAddress = httpConnectionFeature?.LocalIpAddress.ToString();

            //var remoteIpAddress = HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress;
            LoginViewModel model = new LoginViewModel();
            if (Request.Cookies["Identity.External"] != null)
            {
                Response.Cookies.Delete("Identity.External");
            }

            if (!string.IsNullOrEmpty(_externalCookieScheme))
            {
                await HttpContext.Authentication.SignOutAsync(_externalCookieScheme);
            }
            if (Request.Cookies["UserName"] != null && Request.Cookies["Password"] != null)
            {
                model.Email = Request.Cookies["UserName"].ToString();
                model.Password = Request.Cookies["Password"].ToString();
                model.RememberMe = true;
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            if (Request.Cookies["Identity.External"] != null)
            {
                Response.Cookies.Delete("Identity.External");
            }
            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> lockscreen(string returnUrl = null)
        {
            if (Request.Cookies["Identity.External"] != null)
            {
                Response.Cookies.Delete("Identity.External");
            }

            // Clear the existing external cookie to ensure a clean login process
            if (!string.IsNullOrEmpty(_externalCookieScheme))
            {
                await HttpContext.Authentication.SignOutAsync(_externalCookieScheme);
            }
            await _signInManager.SignOutAsync();


            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> lockscreen()
        {

            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(AccountController.lockscreen), "Account");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(LoginViewModel model, string returnUrl = null)
        {
            var CurrentIdAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    bool IsActive;
                    string sUseDoubleVerification = "";

                    UserMaster _usermaster = _UserBLL.UserDetailsOnEmailId(model.Email.ToString());
                    if (_usermaster != null)
                    {
                        IsActive = _usermaster.IsActive.Value;
                        sUseDoubleVerification = Convert.ToString(_usermaster.UseDoubleVerification);
                    }
                    else
                        IsActive = false;

                    if (IsActive)
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(_usermaster.LastLogin)))
                        {
                            _UserBLL.UserSetLastLoginWithIpAddress(_usermaster.ApiUser.ToString(), CurrentIdAddress);
                            return RedirectToLocal(returnUrl);
                        }
                        else
                        {
                            if (!_UserBLL.CheckIpAddressIsExistOrNot(_usermaster.ProfileId, CurrentIdAddress))
                            {
                                if (!string.IsNullOrEmpty(sUseDoubleVerification))
                                {
                                    if (sUseDoubleVerification == "No")
                                    {
                                        if (model.RememberMe)
                                        {
                                            Response.Cookies.Append("UserName", model.Email);
                                            Response.Cookies.Append("Password", model.Password);
                                        }
                                        else
                                        {
                                            Response.Cookies.Delete("UserName");
                                            Response.Cookies.Delete("Password");
                                        }
                                        _UserBLL.UserSetLastLogin(model.Email.ToString());
                                        return RedirectToLocal(returnUrl);
                                    }
                                    else
                                    {
                                        string userAgent = Request.Headers["User-Agent"];
                                        UserAgent.UserAgent ua = new UserAgent.UserAgent(userAgent);

                                        string Browser = ua.Browser.Name.ToString();
                                        string OperatingSystem = ua.OS.Name.ToString() + "-" + ua.OS.Version.ToString();

                                        Random generator = new Random();
                                        String Code = generator.Next(0, 1000000).ToString("D6");

                                        //if (_usermaster.UseDoubleVerification == "By_email")
                                        //{
                                        //    if (string.IsNullOrEmpty(Convert.ToString(_usermaster.ApiUser)))
                                        //    {
                                        //        ModelState.AddModelError(string.Empty, "Please register email address and confirm it.");
                                        //        return View(model);
                                        //    }
                                        //    else if (_usermaster.IsEmailVerifeyed.Value == false)
                                        //    {
                                        //        ModelState.AddModelError(string.Empty, "Please verify email address first.");
                                        //        return View(model);
                                        //    }
                                        //}
                                        if (_usermaster.UseDoubleVerification == "By_SMS")
                                        {
                                            //if (string.IsNullOrEmpty(Convert.ToString(_usermaster.MobileNo)) )
                                            //{
                                            //    ModelState.AddModelError(string.Empty, "Please register mobile number first.");
                                            //    return View(model);
                                            //}
                                            if (_usermaster.IsPhonelVerifeyed.Value == false)
                                            {
                                                if (model.RememberMe)
                                                {
                                                    Response.Cookies.Append("UserName", model.Email);
                                                    Response.Cookies.Append("Password", model.Password);
                                                }
                                                else
                                                {
                                                    Response.Cookies.Delete("UserName");
                                                    Response.Cookies.Delete("Password");
                                                }
                                                _UserBLL.UserSetLastLogin(model.Email.ToString());
                                                return RedirectToLocal(returnUrl);


                                            }
                                        }

                                        if (_UserBLL.SaveVerificationCode(_usermaster.Id, _usermaster.ProfileId, Code))
                                        {
                                            if (sUseDoubleVerification == "By_email")
                                            {
                                                await _signInManager.SignOutAsync();

                                                string sPath = _env.WebRootPath + @"\EmailTemplte\VerificationCode.html";
                                                string sHtml = System.IO.File.ReadAllText(sPath);
                                                sHtml = sHtml.Replace("#Browser ", Browser);
                                                sHtml = sHtml.Replace("#os", OperatingSystem);
                                                sHtml = sHtml.Replace("#Username", _usermaster.ApiUser);
                                                sHtml = sHtml.Replace("#code1", Code.Substring(0, 3));
                                                sHtml = sHtml.Replace("#code2", Code.Substring(3, 3));

                                                bool bIsSend = false;
                                                bIsSend = HomeController.Send(_usermaster.ApiUser, "Identifique-se no Jconsultas", sHtml);

                                                string sPid = objEncryptController.EncryptValue(_usermaster.ProfileId.ToString());
                                                return RedirectToAction("verifyCode", new { ProfileId = sPid });
                                            }
                                            else
                                            {
                                                // Now sms send to user
                                                string sSMSTemplate = "Seu c�digo de verifica��o para o Jconsultas �: " + Code + " ,Browser: " + Browser + ",Sistema operacional: " + OperatingSystem + ",Usuario: " + _usermaster.ApiUser;
                                                string sMobileNo = _usermaster.MobileNo.ToString();

                                                PublishResponse response = await _smsHelper.Send(sSMSTemplate, sMobileNo);

                                                string sPid = objEncryptController.EncryptValue(_usermaster.ProfileId.ToString());
                                                return RedirectToAction("verifyCode", new { ProfileId = sPid });
                                            }
                                        }
                                        else
                                        {
                                            ModelState.AddModelError(string.Empty, "Usu�rio e/ou senha inv�lidos.");
                                            return View(model);
                                        }
                                    }
                                }
                                else
                                {
                                    if (model.RememberMe)
                                    {
                                        Response.Cookies.Append("UserName", model.Email);
                                        Response.Cookies.Append("Password", model.Password);
                                    }
                                    else
                                    {
                                        Response.Cookies.Delete("UserName");
                                        Response.Cookies.Delete("Password");
                                    }
                                    _UserBLL.UserSetLastLogin(model.Email.ToString());
                                    return RedirectToLocal(returnUrl);
                                }
                            }
                            else
                            {
                                if (model.RememberMe)
                                {
                                    Response.Cookies.Append("UserName", model.Email);
                                    Response.Cookies.Append("Password", model.Password);
                                }
                                else
                                {
                                    Response.Cookies.Delete("UserName");
                                    Response.Cookies.Delete("Password");
                                }
                                _UserBLL.UserSetLastLogin(model.Email.ToString());
                                return RedirectToLocal(returnUrl);
                            }
                        }
                    }
                    else
                    {
                        await _signInManager.SignOutAsync();

                        ModelState.AddModelError(string.Empty, "Usu�rio e /ou senha inv�lidos.");
                        return View(model);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Usu�rio e /ou senha inv�lidos.");
                    return View(model);
                }
            }
            else
                return View(model);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("SingleDocumentSearch", "Home");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            var redirectUrl = Url.Action(nameof(ExternalLoginCallback), "Account", new { ReturnUrl = returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }

        //
        // GET: /Account/ExternalLoginCallback
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                ModelState.AddModelError(string.Empty, $"Erro em provedor externo: {remoteError}");
                return View(nameof(Index));
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(nameof(Index));
            }

            var email = info.Principal.FindFirstValue(ClaimTypes.Email);
            var r = _UserBLL.GetAllUserList().ToList().Where(p => p.ApiUser == email).ToList();
            if (r.Count == 0)
            {
                ModelState.AddModelError(string.Empty, $"Voc� n�o est� registrado. Por favor, contacte o administrador.");
                return View(nameof(Index));
            }

            if (r.Count > 0)
            {
                var result = await _signInManager.PasswordSignInAsync(r[0].ApiUser, r[0].ApiPassword, false, lockoutOnFailure: false);
                if (result.Succeeded)
                {

                    bool IsActive;
                    string sUseDoubleVerification = "";

                    UserMaster _usermaster = _UserBLL.UserDetailsOnEmailId(r[0].ApiUser.ToString());
                    if (_usermaster != null)
                    {
                        IsActive = _usermaster.IsActive.Value;
                        sUseDoubleVerification = Convert.ToString(_usermaster.UseDoubleVerification);
                    }
                    else
                        IsActive = false;

                    if (IsActive)
                    {


                        var CurrentIdAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                        if (!_UserBLL.CheckIpAddressIsExistOrNot(_usermaster.ProfileId, CurrentIdAddress))
                        {
                            if (!string.IsNullOrEmpty(sUseDoubleVerification))
                            {
                                if (sUseDoubleVerification == "No")
                                {
                                    _UserBLL.UserSetLastLogin(r[0].ApiUser.ToString());
                                    return RedirectToLocal(returnUrl);
                                }
                                else
                                {
                                    string userAgent = Request.Headers["User-Agent"];
                                    UserAgent.UserAgent ua = new UserAgent.UserAgent(userAgent);

                                    string Browser = ua.Browser.Name.ToString();
                                    string OperatingSystem = ua.OS.Name.ToString() + "-" + ua.OS.Version.ToString();

                                    Random generator = new Random();
                                    String Code = generator.Next(0, 1000000).ToString("D6");

                                    //if (_usermaster.UseDoubleVerification == "By_email")
                                    //{
                                    //    if (string.IsNullOrEmpty(Convert.ToString(_usermaster.ApiUser)))
                                    //    {
                                    //        ModelState.AddModelError(string.Empty, "Please register email address and confirm it.");
                                    //        return View(nameof(Index));
                                    //    }
                                    //    else if (_usermaster.IsEmailVerifeyed.Value == false)
                                    //    {
                                    //        ModelState.AddModelError(string.Empty, "Please verify email address first.");
                                    //        return View(nameof(Index));
                                    //    }
                                    //}
                                    if (_usermaster.UseDoubleVerification == "By_SMS")
                                    {
                                        //if (string.IsNullOrEmpty(Convert.ToString(_usermaster.MobileNo)))
                                        //{
                                        //    ModelState.AddModelError(string.Empty, "Please register mobile number first.");
                                        //    return View(nameof(Index));
                                        //}
                                        if (_usermaster.IsPhonelVerifeyed.Value == false)
                                        {
                                            _UserBLL.UserSetLastLogin(r[0].ApiUser.ToString());
                                            return RedirectToLocal(returnUrl);
                                            //ModelState.AddModelError(string.Empty, "Please verify phone number first.");
                                            //return View(nameof(Index));
                                        }
                                    }

                                    if (_UserBLL.SaveVerificationCode(_usermaster.Id, _usermaster.ProfileId, Code))
                                    {
                                        if (sUseDoubleVerification == "By_email")
                                        {
                                            await _signInManager.SignOutAsync();

                                            string sPath = _env.WebRootPath + @"\EmailTemplte\VerificationCode.html";
                                            string sHtml = System.IO.File.ReadAllText(sPath);
                                            sHtml = sHtml.Replace("#Browser ", Browser);
                                            sHtml = sHtml.Replace("#os", OperatingSystem);
                                            sHtml = sHtml.Replace("#Username", _usermaster.ApiUser);
                                            sHtml = sHtml.Replace("#code1", Code.Substring(0, 3));
                                            sHtml = sHtml.Replace("#code2", Code.Substring(3, 3));

                                            bool bIsSend = false;
                                            bIsSend = HomeController.Send(_usermaster.ApiUser, "Verify your identity in Jconsultas", sHtml);

                                            string sPid = objEncryptController.EncryptValue(_usermaster.ProfileId.ToString());
                                            return RedirectToAction("verifyCode", new { ProfileId = sPid });
                                        }
                                        else
                                        {
                                            // Now sms send to user
                                            string sSMSTemplate = "Your verification Code for Jconsultas is: " + Code + " ,Browser: " + Browser + ",Operating System: " + OperatingSystem + ",Username: " + _usermaster.ApiUser;
                                            string sMobileNo = _usermaster.MobileNo.ToString();

                                            PublishResponse response = await _smsHelper.Send(sSMSTemplate, sMobileNo);

                                            string sPid = objEncryptController.EncryptValue(_usermaster.ProfileId.ToString());
                                            return RedirectToAction("verifyCode", new { ProfileId = sPid });
                                        }
                                        //else
                                        //{
                                        //    ModelState.AddModelError(string.Empty, "Usu�rio e /ou senha inv�lidos.");
                                        //    return View(nameof(Index));
                                        //}
                                    }
                                    else
                                    {
                                        ModelState.AddModelError(string.Empty, "Usu�rio e /ou senha inv�lidos.");
                                        return View(nameof(Index));
                                    }
                                }
                            }
                            else
                            {
                                _UserBLL.UserSetLastLogin(r[0].ApiUser.ToString());
                                return RedirectToLocal(returnUrl);
                            }
                        }
                        else
                        {
                            _UserBLL.UserSetLastLogin(r[0].ApiUser.ToString());
                            return RedirectToLocal(returnUrl);
                        }
                    }
                    else
                    {
                        await _signInManager.SignOutAsync();

                        ModelState.AddModelError(string.Empty, "Usu�rio e /ou senha inv�lidos.");
                        return View(nameof(Index));
                    }

                    //_UserBLL.UserSetLastLogin(email);
                    //return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, $"Erro em provedor externo: ");
                    return View(nameof(Index));
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Usu�rio e /ou senha inv�lidos.");
                return View(nameof(Index));
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Error500()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ForgetPassword()
        {
            return View();
        }

        [HttpGet]
        public PasswordResponseResult ResetPassword(string id, string Password)
        {
            EncryptController e = new EncryptController();
            string sDecId = e.Decrypt(id);

            var claimsIdentity = (ClaimsIdentity)this.User.Identity;
            var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
            var userId = claim.Value;

            return _UserBLL.ResetPassword(sDecId, Password, userId);
        }

        [HttpGet]
        public ResponseResult ForgetPasswordRequest(string emailid)
        {
            string sURL = _settings.Value.sSiteURL.ToString();
            ResponseResult _ResponseResult = new ResponseResult();
            IEnumerable <UserMaster> _users = _UserBLL.CheckEmailIdExistOrNot(emailid);
            if (_users.ToList().Count > 0)
            {
                
                EncryptController e = new EncryptController();
                var EncID = e.EncryptValue(_users.ToList()[0].ProfileId.ToString());

                var dnm = e.Decrypt(EncID);

                string sContent = "";
                sContent = sContent + "Ol� ," + _users.ToList()[0].ApiUser;
                sContent = sContent + " <br><br> Voc� solicitou a altera��o da senha da sua conta no SGDExpress.";
                sContent = sContent + " <br>Acesse o link abaixo para cadastrar nova senha e acessar o sistema.";
                sContent = sContent + " <br><br> " + sURL + "/Account/ForgetPassword?id=" + EncID + "&type=forget";

                sContent = sContent + " <br><br>  Atenciosamente,<br> ";
                sContent = sContent + " Equipe SGDExpress<br> ";

                bool bIsSend = false;
                
                bIsSend = HomeController.Send(emailid, "Redefini��o de senha", sContent, _settings);

                _ResponseResult.Resposne = bIsSend;
            }
            else
            {
                _ResponseResult.Resposne = false;
                _ResponseResult.Msg = "Invalid";
            }

            return _ResponseResult;
        }

        [HttpGet]
        public ActionResult verifyCode([FromQuery]string ProfileId)
        {
            ViewData["ProfileId"] = ProfileId;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> verifyCode([FromQuery]string ProfileId, VerifyCodeModel model)
        {
            ViewData["ProfileId"] = model.ProfileId.ToString();
            string sProfileId = objEncryptController.Decrypt(ViewData["ProfileId"].ToString().Trim());
            if (string.IsNullOrEmpty(sProfileId))
            {
                ModelState.AddModelError(string.Empty, "Some error occurred");
                return View();
            }
            else
            {
                if (ModelState.IsValid)
                {
                    if (_UserBLL.CheckVerificationCode(sProfileId, model.Code, ""))
                    {
                        // Now User Login
                        UserMaster _user = _UserBLL.UserDetailsOnId(sProfileId);
                        var result = await _signInManager.PasswordSignInAsync(_user.ApiUser, _user.ApiPassword, false, lockoutOnFailure: false);
                        if (result.Succeeded)
                        {
                            var CurrentIdAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                            _UserBLL.UserSetLastLoginWithIpAddress(_user.ApiUser.ToString(), CurrentIdAddress);

                            return RedirectToLocal("");
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Some error occurred");
                            return View();
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Invalid or expired code.");
                        return View();
                    }

                }
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ReSendCode(string ProfileId)
        {
            ViewData["ProfileId"] = ProfileId;

            string sProfileId = objEncryptController.Decrypt(ProfileId);
            UserMaster _usermaster = _UserBLL.UserDetailsOnId(sProfileId);
            string userAgent = Request.Headers["User-Agent"];
            UserAgent.UserAgent ua = new UserAgent.UserAgent(userAgent);

            string Browser = ua.Browser.Name.ToString();
            string OperatingSystem = ua.OS.Name.ToString() + "-" + ua.OS.Version.ToString();

            Random generator = new Random();
            String Code = generator.Next(0, 1000000).ToString("D6");

            if (_UserBLL.SaveVerificationCode(_usermaster.Id, _usermaster.ProfileId, Code))
            {
                if (_usermaster.UseDoubleVerification == "By_email")
                {
                    await _signInManager.SignOutAsync();

                    string sPath = _env.WebRootPath + @"\EmailTemplte\VerificationCode.html";
                    string sHtml = System.IO.File.ReadAllText(sPath);
                    sHtml = sHtml.Replace("#os", OperatingSystem);
                    sHtml = sHtml.Replace("#Username", _usermaster.ApiUser);
                    sHtml = sHtml.Replace("#code1", Code.Substring(0, 3));
                    sHtml = sHtml.Replace("#code2", Code.Substring(3, 3));

                    bool bIsSend = false;
                    bIsSend = HomeController.Send(_usermaster.ApiUser, "Verify your identity in Jconsultas", sHtml);

                    return RedirectToAction("verifyCode", new { ProfileId = ProfileId });
                }

                else
                {
                    // Now sms send to user
                    string sSMSTemplate = "Your verification Code for Jconsultas is: " + Code + " ,Browser: " + Browser + ",Operating System: " + OperatingSystem + ",Username: " + _usermaster.ApiUser;
                    string sMobileNo = _usermaster.MobileNo.ToString();

                    PublishResponse response = await _smsHelper.Send(sSMSTemplate, sMobileNo);

                    string sPid = objEncryptController.EncryptValue(_usermaster.ProfileId.ToString());
                    return RedirectToAction("verifyCode", new { ProfileId = ProfileId });
                }

            }
            else
            {
                ModelState.AddModelError(string.Empty, "Usu�rio e /ou senha inv�lidos.");
                return View("verifyCode");
            }
        }


    }

    public class ResponseResult
    {
        public bool Resposne { get; set; }
        public string Msg { get; set; }
    }


}