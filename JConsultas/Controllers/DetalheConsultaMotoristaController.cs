using BusinessLogicLayer;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using JConsultas.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Xml.Serialization;

namespace JConsultas.Controllers
{
    [Produces("application/json")]

    public class DetalheConsultaMotoristaController : Controller
    {
        private readonly ConsultaCNHBLL _ConsultaCNHBLL;
        private IHostingEnvironment _env;

        public DetalheConsultaMotoristaController(IHostingEnvironment env, EntityLayer.Models.JConsultasContext context)
        {
            this._env = env;
            _ConsultaCNHBLL = new ConsultaCNHBLL(context);
        }

        [HttpGet]
        public dynamic GetSigleSearch(int QId)
        {
            try
            {
                var result = _ConsultaCNHBLL.GetDocumentFromDBById(QId);

                if (result.ToString().Contains("<RETORNO>999"))
                {
                    var r1 = new CONSULTA1650();
                    r1.CONSULTAS = new CONSULTAS();
                    r1.CONSULTAS.CONSULTA = new CONSULTA();

                    r1.CONSULTAS.CONSULTA.DESCRICAO = "CONDUTOR N�O ENCONTRADO";
                    return new { status = -1, result = r1 };
                }
                else if (result.ToString().Contains(@"DESCRICAO=""Usu�rio ou senha inv�lida."""))
                {
                    var r1 = new CONSULTA1650();
                    r1.CONSULTAS = new CONSULTAS();
                    r1.CONSULTAS.CONSULTA = new CONSULTA();

                    r1.CONSULTAS.CONSULTA.DESCRICAO = "Usu�rio e senha inv�lidos.";
                    r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Usu�rio e senha inv�lidos.";
                    return new { status = -1, result = r1 };
                }
                else if (result.ToString().Contains(@"DESCRICAO=""Pesquisa esta sendo processada, tente novamente em alguns instantes"""))
                {
                    var r1 = new CONSULTA1650();
                    r1.CONSULTAS = new CONSULTAS();
                    r1.CONSULTAS.CONSULTA = new CONSULTA();

                    r1.CONSULTAS.CONSULTA.DESCRICAO = "Pesquisa esta sendo processada, tente novamente em alguns instantes.";
                    r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Pesquisa esta sendo processada, tente novamente em alguns instantes.";
                    return new { status = -1, result = r1 };
                }
                else
                {
                    var r = DeserializarXMLRetorno(result);
                    return r;
                }
            }
            catch (Exception ex)
            {
                Helper.LogError("ConsultaCNHController.CS -> BuscaUnicaCNH()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }

        }

        public static CONSULTA1650 DeserializarXMLRetorno(string xml)
        {
            try
            {
                CONSULTA1650 retorno = null;
                XmlSerializer serializer = new XmlSerializer(typeof(CONSULTA1650));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                retorno = (CONSULTA1650)serializer.Deserialize(reader);
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }

        public static CONSULTA2300ERR DeserializarXMLRetornoErr(string xml)
        {
            try
            {
                CONSULTA2300ERR retorno = null;
                XmlSerializer serializer = new XmlSerializer(typeof(CONSULTA2300ERR));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                retorno = (CONSULTA2300ERR)serializer.Deserialize(reader);
                //reader.Close();
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }


        [HttpGet]
        public dynamic CreateExcel(int QId)
        {
            try
            {
                string sFileName = "";
                sFileName = "Excel_" + DateTime.Now.ToString("dd_MM_yyyy_H_mm_ss") + ".xlsx";
                string sWebRootFolder = _env.WebRootPath + $@"\uploadExcel\";
                string dir = _env.WebRootPath + $@"\uploadExcel";

                string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
                FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

                if (file.Exists)
                {
                    file.Delete();
                    file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                }

                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;
                CONSULTA1650 consulta = new CONSULTA1650();

                var _DocumentXMLResponse = _ConsultaCNHBLL.GetDocumentFromDBById(QId);
                if (_DocumentXMLResponse.Contains("<RETORNO>999"))
                {
                    return new
                    {
                        status = 0,
                        Msg = "Erro ao ler o retorno da pesquisa"
                    };
                }
                else
                {
                    consulta = DeserializarXMLRetorno(_DocumentXMLResponse);
                }

                var resultado = consulta.CONSULTAS.CONSULTA.RESULTADO;

                List<ExcelWorksheet> works = new List<ExcelWorksheet>();

                using (ExcelPackage package = new ExcelPackage(file))
                {
                    PropertyInfo[] properties = resultado.GetType().GetProperties();

                    foreach (var propertyInfo in properties)
                    {
                        var sheet = package.Workbook.Worksheets.Add(propertyInfo.Name);
                        works.Add(sheet);

                        var detalhe = propertyInfo.PropertyType.GetProperties();

                        int coluna = 1;

                        foreach (var prop in detalhe)
                        {
                            int colunaLista = 1;

                            if (prop.PropertyType.Name.Contains("List"))
                            {
                                if (prop.Name == "RECURSO")
                                {
                                    var propertyRecursos = GetProperties("JConsultas.Models", prop.Name);
                                    foreach (var item in propertyRecursos)
                                    {
                                        var a = GetProperties("JConsultas.Models", item.Name);
                                        foreach (var b in a)
                                        {
                                            if (b.Name == "EVENTO")
                                            {
                                                var c = GetProperties("JConsultas.Models", "EVENTO");
                                                foreach (var d in c)
                                                {
                                                    sheet.Cells[1, colunaLista].Value = "EVENTO_" + d.Name;
                                                    colunaLista++;
                                                }
                                            }
                                            else
                                            {
                                                sheet.Cells[1, colunaLista].Value = item.Name + "_" + b.Name;
                                                colunaLista++;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    var propertyInfos = GetProperties("JConsultas.Models", prop.Name);
                                    foreach (var item in propertyInfos)
                                    {
                                        sheet.Cells[1, colunaLista].Value = item.Name;
                                        colunaLista++;
                                    }
                                }
                            }
                            else
                            {
                                sheet.Cells[1, coluna].Value = prop.Name;
                                coluna++;
                            }
                        }
                    }

                    var planilha = works.Find(x => x.Name == "INFRACOES");
                    PreenchePlanilha<INFRACAO>(resultado.INFRACOES.INFRACAO, planilha);

                    planilha = works.Find(x => x.Name == "EXAMES");
                    PreenchePlanilha<EXAME>(resultado.EXAMES.EXAME, planilha);

                    planilha = works.Find(x => x.Name == "CURSOS");
                    PreenchePlanilha<CURSO>(resultado.CURSOS.CURSO, planilha);

                    planilha = works.Find(x => x.Name == "BLOQUEIOS");
                    PreenchePlanilha<BLOQUEIO>(resultado.BLOQUEIOS.BLOQUEIO, planilha);

                    planilha = works.Find(x => x.Name == "DADOSCONDUTOR");
                    PreenchePlanilha<DADOSCONDUTOR>(resultado.DADOSCONDUTOR, planilha);

                    planilha = works.Find(x => x.Name == "DADOSCNH");
                    PreenchePlanilha<DADOSCNH>(resultado.DADOSCNH, planilha);

                    planilha = works.Find(x => x.Name == "PONTUACAO");
                    PreenchePlanilha<PONTUACAO>(resultado.PONTUACAO, planilha);

                    planilha = works.Find(x => x.Name == "RECURSOS");
                    PreencheRecursoPlanilha(resultado.RECURSOS?.RECURSO, planilha);

                    package.Save();
                }

                return new { status = 1, filename = sFileName, filepath = _env.WebRootPath + $@"\uploadExcel\" + sFileName, fullFilePath = $@"..\..\uploadExcel\" + sFileName };

            }
            catch (Exception ex)
            {
                return new
                {
                    status = 0,
                    Msg = ex.Message.ToString()
                };
            }
        }

        [HttpGet]
        public dynamic CreatePdf(int QId)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;
                
                var result = _ConsultaCNHBLL.GetDocumentFromDBById(QId);
                CONSULTA1650 r = DeserializarXMLRetorno(result);

                string sData = UtilPDF.GenerateHtmlViewPDFCNH(r);

                Document document = new Document(PageSize.A4);
                document.Open();
                string pdfName = "ConsultaCNH_" + r.CONSULTAS.CONSULTA.RESULTADO.DADOSCONDUTOR.CPF + "_" + DateTime.Now.Ticks.ToString() + ".pdf";
                string filepath = _env.WebRootPath + $@"\uploadpdf\" + pdfName;
                string dir = _env.WebRootPath + $@"\uploadpdf";

                bool exists = System.IO.Directory.Exists(dir);
                if (!exists)
                    System.IO.Directory.CreateDirectory(dir);

                using (FileStream fs = new FileStream(filepath, FileMode.Create))
                {
                    PdfWriter pdfWriter = PdfWriter.GetInstance(document, fs);
                    pdfWriter.PageEvent = new PdfEventHandler();

                    using (StringReader stringReader = new StringReader(sData))
                    {
                        StyleSheet styles = new StyleSheet();
                        styles.LoadTagStyle("th", "size", "10px");
                        styles.LoadTagStyle("th", "face", "helvetica");
                        styles.LoadTagStyle("td", "size", "8px");
                        styles.LoadTagStyle("td", "face", "helvetica");

                        System.Collections.ArrayList parsedList = iTextSharp.text.html.simpleparser.HtmlWorker.ParseToList(stringReader, styles);
                        document.Open();
                        foreach (object item in parsedList)
                        {
                            document.Add((IElement)item);
                        }
                        document.Close();
                    }
                }
                return new { status = 1, filename = pdfName, filepath = _env.WebRootPath + $@"\uploadpdf\" + pdfName, fullFilePath = $@"..\..\uploadpdf\" + pdfName };
            }
            catch (Exception ex)
            {
                Helper.LogError("ConsultaCNHController.CS -> CreatePdf()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }
        }

        public bool CreatePdfOnId(int QId)
        {
            try
            {
                var result = _ConsultaCNHBLL.GetDocumentFromDBById(QId);
                if (result.ToString().Contains("<RETORNO>999"))
                {
                    return false;
                }

                CONSULTA1650 r = DeserializarXMLRetorno(result);

                string sData = UtilPDF.GenerateHtmlViewPDFCNH(r);

                Document document = new Document(PageSize.A4);
                document.Open();
                string pdfName = "ConsultaMotorista_" + QId.ToString() + ".pdf";
                string filepath = _env.WebRootPath + $@"\uploadpdf\" + pdfName;
                string dir = _env.WebRootPath + $@"\uploadpdf";

                bool exists = System.IO.Directory.Exists(dir);
                if (!exists)
                    System.IO.Directory.CreateDirectory(dir);

                using (FileStream fs = new FileStream(filepath, FileMode.Create))
                {
                    PdfWriter pdfWriter = PdfWriter.GetInstance(document, fs);
                    pdfWriter.PageEvent = new PdfEventHandler();

                    using (StringReader stringReader = new StringReader(sData))
                    {
                        StyleSheet styles = new StyleSheet();
                        styles.LoadTagStyle("th", "size", "10px");
                        styles.LoadTagStyle("th", "face", "helvetica");
                        styles.LoadTagStyle("td", "size", "8px");
                        styles.LoadTagStyle("td", "face", "helvetica");

                        System.Collections.ArrayList parsedList = iTextSharp.text.html.simpleparser.HtmlWorker.ParseToList(stringReader, styles);
                        document.Open();
                        foreach (object item in parsedList)
                        {
                            document.Add((IElement)item);
                        }
                        document.Close();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Helper.LogError("DocDetailPageController.CS -> CreatePdf()", ex.Message, _env);
                return false;
            }
        }


        public static void PreenchePlanilha<T>(List<T> resultado, ExcelWorksheet planilha)
        {
            if (resultado != null)
            {
                int linha = 2;
                foreach (var objetos in resultado)
                {
                    var propInfracao = objetos.GetType().GetProperties();
                    int col = 1;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(objetos, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }
                    linha++;
                }
            }
        }

        public static void PreenchePlanilha<T>(T resultado, ExcelWorksheet planilha)
        {
            if (resultado != null)
            {
                int linha = 2;

                var propInfracao = resultado.GetType().GetProperties();
                int col = 1;
                foreach (var item in propInfracao)
                {
                    var val = GetPropValue(resultado, item.Name);
                    if (val != null)
                    {
                        planilha.Cells[linha, col].Value = val.ToString();
                    }
                    col++;
                }
                linha++;
            }
        }

        public static void PreencheRecursoPlanilha(List<RECURSO> resultado, ExcelWorksheet planilha)
        {
            if (resultado != null)
            {
                int linha = 2;
                foreach (var objetos in resultado)
                {
                    var veic = objetos.VEICULO;
                    var infracao = objetos.INFRACAO_RECURSO;
                    var dados = objetos.DADOS_PROCESSO;
                    var eventos = objetos.EVENTOS?.EVENTO;

                    var propInfracao = veic.GetType().GetProperties();
                    int col = 1;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(veic, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }

                    propInfracao = infracao.GetType().GetProperties();
                    col = 5;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(infracao, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }

                    propInfracao = dados.GetType().GetProperties();
                    col = 12;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(dados, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }


                    if (eventos != null)
                    {
                        foreach (var ev in eventos)
                        {
                            propInfracao = ev.GetType().GetProperties();
                            col = 18;
                            foreach (var item in propInfracao)
                            {
                                var val = GetPropValue(ev, item.Name);
                                if (val != null)
                                {
                                    planilha.Cells[linha, col].Value = val.ToString();
                                }
                                col++;
                            }
                            linha++;
                        }
                    }

                    linha++;
                }
            }
        }

        public static object GetPropValue(object source, string propertyName)
        {
            var property = source.GetType().GetRuntimeProperties().FirstOrDefault(p => string.Equals(p.Name, propertyName, StringComparison.OrdinalIgnoreCase));
            if (property != null)
            {
                return property.GetValue(source);
            }
            return null;
        }

        private static PropertyInfo[] GetProperties(string nameSpace, string className)
        {
            return Type.GetType(nameSpace + "." + className).GetProperties();
        }
    }
}