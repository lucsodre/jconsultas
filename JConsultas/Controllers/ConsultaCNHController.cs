using BusinessLogicLayer;
using EntityLayer.Models;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using JConsultas.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace JConsultas.Controllers
{
    [Produces("application/json")]
   // [Route("api/ConsultaCNH")]
    public class ConsultaCNHController : Controller
    {
        private readonly ConsultaCNHBLL _ConsultaCNHBLL;
        private readonly UserBLL _UserBLL;
        private IHostingEnvironment _env;
        private IOptions<ApplicationSettings> _settings;

        public ConsultaCNHController(IHostingEnvironment env, IOptions<ApplicationSettings> settings, EntityLayer.Models.JConsultasContext context)
        {
            this._env = env;
            _ConsultaCNHBLL = new ConsultaCNHBLL(context);
            _UserBLL = new UserBLL(context);
            _settings = settings;
        }

        [HttpGet]
        public dynamic VerificaConsultaCNH(string UF, string CPF, string NumRegistro, string NumRenach,
                                            DateTime ValidadeCNH, string RG, DateTime DataNascimento, DateTime DataHabilitacao,
                                            string MunicipioNascimento, string NumCedulaEspelho)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                var result = _ConsultaCNHBLL.CheckExistsInDatabase(userId, UF, CPF, NumRegistro, NumRenach, ValidadeCNH,
                    RG, DataNascimento, DataHabilitacao, MunicipioNascimento, NumCedulaEspelho);
                return result;
            }
            catch (Exception ex)
            {
                Helper.LogError("ConsultaCNHController.CS -> VerificaConsultaCNH()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }

        }

        public dynamic BuscaUnicaCNH(bool userConfirm, string UF, string CPF, string NumRegistro, string NumRenach,
                                            DateTime ValidadeCNH, string RG, DateTime DataNascimento, DateTime DataHabilitacao,
                                            string MunicipioNascimento, string NumCedulaEspelho)
        {
            try
            {

                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                if (userConfirm)
                {
                    
                    string apiCodigo = _settings.Value.ApiCodigo;
                    string apiUsuario = _settings.Value.ApiUsuario;
                    string apiSenha = _settings.Value.ApiSenha;

                    var result =  _ConsultaCNHBLL.GetDocumentFromService(userId, UF, CPF, NumRegistro, NumRenach, ValidadeCNH,
                    RG, DataNascimento, DataHabilitacao, MunicipioNascimento, NumCedulaEspelho, apiUsuario, apiSenha, apiCodigo);

                    if (result.ToString().Contains("<RETORNO>999"))
                    {
                        var r1 = new CONSULTA1650();
                        r1.CONSULTAS = new CONSULTAS();
                        r1.CONSULTAS.CONSULTA = new CONSULTA();

                        try
                        {
                            var xml = new XmlDocument();
                            xml.LoadXml(result);

                            var node = xml.DocumentElement.SelectSingleNode("/CONSULTA1650/DESCRICAO");
                            string descricao = node.InnerText.ToString();

                            r1.CONSULTAS.CONSULTA.DESCRICAO = descricao;
                        }
                        catch (Exception)
                        {
                            r1.CONSULTAS.CONSULTA.DESCRICAO = "Condutor n�o encontrado";
                        }
                        return new { status = -1, result = r1 };
                    }
                    else if (result.ToString().Contains(@"DESCRICAO=""Usu�rio ou senha inv�lida."""))
                    {
                        var r1 = new CONSULTA1650();
                        r1.CONSULTAS = new CONSULTAS();
                        r1.CONSULTAS.CONSULTA = new CONSULTA();

                        r1.CONSULTAS.CONSULTA.DESCRICAO = "Usu�rio e senha inv�lidos.";
                        r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Usu�rio e senha inv�lidos.";
                        return new { status = -1, result = r1 };
                    }
                    else if (result.ToString().Contains(@"DESCRICAO=""Pesquisa esta sendo processada, tente novamente em alguns instantes"""))
                    {
                        var r1 = new CONSULTA1650();
                        r1.CONSULTAS = new CONSULTAS();
                        r1.CONSULTAS.CONSULTA = new CONSULTA();

                        r1.CONSULTAS.CONSULTA.DESCRICAO = "Pesquisa esta sendo processada, tente novamente em alguns instantes.";
                        r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Pesquisa esta sendo processada, tente novamente em alguns instantes.";
                        return new { status = -1, result = r1 };
                    }
                    else
                    {
                        var r = DeserializarXMLRetorno(result);
                        return r;
                    }
                }
                else
                {
                    DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();
                    _DocumentXMLResponse = _ConsultaCNHBLL.GetDocumentFromDB(CPF, userId);
                    if (_DocumentXMLResponse.xml.ToString().Contains("<RETORNO>999"))
                    {
                        var xml = new XmlDocument();
                        xml.LoadXml(_DocumentXMLResponse.xml);

                        var node = xml.DocumentElement.SelectSingleNode("/CONSULTA1650/DESCRICAO");
                        string descricao = node.InnerText.ToString();

                        var r1 = new CONSULTA1650();
                        r1.CONSULTAS = new CONSULTAS();
                        r1.CONSULTAS.CONSULTA = new CONSULTA();
                        r1.CONSULTAS.CONSULTA.DESCRICAO = descricao;

                        return new { status = -1, result = r1 };
                    }
                    else
                    {
                        var r = DeserializarXMLRetorno(_DocumentXMLResponse.xml);
                        return r;
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.LogError("ConsultaCNHController.CS -> BuscaUnicaCNH()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }

        }

        //[HttpGet("BuscaCNH")]
        //[Produces("application/json", "application/xml")]
        //public dynamic BuscaCNH([FromQuery] string UF, [FromQuery] string CPF, [FromQuery] string NumRegistro, [FromQuery] string NumRenach,
        //                                [FromQuery] DateTime ValidadeCNH, [FromQuery] string RG, [FromQuery] DateTime DataNascimento,
        //                                [FromQuery] DateTime DataHabilitacao, [FromQuery] string MunicipioNascimento, [FromQuery] string NumCedulaEspelho)
        //{
        //    try
        //    {
        //        var Usuario = Convert.ToString(Request.Headers["Usuario"]);
        //        var Senha = Convert.ToString(Request.Headers["Senha"]);

        //        string apiCodigo = _settings.Value.ApiCodigo;
        //        string apiUsuario = _settings.Value.ApiUsuario;
        //        string apiSenha = _settings.Value.ApiSenha;

        //        var result = _ConsultaCNHBLL.GetDocumentFromServiceAutentication(UF, CPF, NumRegistro, NumRenach, ValidadeCNH,
        //        RG, DataNascimento, DataHabilitacao, MunicipioNascimento, NumCedulaEspelho, apiUsuario, apiSenha, apiCodigo, Usuario, Senha);

        //        WebServiceResponseMotorista _res = new WebServiceResponseMotorista();         

        //        if (result.ToString().Contains("<RETORNO>999"))
        //        {
        //            var r1 = new CONSULTA1650();
        //            r1.CONSULTAS = new CONSULTAS();
        //            r1.CONSULTAS.CONSULTA = new CONSULTA();

        //            try
        //            {
        //                var xml = new XmlDocument();
        //                xml.LoadXml(result);

        //                var node = xml.DocumentElement.SelectSingleNode("/CONSULTA1650/DESCRICAO");
        //                string descricao = node.InnerText.ToString();

        //                r1.CONSULTAS.CONSULTA.DESCRICAO = descricao;
        //            }
        //            catch (Exception)
        //            {
        //                r1.CONSULTAS.CONSULTA.DESCRICAO = "Condutor n�o encontrado";
        //            }

        //            _res.status = false;
        //            _res.Message = r1.CONSULTAS.CONSULTA.DESCRICAO;
        //            _res.xml = r1;
        //            return _res;
        //        }
        //        else if (result.ToString().Contains(@"DESCRICAO=""Usu�rio ou senha inv�lida."""))
        //        {
        //            var r1 = new CONSULTA1650();
        //            r1.CONSULTAS = new CONSULTAS();
        //            r1.CONSULTAS.CONSULTA = new CONSULTA();

        //            r1.CONSULTAS.CONSULTA.DESCRICAO = "Usu�rio e senha inv�lidos.";
        //            r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Usu�rio e senha inv�lidos.";
        //            _res.status = false;
        //            _res.Message = r1.CONSULTAS.CONSULTA.DESCRICAO;
        //            _res.xml = r1;
        //            return _res;
        //        }
        //        else if (result.ToString().Contains(@"DESCRICAO=""Pesquisa esta sendo processada, tente novamente em alguns instantes"""))
        //        {
        //            var r1 = new CONSULTA1650();
        //            r1.CONSULTAS = new CONSULTAS();
        //            r1.CONSULTAS.CONSULTA = new CONSULTA();

        //            r1.CONSULTAS.CONSULTA.DESCRICAO = "Pesquisa esta sendo processada, tente novamente em alguns instantes.";
        //            r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Pesquisa esta sendo processada, tente novamente em alguns instantes.";
        //            _res.status = false;
        //            _res.Message = r1.CONSULTAS.CONSULTA.DESCRICAO;
        //            _res.xml = r1;
        //            return _res;
        //        }
        //        else if (result.ToString().Contains("Credenciais de usu�rio inv�lidas"))
        //        {
        //            var r1 = new CONSULTA1650();
        //            r1.CONSULTAS = new CONSULTAS();
        //            r1.CONSULTAS.CONSULTA = new CONSULTA();

        //            r1.CONSULTAS.CONSULTA.DESCRICAO = "Credenciais de usu�rio inv�lidas";
        //            r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Credenciais de usu�rio inv�lidas";
        //            _res.status = false;
        //            _res.Message = r1.CONSULTAS.CONSULTA.DESCRICAO;
        //            _res.xml = r1;
        //            return _res;
        //        }
        //        else if (result.ToString().Contains("Credenciais para consulta API inv�lidas"))
        //        {
        //            var r1 = new CONSULTA1650();
        //            r1.CONSULTAS = new CONSULTAS();
        //            r1.CONSULTAS.CONSULTA = new CONSULTA();

        //            r1.CONSULTAS.CONSULTA.DESCRICAO = "Credenciais para consulta API inv�lidas";
        //            r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Credenciais para consulta API inv�lidas";
        //            _res.status = false;
        //            _res.Message = r1.CONSULTAS.CONSULTA.DESCRICAO;
        //            _res.xml = r1;
        //            return _res;
        //        }
        //        else
        //        {
        //            var r = DeserializarXMLRetorno(result);
        //            _res.status = true;
        //            _res.Message = r.CONSULTAS.CONSULTA.DESCRICAO;
        //            _res.xml = r;
        //            return _res;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Helper.LogError("ConsultaCNHController.CS -> BuscaUnicaCNH()", ex.Message, _env);
        //        WebServiceResponseMotorista _res = new WebServiceResponseMotorista(); 
        //        _res.status = false;
        //        _res.Message = ex.Message;
        //        _res.xml = null;
        //        return _res;
        //    }

        //}


        public static CONSULTA1650 DeserializarXMLRetorno(string xml)
        {
            try
            {
                CONSULTA1650 retorno = null;
                XmlSerializer serializer = new XmlSerializer(typeof(CONSULTA1650));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                retorno = (CONSULTA1650) serializer.Deserialize(reader);
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }

        [HttpGet]
        public dynamic CreatePdf(string CPF)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();
                _DocumentXMLResponse = _ConsultaCNHBLL.GetDocumentFromDB(CPF, userId);
                CONSULTA1650 r = DeserializarXMLRetorno(_DocumentXMLResponse.xml);

                string sData = UtilPDF.GenerateHtmlViewPDFCNH(r);

                Document document = new Document(PageSize.A4);
                document.Open();
                string pdfName = "ConsultaCNH_" + r.CONSULTAS.CONSULTA.RESULTADO.DADOSCONDUTOR.CPF + "_" + DateTime.Now.Ticks.ToString() + ".pdf";
                string filepath = _env.WebRootPath + $@"\uploadpdf\" + pdfName;
                string dir = _env.WebRootPath + $@"\uploadpdf";

                bool exists = System.IO.Directory.Exists(dir);
                if (!exists)
                    System.IO.Directory.CreateDirectory(dir);

                using (FileStream fs = new FileStream(filepath, FileMode.Create))
                {
                    PdfWriter pdfWriter = PdfWriter.GetInstance(document, fs);
                    pdfWriter.PageEvent = new PdfEventHandler();

                    using (StringReader stringReader = new StringReader(sData))
                    {
                        StyleSheet styles = new StyleSheet();
                        styles.LoadTagStyle("th", "size", "10px");
                        styles.LoadTagStyle("th", "face", "helvetica");
                        styles.LoadTagStyle("td", "size", "8px");
                        styles.LoadTagStyle("td", "face", "helvetica");

                        System.Collections.ArrayList parsedList = iTextSharp.text.html.simpleparser.HtmlWorker.ParseToList(stringReader, styles);
                        document.Open();

                        foreach (object item in parsedList)
                        {
                            document.Add((IElement)item);
                        }
                        document.Close();
                    }
                    
                }
                return new { status = 1, filename = pdfName, filepath = _env.WebRootPath + $@"\uploadpdf\" + pdfName, fullFilePath = $@"..\..\uploadpdf\" + pdfName };
            }
            catch (Exception ex)
            {
                Helper.LogError("ConsultaCNHController.CS -> CreatePdf()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }
        }

        //[HttpGet]
        //public dynamic CreateExcel(string CPF)
        //{
        //    try
        //    {
        //        var claimsIdentity = (ClaimsIdentity)this.User.Identity;
        //        var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
        //        var userId = claim.Value;

        //        DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();
        //        _DocumentXMLResponse = _ConsultaCNHBLL.GetDocumentFromDB(CPF, userId);

        //        CONSULTA1650 r = DeserializarXMLRetorno(_DocumentXMLResponse.xml);

        //        var CondutorData = r.CONSULTAS.CONSULTA.RESULTADO.DADOSCONDUTOR;
        //        var CnhData = r.CONSULTAS.CONSULTA.RESULTADO.DADOSCNH;
        //        var ExamesData = r.CONSULTAS.CONSULTA.RESULTADO.EXAMES.EXAME;
        //        var CursosData = r.CONSULTAS.CONSULTA.RESULTADO.CURSOS.CURSO;
        //        var RecursosData = r.CONSULTAS.CONSULTA.RESULTADO.RECURSOS.RECURSO;
        //        var PontuacaoData = r.CONSULTAS.CONSULTA.RESULTADO.PONTUACAO;
        //        var InfracoesData = r.CONSULTAS.CONSULTA.RESULTADO.INFRACOES.INFRACAO;
        //        var BloqueiosData = r.CONSULTAS.CONSULTA.RESULTADO.BLOQUEIOS.BLOQUEIO;

        //        string sFileName = "Excel_" + _DocumentXMLResponse.QueryResultid.ToString() + ".xlsx";
        //        string sWebRootFolder = _env.WebRootPath + $@"\uploadExcel\";
        //        string dir = _env.WebRootPath + $@"\uploadExcel";

        //        //string sWebRootFolder = _env.WebRootPath;
        //        //string sFileName = @"demo2.xlsx";
        //        string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
        //        FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
        //        if (file.Exists)
        //        {
        //            file.Delete();
        //            file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
        //        }
        //        using (ExcelPackage package = new ExcelPackage(file))
        //        {
        //            // 1. VEICULO
        //            ExcelWorksheet CONDUTOR = package.Workbook.Worksheets.Add("CONDUTOR");

        //            CONDUTOR.Cells[1, 1].Value = "CPF";
        //            CONDUTOR.Cells[2, 1].Value = Convert.ToString(CondutorData.CPF);

        //            CONDUTOR.Cells[1, 2].Value = "Data de nascimento";
        //            CONDUTOR.Cells[2, 2].Value = Convert.ToString(CondutorData.DATA_NASCIMENTO);

        //            CONDUTOR.Cells[1, 3].Value = "Nome";
        //            CONDUTOR.Cells[2, 3].Value = Convert.ToString(CondutorData.NOME);

        //            CONDUTOR.Cells[1, 4].Value = "Nome da m�e";
        //            CONDUTOR.Cells[2, 4].Value = Convert.ToString(CondutorData.NOME_MAE);

        //            CONDUTOR.Cells[1, 5].Value = "Nome do pai";
        //            CONDUTOR.Cells[2, 5].Value = Convert.ToString(CondutorData.NOME_PAI);

        //            CONDUTOR.Cells[1, 6].Value = "UF";
        //            CONDUTOR.Cells[2, 6].Value = Convert.ToString(CondutorData.UF);

        //            // 3. DPVATS
        //            ExcelWorksheet Cursos = package.Workbook.Worksheets.Add("CURSOS");
        //            for (int i = 0; i < CursosData.Count; i++)
        //            {
        //                var no = i + 2;
        //                if (i == 0)
        //                {
        //                    Cursos.Cells[1, 1].Value = "Carga hor�ria";
        //                    Cursos.Cells[1, 2].Value = "CATEGORIA";
        //                    Cursos.Cells[1, 3].Value = "CNPJ_ENTIDADE_CREDENCIADA";
        //                    Cursos.Cells[1, 4].Value = "CPF_PROFISSIONAL";
        //                    Cursos.Cells[1, 5].Value = "CURSODESC";
        //                    Cursos.Cells[1, 6].Value = "CATEGORIA";
        //                }

        //                Cursos.Cells[no, 1].Value = Convert.ToString(CursosData[i].CARGA_HORARIA);
        //                Cursos.Cells[no, 2].Value = Convert.ToString(CursosData[i].CATEGORIA);
        //                Cursos.Cells[no, 3].Value = Convert.ToString(CursosData[i].CNPJ_ENTIDADE_CREDENCIADA);
        //                Cursos.Cells[no, 4].Value = Convert.ToString(CursosData[i].CPF_PROFISSIONAL);
        //                Cursos.Cells[no, 5].Value = Convert.ToString(CursosData[i].CURSODESC);
        //                Cursos.Cells[no, 6].Value = Convert.ToString(CursosData[i].CATEGORIA);
        //            }

        //            package.Save();
        //        }

        //        return new { status = 1, filename = sFileName, filepath = _env.WebRootPath + $@"\uploadExcel\" + sFileName, fullFilePath = $@"..\..\uploadExcel\" + sFileName };
        //    }
        //    catch (Exception ex)
        //    {
        //        Helper.LogError("ConsultaCNHController.CS -> CreateExcel()", ex.Message, _env);
        //        return new { status = 0, message = ex.Message };
        //    }
        //}

        [HttpGet]
        public dynamic CreateExcel(string CPF)
        {
            try
            {
                string sFileName = "";
                sFileName = "Excel_" + DateTime.Now.ToString("dd_MM_yyyy_H_mm_ss") + ".xlsx";
                string sWebRootFolder = _env.WebRootPath + $@"\uploadExcel\";
                string dir = _env.WebRootPath + $@"\uploadExcel";

                string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
                FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

                if (file.Exists)
                {
                    file.Delete();
                    file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                }

                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;
                CONSULTA1650 consulta = new CONSULTA1650();
                DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();
                _DocumentXMLResponse = _ConsultaCNHBLL.GetDocumentFromDB(CPF, userId);
                if (_DocumentXMLResponse.xml.ToString().Contains("<RETORNO>999"))
                {
                    var xml = new XmlDocument();
                    xml.LoadXml(_DocumentXMLResponse.xml);

                    var node = xml.DocumentElement.SelectSingleNode("/CONSULTA1650/DESCRICAO");
                    string descricao = node.InnerText.ToString();

                    return new
                    {
                        status = 0,
                        Msg = descricao
                    };
                }
                else
                {
                    consulta = DeserializarXMLRetorno(_DocumentXMLResponse.xml);
                }

                var resultado = consulta.CONSULTAS.CONSULTA.RESULTADO;

                List<ExcelWorksheet> works = new List<ExcelWorksheet>();

                using (ExcelPackage package = new ExcelPackage(file))
                {
                    PropertyInfo[] properties = resultado.GetType().GetProperties();

                    foreach (var propertyInfo in properties)
                    {
                        var sheet = package.Workbook.Worksheets.Add(propertyInfo.Name);
                        works.Add(sheet);

                        var detalhe = propertyInfo.PropertyType.GetProperties();

                        int coluna = 1;

                        foreach (var prop in detalhe)
                        {
                            int colunaLista = 1;

                            if (prop.PropertyType.Name.Contains("List"))
                            {
                                if (prop.Name == "RECURSO")
                                {
                                    var propertyRecursos = GetProperties("JConsultas.Models", prop.Name);
                                    foreach (var item in propertyRecursos)
                                    {
                                        var a = GetProperties("JConsultas.Models", item.Name);
                                        foreach (var b in a)
                                        {
                                            if (b.Name == "EVENTO")
                                            {
                                                var c = GetProperties("JConsultas.Models", "EVENTO");
                                                foreach (var d in c)
                                                {
                                                    sheet.Cells[1, colunaLista].Value = "EVENTO_" + d.Name;
                                                    colunaLista++;
                                                }
                                            }
                                            else
                                            {
                                                sheet.Cells[1, colunaLista].Value = item.Name + "_" + b.Name;
                                                colunaLista++;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    var propertyInfos = GetProperties("JConsultas.Models", prop.Name);
                                    foreach (var item in propertyInfos)
                                    {
                                        sheet.Cells[1, colunaLista].Value = item.Name;
                                        colunaLista++;
                                    }
                                }
                            }
                            else
                            {
                                sheet.Cells[1, coluna].Value = prop.Name;
                                coluna++;
                            }
                        }                      
                    }

                    var planilha = works.Find(x => x.Name == "INFRACOES");
                    PreenchePlanilha<INFRACAO>(resultado.INFRACOES.INFRACAO, planilha);

                    planilha = works.Find(x => x.Name == "EXAMES");
                    PreenchePlanilha<EXAME>(resultado.EXAMES.EXAME, planilha);

                    planilha = works.Find(x => x.Name == "CURSOS");
                    PreenchePlanilha<CURSO>(resultado.CURSOS.CURSO, planilha);

                    planilha = works.Find(x => x.Name == "BLOQUEIOS");
                    PreenchePlanilha<BLOQUEIO>(resultado.BLOQUEIOS.BLOQUEIO, planilha);

                    planilha = works.Find(x => x.Name == "DADOSCONDUTOR");
                    PreenchePlanilha<DADOSCONDUTOR>(resultado.DADOSCONDUTOR, planilha);

                    planilha = works.Find(x => x.Name == "DADOSCNH");
                    PreenchePlanilha<DADOSCNH>(resultado.DADOSCNH, planilha);

                    planilha = works.Find(x => x.Name == "PONTUACAO");
                    PreenchePlanilha<PONTUACAO>(resultado.PONTUACAO, planilha);

                    planilha = works.Find(x => x.Name == "RECURSOS");
                    PreencheRecursoPlanilha(resultado.RECURSOS?.RECURSO, planilha);

                    package.Save();
                }

                return new { status = 1, filename = sFileName, filepath = _env.WebRootPath + $@"\uploadExcel\" + sFileName, fullFilePath = $@"..\..\uploadExcel\" + sFileName };

            }
            catch (Exception ex)
            {
                return new
                {
                    status = 0,
                    Msg = ex.Message.ToString()
                };
            }
        }

        public static void PreenchePlanilha<T>(List<T> resultado, ExcelWorksheet planilha)
        {
            if (resultado != null)
            {
                int linha = 2;
                foreach (var objetos in resultado)
                {
                    var propInfracao = objetos.GetType().GetProperties();
                    int col = 1;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(objetos, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }
                    linha++;
                }
            }
        }

        public static void PreenchePlanilha<T>(T resultado, ExcelWorksheet planilha)
        {
            if (resultado != null)
            {
                int linha = 2;

                var propInfracao = resultado.GetType().GetProperties();
                int col = 1;
                foreach (var item in propInfracao)
                {
                    var val = GetPropValue(resultado, item.Name);
                    if (val != null)
                    {
                        planilha.Cells[linha, col].Value = val.ToString();
                    }
                    col++;
                }
                linha++;
            }
        }

        public static void PreencheRecursoPlanilha(List<RECURSO> resultado, ExcelWorksheet planilha)
        {
            if (resultado != null)
            {
                int linha = 2;
                foreach (var objetos in resultado)
                {
                    var veic = objetos.VEICULO;
                    var infracao = objetos.INFRACAO_RECURSO;
                    var dados = objetos.DADOS_PROCESSO;
                    var eventos = objetos.EVENTOS?.EVENTO;

                    var propInfracao = veic.GetType().GetProperties();
                    int col = 1;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(veic, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }

                    propInfracao = infracao.GetType().GetProperties();
                    col = 5;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(infracao, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }

                    propInfracao = dados.GetType().GetProperties();
                    col = 12;
                    foreach (var item in propInfracao)
                    {
                        var val = GetPropValue(dados, item.Name);
                        if (val != null)
                        {
                            planilha.Cells[linha, col].Value = val.ToString();
                        }
                        col++;
                    }


                    if (eventos != null)
                    {
                        foreach (var ev in eventos)
                        {
                            propInfracao = ev.GetType().GetProperties();
                            col = 18;
                            foreach (var item in propInfracao)
                            {
                                var val = GetPropValue(ev, item.Name);
                                if (val != null)
                                {
                                    planilha.Cells[linha, col].Value = val.ToString();
                                }
                                col++;
                            }
                            linha++;
                        }
                    }

                    linha++;
                }
            }
        }

        public static object GetPropValue(object source, string propertyName)
        {
            var property = source.GetType().GetRuntimeProperties().FirstOrDefault(p => string.Equals(p.Name, propertyName, StringComparison.OrdinalIgnoreCase));
            if (property != null)
            {
                return property.GetValue(source);
            }
            return null;
        }

        private static PropertyInfo[] GetProperties(string nameSpace, string className)
        {
            return Type.GetType(nameSpace + "." + className).GetProperties();
        }


        public dynamic UploadPDfToDrive(string pdfname)
        {
            try
            {

                string filepath = _env.WebRootPath + $@"\uploadpdf\" + pdfname;

                Helper.LogError("ConsultaCNHController.CS -> UploadPDfToDrive() Path:=", filepath, _env);
                //string filepath = System.IO.Path.Combine(_env.WebRootPath, @"\uploadpdf\" + pdfname);
                //Upload to drive
                if (System.IO.File.Exists(filepath))
                {
                    Helper.LogError("ConsultaCNHController.CS -> UploadPDfToDrive() Path:=", "File Found..", _env);

                    string[] scopes = new string[] { DriveService.Scope.Drive, DriveService.Scope.DriveFile };
                    //For Other client
                    //var clientId = "626950072734-77uijdql50bse60muueib7k31m33lokb.apps.googleusercontent.com";
                    //var clientSecret = "aycyBhSaexDuCQGQQ60KS5Iz";

                    //For Web client
                    var clientId = "626950072734-uvui0jdfu9v0qnrqkkghdb35fn1c1bfv.apps.googleusercontent.com";
                    var clientSecret = "_GT4C2xqDoWYCq-wg6UAMBou";


                    // Here is where we Request the user to give us access, or use the Refresh Token that was previously stored in %AppData% 
                    var credential = GoogleWebAuthorizationBroker.AuthorizeAsync(new ClientSecrets { ClientId = clientId, ClientSecret = clientSecret },
                        scopes, System.Environment.CurrentManagedThreadId.ToString(), CancellationToken.None, new FileDataStore("Daimto.GoogleDrive.Auth.Store")).Result;

                    Helper.LogError("ConsultaCNHController.CS -> UploadPDfToDrive() ", "Login..", _env);

                    DriveService _service = new DriveService(new BaseClientService.Initializer()
                    {
                        ApiKey = "AIzaSyDbGKc_rppMG0n-5czuYzd3AJqtMr5A2Hw",
                        ApplicationName = "JConsultas",
                        HttpClientInitializer = credential
                    });


                    Helper.LogError("ConsultaCNHController.CS -> UploadPDfToDrive() credential", credential.ToString(), _env);
                    Google.Apis.Drive.v3.Data.File body = new Google.Apis.Drive.v3.Data.File();
                    //body.Title = System.IO.Path.GetFileName(filepath);
                    body.Name = System.IO.Path.GetFileName(filepath);
                    body.Description = "File uploaded by JConsultas";
                    body.MimeType = GetMimeType(filepath);
                    //body.Parents = new List<ParentReference> { new ParentReference() { Id = "root" } };

                    body.Parents = new List<string> { "root" };

                    Helper.LogError("ConsultaCNHController.CS -> UploadPDfToDrive()", "Set body data", _env);
                    // File's content. 
                    byte[] byteArray = System.IO.File.ReadAllBytes(filepath);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);
                    FilesResource.CreateMediaUpload request = _service.Files.Create(body, stream, GetMimeType(filepath));
                    request.Upload();

                    Helper.LogError("ConsultaCNHController.CS -> UploadPDfToDrive() request.responsebody :=", request.ResponseBody.ToString(), _env);
                    if (request.ResponseBody == null)
                        return new { status = 0, message = "Problem in upload file in google drive" };
                    else
                    {
                        System.IO.File.Delete(_env.WebRootPath + $@"\uploadpdf\" + pdfname);
                        return new { status = 1, filename = pdfname, filepath = _env.WebRootPath + $@"\uploadpdf\" + pdfname };
                    }
                }
                else
                {
                    return new { status = 0, message = "File not exists :=" + filepath };
                }
            }
            catch (Exception ex)
            {
                Helper.LogError("ConsultaCNHController.CS -> UploadPDfToDrive()", ex.Message, _env);
                return null;
            }
        }

        private static string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }

        [HttpGet]
        public async Task<dynamic> SaveFileOnDroupBox(string fpath, string fname, string token)
        {

            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                UserMaster _user = new UserMaster();
                _user = _UserBLL.UserDetailsOnMasterId(userId);
                if (_user != null && !string.IsNullOrEmpty(_user.AccessToken))
                {
                    string sToken = _user.AccessToken;
                    string sURL = _settings.Value.ServiceURL.ToString();
                    //sURL = sURL + @"/api/Dropbox/SaveFileOnServer?sPath=" + fpath + "&fName=" + fname + "&Tokan=OxBhas_nZPAAAAAAAAAAl44gReSObzqkywhsevlkMdokO3FDzSBBvlpqa96fwQEZ";
                    sURL = sURL + @"/api/Dropbox/SaveFileOnServer?sPath=" + fpath + "&fName=" + fname + "&Tokan=" + sToken;


                    //string sURL = @"http://localhost:56203/api/Dropbox/SaveFileOnServer?sPath=D:\22.pdf&Tokan=OxBhas_nZPAAAAAAAAAAl44gReSObzqkywhsevlkMdokO3FDzSBBvlpqa96fwQEZ";
                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(sURL);
                    var response = (HttpWebResponse)await Task.Factory.FromAsync<WebResponse>(request.BeginGetResponse, request.EndGetResponse, null);

                    Stream stream = response.GetResponseStream();
                    StreamReader strReader = new StreamReader(stream);
                    string text = await strReader.ReadToEndAsync();

                    return text;
                }
                else
                {
                    return new { Result = false, Error = "Access Token is not available" };
                }

            }
            catch (Exception ex)
            {
                Helper.LogError("ConsultaCNHController.CS -> CreateExcel()", ex.Message, _env);
                return new
                {
                    status = 0,
                    message = ex.Message
                };
            }
        }
    }
}

public class WebServiceResponseMotorista
{
    public bool status { get; set; }
    public string Message { get; set; }
    public CONSULTA1650 xml { get; set; }
}

