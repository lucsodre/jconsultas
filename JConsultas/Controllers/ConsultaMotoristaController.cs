using BusinessLogicLayer;
using EntityLayer.Models;
using JConsultas.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace JConsultas.Controllers
{
    [Produces("application/json")]
    [Route("api/ConsultaCNH")]
    public class ConsultaMotoristaController : Controller
    {
        private readonly ConsultaCNHBLL _ConsultaCNHBLL;
        private readonly UserBLL _UserBLL;
        private IHostingEnvironment _env;
        private IOptions<ApplicationSettings> _settings;

        public ConsultaMotoristaController(IHostingEnvironment env, IOptions<ApplicationSettings> settings, EntityLayer.Models.JConsultasContext context)
        {
            this._env = env;
            _ConsultaCNHBLL = new ConsultaCNHBLL(context);
            _UserBLL = new UserBLL(context);
            _settings = settings;
        }

        [HttpGet("BuscaCNH")]
        [Produces("application/json", "application/xml")]
        public dynamic BuscaCNH([FromQuery] string UF, [FromQuery] string CPF, [FromQuery] string NumRegistro, [FromQuery] string NumRenach,
                                        [FromQuery] DateTime ValidadeCNH, [FromQuery] string RG, [FromQuery] DateTime DataNascimento,
                                        [FromQuery] DateTime DataHabilitacao, [FromQuery] string MunicipioNascimento, [FromQuery] string NumCedulaEspelho)
        {
            try
            {
                var Usuario = Convert.ToString(Request.Headers["Usuario"]);
                var Senha = Convert.ToString(Request.Headers["Senha"]);

                string apiCodigo = _settings.Value.ApiCodigo;
                string apiUsuario = _settings.Value.ApiUsuario;
                string apiSenha = _settings.Value.ApiSenha;

                var result = _ConsultaCNHBLL.GetDocumentFromServiceAutentication(UF, CPF, NumRegistro, NumRenach, ValidadeCNH,
                RG, DataNascimento, DataHabilitacao, MunicipioNascimento, NumCedulaEspelho, apiUsuario, apiSenha, apiCodigo, Usuario, Senha);

                WebServiceResponseMotorista _res = new WebServiceResponseMotorista();

                if (result.ToString().Contains("<RETORNO>999"))
                {
                    var r1 = new CONSULTA1650();
                    r1.CONSULTAS = new CONSULTAS();
                    r1.CONSULTAS.CONSULTA = new CONSULTA();

                    try
                    {
                        var xml = new XmlDocument();
                        xml.LoadXml(result);

                        var node = xml.DocumentElement.SelectSingleNode("/CONSULTA1650/DESCRICAO");
                        string descricao = node.InnerText.ToString();

                        r1.CONSULTAS.CONSULTA.DESCRICAO = descricao;
                    }
                    catch (Exception)
                    {
                        r1.CONSULTAS.CONSULTA.DESCRICAO = "Condutor n�o encontrado";
                    }

                    _res.status = false;
                    _res.Message = r1.CONSULTAS.CONSULTA.DESCRICAO;
                    _res.xml = r1;
                    return _res;
                }
                else if (result.ToString().Contains(@"DESCRICAO=""Usu�rio ou senha inv�lida."""))
                {
                    var r1 = new CONSULTA1650();
                    r1.CONSULTAS = new CONSULTAS();
                    r1.CONSULTAS.CONSULTA = new CONSULTA();

                    r1.CONSULTAS.CONSULTA.DESCRICAO = "Usu�rio e senha inv�lidos.";
                    r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Usu�rio e senha inv�lidos.";
                    _res.status = false;
                    _res.Message = r1.CONSULTAS.CONSULTA.DESCRICAO;
                    _res.xml = r1;
                    return _res;
                }
                else if (result.ToString().Contains(@"DESCRICAO=""Pesquisa esta sendo processada, tente novamente em alguns instantes"""))
                {
                    var r1 = new CONSULTA1650();
                    r1.CONSULTAS = new CONSULTAS();
                    r1.CONSULTAS.CONSULTA = new CONSULTA();

                    r1.CONSULTAS.CONSULTA.DESCRICAO = "Pesquisa esta sendo processada, tente novamente em alguns instantes.";
                    r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Pesquisa esta sendo processada, tente novamente em alguns instantes.";
                    _res.status = false;
                    _res.Message = r1.CONSULTAS.CONSULTA.DESCRICAO;
                    _res.xml = r1;
                    return _res;
                }
                else if (result.ToString().Contains("Credenciais de usu�rio inv�lidas"))
                {
                    var r1 = new CONSULTA1650();
                    r1.CONSULTAS = new CONSULTAS();
                    r1.CONSULTAS.CONSULTA = new CONSULTA();

                    r1.CONSULTAS.CONSULTA.DESCRICAO = "Credenciais de usu�rio inv�lidas";
                    r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Credenciais de usu�rio inv�lidas";
                    _res.status = false;
                    _res.Message = r1.CONSULTAS.CONSULTA.DESCRICAO;
                    _res.xml = r1;
                    return _res;
                }
                else if (result.ToString().Contains("Credenciais para consulta API inv�lidas"))
                {
                    var r1 = new CONSULTA1650();
                    r1.CONSULTAS = new CONSULTAS();
                    r1.CONSULTAS.CONSULTA = new CONSULTA();

                    r1.CONSULTAS.CONSULTA.DESCRICAO = "Credenciais para consulta API inv�lidas";
                    r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Credenciais para consulta API inv�lidas";
                    _res.status = false;
                    _res.Message = r1.CONSULTAS.CONSULTA.DESCRICAO;
                    _res.xml = r1;
                    return _res;
                }
                else
                {
                    var r = DeserializarXMLRetorno(result);
                    _res.status = true;
                    _res.Message = r.CONSULTAS.CONSULTA.DESCRICAO;
                    _res.xml = r;
                    return _res;
                }
            }
            catch (Exception ex)
            {
                Helper.LogError("ConsultaCNHController.CS -> BuscaUnicaCNH()", ex.Message, _env);
                WebServiceResponseMotorista _res = new WebServiceResponseMotorista();
                _res.status = false;
                _res.Message = ex.Message;
                _res.xml = null;
                return _res;
            }
        }

        public static CONSULTA1650 DeserializarXMLRetorno(string xml)
        {
            try
            {
                CONSULTA1650 retorno = null;
                XmlSerializer serializer = new XmlSerializer(typeof(CONSULTA1650));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                retorno = (CONSULTA1650)serializer.Deserialize(reader);
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }
    }
}