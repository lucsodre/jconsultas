using BusinessLogicLayer;
using BusinessLogicLayer.Helpers;
using EntityLayer.Models;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using JConsultas.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Xml.Serialization;

namespace JConsultas.Controllers
{
    [Produces("application/json")]
    //[Route("api/DocDetailPage")]
    public class DocDetailPageController : Controller
    {
        private readonly SingleSearchDocumentBLL _SingleSearchDocumentBLL;
        private IHostingEnvironment _env;
        private static IOptions<ApplicationSettings> _settings;

        public DocDetailPageController(IHostingEnvironment env, EntityLayer.Models.JConsultasContext context, IOptions<ApplicationSettings> settings)
        {
            this._env = env;
            _SingleSearchDocumentBLL = new SingleSearchDocumentBLL(context);
            _settings = settings;
        }

        [HttpGet]
        public dynamic GetSigleSearch(int QId)
        {
            try
            {
                var resultado = _SingleSearchDocumentBLL.GetDocumentFromDBById(QId);
                var result = resultado.xml;
                if (result.ToString().Contains("<RETORNOEXECUCAO>999"))
                {//If xml return with Error
                    result = result.Replace("CONSULTA2300", "CONSULTA2300ERR");
                    var r1 = DeserializarXMLRetornoErr(result);
                    return new { status = -1, result = r1 };
                }
                else if (result.ToString().Contains(@"DESCRICAO=""Usu�rio ou senha inv�lida."""))
                {
                    var r1 = new CONSULTA2300(); // DeserializarXMLRetornoErr(result);
                    r1.CONSULTAS = new CONSULTA2300CONSULTAS();
                    r1.CONSULTAS.CONSULTA = new CONSULTA2300CONSULTASCONSULTA();

                    r1.CONSULTAS.CONSULTA.DESCRICAO = "Usu�rio e senha inv�lidos.";
                    r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Usu�rio e senha inv�lidos.";

                    return new { status = -1, result = r1 };
                }
                else if (result.ToString().Contains(@"DESCRICAO=""Pesquisa esta sendo processada, tente novamente em alguns instantes"""))
                {
                    var r1 = new CONSULTA2300(); // DeserializarXMLRetornoErr(result);
                    r1.CONSULTAS = new CONSULTA2300CONSULTAS();
                    r1.CONSULTAS.CONSULTA = new CONSULTA2300CONSULTASCONSULTA();

                    r1.CONSULTAS.CONSULTA.DESCRICAO = "Pesquisa esta sendo processada, tente novamente em alguns instantes.";
                    r1.CONSULTAS.CONSULTA.RETORNOEXECUCAO = "Pesquisa esta sendo processada, tente novamente em alguns instantes.";
                    return new { status = -1, result = r1 };
                }
                else
                {//If Xml return with success
                    var r = DeserializarXMLRetorno(result);
                    r.CONSULTAS.CONSULTA.CONSULTAAVANCADAVEICULO = SerializadorXML.Deserializar<ConsultaAvancadaVeiculo>(resultado.ConsultaSolicitada);
                    return r;
                }
                //var r = DeserializarXMLRetorno(result);
                //return r;
            }
            catch (Exception ex)
            {
                Helper.LogError("DocDetailPageController.CS -> GetSigleSearch()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }
        }

        public static CONSULTA2300 DeserializarXMLRetorno(string xml)
        {
            try
            {
                CONSULTA2300 retorno = null;
                XmlSerializer serializer = new XmlSerializer(typeof(CONSULTA2300));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                retorno = (CONSULTA2300)serializer.Deserialize(reader);
                //reader.Close();
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }

        public static CONSULTA2300ERR DeserializarXMLRetornoErr(string xml)
        {
            try
            {
                CONSULTA2300ERR retorno = null;
                XmlSerializer serializer = new XmlSerializer(typeof(CONSULTA2300ERR));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                retorno = (CONSULTA2300ERR)serializer.Deserialize(reader);
                //reader.Close();
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }

        //public ActionResult GeneratePDF()
        //{
        //    return new Rotativa.ActionAsPdf("GetPersons");
        //}

        [HttpGet]
        public dynamic CreatePdf(int QId)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                var resultado = _SingleSearchDocumentBLL.GetDocumentFromDBById(QId);
                var result = resultado.xml;
                if (result.ToString().Contains("<RETORNOEXECUCAO>999"))
                {//If xml return with Error
                    result = result.Replace("CONSULTA2300", "CONSULTA2300ERR");
                    var r1 = DeserializarXMLRetornoErr(result);
                    return new { status = -1, result = r1 };
                }

                CONSULTA2300 r = DeserializarXMLRetorno(result);
                r.CONSULTAS.CONSULTA.CONSULTAAVANCADAVEICULO = SerializadorXML.Deserializar<ConsultaAvancadaVeiculo>(resultado.ConsultaSolicitada);
                string sData = UtilPDF.GenerateHtmlViewPDF(r, _settings.Value.sSiteURL);

                Document document = new Document(PageSize.A4);
                document.Open();
                //string pdfName = "Receipt_" + DateTime.Now.Ticks.ToString() + ".pdf";
                string pdfName = "ConsultaVeiculo_" + r.CONSULTAS.CONSULTA.VEICULO.PLACA + "_" + DateTime.Now.Ticks.ToString() + ".pdf";
                string filepath = _env.WebRootPath + $@"\uploadpdf\" + pdfName;
                string dir = _env.WebRootPath + $@"\uploadpdf";

                bool exists = System.IO.Directory.Exists(dir);
                if (!exists)
                    System.IO.Directory.CreateDirectory(dir);

                using (FileStream fs = new FileStream(filepath, FileMode.Create))
                {
                    PdfWriter pdfWriter = PdfWriter.GetInstance(document, fs);
                    pdfWriter.PageEvent = new PdfEventHandler();

                    using (StringReader stringReader = new StringReader(sData))
                    {
                        StyleSheet styles = new StyleSheet();
                        styles.LoadTagStyle("th", "size", "10px");
                        styles.LoadTagStyle("th", "face", "helvetica");
                        styles.LoadTagStyle("td", "size", "8px");
                        styles.LoadTagStyle("td", "face", "helvetica");

                        System.Collections.ArrayList parsedList = iTextSharp.text.html.simpleparser.HtmlWorker.ParseToList(stringReader, styles);
                        document.Open();
                        foreach (object item in parsedList)
                        {
                            document.Add((IElement)item);
                        }
                        document.Close();
                    }
                }
                return new { status = 1, filename = pdfName, filepath = _env.WebRootPath + $@"\uploadpdf\" + pdfName, fullFilePath = $@"..\..\uploadpdf\" + pdfName };
            }
            catch (Exception ex)
            {
                Helper.LogError("DocDetailPageController.CS -> CreatePdf()", ex.Message, _env);
                return new { status = 0, message = ex.Message };
            }
        }

        public bool CreatePdfOnId(int QId)
        {
            try
            {
                var resultado = _SingleSearchDocumentBLL.GetDocumentFromDBById(QId);
                var result = resultado.xml;
                if (result.ToString().Contains("<RETORNOEXECUCAO>999"))
                {//If xml return with Error
                    result = result.Replace("CONSULTA2300", "CONSULTA2300ERR");
                    var r1 = DeserializarXMLRetornoErr(result);
                    return false;
                }

                CONSULTA2300 r = DeserializarXMLRetorno(result);

                r.CONSULTAS.CONSULTA.CONSULTAAVANCADAVEICULO = SerializadorXML.Deserializar<ConsultaAvancadaVeiculo>(resultado.ConsultaSolicitada);
                string sData = UtilPDF.GenerateHtmlViewPDF(r, _settings.Value.sSiteURL);

                Document document = new Document(PageSize.A4);
                document.Open();
                string pdfName = "ConsultaVeiculo_" + QId.ToString() + ".pdf";
                string filepath = _env.WebRootPath + $@"\uploadpdf\" + pdfName;
                string dir = _env.WebRootPath + $@"\uploadpdf";

                bool exists = System.IO.Directory.Exists(dir);
                if (!exists)
                    System.IO.Directory.CreateDirectory(dir);

                using (FileStream fs = new FileStream(filepath, FileMode.Create))
                {
                    PdfWriter pdfWriter = PdfWriter.GetInstance(document, fs);
                    pdfWriter.PageEvent = new PdfEventHandler();

                    using (StringReader stringReader = new StringReader(sData))
                    {
                        StyleSheet styles = new StyleSheet();
                        styles.LoadTagStyle("th", "size", "10px");
                        styles.LoadTagStyle("th", "face", "helvetica");
                        styles.LoadTagStyle("td", "size", "8px");
                        styles.LoadTagStyle("td", "face", "helvetica");

                        System.Collections.ArrayList parsedList = iTextSharp.text.html.simpleparser.HtmlWorker.ParseToList(stringReader, styles);
                        document.Open();
                        foreach (object item in parsedList)
                        {
                            document.Add((IElement)item);
                        }
                        document.Close();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Helper.LogError("DocDetailPageController.CS -> CreatePdf()", ex.Message, _env);
                return false;
            }
        }

        public dynamic CreateExcel(int QId)
        {
            try
            {
                var _DocumentXMLResponse = _SingleSearchDocumentBLL.GetDocumentFromDBById(QId);
                var result = _DocumentXMLResponse.xml;
                if (result.ToString().Contains("<RETORNOEXECUCAO>999"))
                {//If xml return with Error
                    result = result.Replace("CONSULTA2300", "CONSULTA2300ERR");
                    var r1 = DeserializarXMLRetornoErr(result);
                    return false;
                }

                //DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();
                //_DocumentXMLResponse = _SingleSearchDocumentBLL.GetDocumentFromDB(sDocumentCode, userId);

                CONSULTA2300 r = DeserializarXMLRetorno(result);

                var veiculoData = r.CONSULTAS.CONSULTA.VEICULO;
                var proprietarioData = r.CONSULTAS.CONSULTA.PROPRIETARIO;
                var gravameData = r.CONSULTAS.CONSULTA.GRAVAME;
                var gravamE_INTENCAOData = r.CONSULTAS.CONSULTA.GRAVAME_INTENCAO;
                var debitosData = r.CONSULTAS.CONSULTA.TOTAL_DEBITOS;
                var restricoesData = r.CONSULTAS.CONSULTA.RESTRICOES;
                //var multasData = r.CONSULTAS.CONSULTA.MULTAS;
                var COMUNICACAO_VENDAData = r.CONSULTAS.CONSULTA.COMUNICACAO_VENDA;
                var INSPECAO_VEICULARData = r.CONSULTAS.CONSULTA.INSPECAO_VEICULAR;

                var TAXAS_LICENCIAMENTO = r.CONSULTAS.CONSULTA.TAXAS_LICENCIAMENTO;
                var DPVATS = r.CONSULTAS.CONSULTA.DPVATS;
                var IPVAS = r.CONSULTAS.CONSULTA.IPVAS;
                var NOTIFICACOES = r.CONSULTAS.CONSULTA.NOTIFICACOES;
                var MULTAS = r.CONSULTAS.CONSULTA.MULTAS;
                var MULTASRENAINF = r.CONSULTAS.CONSULTA.MULTASRENAINF;
                var BLOQUEIOSRENAJUD = r.CONSULTAS.CONSULTA.BLOQUEIOSRENAJUD;
                var BLOQUEIOSDETRAN = r.CONSULTAS.CONSULTA.BLOQUEIOSDETRAN;

                var consultaAvancada = _DocumentXMLResponse.ConsultaSolicitada;

                var consultaAvancadaVeic = SerializadorXML.Deserializar<ConsultaAvancadaVeiculo>(_DocumentXMLResponse.ConsultaSolicitada);

                string sFileName = "Excel_" + _DocumentXMLResponse.QueryResultid.ToString() + ".xlsx";
                string sWebRootFolder = _env.WebRootPath + $@"\uploadExcel\";
                string dir = _env.WebRootPath + $@"\uploadExcel";

                string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
                FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                if (file.Exists)
                {
                    file.Delete();
                    file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                }
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    #region Condi��es da tela
                    List<string> parametrosConsulta = new List<string>();
                    bool pesquisaCompleta = true;
                    if (consultaAvancadaVeic != null)
                    {
                        if (!consultaAvancadaVeic.DadosVeic)
                        {
                            veiculoData = null;
                            pesquisaCompleta = false;
                        }
                        else
                        {
                            parametrosConsulta.Add("Dados do ve�culo");
                        }

                        if (!consultaAvancadaVeic.ArrendarioFinanceira)
                        {
                            gravameData = null;
                            pesquisaCompleta = false;
                        }
                        else
                        {
                            parametrosConsulta.Add("Gravame");
                        }

                        if (!consultaAvancadaVeic.BloqueiosDetran)
                        {
                            BLOQUEIOSDETRAN = null;
                            pesquisaCompleta = false;
                        }
                        else
                        {
                            parametrosConsulta.Add("Bloqueios Detran");
                        }

                        if (!consultaAvancadaVeic.DadosProp)
                        {
                            proprietarioData = null;
                            pesquisaCompleta = false;
                        }
                        else
                        {
                            parametrosConsulta.Add("Dados do propriet�rio");
                        }

                        if (!consultaAvancadaVeic.DetalhamentoDebitos)
                        {
                            debitosData = null;
                            pesquisaCompleta = false;
                        }
                        else
                        {
                            parametrosConsulta.Add("Detalhamento dos d�bitos");
                        }

                        if (!consultaAvancadaVeic.DetalhamentoDPVAT)
                        {
                            DPVATS = null;
                            pesquisaCompleta = false;
                        }
                        else
                        {
                            parametrosConsulta.Add("Detalhamento DPVAT");
                        }

                        if (!consultaAvancadaVeic.DetalhamentoIPVA)
                        {
                            IPVAS = null;
                            pesquisaCompleta = false;
                        }
                        else
                        {
                            parametrosConsulta.Add("Detalhamento IPVA");
                        }

                        if (!consultaAvancadaVeic.DetalhamentoMultas)
                        {
                            MULTAS = null;
                            pesquisaCompleta = false;
                        }
                        else
                        {
                            parametrosConsulta.Add("Detalhamento das multas");
                        }

                        if (!consultaAvancadaVeic.InspecaoVeicular)
                        {
                            INSPECAO_VEICULARData = null;
                            pesquisaCompleta = false;
                        }
                        else
                        {
                            parametrosConsulta.Add("Inspe��o veicular");
                        }

                        if (!consultaAvancadaVeic.IntencaoGravame)
                        {
                            gravamE_INTENCAOData = null;
                            pesquisaCompleta = false;
                        }
                        else
                        {
                            parametrosConsulta.Add("Inten��o de gravame");
                        }

                        if (!consultaAvancadaVeic.Restricoes)
                        {
                            restricoesData = null;
                            pesquisaCompleta = false;
                        }
                        else
                        {
                            parametrosConsulta.Add("Restri��es");
                        }
                    }
                    #endregion

                    // 1. VEICULO
                    ExcelWorksheet VEICULO = package.Workbook.Worksheets.Add("VEICULO");

                    if (veiculoData != null)
                    {
                        VEICULO.Cells[1, 1].Value = "PLACA";
                        VEICULO.Cells[2, 1].Value = Convert.ToString(veiculoData.PLACA);

                        VEICULO.Cells[1, 2].Value = "CHASSI";
                        VEICULO.Cells[2, 2].Value = Convert.ToString(veiculoData.CHASSI);

                        VEICULO.Cells[1, 3].Value = "RENAVAM";
                        VEICULO.Cells[2, 3].Value = Convert.ToString(veiculoData.RENAVAM);

                        VEICULO.Cells[1, 4].Value = "MUNICIPIO";
                        VEICULO.Cells[2, 4].Value = Convert.ToString(veiculoData.MUNICIPIO);

                        VEICULO.Cells[1, 5].Value = "MUNICIPIODESC";
                        VEICULO.Cells[2, 5].Value = Convert.ToString(veiculoData.MUNICIPIODESC);

                        VEICULO.Cells[1, 6].Value = "UF";
                        VEICULO.Cells[2, 6].Value = Convert.ToString(veiculoData.UF);

                        VEICULO.Cells[1, 7].Value = "PROCEDENCIA";
                        VEICULO.Cells[2, 7].Value = Convert.ToString(veiculoData.PROCEDENCIA);

                        VEICULO.Cells[1, 8].Value = "PROCEDENCIADESC";
                        VEICULO.Cells[2, 8].Value = Convert.ToString(veiculoData.PROCEDENCIADESC);

                        VEICULO.Cells[1, 9].Value = "MARCA";
                        VEICULO.Cells[2, 9].Value = Convert.ToString(veiculoData.MARCA);

                        VEICULO.Cells[1, 10].Value = "MARCADESC";
                        VEICULO.Cells[2, 10].Value = Convert.ToString(veiculoData.MARCADESC);

                        VEICULO.Cells[1, 11].Value = "ANOFABRICACAO";
                        VEICULO.Cells[2, 11].Value = Convert.ToString(veiculoData.ANOFABRICACAO);

                        VEICULO.Cells[1, 12].Value = "ANOMODELO";
                        VEICULO.Cells[2, 12].Value = Convert.ToString(veiculoData.ANOMODELO);

                        VEICULO.Cells[1, 13].Value = "TIPO";
                        VEICULO.Cells[2, 13].Value = Convert.ToString(veiculoData.TIPO);

                        VEICULO.Cells[1, 14].Value = "TIPODESC";
                        VEICULO.Cells[2, 14].Value = Convert.ToString(veiculoData.TIPODESC);

                        VEICULO.Cells[1, 15].Value = "CARROCERIA";
                        VEICULO.Cells[2, 15].Value = Convert.ToString(veiculoData.CARROCERIA);

                        VEICULO.Cells[1, 16].Value = "CARROCERIADESC";
                        VEICULO.Cells[2, 16].Value = Convert.ToString(veiculoData.CARROCERIADESC);

                        VEICULO.Cells[1, 17].Value = "COR";
                        VEICULO.Cells[2, 17].Value = Convert.ToString(veiculoData.COR);

                        VEICULO.Cells[1, 18].Value = "CORDESC";
                        VEICULO.Cells[2, 18].Value = Convert.ToString(veiculoData.CORDESC);

                        VEICULO.Cells[1, 19].Value = "CATEGORIA";
                        VEICULO.Cells[2, 19].Value = Convert.ToString(veiculoData.CATEGORIA);

                        VEICULO.Cells[1, 20].Value = "CATEGORIADESC";
                        VEICULO.Cells[2, 20].Value = Convert.ToString(veiculoData.CATEGORIADESC);

                        VEICULO.Cells[1, 21].Value = "COMBUSTIVEL";
                        VEICULO.Cells[2, 21].Value = Convert.ToString(veiculoData.COMBUSTIVEL);

                        VEICULO.Cells[1, 22].Value = "COMBUSTIVELDESC";
                        VEICULO.Cells[2, 22].Value = Convert.ToString(veiculoData.COMBUSTIVELDESC);

                        VEICULO.Cells[1, 23].Value = "ESPECIE";
                        VEICULO.Cells[2, 23].Value = Convert.ToString(veiculoData.ESPECIE);

                        VEICULO.Cells[1, 24].Value = "ESPECIEDESC";
                        VEICULO.Cells[2, 24].Value = Convert.ToString(veiculoData.ESPECIEDESC);

                        VEICULO.Cells[1, 25].Value = "CAPACIDADEPASSAGEIRO";
                        VEICULO.Cells[2, 25].Value = Convert.ToString(veiculoData.CAPACIDADEPASSAGEIRO);

                        VEICULO.Cells[1, 26].Value = "CAPACIDADECARGA";
                        VEICULO.Cells[2, 26].Value = Convert.ToString(veiculoData.CAPACIDADECARGA);

                        VEICULO.Cells[1, 27].Value = "POTENCIA";
                        VEICULO.Cells[2, 27].Value = Convert.ToString(veiculoData.POTENCIA);

                        VEICULO.Cells[1, 28].Value = "CILINDRADA";
                        VEICULO.Cells[2, 28].Value = Convert.ToString(veiculoData.CILINDRADA);

                        VEICULO.Cells[1, 29].Value = "CMT";
                        VEICULO.Cells[2, 29].Value = Convert.ToString(veiculoData.CMT);

                        VEICULO.Cells[1, 30].Value = "PBT";
                        VEICULO.Cells[2, 30].Value = Convert.ToString(veiculoData.PBT);

                        VEICULO.Cells[1, 31].Value = "EIXOS";
                        VEICULO.Cells[2, 31].Value = Convert.ToString(veiculoData.EIXOS);

                        VEICULO.Cells[1, 32].Value = "NUMEROMOTOR";
                        VEICULO.Cells[2, 32].Value = Convert.ToString(veiculoData.NUMEROMOTOR);

                        VEICULO.Cells[1, 33].Value = "DATAALTERACAO";
                        VEICULO.Cells[2, 33].Value = Convert.ToString(veiculoData.DATAALTERACAO);

                        VEICULO.Cells[1, 34].Value = "NUMEROCAMBIO";
                        VEICULO.Cells[2, 34].Value = Convert.ToString(veiculoData.NUMEROCAMBIO);

                        VEICULO.Cells[1, 35].Value = "TIPOMONTAGEM";
                        VEICULO.Cells[2, 35].Value = Convert.ToString(veiculoData.TIPOMONTAGEM);

                        VEICULO.Cells[1, 36].Value = "SITUACAOVEICULO";
                        VEICULO.Cells[2, 36].Value = Convert.ToString(veiculoData.SITUACAOVEICULO);

                        VEICULO.Cells[1, 37].Value = "TIPOREMARCACAOCHASSI";
                        VEICULO.Cells[2, 37].Value = Convert.ToString(veiculoData.TIPOREMARCACAOCHASSI);

                        VEICULO.Cells[1, 38].Value = "DATAEMISSAOCRV";
                        VEICULO.Cells[2, 38].Value = Convert.ToString(veiculoData.DATAEMISSAOCRV);

                        VEICULO.Cells[1, 39].Value = "DATALICENCIAMENTO";
                        VEICULO.Cells[2, 39].Value = Convert.ToString(veiculoData.DATALICENCIAMENTO);

                        VEICULO.Cells[1, 40].Value = "EXERCICIOLICENCIAMENTO";
                        VEICULO.Cells[2, 40].Value = Convert.ToString(veiculoData.EXERCICIOLICENCIAMENTO);

                        VEICULO.Cells[1, 41].Value = "VALORNF";
                        VEICULO.Cells[2, 41].Value = Convert.ToString(veiculoData.VALORNF);

                        VEICULO.Cells[1, 42].Value = "NUMERONF";
                        VEICULO.Cells[2, 42].Value = Convert.ToString(veiculoData.NUMERONF);

                        VEICULO.Cells[1, 43].Value = "DATANF";
                        VEICULO.Cells[2, 43].Value = Convert.ToString(veiculoData.DATANF);


                    }

                    if (proprietarioData != null)
                    {
                        // 1.  Add PROPRIETARIO data
                        VEICULO.Cells[1, 44].Value = "TIPOCPFPROPRIETARIO";
                        VEICULO.Cells[2, 44].Value = Convert.ToString(proprietarioData.TIPOCPFPROPRIETARIO);

                        VEICULO.Cells[1, 45].Value = "CPFPROPRIETARIO";
                        VEICULO.Cells[2, 45].Value = Convert.ToString(proprietarioData.CPFPROPRIETARIO);

                        VEICULO.Cells[1, 46].Value = "NOMEPROPRIETARIO";
                        VEICULO.Cells[2, 46].Value = Convert.ToString(proprietarioData.NOMEPROPRIETARIO);

                        VEICULO.Cells[1, 47].Value = "RG";
                        VEICULO.Cells[2, 47].Value = Convert.ToString(proprietarioData.RG);

                        VEICULO.Cells[1, 48].Value = "ORGEXP";
                        VEICULO.Cells[2, 48].Value = Convert.ToString(proprietarioData.ORGEXP);

                        VEICULO.Cells[1, 49].Value = "ENDERECO";
                        VEICULO.Cells[2, 49].Value = Convert.ToString(proprietarioData.ENDERECO);

                        VEICULO.Cells[1, 50].Value = "COMPLEMENTO";
                        VEICULO.Cells[2, 50].Value = Convert.ToString(proprietarioData.COMPLEMENTO);

                        VEICULO.Cells[1, 51].Value = "BAIRRO";
                        VEICULO.Cells[2, 51].Value = Convert.ToString(proprietarioData.BAIRRO);

                        VEICULO.Cells[1, 52].Value = "CIDADE";
                        VEICULO.Cells[2, 52].Value = Convert.ToString(proprietarioData.CIDADE);

                        VEICULO.Cells[1, 53].Value = "CEP";
                        VEICULO.Cells[2, 53].Value = Convert.ToString(proprietarioData.CEP);

                        VEICULO.Cells[1, 54].Value = "NUMERO";
                        VEICULO.Cells[2, 54].Value = Convert.ToString(proprietarioData.NUMERO);

                        VEICULO.Cells[1, 55].Value = "DDD";
                        VEICULO.Cells[2, 55].Value = Convert.ToString(proprietarioData.DDD);

                        VEICULO.Cells[1, 56].Value = "TELEFONE";
                        VEICULO.Cells[2, 56].Value = Convert.ToString(proprietarioData.TELEFONE);

                        VEICULO.Cells[1, 57].Value = "NOMEPROPRIETARIOANTERIOR";
                        VEICULO.Cells[2, 57].Value = Convert.ToString(proprietarioData.NOMEPROPRIETARIOANTERIOR);
                    }

                    if (gravameData != null)
                    {
                        // 1.  Add GRAVAME data
                        VEICULO.Cells[1, 58].Value = "CNPJFINANCIADO";
                        VEICULO.Cells[2, 58].Value = Convert.ToString(gravameData.CNPJFINANCIADO);

                        VEICULO.Cells[1, 59].Value = "ARRENDATARIOFINANCIADO";
                        VEICULO.Cells[2, 59].Value = Convert.ToString(gravameData.ARRENDATARIOFINANCIADO);

                        VEICULO.Cells[1, 60].Value = "TIPOTRANSACAO";
                        VEICULO.Cells[2, 60].Value = Convert.ToString(gravameData.TIPOTRANSACAO);

                        VEICULO.Cells[1, 61].Value = "TIPOTRANSACAODESC";
                        VEICULO.Cells[2, 61].Value = Convert.ToString(gravameData.TIPOTRANSACAODESC);

                        VEICULO.Cells[1, 62].Value = "TIPORESTRICAOFINANCEIRA";
                        VEICULO.Cells[2, 62].Value = Convert.ToString(gravameData.TIPORESTRICAOFINANCEIRA);

                        VEICULO.Cells[1, 63].Value = "TIPORESTRICAOFINANCEIRADESC";
                        VEICULO.Cells[2, 63].Value = Convert.ToString(gravameData.TIPORESTRICAOFINANCEIRADESC);

                        VEICULO.Cells[1, 64].Value = "CNPJFINANCEIRA";
                        VEICULO.Cells[2, 64].Value = Convert.ToString(gravameData.CNPJFINANCEIRA);

                        VEICULO.Cells[1, 65].Value = "AGENTEFINANCEIRO";
                        VEICULO.Cells[2, 65].Value = Convert.ToString(gravameData.AGENTEFINANCEIRO);

                        VEICULO.Cells[1, 66].Value = "AGENTEFINANCEIRODESC";
                        VEICULO.Cells[2, 66].Value = Convert.ToString(gravameData.AGENTEFINANCEIRODESC);

                        VEICULO.Cells[1, 67].Value = "NUMEROCONTRATO";
                        VEICULO.Cells[2, 67].Value = Convert.ToString(gravameData.NUMEROCONTRATO);

                        VEICULO.Cells[1, 68].Value = "DATAVIGENCIACONTRATO";
                        VEICULO.Cells[2, 68].Value = Convert.ToString(gravameData.DATAVIGENCIACONTRATO);

                        VEICULO.Cells[1, 69].Value = "DATAINCLUSAOFINANCIAMENTO";
                        VEICULO.Cells[2, 69].Value = Convert.ToString(gravameData.DATAINCLUSAOFINANCIAMENTO);

                        VEICULO.Cells[1, 70].Value = "DATACONTRATO";
                        VEICULO.Cells[2, 70].Value = Convert.ToString(gravameData.DATACONTRATO);

                        VEICULO.Cells[1, 71].Value = "UF_GRAVAME";
                        VEICULO.Cells[2, 71].Value = Convert.ToString(gravameData.UF_GRAVAME);

                        VEICULO.Cells[1, 72].Value = "NUMERORESTRICAO";
                        VEICULO.Cells[2, 72].Value = Convert.ToString(gravameData.NUMERORESTRICAO);

                        VEICULO.Cells[1, 73].Value = "SITUACAO";
                        VEICULO.Cells[2, 73].Value = Convert.ToString(gravameData.SITUACAO);
                    }

                    if (gravamE_INTENCAOData != null)
                    {
                        // 1.  Add GRAVAME_INTENCAO
                        VEICULO.Cells[1, 74].Value = "CNPJFINANCEIRA";
                        VEICULO.Cells[2, 74].Value = Convert.ToString(gravamE_INTENCAOData.CNPJFINANCEIRA);

                        VEICULO.Cells[1, 75].Value = "INFORMANTEFINANCIAMENTO";
                        VEICULO.Cells[2, 75].Value = Convert.ToString(gravamE_INTENCAOData.INFORMANTEFINANCIAMENTO);

                        VEICULO.Cells[1, 76].Value = "TIPOTRANSACAO";
                        VEICULO.Cells[2, 76].Value = Convert.ToString(gravamE_INTENCAOData.TIPOTRANSACAO);

                        VEICULO.Cells[1, 77].Value = "TIPOTRANSACAODESC";
                        VEICULO.Cells[2, 77].Value = Convert.ToString(gravamE_INTENCAOData.TIPOTRANSACAODESC);

                        VEICULO.Cells[1, 78].Value = "TIPORESTRICAOFINANCEIRA";
                        VEICULO.Cells[2, 78].Value = Convert.ToString(gravamE_INTENCAOData.TIPORESTRICAOFINANCEIRA);

                        VEICULO.Cells[1, 79].Value = "TIPORESTRICAOFINANCEIRADESC";
                        VEICULO.Cells[2, 79].Value = Convert.ToString(gravamE_INTENCAOData.TIPORESTRICAOFINANCEIRADESC);

                        VEICULO.Cells[1, 80].Value = "AGENTEFINANCEIRO";
                        VEICULO.Cells[2, 80].Value = Convert.ToString(gravamE_INTENCAOData.AGENTEFINANCEIRO);

                        VEICULO.Cells[1, 81].Value = "AGENTEFINANCEIRODESC";
                        VEICULO.Cells[2, 81].Value = Convert.ToString(gravamE_INTENCAOData.AGENTEFINANCEIRODESC);

                        VEICULO.Cells[1, 82].Value = "CNPJFINANCIADO";
                        VEICULO.Cells[2, 82].Value = Convert.ToString(gravamE_INTENCAOData.CNPJFINANCIADO);

                        VEICULO.Cells[1, 83].Value = "NOMEFINANCIADO";
                        VEICULO.Cells[2, 83].Value = Convert.ToString(gravamE_INTENCAOData.NOMEFINANCIADO);

                        VEICULO.Cells[1, 84].Value = "DATAINCLUSAOINTENCAOTROCAFINANC";
                        VEICULO.Cells[2, 84].Value = Convert.ToString(gravamE_INTENCAOData.DATAINCLUSAOINTENCAOTROCAFINANC);

                        VEICULO.Cells[1, 85].Value = "NUMEROCONTRATOFINANC";
                        VEICULO.Cells[2, 85].Value = Convert.ToString(gravamE_INTENCAOData.NUMEROCONTRATOFINANC);

                        VEICULO.Cells[1, 86].Value = "DATAVIGENCIACONTRATOFINANC";
                        VEICULO.Cells[2, 86].Value = Convert.ToString(gravamE_INTENCAOData.DATAVIGENCIACONTRATOFINANC);

                        VEICULO.Cells[1, 87].Value = "UF_GRAVAME";
                        VEICULO.Cells[2, 87].Value = Convert.ToString(gravamE_INTENCAOData.UF_GRAVAME);

                        VEICULO.Cells[1, 88].Value = "NUMERORESTRICAO";
                        VEICULO.Cells[2, 88].Value = Convert.ToString(gravamE_INTENCAOData.NUMERORESTRICAO);

                        VEICULO.Cells[1, 89].Value = "SITUACAO";
                        VEICULO.Cells[2, 89].Value = Convert.ToString(gravamE_INTENCAOData.SITUACAO);
                    }

                    if (restricoesData != null)
                    {
                        // 1.  Add RESTRICOES
                        VEICULO.Cells[1, 90].Value = "RESTRICOES";
                        VEICULO.Cells[2, 90].Value = Convert.ToString(restricoesData);
                    }

                    if (debitosData != null)
                    {
                        // 1 Add TOTAL_DEBITOS

                        VEICULO.Cells[1, 91].Value = "DEBITOIPVALICENCIAMENTO_DESC";
                        VEICULO.Cells[2, 91].Value = Convert.ToString(debitosData.DEBITOIPVALICENCIAMENTO_DESC);

                        VEICULO.Cells[1, 92].Value = "DEBITOSMULTAS_DESC";
                        VEICULO.Cells[2, 92].Value = Convert.ToString(debitosData.DEBITOSMULTAS_DESC);

                        VEICULO.Cells[1, 93].Value = "DEBITOSDIVIDAATIVA_DESC";
                        VEICULO.Cells[2, 93].Value = Convert.ToString(debitosData.DEBITOSDIVIDAATIVA_DESC);

                        VEICULO.Cells[1, 94].Value = "IPVA";
                        VEICULO.Cells[2, 94].Value = Convert.ToString(debitosData.IPVA);

                        VEICULO.Cells[1, 95].Value = "LICENCIAMENTO";
                        VEICULO.Cells[2, 95].Value = Convert.ToString(debitosData.LICENCIAMENTO);

                        VEICULO.Cells[1, 96].Value = "MULTAS";
                        VEICULO.Cells[2, 96].Value = Convert.ToString(debitosData.MULTAS);

                        VEICULO.Cells[1, 97].Value = "DPVAT";
                        VEICULO.Cells[2, 97].Value = Convert.ToString(debitosData.DPVAT);

                        VEICULO.Cells[1, 98].Value = "IPVA_DIVIDAATIVA";
                        VEICULO.Cells[2, 98].Value = Convert.ToString(debitosData.IPVA_DIVIDAATIVA);
                    }

                    if (COMUNICACAO_VENDAData != null)
                    {
                        // 1 Add COMUNICACAO_VENDA
                        VEICULO.Cells[1, 99].Value = "COMUNICACAOVENDA";
                        VEICULO.Cells[2, 99].Value = Convert.ToString(COMUNICACAO_VENDAData.COMUNICACAOVENDA);

                        VEICULO.Cells[1, 100].Value = "COMUNICACAOVENDADESC";
                        VEICULO.Cells[2, 100].Value = Convert.ToString(COMUNICACAO_VENDAData.COMUNICACAOVENDADESC);

                        VEICULO.Cells[1, 101].Value = "DATAINCLUSAOCOMUNICACAOVENDAS";
                        VEICULO.Cells[2, 101].Value = Convert.ToString(COMUNICACAO_VENDAData.DATAINCLUSAOCOMUNICACAOVENDAS);

                        VEICULO.Cells[1, 102].Value = "DATAVENDA";
                        VEICULO.Cells[2, 102].Value = Convert.ToString(COMUNICACAO_VENDAData.DATAVENDA);

                        VEICULO.Cells[1, 103].Value = "TIPODOCTOCOMPRADOR";
                        VEICULO.Cells[2, 103].Value = Convert.ToString(COMUNICACAO_VENDAData.TIPODOCTOCOMPRADOR);

                        VEICULO.Cells[1, 104].Value = "CNPJCPFCOMPRADOR";
                        VEICULO.Cells[2, 104].Value = Convert.ToString(COMUNICACAO_VENDAData.CNPJCPFCOMPRADOR);

                        VEICULO.Cells[1, 105].Value = "DATANOTAFISCAL";
                        VEICULO.Cells[2, 105].Value = Convert.ToString(COMUNICACAO_VENDAData.DATANOTAFISCAL);

                        VEICULO.Cells[1, 106].Value = "DATAPROTOCOLODETRAN";
                        VEICULO.Cells[2, 106].Value = Convert.ToString(COMUNICACAO_VENDAData.DATAPROTOCOLODETRAN);
                    }

                    if (INSPECAO_VEICULARData != null)
                    {
                        // 1 Add INSPECAO_VEICULAR
                        VEICULO.Cells[1, 107].Value = "INSPECAOVEICULAR";
                        VEICULO.Cells[2, 107].Value = Convert.ToString(INSPECAO_VEICULARData.INSPECAOVEICULAR);

                        VEICULO.Cells[1, 108].Value = "INSPECAOVEICULARDESC";
                        VEICULO.Cells[2, 108].Value = Convert.ToString(INSPECAO_VEICULARData.INSPECAOVEICULARDESC);

                        VEICULO.Cells[1, 109].Value = "DATAINSPECAOVEICULAR";
                        VEICULO.Cells[2, 109].Value = Convert.ToString(INSPECAO_VEICULARData.DATAINSPECAOVEICULAR);

                        VEICULO.Cells[1, 110].Value = "DATAINCLUSACAOINSPECAOVEICULAR";
                        VEICULO.Cells[2, 110].Value = Convert.ToString(INSPECAO_VEICULARData.DATAINCLUSACAOINSPECAOVEICULAR);
                    }

                    // 2. TAXAS_LICENCIAMENTO
                    // ExcelWorksheet _TAXAS_LICENCIAMENTO = package.Workbook.Worksheets.Add("TAXAS_LICENCIAMENTO");

                    if (DPVATS != null)
                    {
                        // 3. DPVATS
                        ExcelWorksheet _DPVATS = package.Workbook.Worksheets.Add("DPVATS");
                        for (int i = 0; i < DPVATS.Length; i++)
                        {
                            var no = i + 2;
                            if (i == 0)
                            {
                                _DPVATS.Cells[1, 1].Value = "ORIGEM";
                                _DPVATS.Cells[1, 2].Value = "ANOREF";
                                _DPVATS.Cells[1, 3].Value = "VALDPVAT";
                                _DPVATS.Cells[1, 4].Value = "NUMPARCELA";
                                _DPVATS.Cells[1, 5].Value = "DTVENCTODPVAT";
                                _DPVATS.Cells[1, 6].Value = "CATEGORIA";
                            }

                            _DPVATS.Cells[no, 1].Value = Convert.ToString(DPVATS[i].ORIGEM);
                            _DPVATS.Cells[no, 2].Value = Convert.ToString(DPVATS[i].ANOREF);
                            _DPVATS.Cells[no, 3].Value = Convert.ToString(DPVATS[i].VALDPVAT);
                            _DPVATS.Cells[no, 4].Value = Convert.ToString(DPVATS[i].NUMPARCELA);
                            _DPVATS.Cells[no, 5].Value = Convert.ToString(DPVATS[i].DTVENCTODPVAT);
                            _DPVATS.Cells[no, 6].Value = Convert.ToString(DPVATS[i].CATEGORIA);
                        }
                    }

                    if (IPVAS != null)
                    {
                        // 4. IPVAS
                        ExcelWorksheet _IPVAS = package.Workbook.Worksheets.Add("IPVAS");
                        for (int i = 0; i < IPVAS.Length; i++)
                        {
                            var no = i + 2;
                            if (i == 0)
                            {
                                _IPVAS.Cells[1, 1].Value = "ORIGEM";
                                _IPVAS.Cells[1, 2].Value = "ANOREF";
                                _IPVAS.Cells[1, 3].Value = "VALIPVA";
                                _IPVAS.Cells[1, 4].Value = "COTAIPVA";
                                _IPVAS.Cells[1, 5].Value = "COTAIPVADES";
                                _IPVAS.Cells[1, 6].Value = "DTVENCTOIPVA";
                            }

                            _IPVAS.Cells[no, 1].Value = Convert.ToString(IPVAS[i].ORIGEM);
                            _IPVAS.Cells[no, 2].Value = Convert.ToString(IPVAS[i].ANOREF);
                            _IPVAS.Cells[no, 3].Value = Convert.ToString(IPVAS[i].VALIPVA);
                            _IPVAS.Cells[no, 4].Value = Convert.ToString(IPVAS[i].COTAIPVA);
                            _IPVAS.Cells[no, 5].Value = Convert.ToString(IPVAS[i].COTAIPVADES);
                            _IPVAS.Cells[no, 6].Value = Convert.ToString(IPVAS[i].DTVENCTOIPVA);
                        }
                    }

                    if (MULTAS != null)
                    {
                        // 5. MULTAS 
                        ExcelWorksheet _MULTAS = package.Workbook.Worksheets.Add("MULTAS");
                        for (int i = 0; i < MULTAS.Length; i++)
                        {
                            var no = i + 2;
                            if (i == 0)
                            {
                                _MULTAS.Cells[1, 1].Value = "ORIGEM";
                                _MULTAS.Cells[1, 2].Value = "NUMGUIA";
                                _MULTAS.Cells[1, 3].Value = "NUMAIIP";
                                _MULTAS.Cells[1, 4].Value = "CODREC";
                                _MULTAS.Cells[1, 5].Value = "DTINFRA";
                                _MULTAS.Cells[1, 6].Value = "LOCALINFRA";
                                _MULTAS.Cells[1, 7].Value = "HORAINFRA";
                                _MULTAS.Cells[1, 8].Value = "MUNINFRA";
                                _MULTAS.Cells[1, 9].Value = "ENQUADRAM";
                                _MULTAS.Cells[1, 10].Value = "ENQUADRAMDESC";
                                _MULTAS.Cells[1, 11].Value = "ENQUADRAMLEG";
                                _MULTAS.Cells[1, 12].Value = "DTVENCTOINFRA";
                                _MULTAS.Cells[1, 13].Value = "VALMULTA";
                                _MULTAS.Cells[1, 14].Value = "ENTTRANSITANT";
                                _MULTAS.Cells[1, 15].Value = "NOMEENTTRANSITANT";
                                _MULTAS.Cells[1, 16].Value = "MUNINFRADESC";
                                _MULTAS.Cells[1, 17].Value = "EMPRESA";
                                _MULTAS.Cells[1, 18].Value = "TIPO";
                            }

                            _MULTAS.Cells[no, 1].Value = Convert.ToString(MULTAS[i].ORIGEM);
                            _MULTAS.Cells[no, 2].Value = Convert.ToString(MULTAS[i].NUMGUIA);
                            _MULTAS.Cells[no, 3].Value = Convert.ToString(MULTAS[i].NUMAIIP);
                            _MULTAS.Cells[no, 4].Value = Convert.ToString(MULTAS[i].CODREC);
                            _MULTAS.Cells[no, 5].Value = Convert.ToString(MULTAS[i].DTINFRA);
                            _MULTAS.Cells[no, 6].Value = Convert.ToString(MULTAS[i].LOCALINFRA);
                            _MULTAS.Cells[no, 7].Value = Convert.ToString(MULTAS[i].HORAINFRA);
                            _MULTAS.Cells[no, 8].Value = Convert.ToString(MULTAS[i].MUNINFRA);
                            _MULTAS.Cells[no, 9].Value = Convert.ToString(MULTAS[i].ENQUADRAM);
                            _MULTAS.Cells[no, 10].Value = Convert.ToString(MULTAS[i].ENQUADRAMDESC);
                            _MULTAS.Cells[no, 11].Value = Convert.ToString(MULTAS[i].ENQUADRAMLEG);
                            _MULTAS.Cells[no, 12].Value = Convert.ToString(MULTAS[i].DTVENCTOINFRA);
                            _MULTAS.Cells[no, 13].Value = Convert.ToString(MULTAS[i].VALMULTA);
                            _MULTAS.Cells[no, 14].Value = Convert.ToString(MULTAS[i].ENTTRANSITANT);
                            _MULTAS.Cells[no, 15].Value = Convert.ToString(MULTAS[i].NOMEENTTRANSITANT);
                            _MULTAS.Cells[no, 16].Value = Convert.ToString(MULTAS[i].MUNINFRADESC);
                            _MULTAS.Cells[no, 17].Value = Convert.ToString(MULTAS[i].EMPRESA);
                            _MULTAS.Cells[no, 18].Value = (MULTAS[i].NOTIFICACAO == "0" ? "Infra��o" : "Notifica��o");
                        }
                    }

                    // 6. MULTASRENAINF
                    // ExcelWorksheet _MULTASRENAINF = package.Workbook.Worksheets.Add("MULTASRENAINF");

                    // 7. BLOQUEIOSRENAJUD
                    //ExcelWorksheet _BLOQUEIOSRENAJUD = package.Workbook.Worksheets.Add("BLOQUEIOSRENAJUD");

                    if (BLOQUEIOSDETRAN != null)
                    {
                        // 8. BLOQUEIOSDETRAN
                        ExcelWorksheet _BLOQUEIOSDETRAN = package.Workbook.Worksheets.Add("BLOQUEIOSDETRAN");
                        if (!string.IsNullOrEmpty(Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN)))
                        {
                            _BLOQUEIOSDETRAN.Cells[1, 1].Value = "TIPO";
                            _BLOQUEIOSDETRAN.Cells[2, 1].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.TIPO);

                            _BLOQUEIOSDETRAN.Cells[1, 2].Value = "MOTIVO";
                            _BLOQUEIOSDETRAN.Cells[2, 2].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.MOTIVO);

                            _BLOQUEIOSDETRAN.Cells[1, 3].Value = "DATAINCLUSAO";
                            _BLOQUEIOSDETRAN.Cells[2, 3].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.DATAINCLUSAO);

                            _BLOQUEIOSDETRAN.Cells[1, 4].Value = "PROTOCOLO_NUMERO";
                            _BLOQUEIOSDETRAN.Cells[2, 4].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.PROTOCOLO_NUMERO);

                            _BLOQUEIOSDETRAN.Cells[1, 5].Value = "PROTOCOLO_ANO";
                            _BLOQUEIOSDETRAN.Cells[2, 5].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.PROTOCOLO_ANO);

                            _BLOQUEIOSDETRAN.Cells[1, 6].Value = "PROCESSO_NUMERO";
                            _BLOQUEIOSDETRAN.Cells[2, 6].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.PROCESSO_NUMERO);

                            _BLOQUEIOSDETRAN.Cells[1, 7].Value = "PROCESSO_ANO";
                            _BLOQUEIOSDETRAN.Cells[2, 7].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.PROCESSO_ANO);

                            _BLOQUEIOSDETRAN.Cells[1, 8].Value = "OFICIO_NUMERO";
                            _BLOQUEIOSDETRAN.Cells[2, 8].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.OFICIO_NUMERO);

                            _BLOQUEIOSDETRAN.Cells[1, 9].Value = "OFICIO_ANO";
                            _BLOQUEIOSDETRAN.Cells[2, 9].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.OFICIO_ANO);

                            _BLOQUEIOSDETRAN.Cells[1, 10].Value = "IDCIDADE";
                            _BLOQUEIOSDETRAN.Cells[2, 10].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.IDCIDADE);

                            _BLOQUEIOSDETRAN.Cells[1, 11].Value = "MUNICIPIO";
                            _BLOQUEIOSDETRAN.Cells[2, 11].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.MUNICIPIO);
                        }
                    }

                    if (!pesquisaCompleta)
                    {
                        // 5. Parametros da consulta
                        ExcelWorksheet parametrosConsultaSheet = package.Workbook.Worksheets.Add("PAR�METROS DA CONSULTA");
                        for (int i = 0; i < parametrosConsulta.Count; i++)
                        {
                            var no = i + 2;
                            if (i == 0)
                            {
                                parametrosConsultaSheet.Cells[1, 1].Value = "DADOS SOLICITADOS";
                            }

                            parametrosConsultaSheet.Cells[no, 1].Value = parametrosConsulta[i];
                        }
                    }
                    else
                    {
                        // 5. Parametros da consulta
                        ExcelWorksheet parametrosConsultaSheet = package.Workbook.Worksheets.Add("PAR�METROS DA CONSULTA");
                        parametrosConsultaSheet.Cells[1, 1].Value = "DADOS SOLICITADOS";


                        parametrosConsultaSheet.Cells[2, 1].Value = "Pesquisa completa";
                    }

                    package.Save();
                }

                return new { status = 1, filename = sFileName, filepath = _env.WebRootPath + $@"\uploadExcel\" + sFileName, fullFilePath = $@"..\..\uploadExcel\" + sFileName };
                //return new { status = 1 };
            }
            catch (Exception ex)
            {
                Helper.LogError("SingleSearchController.CS -> CreateExcel()", ex.Message, _env);
                //return false;
                return new { status = 0, message = ex.Message };
            }
        }

        public dynamic CreateExcelOnId(int QId)
        {
            try
            {
                var result = _SingleSearchDocumentBLL.GetDocumentFromDBById(QId).xml;
                if (result.ToString().Contains("<RETORNOEXECUCAO>999"))
                {//If xml return with Error
                    result = result.Replace("CONSULTA2300", "CONSULTA2300ERR");
                    var r1 = DeserializarXMLRetornoErr(result);
                    return false;
                }

                //DocumentXMLResponse _DocumentXMLResponse = new DocumentXMLResponse();
                //_DocumentXMLResponse = _SingleSearchDocumentBLL.GetDocumentFromDB(sDocumentCode, userId);

                CONSULTA2300 r = DeserializarXMLRetorno(result);

                var veiculoData = r.CONSULTAS.CONSULTA.VEICULO;
                var proprietarioData = r.CONSULTAS.CONSULTA.PROPRIETARIO;
                var gravameData = r.CONSULTAS.CONSULTA.GRAVAME;
                var gravamE_INTENCAOData = r.CONSULTAS.CONSULTA.GRAVAME_INTENCAO;
                var debitosData = r.CONSULTAS.CONSULTA.TOTAL_DEBITOS;
                var restricoesData = r.CONSULTAS.CONSULTA.RESTRICOES;
                var multasData = r.CONSULTAS.CONSULTA.MULTAS;
                var COMUNICACAO_VENDAData = r.CONSULTAS.CONSULTA.COMUNICACAO_VENDA;
                var INSPECAO_VEICULARData = r.CONSULTAS.CONSULTA.INSPECAO_VEICULAR;

                var TAXAS_LICENCIAMENTO = r.CONSULTAS.CONSULTA.TAXAS_LICENCIAMENTO;
                var DPVATS = r.CONSULTAS.CONSULTA.DPVATS;
                var IPVAS = r.CONSULTAS.CONSULTA.IPVAS;
                var NOTIFICACOES = r.CONSULTAS.CONSULTA.NOTIFICACOES;
                var MULTAS = r.CONSULTAS.CONSULTA.MULTAS;
                var MULTASRENAINF = r.CONSULTAS.CONSULTA.MULTASRENAINF;
                var BLOQUEIOSRENAJUD = r.CONSULTAS.CONSULTA.BLOQUEIOSRENAJUD;
                var BLOQUEIOSDETRAN = r.CONSULTAS.CONSULTA.BLOQUEIOSDETRAN;

                string sFileName = "Excel_" + QId.ToString() + ".xlsx";
                string sWebRootFolder = _env.WebRootPath + $@"\uploadExcel\";
                string dir = _env.WebRootPath + $@"\uploadExcel";

                //string sWebRootFolder = _env.WebRootPath;
                //string sFileName = @"demo2.xlsx";
                // string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
                FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                if (file.Exists)
                {
                    file.Delete();
                    file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                }
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    // 1. VEICULO
                    ExcelWorksheet VEICULO = package.Workbook.Worksheets.Add("VEICULO");

                    VEICULO.Cells[1, 1].Value = "PLACA";
                    VEICULO.Cells[2, 1].Value = Convert.ToString(veiculoData.PLACA);

                    VEICULO.Cells[1, 2].Value = "CHASSI";
                    VEICULO.Cells[2, 2].Value = Convert.ToString(veiculoData.CHASSI);

                    VEICULO.Cells[1, 3].Value = "RENAVAM";
                    VEICULO.Cells[2, 3].Value = Convert.ToString(veiculoData.RENAVAM);

                    VEICULO.Cells[1, 4].Value = "MUNICIPIO";
                    VEICULO.Cells[2, 4].Value = Convert.ToString(veiculoData.MUNICIPIO);

                    VEICULO.Cells[1, 5].Value = "MUNICIPIODESC";
                    VEICULO.Cells[2, 5].Value = Convert.ToString(veiculoData.MUNICIPIODESC);

                    VEICULO.Cells[1, 6].Value = "UF";
                    VEICULO.Cells[2, 6].Value = Convert.ToString(veiculoData.UF);

                    VEICULO.Cells[1, 7].Value = "PROCEDENCIA";
                    VEICULO.Cells[2, 7].Value = Convert.ToString(veiculoData.PROCEDENCIA);

                    VEICULO.Cells[1, 8].Value = "PROCEDENCIADESC";
                    VEICULO.Cells[2, 8].Value = Convert.ToString(veiculoData.PROCEDENCIADESC);

                    VEICULO.Cells[1, 9].Value = "MARCA";
                    VEICULO.Cells[2, 9].Value = Convert.ToString(veiculoData.MARCA);

                    VEICULO.Cells[1, 10].Value = "MARCADESC";
                    VEICULO.Cells[2, 10].Value = Convert.ToString(veiculoData.MARCADESC);

                    VEICULO.Cells[1, 11].Value = "ANOFABRICACAO";
                    VEICULO.Cells[2, 11].Value = Convert.ToString(veiculoData.ANOFABRICACAO);

                    VEICULO.Cells[1, 12].Value = "ANOMODELO";
                    VEICULO.Cells[2, 12].Value = Convert.ToString(veiculoData.ANOMODELO);

                    VEICULO.Cells[1, 13].Value = "TIPO";
                    VEICULO.Cells[2, 13].Value = Convert.ToString(veiculoData.TIPO);

                    VEICULO.Cells[1, 14].Value = "TIPODESC";
                    VEICULO.Cells[2, 14].Value = Convert.ToString(veiculoData.TIPODESC);

                    VEICULO.Cells[1, 15].Value = "CARROCERIA";
                    VEICULO.Cells[2, 15].Value = Convert.ToString(veiculoData.CARROCERIA);

                    VEICULO.Cells[1, 16].Value = "CARROCERIADESC";
                    VEICULO.Cells[2, 16].Value = Convert.ToString(veiculoData.CARROCERIADESC);

                    VEICULO.Cells[1, 17].Value = "COR";
                    VEICULO.Cells[2, 17].Value = Convert.ToString(veiculoData.COR);

                    VEICULO.Cells[1, 18].Value = "CORDESC";
                    VEICULO.Cells[2, 18].Value = Convert.ToString(veiculoData.CORDESC);

                    VEICULO.Cells[1, 19].Value = "CATEGORIA";
                    VEICULO.Cells[2, 19].Value = Convert.ToString(veiculoData.CATEGORIA);

                    VEICULO.Cells[1, 20].Value = "CATEGORIADESC";
                    VEICULO.Cells[2, 20].Value = Convert.ToString(veiculoData.CATEGORIADESC);

                    VEICULO.Cells[1, 21].Value = "COMBUSTIVEL";
                    VEICULO.Cells[2, 21].Value = Convert.ToString(veiculoData.COMBUSTIVEL);

                    VEICULO.Cells[1, 22].Value = "COMBUSTIVELDESC";
                    VEICULO.Cells[2, 22].Value = Convert.ToString(veiculoData.COMBUSTIVELDESC);

                    VEICULO.Cells[1, 23].Value = "ESPECIE";
                    VEICULO.Cells[2, 23].Value = Convert.ToString(veiculoData.ESPECIE);

                    VEICULO.Cells[1, 24].Value = "ESPECIEDESC";
                    VEICULO.Cells[2, 24].Value = Convert.ToString(veiculoData.ESPECIEDESC);

                    VEICULO.Cells[1, 25].Value = "CAPACIDADEPASSAGEIRO";
                    VEICULO.Cells[2, 25].Value = Convert.ToString(veiculoData.CAPACIDADEPASSAGEIRO);

                    VEICULO.Cells[1, 26].Value = "CAPACIDADECARGA";
                    VEICULO.Cells[2, 26].Value = Convert.ToString(veiculoData.CAPACIDADECARGA);

                    VEICULO.Cells[1, 27].Value = "POTENCIA";
                    VEICULO.Cells[2, 27].Value = Convert.ToString(veiculoData.POTENCIA);

                    VEICULO.Cells[1, 28].Value = "CILINDRADA";
                    VEICULO.Cells[2, 28].Value = Convert.ToString(veiculoData.CILINDRADA);

                    VEICULO.Cells[1, 29].Value = "CMT";
                    VEICULO.Cells[2, 29].Value = Convert.ToString(veiculoData.CMT);

                    VEICULO.Cells[1, 30].Value = "PBT";
                    VEICULO.Cells[2, 30].Value = Convert.ToString(veiculoData.PBT);

                    VEICULO.Cells[1, 31].Value = "EIXOS";
                    VEICULO.Cells[2, 31].Value = Convert.ToString(veiculoData.EIXOS);

                    VEICULO.Cells[1, 32].Value = "NUMEROMOTOR";
                    VEICULO.Cells[2, 32].Value = Convert.ToString(veiculoData.NUMEROMOTOR);

                    VEICULO.Cells[1, 33].Value = "DATAALTERACAO";
                    VEICULO.Cells[2, 33].Value = Convert.ToString(veiculoData.DATAALTERACAO);

                    VEICULO.Cells[1, 34].Value = "NUMEROCAMBIO";
                    VEICULO.Cells[2, 34].Value = Convert.ToString(veiculoData.NUMEROCAMBIO);

                    VEICULO.Cells[1, 35].Value = "TIPOMONTAGEM";
                    VEICULO.Cells[2, 35].Value = Convert.ToString(veiculoData.TIPOMONTAGEM);

                    VEICULO.Cells[1, 36].Value = "SITUACAOVEICULO";
                    VEICULO.Cells[2, 36].Value = Convert.ToString(veiculoData.SITUACAOVEICULO);

                    VEICULO.Cells[1, 37].Value = "TIPOREMARCACAOCHASSI";
                    VEICULO.Cells[2, 37].Value = Convert.ToString(veiculoData.TIPOREMARCACAOCHASSI);

                    VEICULO.Cells[1, 38].Value = "DATAEMISSAOCRV";
                    VEICULO.Cells[2, 38].Value = Convert.ToString(veiculoData.DATAEMISSAOCRV);

                    VEICULO.Cells[1, 39].Value = "DATALICENCIAMENTO";
                    VEICULO.Cells[2, 39].Value = Convert.ToString(veiculoData.DATALICENCIAMENTO);

                    VEICULO.Cells[1, 40].Value = "EXERCICIOLICENCIAMENTO";
                    VEICULO.Cells[2, 40].Value = Convert.ToString(veiculoData.EXERCICIOLICENCIAMENTO);

                    VEICULO.Cells[1, 41].Value = "VALORNF";
                    VEICULO.Cells[2, 41].Value = Convert.ToString(veiculoData.VALORNF);

                    VEICULO.Cells[1, 42].Value = "NUMERONF";
                    VEICULO.Cells[2, 42].Value = Convert.ToString(veiculoData.NUMERONF);

                    VEICULO.Cells[1, 43].Value = "DATANF";
                    VEICULO.Cells[2, 43].Value = Convert.ToString(veiculoData.DATANF);

                    // 1.  Add PROPRIETARIO data

                    VEICULO.Cells[1, 44].Value = "TIPOCPFPROPRIETARIO";
                    VEICULO.Cells[2, 44].Value = Convert.ToString(proprietarioData.TIPOCPFPROPRIETARIO);

                    VEICULO.Cells[1, 45].Value = "CPFPROPRIETARIO";
                    VEICULO.Cells[2, 45].Value = Convert.ToString(proprietarioData.CPFPROPRIETARIO);

                    VEICULO.Cells[1, 46].Value = "NOMEPROPRIETARIO";
                    VEICULO.Cells[2, 46].Value = Convert.ToString(proprietarioData.NOMEPROPRIETARIO);

                    VEICULO.Cells[1, 47].Value = "RG";
                    VEICULO.Cells[2, 47].Value = Convert.ToString(proprietarioData.RG);

                    VEICULO.Cells[1, 48].Value = "ORGEXP";
                    VEICULO.Cells[2, 48].Value = Convert.ToString(proprietarioData.ORGEXP);

                    VEICULO.Cells[1, 49].Value = "ENDERECO";
                    VEICULO.Cells[2, 49].Value = Convert.ToString(proprietarioData.ENDERECO);

                    VEICULO.Cells[1, 50].Value = "COMPLEMENTO";
                    VEICULO.Cells[2, 50].Value = Convert.ToString(proprietarioData.COMPLEMENTO);

                    VEICULO.Cells[1, 51].Value = "BAIRRO";
                    VEICULO.Cells[2, 51].Value = Convert.ToString(proprietarioData.BAIRRO);

                    VEICULO.Cells[1, 52].Value = "CIDADE";
                    VEICULO.Cells[2, 52].Value = Convert.ToString(proprietarioData.CIDADE);

                    VEICULO.Cells[1, 53].Value = "CEP";
                    VEICULO.Cells[2, 53].Value = Convert.ToString(proprietarioData.CEP);

                    VEICULO.Cells[1, 54].Value = "NUMERO";
                    VEICULO.Cells[2, 54].Value = Convert.ToString(proprietarioData.NUMERO);

                    VEICULO.Cells[1, 55].Value = "DDD";
                    VEICULO.Cells[2, 55].Value = Convert.ToString(proprietarioData.DDD);

                    VEICULO.Cells[1, 56].Value = "TELEFONE";
                    VEICULO.Cells[2, 56].Value = Convert.ToString(proprietarioData.TELEFONE);

                    VEICULO.Cells[1, 57].Value = "NOMEPROPRIETARIOANTERIOR";
                    VEICULO.Cells[2, 57].Value = Convert.ToString(proprietarioData.NOMEPROPRIETARIOANTERIOR);

                    // 1.  Add GRAVAME data

                    VEICULO.Cells[1, 58].Value = "CNPJFINANCIADO";
                    VEICULO.Cells[2, 58].Value = Convert.ToString(gravameData.CNPJFINANCIADO);

                    VEICULO.Cells[1, 59].Value = "ARRENDATARIOFINANCIADO";
                    VEICULO.Cells[2, 59].Value = Convert.ToString(gravameData.ARRENDATARIOFINANCIADO);

                    VEICULO.Cells[1, 60].Value = "TIPOTRANSACAO";
                    VEICULO.Cells[2, 60].Value = Convert.ToString(gravameData.TIPOTRANSACAO);

                    VEICULO.Cells[1, 61].Value = "TIPOTRANSACAODESC";
                    VEICULO.Cells[2, 61].Value = Convert.ToString(gravameData.TIPOTRANSACAODESC);

                    VEICULO.Cells[1, 62].Value = "TIPORESTRICAOFINANCEIRA";
                    VEICULO.Cells[2, 62].Value = Convert.ToString(gravameData.TIPORESTRICAOFINANCEIRA);

                    VEICULO.Cells[1, 63].Value = "TIPORESTRICAOFINANCEIRADESC";
                    VEICULO.Cells[2, 63].Value = Convert.ToString(gravameData.TIPORESTRICAOFINANCEIRADESC);

                    VEICULO.Cells[1, 64].Value = "CNPJFINANCEIRA";
                    VEICULO.Cells[2, 64].Value = Convert.ToString(gravameData.CNPJFINANCEIRA);

                    VEICULO.Cells[1, 65].Value = "AGENTEFINANCEIRO";
                    VEICULO.Cells[2, 65].Value = Convert.ToString(gravameData.AGENTEFINANCEIRO);

                    VEICULO.Cells[1, 66].Value = "AGENTEFINANCEIRODESC";
                    VEICULO.Cells[2, 66].Value = Convert.ToString(gravameData.AGENTEFINANCEIRODESC);

                    VEICULO.Cells[1, 67].Value = "NUMEROCONTRATO";
                    VEICULO.Cells[2, 67].Value = Convert.ToString(gravameData.NUMEROCONTRATO);

                    VEICULO.Cells[1, 68].Value = "DATAVIGENCIACONTRATO";
                    VEICULO.Cells[2, 68].Value = Convert.ToString(gravameData.DATAVIGENCIACONTRATO);

                    VEICULO.Cells[1, 69].Value = "DATAINCLUSAOFINANCIAMENTO";
                    VEICULO.Cells[2, 69].Value = Convert.ToString(gravameData.DATAINCLUSAOFINANCIAMENTO);

                    VEICULO.Cells[1, 70].Value = "DATACONTRATO";
                    VEICULO.Cells[2, 70].Value = Convert.ToString(gravameData.DATACONTRATO);

                    VEICULO.Cells[1, 71].Value = "UF_GRAVAME";
                    VEICULO.Cells[2, 71].Value = Convert.ToString(gravameData.UF_GRAVAME);

                    VEICULO.Cells[1, 72].Value = "NUMERORESTRICAO";
                    VEICULO.Cells[2, 72].Value = Convert.ToString(gravameData.NUMERORESTRICAO);

                    VEICULO.Cells[1, 73].Value = "SITUACAO";
                    VEICULO.Cells[2, 73].Value = Convert.ToString(gravameData.SITUACAO);

                    // 1.  Add GRAVAME_INTENCAO

                    VEICULO.Cells[1, 74].Value = "CNPJFINANCEIRA";
                    VEICULO.Cells[2, 74].Value = Convert.ToString(gravamE_INTENCAOData.CNPJFINANCEIRA);

                    VEICULO.Cells[1, 75].Value = "INFORMANTEFINANCIAMENTO";
                    VEICULO.Cells[2, 75].Value = Convert.ToString(gravamE_INTENCAOData.INFORMANTEFINANCIAMENTO);

                    VEICULO.Cells[1, 76].Value = "TIPOTRANSACAO";
                    VEICULO.Cells[2, 76].Value = Convert.ToString(gravamE_INTENCAOData.TIPOTRANSACAO);

                    VEICULO.Cells[1, 77].Value = "TIPOTRANSACAODESC";
                    VEICULO.Cells[2, 77].Value = Convert.ToString(gravamE_INTENCAOData.TIPOTRANSACAODESC);

                    VEICULO.Cells[1, 78].Value = "TIPORESTRICAOFINANCEIRA";
                    VEICULO.Cells[2, 78].Value = Convert.ToString(gravamE_INTENCAOData.TIPORESTRICAOFINANCEIRA);

                    VEICULO.Cells[1, 79].Value = "TIPORESTRICAOFINANCEIRADESC";
                    VEICULO.Cells[2, 79].Value = Convert.ToString(gravamE_INTENCAOData.TIPORESTRICAOFINANCEIRADESC);

                    VEICULO.Cells[1, 80].Value = "AGENTEFINANCEIRO";
                    VEICULO.Cells[2, 80].Value = Convert.ToString(gravamE_INTENCAOData.AGENTEFINANCEIRO);

                    VEICULO.Cells[1, 81].Value = "AGENTEFINANCEIRODESC";
                    VEICULO.Cells[2, 81].Value = Convert.ToString(gravamE_INTENCAOData.AGENTEFINANCEIRODESC);

                    VEICULO.Cells[1, 82].Value = "CNPJFINANCIADO";
                    VEICULO.Cells[2, 82].Value = Convert.ToString(gravamE_INTENCAOData.CNPJFINANCIADO);

                    VEICULO.Cells[1, 83].Value = "NOMEFINANCIADO";
                    VEICULO.Cells[2, 83].Value = Convert.ToString(gravamE_INTENCAOData.NOMEFINANCIADO);

                    VEICULO.Cells[1, 84].Value = "DATAINCLUSAOINTENCAOTROCAFINANC";
                    VEICULO.Cells[2, 84].Value = Convert.ToString(gravamE_INTENCAOData.DATAINCLUSAOINTENCAOTROCAFINANC);

                    VEICULO.Cells[1, 85].Value = "NUMEROCONTRATOFINANC";
                    VEICULO.Cells[2, 85].Value = Convert.ToString(gravamE_INTENCAOData.NUMEROCONTRATOFINANC);

                    VEICULO.Cells[1, 86].Value = "DATAVIGENCIACONTRATOFINANC";
                    VEICULO.Cells[2, 86].Value = Convert.ToString(gravamE_INTENCAOData.DATAVIGENCIACONTRATOFINANC);

                    VEICULO.Cells[1, 87].Value = "UF_GRAVAME";
                    VEICULO.Cells[2, 87].Value = Convert.ToString(gravamE_INTENCAOData.UF_GRAVAME);

                    VEICULO.Cells[1, 88].Value = "NUMERORESTRICAO";
                    VEICULO.Cells[2, 88].Value = Convert.ToString(gravamE_INTENCAOData.NUMERORESTRICAO);

                    VEICULO.Cells[1, 89].Value = "SITUACAO";
                    VEICULO.Cells[2, 89].Value = Convert.ToString(gravamE_INTENCAOData.SITUACAO);

                    // 1.  Add RESTRICOES

                    VEICULO.Cells[1, 90].Value = "RESTRICOES";
                    VEICULO.Cells[2, 90].Value = Convert.ToString(restricoesData);

                    // 1 Add TOTAL_DEBITOS

                    VEICULO.Cells[1, 91].Value = "DEBITOIPVALICENCIAMENTO_DESC";
                    VEICULO.Cells[2, 91].Value = Convert.ToString(debitosData.DEBITOIPVALICENCIAMENTO_DESC);

                    VEICULO.Cells[1, 92].Value = "DEBITOSMULTAS_DESC";
                    VEICULO.Cells[2, 92].Value = Convert.ToString(debitosData.DEBITOSMULTAS_DESC);

                    VEICULO.Cells[1, 93].Value = "DEBITOSDIVIDAATIVA_DESC";
                    VEICULO.Cells[2, 93].Value = Convert.ToString(debitosData.DEBITOSDIVIDAATIVA_DESC);

                    VEICULO.Cells[1, 94].Value = "IPVA";
                    VEICULO.Cells[2, 94].Value = Convert.ToString(debitosData.IPVA);

                    VEICULO.Cells[1, 95].Value = "LICENCIAMENTO";
                    VEICULO.Cells[2, 95].Value = Convert.ToString(debitosData.LICENCIAMENTO);

                    VEICULO.Cells[1, 96].Value = "MULTAS";
                    VEICULO.Cells[2, 96].Value = Convert.ToString(debitosData.MULTAS);

                    VEICULO.Cells[1, 97].Value = "DPVAT";
                    VEICULO.Cells[2, 97].Value = Convert.ToString(debitosData.DPVAT);

                    VEICULO.Cells[1, 98].Value = "IPVA_DIVIDAATIVA";
                    VEICULO.Cells[2, 98].Value = Convert.ToString(debitosData.IPVA_DIVIDAATIVA);

                    // 1 Add COMUNICACAO_VENDA

                    VEICULO.Cells[1, 99].Value = "COMUNICACAOVENDA";
                    VEICULO.Cells[2, 99].Value = Convert.ToString(COMUNICACAO_VENDAData.COMUNICACAOVENDA);

                    VEICULO.Cells[1, 100].Value = "COMUNICACAOVENDADESC";
                    VEICULO.Cells[2, 100].Value = Convert.ToString(COMUNICACAO_VENDAData.COMUNICACAOVENDADESC);

                    VEICULO.Cells[1, 101].Value = "DATAINCLUSAOCOMUNICACAOVENDAS";
                    VEICULO.Cells[2, 101].Value = Convert.ToString(COMUNICACAO_VENDAData.DATAINCLUSAOCOMUNICACAOVENDAS);

                    VEICULO.Cells[1, 102].Value = "DATAVENDA";
                    VEICULO.Cells[2, 102].Value = Convert.ToString(COMUNICACAO_VENDAData.DATAVENDA);

                    VEICULO.Cells[1, 103].Value = "TIPODOCTOCOMPRADOR";
                    VEICULO.Cells[2, 103].Value = Convert.ToString(COMUNICACAO_VENDAData.TIPODOCTOCOMPRADOR);

                    VEICULO.Cells[1, 104].Value = "CNPJCPFCOMPRADOR";
                    VEICULO.Cells[2, 104].Value = Convert.ToString(COMUNICACAO_VENDAData.CNPJCPFCOMPRADOR);

                    VEICULO.Cells[1, 105].Value = "DATANOTAFISCAL";
                    VEICULO.Cells[2, 105].Value = Convert.ToString(COMUNICACAO_VENDAData.DATANOTAFISCAL);

                    VEICULO.Cells[1, 106].Value = "DATAPROTOCOLODETRAN";
                    VEICULO.Cells[2, 106].Value = Convert.ToString(COMUNICACAO_VENDAData.DATAPROTOCOLODETRAN);

                    // 1 Add INSPECAO_VEICULAR

                    VEICULO.Cells[1, 107].Value = "INSPECAOVEICULAR";
                    VEICULO.Cells[2, 107].Value = Convert.ToString(INSPECAO_VEICULARData.INSPECAOVEICULAR);

                    VEICULO.Cells[1, 108].Value = "INSPECAOVEICULARDESC";
                    VEICULO.Cells[2, 108].Value = Convert.ToString(INSPECAO_VEICULARData.INSPECAOVEICULARDESC);

                    VEICULO.Cells[1, 109].Value = "DATAINSPECAOVEICULAR";
                    VEICULO.Cells[2, 109].Value = Convert.ToString(INSPECAO_VEICULARData.DATAINSPECAOVEICULAR);

                    VEICULO.Cells[1, 10].Value = "DATAINCLUSACAOINSPECAOVEICULAR";
                    VEICULO.Cells[2, 10].Value = Convert.ToString(INSPECAO_VEICULARData.DATAINCLUSACAOINSPECAOVEICULAR);

                    // 2. TAXAS_LICENCIAMENTO
                    ExcelWorksheet _TAXAS_LICENCIAMENTO = package.Workbook.Worksheets.Add("TAXAS_LICENCIAMENTO");

                    // 3. DPVATS
                    ExcelWorksheet _DPVATS = package.Workbook.Worksheets.Add("DPVATS");
                    for (int i = 0; i < DPVATS.Length; i++)
                    {
                        var no = i + 2;
                        if (i == 0)
                        {
                            _DPVATS.Cells[1, 1].Value = "ORIGEM";
                            _DPVATS.Cells[1, 2].Value = "ANOREF";
                            _DPVATS.Cells[1, 3].Value = "VALDPVAT";
                            _DPVATS.Cells[1, 4].Value = "NUMPARCELA";
                            _DPVATS.Cells[1, 5].Value = "DTVENCTODPVAT";
                            _DPVATS.Cells[1, 6].Value = "CATEGORIA";
                        }

                        _DPVATS.Cells[no, 1].Value = Convert.ToString(DPVATS[i].ORIGEM);
                        _DPVATS.Cells[no, 2].Value = Convert.ToString(DPVATS[i].ANOREF);
                        _DPVATS.Cells[no, 3].Value = Convert.ToString(DPVATS[i].VALDPVAT);
                        _DPVATS.Cells[no, 4].Value = Convert.ToString(DPVATS[i].NUMPARCELA);
                        _DPVATS.Cells[no, 5].Value = Convert.ToString(DPVATS[i].DTVENCTODPVAT);
                        _DPVATS.Cells[no, 6].Value = Convert.ToString(DPVATS[i].CATEGORIA);
                    }

                    // 4. IPVAS

                    ExcelWorksheet _IPVAS = package.Workbook.Worksheets.Add("IPVAS");
                    for (int i = 0; i < IPVAS.Length; i++)
                    {
                        var no = i + 2;
                        if (i == 0)
                        {
                            _IPVAS.Cells[1, 1].Value = "ORIGEM";
                            _IPVAS.Cells[1, 2].Value = "ANOREF";
                            _IPVAS.Cells[1, 3].Value = "VALIPVA";
                            _IPVAS.Cells[1, 4].Value = "COTAIPVA";
                            _IPVAS.Cells[1, 5].Value = "COTAIPVADES";
                            _IPVAS.Cells[1, 6].Value = "DTVENCTOIPVA";
                        }

                        _IPVAS.Cells[no, 1].Value = Convert.ToString(IPVAS[i].ORIGEM);
                        _IPVAS.Cells[no, 2].Value = Convert.ToString(IPVAS[i].ANOREF);
                        _IPVAS.Cells[no, 3].Value = Convert.ToString(IPVAS[i].VALIPVA);
                        _IPVAS.Cells[no, 4].Value = Convert.ToString(IPVAS[i].COTAIPVA);
                        _IPVAS.Cells[no, 5].Value = Convert.ToString(IPVAS[i].COTAIPVADES);
                        _IPVAS.Cells[no, 6].Value = Convert.ToString(IPVAS[i].DTVENCTOIPVA);
                    }

                    // 5. MULTAS 
                    ExcelWorksheet _MULTAS = package.Workbook.Worksheets.Add("MULTAS");
                    for (int i = 0; i < MULTAS.Length; i++)
                    {
                        var no = i + 2;
                        if (i == 0)
                        {
                            _MULTAS.Cells[1, 1].Value = "ORIGEM";
                            _MULTAS.Cells[1, 2].Value = "NUMGUIA";
                            _MULTAS.Cells[1, 3].Value = "NUMAIIP";
                            _MULTAS.Cells[1, 4].Value = "CODREC";
                            _MULTAS.Cells[1, 5].Value = "DTINFRA";
                            _MULTAS.Cells[1, 6].Value = "LOCALINFRA";
                            _MULTAS.Cells[1, 7].Value = "HORAINFRA";
                            _MULTAS.Cells[1, 8].Value = "MUNINFRA";
                            _MULTAS.Cells[1, 9].Value = "ENQUADRAM";
                            _MULTAS.Cells[1, 10].Value = "ENQUADRAMDESC";
                            _MULTAS.Cells[1, 11].Value = "ENQUADRAMLEG";
                            _MULTAS.Cells[1, 12].Value = "DTVENCTOINFRA";
                            _MULTAS.Cells[1, 13].Value = "VALMULTA";
                            _MULTAS.Cells[1, 14].Value = "ENTTRANSITANT";
                            _MULTAS.Cells[1, 15].Value = "NOMEENTTRANSITANT";
                            _MULTAS.Cells[1, 16].Value = "MUNINFRADESC";
                            _MULTAS.Cells[1, 17].Value = "EMPRESA";
                            _MULTAS.Cells[1, 18].Value = "TIPO";
                        }

                        _MULTAS.Cells[no, 1].Value = Convert.ToString(MULTAS[i].ORIGEM);
                        _MULTAS.Cells[no, 2].Value = Convert.ToString(MULTAS[i].NUMGUIA);
                        _MULTAS.Cells[no, 3].Value = Convert.ToString(MULTAS[i].NUMAIIP);
                        _MULTAS.Cells[no, 4].Value = Convert.ToString(MULTAS[i].CODREC);
                        _MULTAS.Cells[no, 5].Value = Convert.ToString(MULTAS[i].DTINFRA);
                        _MULTAS.Cells[no, 6].Value = Convert.ToString(MULTAS[i].LOCALINFRA);
                        _MULTAS.Cells[no, 7].Value = Convert.ToString(MULTAS[i].HORAINFRA);
                        _MULTAS.Cells[no, 8].Value = Convert.ToString(MULTAS[i].MUNINFRA);
                        _MULTAS.Cells[no, 9].Value = Convert.ToString(MULTAS[i].ENQUADRAM);
                        _MULTAS.Cells[no, 10].Value = Convert.ToString(MULTAS[i].ENQUADRAMDESC);
                        _MULTAS.Cells[no, 11].Value = Convert.ToString(MULTAS[i].ENQUADRAMLEG);
                        _MULTAS.Cells[no, 12].Value = Convert.ToString(MULTAS[i].DTVENCTOINFRA);
                        _MULTAS.Cells[no, 13].Value = Convert.ToString(MULTAS[i].VALMULTA);
                        _MULTAS.Cells[no, 14].Value = Convert.ToString(MULTAS[i].ENTTRANSITANT);
                        _MULTAS.Cells[no, 15].Value = Convert.ToString(MULTAS[i].NOMEENTTRANSITANT);
                        _MULTAS.Cells[no, 16].Value = Convert.ToString(MULTAS[i].MUNINFRADESC);
                        _MULTAS.Cells[no, 17].Value = Convert.ToString(MULTAS[i].EMPRESA);
                        _MULTAS.Cells[no, 18].Value = (MULTAS[i].NOTIFICACAO == "0" ? "Infra��o" : "Notifica��o");
                    }

                    // 6. MULTASRENAINF
                    ExcelWorksheet _MULTASRENAINF = package.Workbook.Worksheets.Add("MULTASRENAINF");

                    // 7. BLOQUEIOSRENAJUD
                    ExcelWorksheet _BLOQUEIOSRENAJUD = package.Workbook.Worksheets.Add("BLOQUEIOSRENAJUD");

                    // 8. BLOQUEIOSDETRAN
                    ExcelWorksheet _BLOQUEIOSDETRAN = package.Workbook.Worksheets.Add("BLOQUEIOSDETRAN");
                    if (!string.IsNullOrEmpty(Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN)))
                    {
                        _BLOQUEIOSDETRAN.Cells[1, 1].Value = "TIPO";
                        _BLOQUEIOSDETRAN.Cells[2, 1].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.TIPO);

                        _BLOQUEIOSDETRAN.Cells[1, 2].Value = "MOTIVO";
                        _BLOQUEIOSDETRAN.Cells[2, 2].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.MOTIVO);

                        _BLOQUEIOSDETRAN.Cells[1, 3].Value = "DATAINCLUSAO";
                        _BLOQUEIOSDETRAN.Cells[2, 3].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.DATAINCLUSAO);

                        _BLOQUEIOSDETRAN.Cells[1, 4].Value = "PROTOCOLO_NUMERO";
                        _BLOQUEIOSDETRAN.Cells[2, 4].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.PROTOCOLO_NUMERO);

                        _BLOQUEIOSDETRAN.Cells[1, 5].Value = "PROTOCOLO_ANO";
                        _BLOQUEIOSDETRAN.Cells[2, 5].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.PROTOCOLO_ANO);

                        _BLOQUEIOSDETRAN.Cells[1, 6].Value = "PROCESSO_NUMERO";
                        _BLOQUEIOSDETRAN.Cells[2, 6].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.PROCESSO_NUMERO);

                        _BLOQUEIOSDETRAN.Cells[1, 7].Value = "PROCESSO_ANO";
                        _BLOQUEIOSDETRAN.Cells[2, 7].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.PROCESSO_ANO);

                        _BLOQUEIOSDETRAN.Cells[1, 8].Value = "OFICIO_NUMERO";
                        _BLOQUEIOSDETRAN.Cells[2, 8].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.OFICIO_NUMERO);

                        _BLOQUEIOSDETRAN.Cells[1, 9].Value = "OFICIO_ANO";
                        _BLOQUEIOSDETRAN.Cells[2, 9].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.OFICIO_ANO);

                        _BLOQUEIOSDETRAN.Cells[1, 10].Value = "IDCIDADE";
                        _BLOQUEIOSDETRAN.Cells[2, 10].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.IDCIDADE);

                        _BLOQUEIOSDETRAN.Cells[1, 11].Value = "MUNICIPIO";
                        _BLOQUEIOSDETRAN.Cells[2, 11].Value = Convert.ToString(BLOQUEIOSDETRAN.BLOQUEIODETRAN.MUNICIPIO);

                    }

                    package.Save();
                }

                return true;
                //return new { status = 1 };
            }
            catch (Exception ex)
            {
                Helper.LogError("SingleSearchController.CS -> CreateExcel()", ex.Message, _env);
                return false;
            }
        }
    }
}