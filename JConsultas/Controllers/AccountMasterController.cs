using BusinessLogicLayer;
using EntityLayer.Models;
using JConsultas.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace JConsultas.Controllers
{
    [Produces("application/json")]
    //[Route("api/AccountMaster")]
    [Authorize]
    public class AccountMasterController : Controller
    {
        private readonly AccountBLL _accountbll;
        private IHostingEnvironment _env;
        public AccountMasterController(IHostingEnvironment env, EntityLayer.Models.JConsultasContext context )
        {
            this._env = env;
            _accountbll = new AccountBLL(context);
        }

        [HttpGet]
        public IEnumerable<AccountMaster> GetAccountList()
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                var r = _accountbll.GetAccountList(userId);
                _accountbll.GetAuditorias(r.ToList());
                return r;
            }
            catch (Exception ex)
            {
                Helper.LogError("AccountMasterController.CS -> GetAccountList()", ex.Message, _env);
                return null;
            }
        }

        [HttpGet]
        public IEnumerable<AccountMaster> GetActiveAccountList()
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                var r = _accountbll.GetActiveAccount(userId);
                _accountbll.GetAuditorias(r.ToList());
                return r;
            }
            catch (Exception ex)
            {
                Helper.LogError("AccountMasterController.CS -> GetActiveAccountList()", ex.Message, _env);
                return null;
            }
        }

        [HttpPost]
        public bool AddUpdateAccount([FromBody]AccountMaster data)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                data.UsuarioAlterando = userId;

                if (string.IsNullOrEmpty(Convert.ToString(data.IsActive)))
                {
                    data.IsActive = false;
                }

                if (data.AccountId == 0)
                {
                    data.CreatedBy = userId;
                    data.CreatedAt = DateTime.Now;
                }
                else
                {
                    data.ModifiedAt = DateTime.Now;
                    data.ModifiedBy = userId;
                }

                return _accountbll.AddUpdateAccount(data);
            }
            catch (Exception ex)
            {
                Helper.LogError("AccountMasterController.CS -> AddUpdateAccount()", ex.Message, _env);
                return false;
            }
        }

        [HttpDelete]
        public bool DeleteAccount(int Id)
        {
            try
            {
                var claimsIdentity = (ClaimsIdentity)this.User.Identity;
                var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
                var userId = claim.Value;

                return _accountbll.DeleteAccount(Id, userId);
            }
            catch (Exception ex)
            {
                Helper.LogError("AccountMasterController.CS -> DeleteAccount()", ex.Message, _env);
                return false;
            }
        }

       
    }


}