﻿
// declare a main-angular module and submodule
var myApp = angular.module('myApp',
    [
        "myApp.Controllers",
        "myApp.Services",
        "pascalprecht.translate", "ngMask", "daterangepicker"

    ])

//Sub modules
angular.module("myApp.Controllers", []);
angular.module("myApp.Services", []);
angular.module("myApp.config", []);


angular.module('myApp').config(function ($translateProvider) {
    $translateProvider.useStaticFilesLoader({
        prefix: '/Localization/locale-',
        suffix: '.json'
    });

    $translateProvider.use('pt_BR');
});

//Initialize variable when dom is ready
angular.module("myApp").run(["$rootScope", "$http", "$cacheFactory", "$anchorScroll",
    function ($rootScope, $http, $cacheFactory, $anchorScroll) {
        $rootScope.$safeApply = function ($scope, fn) {
            fn = fn || function () { };
            if ($scope.$$phase) {
                fn();
            }
            else {
                $scope.$apply(fn);
            }
        };
    }
]);

myApp.factory('sharedProperties', function () {
    var property = '';
    return {
        getProperty: function () {
            return property;
        },
        setProperty: function (value) {
            property = value;
        }
    };
});

myApp.directive('pctComplete', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            value: '='
        },
        template: ' <div class="c100 p{{value}} big blue">\
                      <span>{{value}}%</span>\
                      <div class="slice">\
                        <div class="bar"></div>\
                        <div class="fill"></div>\
                      </div>\
                    </div>'
    };
});