﻿angular.module('myApp.Controllers').controller('ListOfSearchedDocsCntrl', ['$scope', '$filter', '$http', 'ListOfSearchedDocsService', 'AccountService', 'UserService', function ($scope, $filter, $http, ListOfSearchedDocsService, AccountService, UserService) {
    //Date-range-picker Start

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    $scope.startDate = moment().subtract(7, "days");
    $scope.endDate = moment();
    $scope.date = {
        startDate: moment().subtract(7, "days"),
        endDate: moment()
    };

    $scope.$watch('endDate', function (newDate) {
        if ($scope.endDate < $scope.startDate)
            $scope.startDate = $scope.endDate;
    }, false);

    $scope.today = new Date();
    $scope.opts = {
        singleDatePicker: true
    }
    //Watch for date changes
    $scope.$watch('date', function (newDate) {
        console.log('New date set: ', newDate);
    }, false);
    //Date-range-picker End

    $scope.objSearch = {
        DocumentCode: "",
        Id: "",
        AccountID: "",
        ConsultaID: ""
    };

    $scope.TiposConsulta = [
        { ConsultaID: 0, ConsultaNome: "Site" },
        { ConsultaID: 1, ConsultaNome: "Api" }
    ];

    $scope.UserList = [];
    $scope.AllUserList = [];
    $scope.GetUserList = function () {
        $('#loading').fadeIn(400, "linear");
        ListOfSearchedDocsService.GetUserList().then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                if (result.data.length > 0) {
                    $scope.UserList = result.data;
                    $scope.AllUserList = result.data;

                    if (getCookie("AccountID") != "null") {
                        var AccountID = getCookie("AccountID");

                        $scope.UserList = $scope.UserList.filter(function (elem) {
                            return elem["AccountId"] == AccountID;
                        });
                    }
                }
            }, function errorCallback(response) {
                if (response.statusText == 'Unauthorized') {
                }
            });
        return false;
    }

    $scope.AccountChange = function () {
        if (getCookie("AccountID") == "null") {
            var AccountID = $scope.objSearch.AccountID;

            if (AccountID == "") {
                $scope.UserList = $scope.AllUserList;
            }
            else {
                $scope.UserList = $scope.AllUserList.filter(function (elem) {
                    return elem["AccountId"] == AccountID;
                });
            }
            $scope.objSearch.Id = "";
        }

    }

    $scope.AccountList = [];
    $scope.GetAccountList = function () {
        AccountService.GetActiveAccounts().then(
            function successCallback(result) {
                if (result.data.length > 0) {
                    $scope.AccountList = result.data;
                }
            }, function errorCallback(response) {
            });
    }

    $scope.GetUserList();
    $scope.IsSearch = false;
    $scope.IsRecordAvailable = true;
    $scope.SearchDocument = function (obj) {

        $scope.FMsg = false;
        $scope.TMsg = false;
        $scope.WMsg = false;
        $scope.ShowZipSaveButton = false;
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            $scope.IsSearch = true;
            $('#loading').fadeIn(400, "linear");

            obj.StartDate = $scope.startDate;
            obj.EndDate = $scope.endDate;

            ListOfSearchedDocsService.GetListOfSearchedDocs(obj).then(
                function successCallback(result) {
                    $('#loading').fadeOut(400, "linear");
                    if (result.data.length > 0) {

                        $scope.IsRecordAvailable = true;
                        $scope.SearchedDocumentList = result.data;
                        try {
                            $("#dtDocList").dataTable().fnDestroy();
                        } catch (e) {

                        }
                        var table = $('#dtDocList').DataTable({
                            "aaData": result.data,
                            "pageLength": 50,
                            "destroy": true,
                            responsive: true,
                            "aaSorting": [[4, "desc"]],
                            "aoColumns": [
                                {
                                    "data": "QueryResultid",
                                    'targets': 0,
                                    'searchable': false,
                                    'orderable': false,
                                    'render': function (data, type, full, meta) {
                                        return '<input type="checkbox" class="call-checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                                    },
                                    "sWidth": "100px"
                                },
                                { "data": "DocumentCode", "sWidth": "200px", "responsivePriority": 1, "bSortable": false },
                                { "data": "UserName", "sWidth": "200px", "responsivePriority": 1, "bSortable": false },
                                { "data": "AccountName", "sWidth": "200px", "responsivePriority": 1, "bSortable": false, "visible": getCookie("ProfileName") == 'Admin User' },
                                {
                                    "data": "CreatedAt",
                                    "mRender": function (data, type, full) {
                                        var date = $filter('date')(full.CreatedAt, 'dd/MM/yyyy HH:mm');
                                        var d = "<div class='btn-group btn-group-xs'>" + date + " </div>";
                                        return d;
                                    },
                                    "sType": "date",
                                    //"sWidth": "10px", 
                                    "responsivePriority": 2,
                                    "bSortable": true,
                                    "sWidth": "250px"
                                },
                                {
                                    "data": "IsConsultaApi", "mRender": function (data, type, full) {
                                        if (full.IsConsultaApi) {
                                            return "Sim";
                                        }
                                        else
                                            return "Não";
                                    },
                                    "sWidth": "200px", "responsivePriority": 1, "bSortable": false
                                },
                                {
                                    "data": "DocumentCode",
                                    "mRender": function (data, type, full) {
                                        if (!full.IsError) {
                                            //var d = "<div class='btn-group btn-group-xs text-center'><a href='/home/DocDetailPage?qid=" + full.QueryResultid + "&docid=" + full.DocumentCode + "' target='_blank' ><span class='glyph-icon icon-list'></span></a> </div>";
                                            var d = "<a href='/home/DocDetailPage?qid=" + full.QueryResultid + "&docid=" + full.DocumentCode + "' target='_blank' >   <i class='glyph-icon tooltip-button demo-icon icon-list'></i> </a>";
                                            return d;
                                        }
                                        else
                                            return null;
                                    },
                                    //"sWidth": "10px",
                                    "responsivePriority": 2,
                                    "bSortable": false
                                }, {
                                    "data": "DocumentCode",
                                    "mRender": function (data, type, full) {
                                        if (full.IsError) {
                                            return full.Error
                                        }
                                        else {
                                            var d = "<i class='glyph-icon tooltip-button demo-icon icon-floppy-o' title='' data-original-title='Download PDF'  onclick='CreatePdf(" + JSON.stringify(full.QueryResultid) + ")' ></i>";
                                            d += "&nbsp;&nbsp;&nbsp;<i class='glyph-icon tooltip-button demo-icon icon-file-excel-o' title='Download Excel' data-original-title='Save Excel' onclick='CreateExcel(" + JSON.stringify(full.QueryResultid) + ", true)'></i>";
                                            d += "&nbsp;&nbsp;&nbsp;<i class='glyph-icon tooltip-button demo-icon icon-cloud-upload' title='' data-original-title='Save to Drive'  onclick='UploadToDrive(" + JSON.stringify(full.QueryResultid) + ")'></i>";
                                            d += "&nbsp;&nbsp;&nbsp;<div ng-show='ShowSaveButton' class='savetodrive' id='savetodrive-div" + full.QueryResultid + "'></div>";
                                            return d;
                                        }

                                        //var d = "<i class='glyph-icon tooltip-button demo-icon  icon-floppy-o' data-original-title='Download PDF'  onclick='CreatePdf(" + JSON.stringify(full.queryResultid) + ")' ></i>";
                                        //d += "&nbsp;&nbsp;&nbsp;<i class='glyph-icon tooltip-button demo-icon icon-cloud-upload' data-original-title='Save to Drive'  onclick='UploadToDrive(" + JSON.stringify(full.queryResultid) + ")'></i>";
                                        //d += "&nbsp;&nbsp;&nbsp;<div ng-show='ShowSaveButton' id='savetodrive-div" + full.queryResultid + "'></div>";
                                        //return d;
                                    },
                                    "sWidth": "600px"
                                    , "responsivePriority": 4
                                    , "bSortable": false
                                }
                            ]
                        });
                    }
                    else if (result.data.length == 0) {
                        $scope.WMsg = true;
                        $scope.FMsg = false;
                        $scope.TMsg = false;

                        $scope.IsRecordAvailable = false;
                        $scope.WarningMsg = "Nenhum registro encontrado";

                        $("#dtDocList").dataTable().fnDestroy();
                        $('#dtDocList').DataTable({
                            "aaData": result.data
                        });
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;

                        $scope.ErrorMsg = "Ocorreu um erro";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }

        return false;
    }

    $scope.GetLastSearchedDocs = function () {
        $scope.FMsg = false;
        $scope.TMsg = false;
        $scope.WMsg = false;
        $scope.searchsubmitted = true;
        $('#loading').fadeIn(400, "linear");

        ListOfSearchedDocsService.GetLastSearchedDocs().then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                if (result.data.length > 0) {
                    $scope.IsRecordAvailable = true;
                    $scope.SearchedDocumentList = result.data;
                    try {
                        $("#dtDocList").dataTable().fnDestroy();
                    } catch (e) {

                    }
                    var table = $('#dtDocList').DataTable({
                        "aaData": result.data,
                        "pageLength": 50,
                        responsive: true,
                        "aaSorting": [[4, "desc"]],
                        "destroy": true,
                        "aoColumns": [
                            {
                                "data": "QueryResultid",
                                'targets': 0,
                                'searchable': false,
                                'orderable': false,
                                'render': function (data, type, full, meta) {
                                    return '<input type="checkbox" class="call-checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                                },
                                "sWidth": "100px"
                            },
                            { "data": "DocumentCode", "sWidth": "200px", "responsivePriority": 1, "bSortable": false },
                            { "data": "UserName", "sWidth": "200px", "responsivePriority": 1, "bSortable": false },
                            { "data": "AccountName", "sWidth": "200px", "responsivePriority": 1, "bSortable": false, "visible": getCookie("ProfileName") == 'Admin User' },
                            {
                                "data": "CreatedAt",
                                "mRender": function (data, type, full) {
                                    var date = $filter('date')(full.CreatedAt, 'dd/MM/yyyy HH:mm');
                                    var d = "<div class='btn-group btn-group-xs'>" + date + " </div>";
                                    return d;
                                },
                                //"sWidth": "10px", 
                                "responsivePriority": 1,
                                "bSortable": true,
                                "sWidth": "250px"
                            },
                            {
                                "data": "IsConsultaApi", "mRender": function (data, type, full) {
                                    if (full.IsConsultaApi) {
                                        return "Sim";
                                    }
                                    else
                                        return "Não";
                                },
                                "sWidth": "200px", "responsivePriority": 1, "bSortable": false
                            },
                            {
                                "data": "DocumentCode",
                                "mRender": function (data, type, full) {
                                    if (!full.IsError) {
                                        //var d = "<div class='glyph-icon btn-group btn-group-xs text-center'><a href='/home/DocDetailPage?qid=" + full.QueryResultid + "&docid=" + full.DocumentCode + "' target='_blank' ><span class='glyph-icon icon-list'></i></span></a> </div>";
                                        var d = "<a href='/home/DocDetailPage?qid=" + full.QueryResultid + "&docid=" + full.DocumentCode + "' target='_blank' >   <i class='glyph-icon tooltip-button demo-icon icon-list'></i> </a>";
                                        return d;
                                    }
                                    else
                                        return null;
                                },
                                //"sWidth": "10px",
                                "responsivePriority": 2,
                                "bSortable": false
                            }, {
                                "data": "DocumentCode",
                                "mRender": function (data, type, full) {
                                    if (full.IsError) {
                                        return full.Error
                                    }
                                    else {
                                        var d = "<i class='glyph-icon tooltip-button demo-icon icon-floppy-o' title='' data-original-title='Download PDF'  onclick='CreatePdf(" + JSON.stringify(full.QueryResultid) + ")' ></i>";
                                        d += "&nbsp;&nbsp;&nbsp;<i class='glyph-icon tooltip-button demo-icon icon-file-excel-o' title='Download Excel' data-original-title='Save Excel' onclick='CreateExcel(" + JSON.stringify(full.QueryResultid) + ", true)'></i>";
                                        d += "&nbsp;&nbsp;&nbsp;<i class='glyph-icon tooltip-button demo-icon icon-cloud-upload' title='' data-original-title='Save to Drive'  onclick='UploadToDrive(" + JSON.stringify(full.QueryResultid) + ")'></i>";
                                        d += "&nbsp;&nbsp;&nbsp;<div ng-show='ShowSaveButton' class='savetodrive' id='savetodrive-div" + full.QueryResultid + "'></div>";
                                        return d;
                                    }

                                    //var d = "<i class='glyph-icon tooltip-button demo-icon icon-floppy-o' title='' data-original-title='Download PDF'  onclick='CreatePdf(" + JSON.stringify(full.queryResultid) + ")' ></i>";
                                    //d += "&nbsp;&nbsp;&nbsp;<i class='glyph-icon tooltip-button demo-icon icon-cloud-upload' title='' data-original-title='Save to Drive'  onclick='UploadToDrive(" + JSON.stringify(full.queryResultid) + ")'></i>";
                                    //d += "&nbsp;&nbsp;&nbsp;<div ng-show='ShowSaveButton' id='savetodrive-div" + full.queryResultid + "'></div>";
                                    //return d;
                                },
                                "sWidth": "600px"
                                , "responsivePriority": 4
                                , "bSortable": false
                            }
                        ]
                    });
                }
                else if (result.data.length == 0) {
                    $scope.WMsg = true;
                    $scope.FMsg = false;
                    $scope.TMsg = false;

                    $scope.IsRecordAvailable = false;
                    $scope.WarningMsg = "Nenhum registro encontrado";
                    $("#dtDocList").dataTable().fnDestroy();
                    $('#dtDocList').DataTable({
                        "aaData": result.data
                    });
                }
                else {
                    $scope.FMsg = true;
                    $scope.TMsg = false;

                    $scope.ErrorMsg = "Ocorreu um erro";
                }
            }, function errorCallback(response) {
                if (response.statusText == 'Unauthorized') {
                }
            });

        return false;
    }

    $scope.MultipleSave = function (IsDownload, isUpload) {
        var QueryResultid = [];
        var oTable = $('#dtDocList').dataTable();
        var rowcollection = oTable.$(".call-checkbox:checked", { "page": "all" });
        rowcollection.each(function (index, elem) {
            var checkbox_value = $(elem).val();
            QueryResultid.push(checkbox_value);
        });

        if (QueryResultid.length == 0) {
            $scope.FMsg = true;
            $scope.TMsg = false;
            $scope.ErrorMsg = 'Nenhum registro selecionado.';

            $("html, body").animate({ scrollTop: 120 }, "slow");
            //$('html,body').animate({
            //    scrollTop: $("#Fmsgbox").offset().top
            //}, 'slow');

            return;
        }
        else {
            $scope.FMsg = false;
            $scope.TMsg = false;
            $scope.ErrorMsg = '';
        }

        $scope.obj = {
            QueryResultid: QueryResultid
        };

        $('#loading').fadeIn(400, "linear");
        ListOfSearchedDocsService.GetMultipleFile($scope.obj).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");

                //Check error-response in xml
                if (result.data.status == "-1") {
                    $scope.FMsg = true;
                    $scope.TMsg = false;
                    $scope.ErrorMsg = result.data.message;
                    return;
                }

                if (result.data.status == "1") {
                    //$scope.ShowDriveButton = true;
                    //$scope.FileName = result.data.filename;
                    //$scope.FilePath = result.data.filepath;
                    //$scope.FullFilePath = result.data.fullFilePath;

                    $scope.PdfFilePath = result.data.filepath;
                    $scope.PdfFullFilePath = result.data.fullFilePath;
                    $scope.PdfFileName = result.data.filename;


                    if (IsDownload)
                        window.location.href = $scope.PdfFullFilePath;
                    if (isUpload) {

                        //if (getCookie("AccessToken") == "null") {
                        if (getCookie("AccessToken") === undefined || getCookie("AccessToken") === null || getCookie("AccessToken") == "null" || getCookie("AccessToken") == "") {
                            renderSaveZipToDrive('Multiple');
                        }
                        else {
                            $('#myModal').modal('show');
                        }
                    }

                }
                else {
                    $scope.FMsg = true;
                    $scope.TMsg = false;
                    $scope.ErrorMsg = result.data.message;
                    $scope.FileName = "";
                    $scope.FilePath = "";
                }

            }, function errorCallback(response) {
                if (response.statusText == 'Unauthorized') {
                }
            });
    }

    $scope.MultipleExcelFileSave = function (IsDownload) {
        var QueryResultid = [];
        var oTable = $('#dtDocList').dataTable();
        var rowcollection = oTable.$(".call-checkbox:checked", { "page": "all" });
        rowcollection.each(function (index, elem) {
            var checkbox_value = $(elem).val();
            QueryResultid.push(checkbox_value);
        });

        if (QueryResultid.length == 0) {
            $scope.FMsg = true;
            $scope.TMsg = false;
            $scope.ErrorMsg = 'Nenhum registro selecionado';

            $("html, body").animate({ scrollTop: 120 }, "slow");
            //$('html,body').animate({
            //    scrollTop: $("#Fmsgbox").offset().top
            //}, 'slow');

            return;
        }
        else {
            $scope.FMsg = false;
            $scope.TMsg = false;
            $scope.ErrorMsg = '';
        }

        $scope.obj = {
            QueryResultid: QueryResultid
        };

        $('#loading').fadeIn(400, "linear");
        ListOfSearchedDocsService.GetMultipleExcelFile($scope.obj).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");

                //Check error-response in xml
                if (result.data.status == "-1") {
                    $scope.FMsg = true;
                    $scope.TMsg = false;
                    $scope.ErrorMsg = result.data.message;
                    return;
                }

                if (result.data.status == "1") {
                    //$scope.ShowDriveButton = true;
                    $scope.FileName = result.data.filename;
                    $scope.FilePath = result.data.filepath;
                    $scope.FullFilePath = result.data.fullFilePath;

                    if (IsDownload)
                        window.location.href = $scope.FullFilePath;
                    else
                        renderSaveZipToDrive($scope.FullFilePath, $scope.FileName);
                }
                else {
                    $scope.FMsg = true;
                    $scope.TMsg = false;
                    $scope.ErrorMsg = result.data.message;
                    $scope.FileName = "";
                    $scope.FilePath = "";
                }

            }, function errorCallback(response) {
                if (response.statusText == 'Unauthorized') {
                }
            });
    }

    // Handle click on "Select all" control
    $('#example-select-all').on('click', function () {
        $('#dtDocList tbody input[type="checkbox"]').prop('checked', this.checked);

        //// Check/uncheck all checkboxes in the table

        //var table = $('#dtDocList').dataTable();
        //var rows = table.rows({ 'search': 'applied' }).nodes();
        //$('input[type="checkbox"]', rows).prop('checked', this.checked);
    });


    $scope.renderSaveZipToDrive = function () {
        renderSaveZipToDrive(null);
    };

    //Save To Google Drive
    function renderSaveZipToDrive(id) {
        if (id == null) {
            gapi.savetodrive.render('savetodrive-div', {
                src: $scope.PdfFullFilePath,
                filename: $scope.PdfFileName,
                sitename: 'JConsultans'
            });
        }
        else {
            gapi.savetodrive.render('savetodrive-div' + id, {
                src: $scope.PdfFullFilePath,
                filename: $scope.PdfFileName,
                sitename: 'JConsultans'
            });
        }
        $scope.ShowSaveButton = true;
    }

    $scope.renderSaveToDroupBox = function () {

        $scope.TMsg = false;
        $scope.FMsg = false;

        $scope.ShowSaveButton = true;
        $('#loading').fadeIn(400, "linear");
        UserService.UploadFileToDroupBox($scope.PdfFilePath, $scope.PdfFileName, null).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                if (result.data.Result) {
                    $scope.DTMsg = true;
                    $scope.DsuccessMsg = "Arquivo salvo com sucesso em Dropbox.";
                }
                else {
                    $scope.DFMsg = true;
                    $scope.DErrorMsg = result.data.Error;
                }
            }, function errorCallback(response) {
                $scope.DFMsg = true;
                $scope.DErrorMsg = "ocorreu um erro.";
            });

    }

    $scope.FileArray = [];

    $scope.UploadToDrive = function () {
        $scope.ShowSaveButton = false;
        $scope.CreatePdf($scope.objSearch, false);
    }

    $('#myModal').on('hidden.bs.modal', function (e) {
        $scope.DFMsg = false;
        $scope.DTMsg = false;

        $scope.ShowSaveButton = false;
    })

    $scope.CreatePdf = function (qId, isdownload, isUpload) {
        //If file already created on server
        var found = _.find($scope.FileArray, function (d) { return d.QueryResultid == qId; });
        if (found) {
            if (isdownload)
                window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + found.PdfFileName + "&filePath=" + found.PdfFilePath;

            if (isUpload) {
                $scope.PdfFilePath = found.PdfFilePath;
                $scope.PdfFullFilePath = found.fullFilePath;
                $scope.PdfFileName = found.PdfFileName;

                //if (getCookie("AccessToken") == "null") {
                if (getCookie("AccessToken") === undefined || getCookie("AccessToken") === null || getCookie("AccessToken") == "null" || getCookie("AccessToken") == "") {
                    renderSaveZipToDrive(qId);
                }
                else {
                    $('#myModal').modal('show');
                }

                // renderSaveToDrive(found.PdfFilePath, found.PdfFileName, qId);
            }
            return;
        }

        //If file not created on server
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            $('#loading').fadeIn(400, "linear");
            ListOfSearchedDocsService.CreatePdf(qId).then(
                function successCallback(result) {

                    $('#loading').fadeOut(400, "linear");

                    //Check error-response in xml
                    if (result.data.status == "-1") {
                        $scope.FMsg = true;
                        $scope.TMsg = false;
                        $scope.ErrorMsg = result.data.result.descricao;
                        return;
                    }

                    if (result.data.status == "1") {
                        //$scope.ShowDriveButton = true;
                        $scope.PdfFileName = result.data.filename;
                        $scope.PdfFilePath = result.data.filepath;
                        $scope.PdfFullFilePath = result.data.fullFilePath;
                        $scope.QueryId = qId;

                        $scope.FileArray.push({
                            QueryResultid: qId,
                            PdfFileName: result.data.filename,
                            PdfFilePath: result.data.filepath,
                            PdfFullFilePath: result.data.fullFilePath
                        });

                        if (isdownload)
                            window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + $scope.PdfFileName + "&filePath=" + $scope.PdfFilePath;

                        if (isUpload) {
                            if (getCookie("AccessToken") === undefined || getCookie("AccessToken") === null || getCookie("AccessToken") == "null" || getCookie("AccessToken") == "") {
                                renderSaveZipToDrive(qId);
                            }
                            else {
                                $('#myModal').modal('show');
                            }
                        }
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;
                        //$scope.ErrorMsg = result.data.message;
                        $scope.ErrorMsg = "Ocorreu um erro interno, por favor contacte o suporte!";
                        $scope.PdfFileName = "";
                        $scope.PdfFilePath = "";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }
        return false;
    }

    $scope.ExcelFileArray = [];

    $scope.CreateExcel = function (qId, isdownload) {
        //If file already created on server
        var found = _.find($scope.ExcelFileArray, function (d) { return d.QueryResultid == qId; });
        if (found) {
            if (isdownload)
                window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + found.ExcelFileName + "&filePath=" + found.ExcelFilePath;

            if (!isdownload)
                renderSaveToDrive(found.PdfFullFilePath, found.PdfFileName, qId);
            return;
        }

        $('#loading').fadeIn(400, "linear");
        ListOfSearchedDocsService.CreateExcel(qId).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                if (result.data.status == "1") {
                    //$scope.ShowDriveButton = true;
                    $scope.ExcelFileName = result.data.filename;
                    //$scope.PdfFileName = result.data.filename;
                    $scope.ExcelFilePath = result.data.filepath;
                    $scope.ExcelFullFilePath = result.data.fullFilePath;

                    $scope.ExcelFileArray.push({
                        QueryResultid: qId,
                        ExcelFileName: result.data.filename,
                        ExcelFilePath: result.data.filepath,
                        ExcelFullFilePath: result.data.fullFilePath
                    });

                    if (isdownload)
                        window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + result.data.filename + "&filePath=" + result.data.filepath;

                    if (!isdownload) {
                        renderSaveToDrive($scope.PdfFullFilePath, $scope.PdfFileName);
                    }
                }
                else {
                    $scope.FMsg = true;
                    $scope.TMsg = false;
                    $scope.ErrorMsg = result.data.message;
                    $scope.ExcelFileName = "";
                    $scope.ExcelFilePath = "";
                }
            }, function errorCallback(response) {
                if (response.statusText == 'Unauthorized') {
                }
            });

        return false;
    }

    // Searched from Multiple start
    $scope.IsMultipleData = false;
    $scope.IsSingleSearch = false;
    $scope.LoadData = function () {
        var MultipleMasterDataid = getParameterByName("mid");
        var errorlog = getParameterByName("elog");
        if (MultipleMasterDataid != null) {
            $scope.IsMultipleData = true;
            $('#loading').fadeIn(400, "linear");

            var objdata = { id: MultipleMasterDataid };

            if (errorlog == "true")
                objdata.iserror = true;
            else
                objdata.iserror = false;

            ListOfSearchedDocsService.GetDataOnMultipleData(objdata).then(
                function successCallback(result) {
                    $('#loading').fadeOut(400, "linear");
                    if (result.data.length > 0) {
                        $scope.IsRecordAvailable = true;
                        var sel = $('#selectedCode');
                        $.each(result.data, function (key, value) {
                            sel.append(' <div class="checkbox"><label> <input type="checkbox" name="op" value=' + value.QueryResultid + ' />  ' + value.DocumentCode + '</label> </div>');
                        });

                        $scope.SearchedDocumentList = result.data;
                        try {
                            $("#dtDocList").dataTable().fnDestroy();
                        } catch (e) {

                        }
                        var table = $('#dtDocList').DataTable({
                            "aaData": result.data,
                            "pageLength": 50,
                            responsive: true,
                            "destroy": true,
                            "aaSorting": [[4, "desc"]],
                            "aoColumns": [
                                {
                                    "data": "QueryResultid",
                                    'targets': 0,
                                    'searchable': false,
                                    'orderable': false,
                                    'render': function (data, type, full, meta) {
                                        return '<input type="checkbox" class="call-checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                                    },
                                    "sWidth": "100px"
                                },
                                { "data": "DocumentCode", "sWidth": "200px", "responsivePriority": 1, "bSortable": false },
                                { "data": "UserName", "sWidth": "200px", "responsivePriority": 1, "bSortable": false },
                                { "data": "AccountName", "sWidth": "150px", "responsivePriority": 1, "bSortable": false, "visible": getCookie("ProfileName") == 'Admin User' },
                                {
                                    "data": "CreatedAt",
                                    "mRender": function (data, type, full) {
                                        var date = $filter('date')(full.CreatedAt, 'dd/MM/yyyy HH:mm');
                                        var d = "<div class='btn-group btn-group-xs'>" + date + " </div>";
                                        return d;
                                    },
                                    //"sWidth": "10px", 
                                    "responsivePriority": 2,
                                    "bSortable": true,
                                    "sWidth": "275px"
                                },
                                {
                                    "data": "IsConsultaApi", "mRender": function (data, type, full) {
                                        if (full.IsConsultaApi) {
                                            return "Sim";
                                        }
                                        else
                                            return "Não";
                                    },
                                    "sWidth": "200px", "responsivePriority": 1, "bSortable": false
                                },
                                {
                                    "data": "DocumentCode",
                                    "mRender": function (data, type, full) {
                                        if (!full.IsError) {
                                            //var d = "<div class='btn-group btn-group-xs text-center'><a href='/home/DocDetailPage?qid=" + full.QueryResultid + "&docid=" + full.DocumentCode + "' target='_blank' ><span class='glyph-icon icon-list'></span></a> </div>";
                                            var d = "<a href='/home/DocDetailPage?qid=" + full.QueryResultid + "&docid=" + full.DocumentCode + "' target='_blank' >   <i class='glyph-icon tooltip-button demo-icon icon-list'></i> </a>";
                                            return d;
                                        }
                                        else
                                            return null;
                                    },
                                    //"sWidth": "10px",
                                    "responsivePriority": 2,
                                    "bSortable": false
                                }, {
                                    "data": "DocumentCode",
                                    "mRender": function (data, type, full) {
                                        if (full.IsError) {
                                            return full.Error
                                        }
                                        else {
                                            var d = "<i class='glyph-icon tooltip-button demo-icon icon-floppy-o' title='' data-original-title='Download PDF'  onclick='CreatePdf(" + JSON.stringify(full.QueryResultid) + ")' ></i>";
                                            d += "&nbsp;&nbsp;&nbsp;<i class='glyph-icon tooltip-button demo-icon icon-file-excel-o' title='Download Excel' data-original-title='Save Excel' onclick='CreateExcel(" + JSON.stringify(full.QueryResultid) + ", true)'></i>";
                                            d += "&nbsp;&nbsp;&nbsp;<i class='glyph-icon tooltip-button demo-icon icon-cloud-upload' title='' data-original-title='Save to Drive'  onclick='UploadToDrive(" + JSON.stringify(full.QueryResultid) + ")'></i>";
                                            d += "&nbsp;&nbsp;&nbsp;<div ng-show='ShowSaveButton' class='savetodrive' id='savetodrive-div" + full.QueryResultid + "'></div>";
                                            return d;
                                        }
                                    },
                                    "sWidth": "600px"
                                    , "responsivePriority": 4
                                    , "bSortable": false
                                }
                            ]
                        });


                    }
                    else if (result.data.length == 0) {
                        $scope.WMsg = true;
                        $scope.FMsg = false;
                        $scope.TMsg = false;
                        $scope.IsMultipleData = false;
                        $scope.IsSingleSearch = false;

                        $scope.IsRecordAvailable = false;
                        $scope.WarningMsg = "Nenhum registro encontrado";
                        $("#dtDocList").dataTable().fnDestroy();
                        $('#dtDocList').DataTable({
                            "aaData": result.data
                        });
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;

                        $scope.ErrorMsg = "Ocorreu um erro";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            $scope.IsSingleSearch = true;
            $scope.GetLastSearchedDocs();
        }
    }

    $scope.SearchMultipleDoc = function () {
        var MultipleMasterDataid = getParameterByName("mid");
        var errorlog = getParameterByName("elog");

        if (MultipleMasterDataid != null) {
            $scope.IsMultipleData = true;
            $('#loading').fadeIn(400, "linear");

            var QueryResultids = []

            var Codechecked_checkboxes = $("#selectedCode input:checked");
            for (var i = 0; i < Codechecked_checkboxes.length; i++) {
                QueryResultids.push(Codechecked_checkboxes[i].value);
            }

            var objdata = { id: MultipleMasterDataid, QueryResultid: QueryResultids };

            if (errorlog == "true")
                objdata.iserror = true;
            else
                objdata.iserror = false;

            ListOfSearchedDocsService.GetDataOnMultipleData(objdata).then(
                function successCallback(result) {
                    $('#loading').fadeOut(400, "linear");
                    if (result.data.length > 0) {
                        $scope.IsRecordAvailable = true;
                        $scope.SearchedDocumentList = result.data;
                        try {
                            $("#dtDocList").dataTable().fnDestroy();
                        } catch (e) {

                        }
                        var table = $('#dtDocList').DataTable({
                            "aaData": result.data,
                            "pageLength": 50,
                            responsive: true,
                            "destroy": true,
                            "aaSorting": [[3, "desc"]],
                            "aoColumns": [
                                {
                                    "data": "QueryResultid",
                                    'targets': 0,
                                    'searchable': false,
                                    'orderable': false,
                                    'render': function (data, type, full, meta) {
                                        return '<input type="checkbox" class="call-checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                                    },
                                    "sWidth": "100px"
                                },
                                { "data": "DocumentCode", "sWidth": "200px", "responsivePriority": 1, "bSortable": false },
                                { "data": "UserName", "sWidth": "200px", "responsivePriority": 1, "bSortable": false, "visible": getCookie("ProfileName") == 'Admin User' },
                                {
                                    "data": "CreatedAt",
                                    "mRender": function (data, type, full) {
                                        var date = $filter('date')(full.CreatedAt, 'dd/MM/yyyy HH:mm');
                                        var d = "<div class='btn-group btn-group-xs'>" + date + " </div>";
                                        return d;
                                    },
                                    //"sWidth": "10px", 
                                    "responsivePriority": 2,
                                    "bSortable": true,
                                    "sWidth": "250px"
                                },
                                {
                                    "data": "IsConsultaApi", "mRender": function (data, type, full) {
                                        if (full.IsConsultaApi) {
                                            return "Sim";
                                        }
                                        else
                                            return "Não";
                                    },
                                    "sWidth": "200px", "responsivePriority": 1, "bSortable": false
                                },
                                {
                                    "data": "DocumentCode",
                                    "mRender": function (data, type, full) {
                                        if (!full.isError) {
                                            //var d = "<div class='btn-group btn-group-xs text-center'><a href='/home/DocDetailPage?qid=" + full.QueryResultid + "&docid=" + full.DocumentCode + "' target='_blank' ><span class='glyph-icon icon-list'></span></a> </div>";
                                            var d = "<a href='/home/DocDetailPage?qid=" + full.QueryResultid + "&docid=" + full.DocumentCode + "' target='_blank' >   <i class='glyph-icon tooltip-button demo-icon icon-list'></i> </a>";
                                            return d;
                                        }
                                        else
                                            return null;
                                    },
                                    //"sWidth": "10px",
                                    "responsivePriority": 2,
                                    "bSortable": false
                                }, {
                                    "data": "DocumentCode",
                                    "mRender": function (data, type, full) {

                                        if (full.IsError) {
                                            return full.Error
                                        }
                                        else {
                                            var d = "<i class='glyph-icon tooltip-button demo-icon icon-floppy-o' title='' data-original-title='Download PDF'  onclick='CreatePdf(" + JSON.stringify(full.QueryResultid) + ")' ></i>";
                                            d += "&nbsp;&nbsp;&nbsp;<i class='glyph-icon tooltip-button demo-icon icon-file-excel-o' title='Download Excel' data-original-title='Save Excel' onclick='CreateExcel(" + JSON.stringify(full.QueryResultid) + ", true)'></i>";
                                            d += "&nbsp;&nbsp;&nbsp;<i class='glyph-icon tooltip-button demo-icon icon-cloud-upload' title='' data-original-title='Save to Drive'  onclick='UploadToDrive(" + JSON.stringify(full.QueryResultid) + ")'></i>";
                                            d += "&nbsp;&nbsp;&nbsp;<div ng-show='ShowSaveButton' class='savetodrive' id='savetodrive-div" + full.QueryResultid + "'></div>";
                                            return d;
                                        }
                                        //var d = "<i class='glyph-icon tooltip-button demo-icon icon-floppy-o' title='' data-original-title='Download PDF'  onclick='CreatePdf(" + JSON.stringify(full.queryResultid) + ")' ></i>";
                                        //d += "&nbsp;&nbsp;&nbsp;<i class='glyph-icon tooltip-button demo-icon icon-cloud-upload' title='' data-original-title='Save to Drive'  onclick='UploadToDrive(" + JSON.stringify(full.queryResultid) + ")'></i>";
                                        //d += "&nbsp;&nbsp;&nbsp;<div ng-show='ShowSaveButton' id='savetodrive-div" + full.queryResultid + "'></div>";
                                        //return d;
                                    },
                                    "sWidth": "600px"
                                    , "responsivePriority": 4
                                    , "bSortable": false
                                }
                            ]
                        });


                    }
                    else if (result.data.length == 0) {
                        $scope.WMsg = true;
                        $scope.FMsg = false;
                        $scope.TMsg = false;

                        $scope.IsRecordAvailable = false;
                        $scope.WarningMsg = "Nenhum registro encontrado";
                        $("#dtDocList").dataTable().fnDestroy();
                        $('#dtDocList').DataTable({
                            "aaData": result.data
                        });
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;

                        $scope.ErrorMsg = "Ocorreu um erro";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });

        }
    }

    function getParameterByName(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    // Searched from Multiple End
}
]);

