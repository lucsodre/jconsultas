﻿angular.module('myApp.Controllers').controller('ListaBuscaEmLoteCNHCntrl', ['$scope', '$filter', '$http', '$timeout', 'ListaBuscaEmLoteCNHService', 'AccountService', function ($scope, $filter, $http, $timeout, ListaBuscaEmLoteCNHService, AccountService) {
    //Date-range-picker Start

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    $scope.startDate = moment().subtract(7, "days");
    $scope.endDate = moment();
    $scope.date = {
        startDate: moment().subtract(7, "days"),
        endDate: moment()
    };

    $scope.$watch('endDate', function (newDate) {
        if ($scope.endDate < $scope.startDate)
            $scope.startDate = $scope.endDate;
    }, false);

    $scope.today = new Date();
    $scope.opts = {
        singleDatePicker: true
    }

    //Watch for date changessq
    $scope.$watch('date', function (newDate) {
        console.log('New date set: ', newDate);
    }, false);
    //Date-range-picker End

    $scope.objSearch = {
        DocumentCode: "",
        Id: "",
        AccountID: ""
    };

    $scope.UserList = [];
    $scope.AllUserList = [];
    $scope.GetUserList = function () {
        $('#loading').fadeIn(400, "linear");
        ListaBuscaEmLoteCNHService.GetUserList().then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                if (result.data.length > 0) {
                    $scope.UserList = result.data;
                    $scope.AllUserList = result.data;

                    if (getCookie("AccountID") != "null") {
                        var AccountID = getCookie("AccountID");

                        $scope.UserList = $scope.UserList.filter(function (elem) {
                            return elem["AccountId"] == AccountID;
                        });
                    }
                }
            }, function errorCallback(response) {
                if (response.statusText == 'Unauthorized') {
                }
            });


        return false;
    }
    $scope.GetUserList();

    $scope.AccountChange = function () {
        if (getCookie("AccountID") == "null") {
            var AccountID = $scope.objSearch.AccountID;

            if (AccountID == "") {
                $scope.UserList = $scope.AllUserList;
            }
            else {
                $scope.UserList = $scope.AllUserList.filter(function (elem) {
                    return elem["AccountId"] == AccountID;
                });
            }
            $scope.objSearch.Id = "";
        }
    }

    $scope.AccountList = [];
    $scope.GetAccountList = function () {
        AccountService.GetActiveAccounts().then(
            function successCallback(result) {
                if (result.data.length > 0) {
                    $scope.AccountList = result.data;
                }
            }, function errorCallback(response) {
            });
    }

    $scope.IsSearch = false;
    $scope.SearchDocument = function (obj) {
        $scope.FMsg = false;
        $scope.TMsg = false;
        $scope.WMsg = false;
        $scope.ShowZipSaveButton = false;
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            $scope.IsSearch = true;
            $('#loading').fadeIn(400, "linear");

            obj.StartDate = $scope.startDate;
            obj.EndDate = $scope.endDate;

            //setInterval(function () {
            ListaBuscaEmLoteCNHService.GetListOfMultipleSearchedDocs(obj).then(
                function successCallback(result) {
                    $('#loading').fadeOut(400, "linear");
                    if (result.data.length > 0) {
                        if (obj.Status == "true") {
                        }
                        else {
                            var filterdata = result.data.filter(function (elem) {
                                return elem["IsProceedDone"] == "Pending";
                            });

                            if (filterdata.length > 0) {
                                $scope.AutomaticCall(obj);
                            }
                        }

                        $scope.SearchedDocumentList = result.data;
                        try {
                            $("#dtDocList").dataTable().fnDestroy();
                        } catch (e) {

                        }
                        var table = $('#dtDocList').DataTable({
                            "aaData": result.data,
                            "pageLength": 50,
                            responsive: true,
                            "bDestroy": true,
                            "aaSorting": [[1, "desc"]],
                            "aoColumns": [
                                { "data": "FileName", "sWidth": "100px", "responsivePriority": 1 },
                                {
                                    "data": "CreatedBy",

                                    "mRender": function (data, type, full) {
                                        var date = $filter('date')(full.CreatedBy, 'dd/MM/yyyy HH:mm');
                                        var d = "<div class='btn-group btn-group-xs'>" + date + " </div>";
                                        return d;
                                    },
                                    //"sWidth": "10px", 
                                    "responsivePriority": 2,
                                    "bSortable": false,
                                    "sWidth": "100px"
                                },
                                { "data": "TotalFile", "sWidth": "50px", "responsivePriority": 1, "bSortable": false },
                                { "data": "TotalProceed", "sWidth": "50px", "responsivePriority": 1, "bSortable": false },
                                {
                                    "data": "TotlaFailed", "sWidth": "150px", "responsivePriority": 1, "bSortable": false,
                                    "mRender": function (data, type, full) {
                                        if (data == 0) {
                                            return "<p align='center'>" + data + "</p>";
                                            //return data;
                                        }
                                        else if (full.IsProceedDone == "Pending" && full.IsRetryProcess == true) {
                                            return "<p align='center'>0</p>";
                                            //return "0";
                                        }
                                        else {

                                            var output = '<p align="center"><button type="button" class="btn btn-default btn-sm" onclick="View(' + full.MultipleMasterDataid + ', true)">' + data + '</button> ';
                                            output = output + '<button type="button" title="Try again" class="btn btn-default btn-sm" onclick="Retry(' + full.MultipleMasterDataid + ')"> <span class="glyphicon glyphicon-refresh" ></span ></button > </p>';
                                            //if (full.IsRetryProcess == false)

                                            return output;
                                        }
                                    }
                                },
                                //{
                                //    "data": "TotlaFailed", "sWidth": "130px", "responsivePriority": 1, "bSortable": false,
                                //    "mRender": function (data, type, full) {
                                //        debugger;
                                //        //if (data == 0)
                                //        //    return data;
                                //        //else if (full.IsProceedDone == "Pending")
                                //        //    return "0"
                                //        if (full.IsProceedDone == "Complete") {
                                //           // var output = '<button type="button" class="btn btn-default btn-sm" onclick="View(' + full.MultipleMasterDataid + ', true)">' + data + '</button> ';
                                //            //if (full.IsProceedDone == "Complete" && full.IsRetryProcess == false)
                                //            var output = '<button type="button" title="Try again" class="btn btn-default btn-sm" onclick="Retry(' + full.MultipleMasterDataid + ')"> <span class="glyphicon glyphicon-refresh" ></span ></button > ';

                                //            return output;
                                //        }
                                //        else
                                //            return null;

                                //    }
                                //},
                                {
                                    "data": "IsProceedDone", "sWidth": "100px", "responsivePriority": 1, "bSortable": false,
                                    "mRender": function (data, type, full) {
                                        if (full.IsProceedDone == "Finalizado") {
                                            //var out = '<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%"> 70% </div>';
                                            return "<p> Finalizado </p>";
                                        }
                                        else {

                                            var t = 0
                                            if (full.IsRetryProcess == false)
                                                t = parseInt(full.TotalProceed) + parseInt(full.TotlaFailed);
                                            else
                                                t = parseInt(full.TotalProceed);

                                            var per = (t * 100) / parseInt(full.TotalFile);
                                            var percentage = parseFloat(per).toFixed(2) + '% Finalizado';

                                            var out = '<div class="progress">';
                                            out = out + '<div class="progress-bar" role="progressbar" aria-valuenow="' + parseFloat(per).toFixed(2) + '" aria-valuemin="0" aria-valuemax="100" style="width:' + parseFloat(per).toFixed(2) + '%">';
                                            out = out + parseFloat(per).toFixed(2) + ' %';
                                            out = out + '</div>';
                                            out = out + '</div>';

                                            return out;
                                        }
                                    }
                                },
                                { "data": "UserName", "sWidth": "100px", "responsivePriority": 1, "bSortable": false, "visible": getCookie("ProfileName") == 'Admin User' || getCookie("ProfileName") == 'Usuário administrador' },
                                { "data": "AccountName", "sWidth": "100px", "responsivePriority": 1, "bSortable": false, "visible": getCookie("ProfileName") == 'Admin User' || getCookie("ProfileName") == 'Usuário administrador' },
                                {
                                    "data": "MultipleMasterDataid", "sWidth": "250px", "responsivePriority": 1, "bSortable": false,
                                    "mRender": function (data, type, full) {
                                        var output = '<button type="button" title="View" class="btn btn-default btn-sm" onclick="View(' + data + ', false)"> <span class="glyph-icon icon-search" ></span ></button >';
                                        output = output + '&nbsp; <button type="button" title="Excel" class="btn btn-default btn-sm" onclick="ExportExcel(' + data + ')"> <span class="glyph-icon icon-file-excel-o" ></span ></button >';
                                        //<i class='glyph-icon demo-icon icon-search' title='' data-original-title='Download PDF'  onclick='View(" + data + ",false)' ></i>";
                                        // output = output + '<i class="glyph-icon tooltip-button btn-sm demo-icon icon-file-excel-o" title="" data-original-title="Save Excel" ng-click="MultipleExcelFileSave(true)"></i>';
                                        return output;
                                    }
                                }
                            ]
                        });

                        //table.column(6).visible(false);

                    }
                    else if (result.data.length == 0) {
                        $scope.WMsg = true;
                        $scope.FMsg = false;
                        $scope.TMsg = false;

                        $scope.WarningMsg = "Nenhum registro encontrado!";
                        $("#dtDocList").dataTable().fnDestroy();
                        $('#dtDocList').DataTable({
                            "aaData": result.data
                        });
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;

                        $scope.ErrorMsg = "Ocorreu um erro inesperado";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
            // }, 10000); // 60 * 1000 milsec


        }
        else {
            return false;
        }

        return false;
    }

    $scope.view = function (id, iserror) {

        var lnk = "/Home/ListaCNHBuscadas?mid=" + id + "&elog=" + iserror;
        window.location.href = lnk;
    }

    $scope.Retry = function (id) {
        $('#loading').fadeIn(400, "linear");
        ListaBuscaEmLoteCNHService.Retry(id).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                if (result.data.status == 1) {
                    $scope.TMsg = true;
                    $scope.FMsg = false;

                    $scope.successMsg = 'Processo iniciado com sucesso.';
                    $scope.SearchDocument($scope.objSearch);

                }
                else {
                    $scope.FMsg = true;
                    $scope.TMsg = false;

                    $scope.successMsg = 'Erro ao iniciar processo.';
                }
            }, function errorCallback(response) {
                if (response.statusText == 'Unauthorized') {
                }
            });
    }

    $scope.AutomaticCall = function (obj) {
        setInterval(function () {
            ListaBuscaEmLoteCNHService.GetListOfMultipleSearchedPendingDocs(obj).then(
                function successCallback(result) {
                    $('#loading').fadeOut(400, "linear");
                    if (result.data.length > 0) {
                        for (var i = 0; i < result.data.length; i++) {
                            var FileName = result.data[i].FileName;
                            var totalFile = result.data[i].TotalFile;
                            var ProcesFile = result.data[i].TotalProceed;
                            var totlaFailed = result.data[i].TotlaFailed;
                            var isProceedDone = result.data[i].IsProceedDone;
                            var IsRetryProcess = result.data[i].IsRetryProcess;
                            var MultipleMasterDataid = result.data[i].MultipleMasterDataid;

                            var table = $('#dtDocList').DataTable();
                            var table = $('#dtDocList').DataTable();
                            var rowId = $('#dtDocList').dataTable().fnFindCellRowIndexes(FileName, 0);

                            table.cell(rowId, 3).data(ProcesFile).draw(false);
                            //if (isProceedDone != "Complete") {
                            //    table.cell(rowId, 4).data("0").draw(false);
                            //}
                            //else
                            table.cell(rowId, 4).data(totlaFailed).draw(false);


                            //if (isProceedDone == "Complete") {
                            //    //var output = '<button type="button" class="btn btn-default btn-sm" onclick="View(' + MultipleMasterDataid + ', true)">' + totlaFailed + '</button> ';
                            //    var output = totlaFailed;
                            //    var output = totlaFailed + '<button type="button" title="Try again" class="btn btn-default btn-sm" onclick="Retry(' + MultipleMasterDataid + ')"> <span class="glyphicon glyphicon-refresh" ></span ></button > ';

                            //    table.cell(rowId, 4).data(output).draw(false);
                            //}
                            //else
                            //    table.cell(rowId, 4).data(0).draw(false);



                            if (isProceedDone == "Finalizado") {
                                table.cell(rowId, 5).data("Finalizado").draw(false);
                            }
                            else {

                                var t = 0;
                                if (IsRetryProcess == false)
                                    t = parseInt(ProcesFile) + parseInt(totlaFailed);
                                else
                                    t = parseInt(ProcesFile);

                                //var t = parseInt(ProcesFile) + parseInt(totlaFailed);
                                var per = (t * 100) / parseInt(totalFile);
                                var percentage = parseFloat(per).toFixed(2) + '% Finalizado';

                                var out = '<div class="progress">';
                                out = out + '<div class="progress-bar" role="progressbar" aria-valuenow="' + parseFloat(per).toFixed(2) + '" aria-valuemin="0" aria-valuemax="100" style="width:' + parseFloat(per).toFixed(2) + '%">';
                                out = out + parseFloat(per).toFixed(2) + ' %';
                                out = out + '</div>';
                                out = out + '</div>';

                                table.cell(rowId, 5).data(out).draw(false);
                            }

                            //var s = "<img src='~/download.jpg' style='width:2%;' />";
                            //table.cell(rowId, 5).data(s).draw(false);
                        }
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });

        }, 10000);
    }

    $scope.ExportExcel = function (id) {
        $('#loading').fadeIn(400, "linear");
        ListaBuscaEmLoteCNHService.ExportExcel(id).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                if (result.data.status == 1) {
                    window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + result.data.filename + "&filePath=" + result.data.filepath;
                }
                else if (result.data.status == 0) {
                    $scope.FMsg = true;
                    $scope.TMsg = false;

                    $scope.ErrorMsg = result.Msg;
                }
                else {
                    $scope.FMsg = true;
                    $scope.TMsg = false;

                    $scope.ErrorMsg = "Ocorreu um erro";
                }
            }, function errorCallback(response) {
                if (response.statusText == 'Unauthorized') {
                }
            });

    }

    $scope.Test = function (id) {
        var table = $('#dtDocList').DataTable();
        var rowId = $('#dtDocList').dataTable().fnFindCellRowIndexes('25_5_2017_05_18_05_PM.xlsx', 0);
        table.cell(rowId, 1).data('Angelica Ramos (UPDATED)').draw(false);

        return;
        var rowId = $('#dtDocList').DataTable().fnFindCellRowIndexes('1 -5', 0);

        $('#dtDocList').DataTable().cell(rowId, 1).data('Angelica Ramos (UPDATED)').draw(false);



        $('#dtDocList').DataTable().column(0).nodes().each(function (node, index, dt) {
            if ($('#dtDocList').DataTable().cell(node).data() == "1 -5") {
                var cell = table.cell(this);
                cell.data(cell.data() + 1).draw();

                $('#dtDocList').DataTable().cell(node).data('vipulpithiya.xlsx');
            }
        });

    }
}
]);

