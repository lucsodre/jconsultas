﻿angular.module('myApp.Controllers').controller('UserCntrl', ['$scope', '$rootScope', '$http', 'AccountService', 'UserService', 'HomeService', '$timeout', function ($scope, $rootScope, $http, AccountService, UserService, HomeService, $timeout) {

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    $scope.TMsg = false;
    $scope.FMsg = false;
    $scope.EditUserdata = false;
    $scope.successMsg = '';
    $scope.ErrorMsg = '';
    $scope.ProfileMasterId = '';

    $scope.objUser = {
        ProfileID: 0, ProfileId: 0, OldFileName: null, IsActive: true, SendPasswordEmail: true, ApiPassword: null, ProfileMasterId: null, UseDoubleVerification: 'No'
    };

    $scope.ErrorList = [];
    $scope.msgList = [];

    $scope.AccountList = [];
    $scope.GetAccountList = function () {
        AccountService.GetActiveAccounts().then(
            function successCallback(result) {
                if (result.data.length > 0) {

                    $scope.AccountList = result.data;
                }
            }, function errorCallback(response) {
            });
    }

    $scope.ProfileList = [];
    $scope.GetProfile = function () {
        UserService.GetProfile().then(
            function successCallback(result) {
                if (result.data.length > 0) {
                    $scope.ProfileList = result.data;

                    $scope.ProfileList = $scope.ProfileList.filter(function (obj) {
                        return (obj.ProfileName !== "Admin User" && obj.ProfileName !== "Usuário administrador");
                    });

                    var uid = $scope.ProfileList.filter(function (obj) {
                        return (obj.ProfileName === "Account single query" || obj.ProfileName === "Conta consulta veículo");
                    });

                    $scope.objUser.ProfileMasterId = uid[0].ProfileMasterId;
                    $scope.ProfileMasterId = uid[0].ProfileMasterId;
                }
            }, function errorCallback(response) {
            });
    }

    $scope.GetUserList = function () {

        UserService.GetUserList().then(
            function successCallback(result) {
                try {
                    $("#dtUser").dataTable().fnDestroy();
                } catch (e) {
                }

                var UserData = result.data.filter(function (obj) {
                    return (obj.RoleName !== "Admin User");
                });

                if (result.data.length > 0) {
                    var table = $('#dtUser').DataTable({
                        destroy: true,
                        "aaData": UserData,
                        responsive: true,
                        "aoColumns": [
                            { "data": "ApiUser", "sWidth": "100px", "responsivePriority": 1, "visible": getCookie("ProfileName") == 'Admin User' || getCookie("ProfileName") == 'Usuário administrador'},
                            { "data": "Name", "sWidth": "100px", "responsivePriority": 2 },
                            { "data": "LastName", "sWidth": "100px", "responsivePriority": 3 },
                            { "data": "AccountName", "sWidth": "100px", "responsivePriority": 4 },
                            //{ "data": "createdBy", "sWidth": "100px", "responsivePriority": 3 },
                            //{ "data": "createdAt", "sWidth": "100px", "responsivePriority": 3 },
                            //{ "data": "modifiedBy", "sWidth": "100px", "responsivePriority": 3 },
                            //{ "data": "modifiedAt", "sWidth": "100px", "responsivePriority": 3 },
                            {
                                "data": "ProfileID",
                                "mRender": function (data, type, full) {
                                    var d = "<div class='btn-group btn-group-xs'><a data-toggle='tooltip' title='' data-original-title='View' class='btn btn-default' href='javascript:void(0)' onclick='ViewUser(" + JSON.stringify(full) + ")' ><i class='fa fa-edit'> </i> &nbsp; Visualizar </a> </div>";
                                    return d;
                                },
                                "sWidth": "10px"
                                , "responsivePriority": 4
                                , "bSortable": false
                            },

                            {
                                "data": "ProfileID",
                                "mRender": function (data, type, full) {
                                    var d = "<div class='btn-group btn-group-xs'><a data-toggle='tooltip' title='' data-original-title='Edit' class='btn btn-default' href='javascript:void(0)' onclick='EditUser(" + JSON.stringify(full) + ")' ><i class='fa fa-edit'> </i> &nbsp; Editar</a> </div>";
                                    return d;
                                },
                                "sWidth": "10px"
                                , "responsivePriority": 4
                                , "bSortable": false
                            },
                            {
                                "data": "ProfileID",
                                "mRender": function (data, type, full) {
                                    var d = "<div class='btn-group btn-group-xs'><a data-toggle='Email' title='' data-original-title='Email' class='btn btn-default' href='javascript:void(0)' onclick='SendAuthenticationEmail(" + JSON.stringify(full) + ")' ><i class='fa fa-envelope '> </i> &nbsp; Enviar email </a> </div> &nbsp;&nbsp;";
                                    // d = d + "<div class='btn-group btn-group-xs'><a data-toggle='Security' title='Security' data-original-title='Security' class='btn btn-default' href='javascript:void(0)' onclick='Security(" + JSON.stringify(full) + ")' ><i class='fa fa-key'> Security</i></a> </div>";
                                    return d;
                                },
                                "sWidth": "150px"
                                , "responsivePriority": 4
                                , "bSortable": false
                            }
                        ]
                    });
                }
                else {
                    var table = $('#dtUser').DataTable();
                    table.clear().draw();
                }

            }, function errorCallback(response) {
            });
    }

    $scope.AuthenticationRequired = false;
    $scope.SaveUser = function (obj) {
        $scope.Usersubmitted = true;
        if ($scope.fmUser.$valid) {
            var objPictureName = $('input[type=file]').val().split('\\').pop();
            if (objPictureName == '')
                obj.Picture = obj.OldFileName;
            else
                obj.Picture = $('input[type=file]').val().split('\\').pop();

            var objSend =
                {
                    "Name": obj.Name,
                    "LastName": obj.LastName,
                    "Picture": obj.Picture,
                    "ApiUser": obj.ApiUser,
                    "ApiPassword": obj.ApiPassword ? obj.ApiPassword : "",
                    "AccountId": obj.AccountID,
                    "ProfileId": obj.ProfileID,
                    "ModifiedBy": obj.ModifiedBy,
                    "ModifiedAt": obj.ModifiedAt ? new Date(obj.ModifiedAt) : null,
                    "CreatedBy": obj.CreatedBy,
                    "CreatedAt": obj.CreatedAt ? new Date(obj.CreatedAt) : null,
                    "IsActive": obj.IsActive,
                    "Id": obj.Id,
                    "RoleID": obj.RoleID,
                    "SendPasswordEmail": obj.SendPasswordEmail,
                    "MobileNo": obj.MobileNo,
                    "ProfileMasterId": obj.ProfileMasterId,
                    "apiUsuario": obj.apiUsuario,
                    "apiSenha": obj.apiSenha,
                    "UseDoubleVerification": obj.UseDoubleVerification,
                    "IsEmailVerifeyed": obj.IsEmailVerifeyed,
                    "IsPhonelVerifeyed": obj.IsPhonelVerifeyed,
                    "IpAddress": obj.IpAddress,
                    "AccessToken": obj.AccessToken,

                };

            $('#loading').fadeIn(400, "linear");
            UserService.SaveUser(objSend).then(
                function successCallback(result) {
                    if (result.data.Respones) {
                        if ($("#Picture").val() != "") {
                            var fileUpload = $("#Picture").get(0);
                            var Id = result.data.id;
                            var name = Id + '.jpg';
                            var files = fileUpload.files;
                            var data = new FormData();
                            for (var i = 0; i < files.length; i++) {
                                files[i].FileName = name;
                                data.append(fileUpload, files[i]);
                            }

                            $.ajax({
                                type: "POST",
                                url: "/home/UploadFilesAjax?sImage=" + obj.OldFileName,
                                contentType: false,
                                processData: false,
                                data: data,
                                success: function (message) {
                                    $('#loading').fadeOut(400, "linear");
                                    if (message) {
                                        if ($scope.EditUserdata) {
                                            $('#loading').fadeOut(400, "linear");
                                            $scope.ErrorList = [];
                                            $scope.msgList.push('Registro atualizado com sucesso');

                                            $timeout(function () {
                                                var scope = angular.element(document.getElementById("HomeCntrl")).scope();
                                                scope.GetCurrentUserDetails();
                                            }, 500);

                                            $timeout(function () {
                                                $scope.GetCurrentUserdetails();
                                            }, 500);

                                        }
                                        else {
                                            //vp
                                            $scope.TMsg = true;
                                            $scope.FMsg = false;

                                            $('#loading').fadeOut(400, "linear");
                                            $('.UserModel').modal('hide');

                                            if (obj.ProfileID == 0) {
                                                $scope.successMsg = 'Usuário salvo com sucesso';
                                            }
                                            else {
                                                $scope.successMsg = 'Usuário atualizado com sucesso';
                                            }

                                            //if (result.data.AuthRequired) {
                                            //    $scope.AuthenticationRequired = true;
                                            //    $scope.objUser.ProfileId = result.data.id;
                                            //    $scope.$apply();
                                            //}
                                            //else {
                                            //    $scope.ClearUser();
                                            //    $scope.GetUserList();
                                            //}

                                            $scope.ClearUser();
                                            $scope.GetUserList();
                                        }
                                    }
                                    else {
                                        $scope.ErrorList.push('Registro salvo mas erro ao enviar imagem');
                                    }
                                },
                                error: function () {
                                    alert("Erro ao enviar o arquivo!");
                                }
                            });

                        }
                        else {
                            if ($scope.EditUserdata) {
                                $('#loading').fadeOut(400, "linear");
                                $scope.ErrorList = [];
                                $scope.msgList.push('Registro atualizado com sucesso');

                                $timeout(function () {
                                    var scope = angular.element(document.getElementById("HomeCntrl")).scope();
                                    scope.GetCurrentUserDetails();
                                }, 500);

                                $timeout(function () {
                                    $scope.GetCurrentUserdetails();
                                }, 500);

                            }
                            else {
                                $scope.TMsg = true;
                                $scope.FMsg = false;

                                $('#loading').fadeOut(400, "linear");
                                $('.UserModel').modal('hide');

                                if (obj.ProfileID == 0) {
                                    $scope.successMsg = 'Usuário salvo com sucesso';
                                }
                                else {
                                    $scope.successMsg = 'Usuário atualizado com sucesso';
                                }

                                //if (result.data.AuthRequired) {
                                //    $scope.AuthenticationRequired = true;
                                //    $scope.objUser.ProfileId = result.data.id;
                                //    $scope.$apply();
                                //}
                                //else {
                                //    $scope.ClearUser();
                                //    $scope.GetUserList();
                                //}
                                $scope.ClearUser();
                                $scope.GetUserList();
                            }
                        }
                    }
                    else {
                        $('#loading').fadeOut(400, "linear");

                        $scope.msgList = [];
                        for (var i = 0; i < result.data.Error.length; i++) {
                            $scope.ErrorList.push(result.data.Error[i].Description);
                        }

                        if ($scope.ErrorList.length == 0) {
                            if (obj.ProfileID == 0) {
                                $scope.ErrorList.push('Erro ao inserir registro');
                            }
                            else {
                                $scope.ErrorList.push('Erro ao atualizar registro');
                            }
                        }
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }

        return false;
    }

    $scope.Security = function (obj) {
        $('#mySecurityModal').modal('show');
        $scope.objUser = obj;
        $scope.$apply();
    }

    var _URL = window.URL || window.webkitURL;
    $('INPUT[type="file"]').change(function (e) {
        var file, img;
        if ((file = this.files[0])) {
            img = new Image();
            var size = this.files[0].size;
            img.onload = function () {
                $scope.ErrorList = [];
                $scope.$apply();
                this.value = '';

                if (this.width != 128) {
                    $scope.ErrorList = [];
                    $scope.ErrorList.push('Largura do arquivo permitida: 128');
                    $scope.$apply();
                    this.value = '';
                    return;
                }
                if (this.height != 128) {
                    $scope.ErrorList = [];
                    $scope.ErrorList.push('Altura do arquivo permitida: 128');
                    $scope.$apply();
                    this.value = '';
                    return;
                }


                //if (size > 2000) {
                //    $scope.msgList = [];
                //    $scope.ErrorList.push('File max size is 2mb');
                //    $scope.$apply();
                //    this.value = '';
                //    return;
                //}
            };
            img.onerror = function (e) {
                this.value = '';
                $scope.ErrorList = [];
                $scope.ErrorList.push('Formato de arquivo inválido: ' + file.type);
                $scope.$apply();

            };
            img.src = _URL.createObjectURL(file);
        }

        //if ((file = this.files[0])) {
        //    img = new Image();
        //    img.onload  = function () {
        //        alert(this.width + " " + this.height);

        //        var ext = this.value.match(/\.(.+)$/)[1];
        //        switch (ext) {
        //            case 'jpg':
        //                break;
        //            case 'gif':
        //                break;
        //            case 'png':
        //                break;
        //            case 'bmp':
        //                break;
        //            default:
        //                $scope.msgList = [];
        //                $scope.ErrorList.push('This is not an allowed file type');
        //                $scope.$apply();
        //                this.value = '';
        //        }
        //        if (this.width != 128) {
        //            $scope.msgList = [];
        //            $scope.ErrorList.push('File width only allow 128');
        //            $scope.$apply();
        //            this.value = '';
        //        }
        //        if (this.height != 128) {
        //            $scope.msgList = [];
        //            $scope.ErrorList.push('File Height only allow 128');
        //            $scope.$apply();
        //            this.value = '';
        //        }
        //        alert(this.size);

        //    };
        //    img.onerror = function (e) {
        //        $scope.msgList = [];
        //        $scope.ErrorList.push('Not a valid file: ' + file.type);
        //        $scope.$apply();
        //        this.value = '';
        //    };
        //}
    });

    $scope.GetCurrentUserdetails = function () {
        $timeout(function () {

            HomeService.GetCurrentUserDetails().then(
                function successCallback(result) {
                    if (result.data.userdata.length > 0) {
                        $scope.EditUserdata = true;
                        var obj = result.data.userdata[0];
                        //$scope.objUser = { Id: obj.id, RoleID: obj.roleID, RoleName: obj.roleName, Name: obj.name, LastName: obj.lastName, OldFileName: obj.picture, ApiUser: obj.apiUser, ApiPassword: obj.apiPassword, ProfileID: obj.profileId, AccountID: obj.accountId, IsActive: obj.isActive }
                        //$scope.objUser = { Id: obj.id, RoleID: obj.roleID, RoleName: obj.roleName, Name: obj.name, LastName: obj.lastName, OldFileName: obj.picture, ApiUser: obj.apiUser, ApiPassword: obj.apiPassword, ProfileID: obj.profileId, AccountID: obj.accountId, IsActive: obj.isActive, ModifiedBy: obj.modifiedBy, ModifiedAt: obj.modifiedAt, CreatedBy: obj.createdBy, CreatedAt: obj.createdAt, LastLogin: obj.lastLogin, CreatedByName: obj.createdByName, ModifiedByName: obj.modifiedByName }

                        //$scope.objUser = { Id: obj.id, RoleID: obj.roleID, RoleName: obj.roleName, Name: obj.name, LastName: obj.lastName, OldFileName: obj.picture, ApiUser: obj.apiUser, ApiPassword: obj.apiPassword, ProfileID: obj.profileId, AccountID: obj.accountId, IsActive: obj.isActive, ModifiedBy: obj.modifiedBy, ModifiedByName: obj.modifiedByName, ModifiedAt: obj.modifiedAt, CreatedBy: obj.createdBy, CreatedByName: obj.createdByName, CreatedAt: obj.createdAt, LastLogin: obj.lastLogin, ProfileMasterId: obj.profileMasterId, MobileNo: obj.mobileNo }
                        $scope.objUser = {
                            Id: obj.Id, UseDoubleVerification: obj.UseDoubleVerification, RoleID: obj.RoleID, RoleName: obj.RoleName, Name: obj.Name, LastName: obj.LastName, OldFileName: obj.Picture, ApiUser: obj.ApiUser, ApiPassword: obj.ApiPassword, ProfileID: obj.ProfileId, AccountID: obj.AccountId, IsActive: obj.IsActive, ModifiedBy: obj.ModifiedBy, ModifiedByName: obj.ModifiedByName, ModifiedAt: obj.ModifiedAt, CreatedBy: obj.CreatedBy, CreatedByName: obj.CreatedByName, CreatedAt: obj.CreatedAt, LastLogin: obj.LastLogin, ProfileId: obj.ProfileId, ProfileMasterId: obj.ProfileMasterId, ProfileID: obj.ProfileId, MobileNo: obj.MobileNo, apiUsuario: obj.apiUsuario, apiSenha: obj.apiSenha, AccessToken: obj.AccessToken, IsEmailVerifeyed: obj.IsEmailVerifeyed, IsPhonelVerifeyed: obj.IsPhonelVerifeyed
                        }
                        $scope.$apply();
                    }
                    else {
                    }
                }, function errorCallback(response) {

                });
        }, 500);
    }

    $scope.OpenSecurity = function () {
        $('#mySecurityModal').modal('show');
    }


    $scope.Type = '';
    $scope.EditUser = function (obj, type) {
        $scope.AuditoriaData = obj.Auditorias;
        $scope.Type = type;
        $('.UserModel').modal('show');
        $scope.objUser = { Id: obj.Id, RoleID: obj.RoleID, RoleName: obj.RoleName, Name: obj.Name, LastName: obj.LastName, OldFileName: obj.Picture, ApiUser: obj.ApiUser, ApiPassword: obj.ApiPassword, ProfileID: obj.ProfileId, AccountID: obj.AccountId, IsActive: obj.IsActive, ModifiedBy: obj.ModifiedBy, ModifiedByName: obj.ModifiedByName, ModifiedAt: obj.ModifiedAt, CreatedBy: obj.CreatedBy, CreatedByName: obj.CreatedByName, CreatedAt: obj.CreatedAt, LastLogin: obj.LastLogin, ProfileMasterId: obj.ProfileMasterId, MobileNo: obj.MobileNo, apiUsuario: obj.apiUsuario, apiSenha: obj.apiSenha, UseDoubleVerification: obj.UseDoubleVerification, IsEmailVerifeyed: obj.IsEmailVerifeyed, IsPhonelVerifeyed: obj.IsPhonelVerifeyed, IpAddress: obj.IpAddress, AccessToken: obj.AccessToken }
        // $scope.objUser.AccountID = obj.AccountId;
        $scope.$apply();
    }

    $scope.SendAuthenticationEmail = function (obj) {
        $('#loading').fadeIn(400, "linear");

        UserService.SendAuthenticationEmail(obj.ProfileId, obj.ApiUser, obj.Name).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                if (result.data) {
                    $scope.TMsg = true;
                    $scope.FMsg = false;

                    $scope.successMsg = 'Email enviado com sucesso';
                }
                else {
                    $scope.FMsg = true;
                    $scope.TMsg = false;

                    $scope.ErrorMsg = 'Erro ao enviar de email';
                }
            }, function errorCallback(response) {
                if (response.statusText == 'Unauthorized') {
                }
            });

        return false;
    }

    $('.UserModel').on('hidden.bs.modal', function () {
        if ($scope.AuthenticationRequired) {
            if ($scope.objUser.UseDoubleVerification != 'No') {
                $('#mySecurityModal').modal('show');

                $scope.successMsg = "";
                $scope.ErrorMsg = "";
            }
        }
        else
            $scope.ClearUser();

    })


    $('#mySecurityModal').on('hidden.bs.modal', function () {
        if ($scope.EditUserdata == true) {
            $scope.GetCurrentUserdetails();
            $scope.successMsg = "";
            $scope.ErrorMsg = "";
        }
        else {
            $scope.AuthenticationRequired = false;
            $scope.ClearUser();
            $scope.GetUserList();
        }
    })

    $scope.ClearUser = function () {
        $scope.objUser = { ProfileID: 0, OldFileName: null, IsActive: true, SendPasswordEmail: true, ApiPassword: null, ProfileMasterId: $scope.ProfileMasterId, UseDoubleVerification: 'No' };

        $('#upldimg').val('');
        $scope.ErrorList = [];
        $scope.Usersubmitted = false;
        $scope.fmUser.$setPristine();
        $scope.$apply();
    }

    $scope.CodeErrorList = [];
    $scope.CodeSucessList = [];
    $scope.SendEmailCode = function () {
        debugger;
        if ($scope.objUser.ProfileId > 0) {
            $('#loading').fadeIn(400, "linear");
            UserService.SendEmailCode($scope.objUser.ProfileId).then(
                function successCallback(result) {
                    if (result.data.Result == 1) {
                        $('#loading').fadeOut(400, "linear");
                        $scope.CodeErrorList = [];
                        $scope.CodeSucessList.push('Código de verificação enviado com sucesso');
                    }
                    else {
                        $('#loading').fadeOut(400, "linear");
                        $scope.CodeSucessList = [];
                        $scope.CodeErrorList.push(result.data.Msg);
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
    }

    $scope.CheckEmailVerficationCode = function () {
        $scope.EmailVeficationUsersubmitted = true;
        if ($scope.fmEmailVefication.$valid) {
            if ($scope.objUser.ProfileId > 0) {

                $('#loading').fadeIn(400, "linear");
                UserService.CheckEmailVerficationCode($scope.objUser.ProfileId, $scope.EmailVarificationCode).then(
                    function successCallback(result) {
                        $scope.CodeErrorList = [];
                        $scope.CodeSucessList = [];

                        if (result.data) {
                            $('#loading').fadeOut(400, "linear");
                            $scope.CodeSucessList.push("Endereço de email verificado com sucesso");

                            $scope.EmailVarificationCode = '';

                            $scope.EmailVeficationUsersubmitted = false;
                            $scope.fmEmailVefication.$setPristine();


                        }
                        else {
                            $('#loading').fadeOut(400, "linear");
                            $scope.CodeErrorList.push("Código inválido ou expirado. Tente novamente.");
                        }
                    }, function errorCallback(response) {
                        if (response.statusText == 'Unauthorized') {
                        }
                    });
            }
        }
    }

    $scope.PhoneCodeErrorList = [];
    $scope.PhoneCodeSucessList = [];
    $scope.SendPhoneCode = function () {
        if ($scope.objUser.ProfileId > 0) {
            $('#loading').fadeIn(400, "linear");
            UserService.SendPhoneCode($scope.objUser.ProfileId).then(
                function successCallback(result) {
                    if (result.data.Result == 1) {
                        $('#loading').fadeOut(400, "linear");
                        $scope.PhoneCodeErrorList = [];
                        $scope.PhoneCodeSucessList.push('Código de verificação enviado com sucesso');
                    }
                    else {
                        $('#loading').fadeOut(400, "linear");
                        $scope.PhoneCodeSucessList = [];
                        $scope.PhoneCodeErrorList.push(result.data.Msg);
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
    }

    $scope.CheckPhoneVerficationCode = function () {
        $scope.PhoneVeficationUsersubmitted = true;
        if ($scope.fmPhoneVefication.$valid) {
            if ($scope.objUser.ProfileId > 0) {

                $('#loading').fadeIn(400, "linear");
                UserService.CheckPhoneVerficationCode($scope.objUser.ProfileId, $scope.PhoneVarificationCode).then(
                    function successCallback(result) {
                        $scope.PhoneCodeErrorList = [];
                        $scope.PhoneCodeSucessList = [];

                        if (result.data) {
                            $('#loading').fadeOut(400, "linear");
                            $scope.PhoneCodeSucessList.push("Número de telefone verificado com sucesso");

                            $scope.PhoneVarificationCode = "";
                            $scope.PhoneVeficationUsersubmitted = false;
                            $scope.fmPhoneVefication.$setPristine();

                        }
                        else {
                            $('#loading').fadeOut(400, "linear");
                            $scope.PhoneCodeErrorList.push("Código inválido ou expirado. Tente novamente.");
                        }
                    }, function errorCallback(response) {
                        if (response.statusText == 'Unauthorized') {
                        }
                    });
            }
        }
    }

}
]);

