﻿angular.module('myApp.Controllers').controller('ProfileCntrl', ['$scope', '$rootScope', '$http', 'ProfileService', 'UserService', function ($scope, $rootScope, $http, ProfileService, UserService) {

    $scope.TMsg = false;
    $scope.FMsg = false;
    $scope.successMsg = '';
    $scope.ErrorMsg = '';
    $scope.objProfile = { UserProfileId: 0, UserMaster: false, Account: false, SingleSearch: false, BatchSearch: false, ConsultaMotorista: false, ConsultaMotoristaLote: false};

    //$scope.UserList = [];
    //$scope.GetUserList = function () {
    //    UserService.GetActiveUserList().then(
    //        function successCallback(result) {
    //            if (result.data.length > 0) {
    //                $scope.UserList = result.data;
    //            }
    //        }, function errorCallback(response) {
    //        });
    //}

    $scope.selectall = function () {
        if ($scope.objProfile.select) {
            $scope.objProfile.UserMaster = true;
            $scope.objProfile.Account = true;
            $scope.objProfile.SingleSearch = true;
            $scope.objProfile.BatchSearch = true;
            $scope.ConsultaMotorista = true;
            $scope.ConsultaMotoristaLote = true;
            $scope.$apply();

        }
        else {
            $scope.objProfile.UserMaster = false;
            $scope.objProfile.Account = false;
            $scope.objProfile.SingleSearch = false;
            $scope.objProfile.BatchSearch = false;
            $scope.ConsultaMotorista = false;
            $scope.ConsultaMotoristaLote = false;
            $scope.$apply();
        }
    };

    $scope.GetUserProfileList = function () {
        $('#loading').fadeIn(400, "linear");
        ProfileService.GetUserProfileList().then(
            function successCallback(result) {
                if (result.data.length > 0) {
                    $('#loading').fadeOut(400, "linear");
                    try {
                        $("#dtProfile").dataTable().fnDestroy();
                    } catch (e) {

                    }
                    var table = $('#dtProfile').DataTable({
                        "aaData": result.data,
                        responsive: true,
                        "aoColumns": [
                            { "data": "ProfileNameTraduzido", "sWidth": "100px", "responsivePriority": 1 },
                            {
                                "data": "ProfileMasterId",
                                "mRender": function (data, type, full) {
                                    var d = "<div class='btn-group btn-group-xs'><a data-toggle='tooltip' title='' data-original-title='Editar' class='btn btn-default' href='javascript:void(0)' onclick='EditProfile(" + JSON.stringify(full) + ")' ><i class='fa fa-edit'> </i> &nbsp; Editar </a> </div>";
                                    return d;
                                },
                                "sWidth": "10px"
                                , "responsivePriority": 2
                                , "bSortable": false
                            }
                        ]
                    });
                }
                else {
                    var table = $('#dtProfile').DataTable();
                    table.clear().draw();
                }

            }, function errorCallback(response) {
            });
    }

    $scope.SaveProfile = function (obj) {
        $scope.profilesubmitted = true;

        if ($scope.fmProfile.$valid) {
            $('#loading').fadeIn(400, "linear");
            ProfileService.AddUpdateUserProfile(obj).then(
                function successCallback(result) {

                    if (result.data) {
                        $scope.TMsg = true;
                        $scope.FMsg = false;

                        if (obj.UserProfileId == 0) {
                            $scope.successMsg = 'Perfil salvo com sucesso';
                        }
                        else
                            $scope.successMsg = 'Perfil atualizado com sucesso';

                        $scope.ClearProfile();
                        $('.ProfileModel').modal('hide');
                        $scope.GetUserProfileList();
                    }
                    else {
                        $('#loading').fadeOut(400, "linear");
                        $scope.FMsg = true;
                        $scope.TMsg = false;

                        if (obj.userProfileId == 0) {
                            $scope.successMsg = 'Erro ao salvar perfil';
                        }
                        else
                            $scope.successMsg = 'Erro ao atualizar perfil';

                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }

        return false;
    }

    $scope.EditProfile = function (obj) {
        $scope.AuditoriaData = obj.Auditorias;
        $('.ProfileModel').modal('show');
        //$scope.objProfile = { ProfileMasterId: obj.profileMasterId, UserMaster: obj.userMaster, Account: obj.account, SingleSearch: obj.singleSearch, BatchSearch: obj.batchSearch, ProfileName: obj.profileName, IsActive: obj.isActive, ModifiedBy: obj.modifiedBy, ModifiedAt: obj.modifiedAt, CreatedBy: obj.createdBy, CreatedAt: obj.createdAt }
        $scope.objProfile = obj;//{ ProfileMasterId: obj.profileMasterId, UserMaster: obj.userMaster, Account: obj.account, SingleSearch: obj.singleSearch, BatchSearch: obj.batchSearch, ProfileName: obj.profileName, IsActive: obj.isActive, ModifiedBy: obj.modifiedBy, ModifiedAt: obj.modifiedAt, CreatedBy: obj.createdBy, CreatedAt: obj.createdAt }
        $scope.$apply();
    }

    $('.ProfileModel').on('hidden.bs.modal', function () {
        $scope.ClearProfile();
    })

    $scope.ClearProfile = function () {
        $scope.objProfile = { UserProfileId: 0 };
        $scope.AuditoriaData = null;
        $scope.profilesubmitted = false;
        $scope.fmProfile.$setPristine();
        $scope.$apply();
    }

}
]);

