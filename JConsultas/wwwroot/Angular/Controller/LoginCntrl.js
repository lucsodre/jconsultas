﻿angular.module('myApp.Controllers').controller('LoginCntrl', ['$scope', '$rootScope', '$http', 'LoginService', function ($scope, $rootScope, $http, LoginService) {

    $scope.TMsg = false;
    $scope.FMsg = false;
    $scope.ErrorMsg = '';
    $scope.Msg = '';


    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }


    $scope.ClearForgetPassword = function () {
        $scope.TMsg = false;
        $scope.FMsg = false;
        $scope.ErrorMsg = '';
        try {
            //$scope.objForget.EmailID = '';

            $scope.Emailsubmitted = false;
            $scope.fmForget.$setPristine();
            $scope.$apply();
        } catch (e) {

        }
    }

    $scope.SendEmail = function (obj) {
        $scope.Emailsubmitted = true;
        if ($scope.fmForget.$valid) {
            $('#loading').fadeIn(400, "linear");
            LoginService.ForgetPassword(obj.EmailID).then(
                function successCallback(result) {
                    $('#loading').fadeOut(400, "linear");
                    if (result.data.Resposne) {
                        $scope.TMsg = result.data.resposne;
                        //    $scope.objForget.EmailID = '';
                    }
                    else {
                        $scope.FMsg = true;

                        if (result.data.Msg == "Invalid") {
                            $scope.ErrorMsg = 'Usuário inválido.';
                        }
                        else
                            $scope.ErrorMsg = 'Erro no envio do email.';
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }
    }

    function getParameterByName(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.ResetPassword = function (obj) {
        $scope.changePasswordsubmitted = true;
        if ($scope.fmChangePassword.$valid) {
            if (obj.Password != obj.cPassword) {
                $scope.FMsg = true;
                $scope.ErrorMsg = 'Senha e confirmação de senha devem ser as mesmas.';
                return;
            }
            var type = getParameterByName('type');
            var Encid = getParameterByName('id');
            if (Encid == null) {
                $scope.FMsg = true;
                $scope.ErrorMsg = 'Operação  inválida.';
                return;
            }
            $('#loading').fadeIn(400, "linear");
            LoginService.ResetPassword(Encid, obj.Password).then(
                function successCallback(result) {
                    $('#loading').fadeOut(400, "linear");
                    if (result.data.Resposne) {
                        $scope.TMsg = true;
                        $scope.FMsg = false;

                        if (type == "forget")
                            $scope.Msg = 'Senha alterada com sucesso';
                        else
                            $scope.Msg = 'Senha criada com sucesso';

                        $scope.Emailsubmitted = false;
                        $scope.fmForget.$setPristine();
                        $scope.$apply();
                    }
                    else {
                        $scope.FMsg = true;
                        if (result.data.msg == "Invalid") {
                            $scope.ErrorMsg = 'Usuário inválido.';
                        }
                        else if (result.data.msg == "NotExist") {
                            $scope.ErrorMsg = 'Usuário não existente.';
                        }
                        else
                            $scope.ErrorMsg = 'Operação  inválida.';
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }
    }

    $('.UserModel').on('hidden.bs.modal', function () {
        $scope.ClearForgetPassword();
    })

    $scope.UserLogin = function () {
        //$('#loading').fadeIn(400, "linear"); 
    }
}
]);

