﻿angular.module('myApp.Controllers').controller('SingleSearchCntrl', ['$scope', '$http', '$filter', '$window', 'SingleSearchService', 'UserService', function ($scope, $http, $filter, $window, SingleSearchService, UserService) {

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    $scope.objConsultaAvancada = null;
    $scope.TMsg = false;
    $scope.FMsg = false;
    $scope.successMsg = '';
    $scope.ErrorMsg = '';
    $scope.ShowPdfButton = false;
    $scope.ShowDriveButton = false;
    $scope.ShowSaveButton = false;
    $scope.PdfFileName = "";
    $scope.ExcelFileName = "";
    $scope.ExcelFilePath = "";
    $scope.ShowErroParam = false;
    $scope.ErroParam = "";
    $scope.parseFloat = parseFloat;
    $scope.parseInt = parseInt;
    $scope.getFormattedDate = function (dt) {
        if (dt)
            return dt.replace(/^(\d{2})(\d{2})(\d{4})$/, '$1/$2/$3');
    }
    $scope.getFormattedValue = function (valor) {
        if (valor)
            return parseFloat(valor) / 100;
    }

    $scope.documentData = [];
    $scope.SearchDocument = function (obj) {
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            if (obj.DocumentCode.length != 7 && obj.DocumentCode.length != 17) {
                $scope.FMsg = true;
                $scope.TMsg = false;
                $scope.ErrorMsg = "Insira uma placa (7 caracteres) ou chassi (17 caracteres) para realizar a pesquisa.";
                return;
            }
            $scope.ShowPdfButton = false;
            $scope.ShowDriveButton = false;
            $scope.ShowSaveButton = false;
            $scope.PdfFileName = "";
            $scope.searchsubmitted = true;
            $scope.TMsg = false;
            $scope.FMsg = false;

            SingleSearchService.CheckExistsInDatabase(obj.DocumentCode).then(function successCallback(r1) {

                $scope.ExcelFileName = "";
                $scope.ExcelFilePath = "";

                var userConfirmedOrNot = true;
                if (r1.data.status == true) {
                    bootbox.dialog({
                        closeButton: false,
                        message: "Existe uma consulta realizada para este veículo em " + ($filter('date')(new Date(r1.data.date), 'dd/MM/yyyy hh:mm:ss a')) + " O que deseja fazer?", buttons: [{
                            "label": "Realizar nova consulta",
                            "class": "success",
                            "callback": function () {
                                if ($scope.fmSearch.$valid) {
                                    $('#loading').fadeIn(400, "linear");
                                    SingleSearchService.SearchDocument(obj.DocumentCode, true).then(
                                        function successCallback(result) {
                                            $('#loading').fadeOut(400, "linear");
                                            //Check for error
                                            if (result.data.status == "0") {
                                                $scope.FMsg = true;
                                                $scope.TMsg = false;
                                                $scope.ErrorMsg = "Oops! Um erro interno ocorreu, por favor contacte o administrador!";
                                                return;
                                            }

                                            //Check error-response in xml
                                            if (result.data.status == "-1") {
                                                $scope.FMsg = true;
                                                $scope.TMsg = false;
                                                $scope.ErrorMsg = result.data.result.CONSULTAS.CONSULTA.DESCRICAO;
                                                return;
                                            }

                                            if (result.data.CONSULTAS.CONSULTA.RETORNOEXECUCAO == "000") {

                                                $scope.ShowPdfButton = true;
                                                $scope.ShowDriveButton = true;

                                                $scope.veiculoData = result.data.CONSULTAS.CONSULTA.VEICULO;
                                                $scope.proprietarioData = result.data.CONSULTAS.CONSULTA.PROPRIETARIO;
                                                $scope.gravameData = result.data.CONSULTAS.CONSULTA.GRAVAME;
                                                $scope.gravamE_INTENCAOData = result.data.CONSULTAS.CONSULTA.GRAVAME_INTENCAO;
                                                $scope.debitosData = result.data.CONSULTAS.CONSULTA.TOTAL_DEBITOS;
                                                $scope.restricoesData = result.data.CONSULTAS.CONSULTA.RESTRICOES;
                                                $scope.multasData = result.data.CONSULTAS.CONSULTA.MULTAS;
                                                $scope.bloqueiosDetranData = result.data.CONSULTAS.CONSULTA.BLOQUEIOSDETRAN;
                                                $scope.inspecaoVeicularData = result.data.CONSULTAS.CONSULTA.INSPECAO_VEICULAR;
                                                $scope.comunicacaoVendaData = result.data.CONSULTAS.CONSULTA.COMUNICACAO_VENDA;
                                                $scope.debitosIPVAData = result.data.CONSULTAS.CONSULTA.IPVAS;
                                                $scope.debitosDPVATData = result.data.CONSULTAS.CONSULTA.DPVATS;

                                                if (result.data.CONSULTAS.CONSULTA.CONSULTAAVANCADAVEICULO == null)
                                                {
                                                    $scope.ConsultaAvancadaData =
                                                        {
                                                            "PesquisaCompleta": true,
                                                            "DetalhamentoMultas": true,
                                                            "DetalhamentoDPVAT": true,
                                                            "DetalhamentoIPVA": true,
                                                            "DetalhamentoDebitos": true,
                                                            "BloqueiosDetran": true,
                                                            "InspecaoVeicular": true,
                                                            "Restricoes": true,
                                                            "IntencaoGravame": true,
                                                            "ArrendarioFinanceira": true,
                                                            "DadosProp": true,
                                                            "DadosVeic": true
                                                        };
                                                }
                                                else {
                                                    $scope.ConsultaAvancadaData = result.data.CONSULTAS.CONSULTA.CONSULTAAVANCADAVEICULO;
                                                }

                                                $scope.objConsultaAvancadaPesquisa =
                                                    {
                                                        "PesquisaCompleta": $scope.ConsultaAvancadaData.PesquisaCompleta,
                                                        "DetalhamentoMultas": $scope.ConsultaAvancadaData.DetalhamentoMultas,
                                                        "DetalhamentoDPVAT": $scope.ConsultaAvancadaData.DetalhamentoDPVAT,
                                                        "DetalhamentoIPVA": $scope.ConsultaAvancadaData.DetalhamentoIPVA,
                                                        "DetalhamentoDebitos": $scope.ConsultaAvancadaData.DetalhamentoDebitos,
                                                        "BloqueiosDetran": $scope.ConsultaAvancadaData.BloqueiosDetran,
                                                        "InspecaoVeicular": $scope.ConsultaAvancadaData.InspecaoVeicular,
                                                        "Restricoes": $scope.ConsultaAvancadaData.Restricoes,
                                                        "IntencaoGravame": $scope.ConsultaAvancadaData.IntencaoGravame,
                                                        "ArrendarioFinanceira": $scope.ConsultaAvancadaData.ArrendarioFinanceira,
                                                        "DadosProp": $scope.ConsultaAvancadaData.DadosProp,
                                                        "DadosVeic": $scope.ConsultaAvancadaData.DadosVeic
                                                    };

                                                $scope.ShowOpcoesDaConsulta = true;

                                                $scope.TotalDeDebitos = parseFloat($scope.debitosData.IPVA) + parseFloat($scope.debitosData.LICENCIAMENTO) + parseFloat($scope.debitosData.MULTAS) + parseFloat($scope.debitosData.DPVAT);
                                            }
                                            else {
                                                $scope.FMsg = true;
                                                $scope.TMsg = false;
                                                $scope.ErrorMsg = result.data.CONSULTAS.CONSULTA.DESCRICAO;
                                            }
                                        }, function errorCallback(response) {
                                            if (response.statusText == 'Unauthorized') {
                                            }
                                        });
                                }
                                else {
                                    return false;
                                }
                            }
                        }, {
                            "label": "Exibir mesma consulta",
                            "class": "danger",
                            "callback": function () {
                                if ($scope.fmSearch.$valid) {
                                    $('#loading').fadeIn(400, "linear");
                                    SingleSearchService.SearchDocument(obj.DocumentCode, false).then(
                                        function successCallback(result) {
                                            $('#loading').fadeOut(400, "linear");
                                            //Check for error
                                            if (result.data.status == "0") {
                                                $scope.FMsg = true;
                                                $scope.TMsg = false;
                                                $scope.ErrorMsg = "Oops! Um erro interno ocorreu, por favor contacte o administrador!";
                                                return;
                                            }

                                            //Check error-response in xml
                                            //if (result.data.status == "-1") {
                                            //    $scope.FMsg = true;
                                            //    $scope.TMsg = false;
                                            //    $scope.ErrorMsg = result.data.result.descricao;
                                            //    return;
                                            //}

                                            if (result.data.status == "-1") {
                                                $scope.FMsg = true;
                                                $scope.TMsg = false;
                                                $scope.ErrorMsg = result.data.result.CONSULTAS.CONSULTA.DESCRICAO;
                                                return;
                                            }
                                            //Check success-response in xml
                                            if (result.data.CONSULTAS.CONSULTA.RETORNOEXECUCAO == "000") {

                                                $scope.ShowPdfButton = true;
                                                $scope.ShowDriveButton = true;

                                                $scope.veiculoData = result.data.CONSULTAS.CONSULTA.VEICULO;
                                                $scope.proprietarioData = result.data.CONSULTAS.CONSULTA.PROPRIETARIO;
                                                $scope.gravameData = result.data.CONSULTAS.CONSULTA.GRAVAME;
                                                $scope.gravamE_INTENCAOData = result.data.CONSULTAS.CONSULTA.GRAVAME_INTENCAO;
                                                $scope.debitosData = result.data.CONSULTAS.CONSULTA.TOTAL_DEBITOS;
                                                $scope.restricoesData = result.data.CONSULTAS.CONSULTA.RESTRICOES;
                                                $scope.multasData = result.data.CONSULTAS.CONSULTA.MULTAS;
                                                $scope.bloqueiosDetranData = result.data.CONSULTAS.CONSULTA.BLOQUEIOSDETRAN;
                                                $scope.inspecaoVeicularData = result.data.CONSULTAS.CONSULTA.INSPECAO_VEICULAR;
                                                $scope.comunicacaoVendaData = result.data.CONSULTAS.CONSULTA.COMUNICACAO_VENDA;
                                                $scope.debitosIPVAData = result.data.CONSULTAS.CONSULTA.IPVAS;
                                                $scope.debitosDPVATData = result.data.CONSULTAS.CONSULTA.DPVATS;
                                                if (result.data.CONSULTAS.CONSULTA.CONSULTAAVANCADAVEICULO == null)
                                                {
                                                    $scope.ConsultaAvancadaData =
                                                        {
                                                            "PesquisaCompleta": true,
                                                            "DetalhamentoMultas": true,
                                                            "DetalhamentoDPVAT": true,
                                                            "DetalhamentoIPVA": true,
                                                            "DetalhamentoDebitos": true,
                                                            "BloqueiosDetran": true,
                                                            "InspecaoVeicular": true,
                                                            "Restricoes": true,
                                                            "IntencaoGravame": true,
                                                            "ArrendarioFinanceira": true,
                                                            "DadosProp": true,
                                                            "DadosVeic": true
                                                        };
                                                }
                                                else {
                                                    $scope.ConsultaAvancadaData = result.data.CONSULTAS.CONSULTA.CONSULTAAVANCADAVEICULO;
                                                }

                                                $scope.objConsultaAvancadaPesquisa =
                                                    {
                                                        "PesquisaCompleta": $scope.ConsultaAvancadaData.PesquisaCompleta,
                                                        "DetalhamentoMultas": $scope.ConsultaAvancadaData.DetalhamentoMultas,
                                                        "DetalhamentoDPVAT": $scope.ConsultaAvancadaData.DetalhamentoDPVAT,
                                                        "DetalhamentoIPVA": $scope.ConsultaAvancadaData.DetalhamentoIPVA,
                                                        "DetalhamentoDebitos": $scope.ConsultaAvancadaData.DetalhamentoDebitos,
                                                        "BloqueiosDetran": $scope.ConsultaAvancadaData.BloqueiosDetran,
                                                        "InspecaoVeicular": $scope.ConsultaAvancadaData.InspecaoVeicular,
                                                        "Restricoes": $scope.ConsultaAvancadaData.Restricoes,
                                                        "IntencaoGravame": $scope.ConsultaAvancadaData.IntencaoGravame,
                                                        "ArrendarioFinanceira": $scope.ConsultaAvancadaData.ArrendarioFinanceira,
                                                        "DadosProp": $scope.ConsultaAvancadaData.DadosProp,
                                                        "DadosVeic": $scope.ConsultaAvancadaData.DadosVeic
                                                    };

                                                $scope.ShowOpcoesDaConsulta = true;

                                                $scope.TotalDeDebitos = parseFloat($scope.debitosData.IPVA) + parseFloat($scope.debitosData.LICENCIAMENTO) + parseFloat($scope.debitosData.MULTAS) + parseFloat($scope.debitosData.DPVAT);
                                            }
                                            else {
                                                $scope.FMsg = true;
                                                $scope.TMsg = false;
                                                $scope.ErrorMsg = result.data.CONSULTAS.CONSULTA.DESCRICAO;
                                            }
                                        }, function errorCallback(response) {
                                            if (response.statusText == 'Unauthorized') {
                                            }
                                        });
                                }
                                else {
                                    return false;
                                }
                            }
                        }, {
                            "label": "Cancelar",
                            "class": "primary",
                            "callback": function () {

                            }
                        }]
                    });
                }
                else {
                    userConfirmedOrNot = true;
                    if ($scope.fmSearch.$valid) {
                        $('#loading').fadeIn(400, "linear");
                        SingleSearchService.SearchDocument(obj.DocumentCode, userConfirmedOrNot).then(
                            function successCallback(result) {
                                $('#loading').fadeOut(400, "linear");
                                //Check for error
                                if (result.data.status == "0") {
                                    $scope.FMsg = true;
                                    $scope.TMsg = false;
                                    $scope.ErrorMsg = "Oops! Um erro interno ocorreu, por favor contacte o administrador!";
                                    return;
                                }

                                //Check error-response in xml
                                //if (result.data.status == "-1") {
                                //    $scope.FMsg = true;
                                //    $scope.TMsg = false;
                                //    $scope.ErrorMsg = result.data.result.descricao;
                                //    return;
                                //}

                                if (result.data.status == "-1") {
                                    $scope.FMsg = true;
                                    $scope.TMsg = false;
                                    $scope.ErrorMsg = result.data.result.CONSULTAS.CONSULTA.DESCRICAO;
                                    return;
                                }

                                //Check success-response in xml
                                if (result.data.CONSULTAS.CONSULTA.RETORNOEXECUCAO == "000") {

                                    $scope.ShowPdfButton = true;
                                    $scope.ShowDriveButton = true;

                                    $scope.veiculoData = result.data.CONSULTAS.CONSULTA.VEICULO;
                                    $scope.proprietarioData = result.data.CONSULTAS.CONSULTA.PROPRIETARIO;
                                    $scope.gravameData = result.data.CONSULTAS.CONSULTA.GRAVAME;
                                    $scope.gravamE_INTENCAOData = result.data.CONSULTAS.CONSULTA.GRAVAME_INTENCAO;
                                    $scope.debitosData = result.data.CONSULTAS.CONSULTA.TOTAL_DEBITOS;
                                    $scope.restricoesData = result.data.CONSULTAS.CONSULTA.RESTRICOES;
                                    $scope.multasData = result.data.CONSULTAS.CONSULTA.MULTAS;
                                    $scope.bloqueiosDetranData = result.data.CONSULTAS.CONSULTA.BLOQUEIOSDETRAN;
                                    $scope.inspecaoVeicularData = result.data.CONSULTAS.CONSULTA.INSPECAO_VEICULAR;
                                    $scope.comunicacaoVendaData = result.data.CONSULTAS.CONSULTA.COMUNICACAO_VENDA;
                                    $scope.debitosIPVAData = result.data.CONSULTAS.CONSULTA.IPVAS;
                                    $scope.debitosDPVATData = result.data.CONSULTAS.CONSULTA.DPVATS;

                                    if (result.data.CONSULTAS.CONSULTA.CONSULTAAVANCADAVEICULO == null)
                                    {
                                        $scope.ConsultaAvancadaData =
                                            {
                                                "PesquisaCompleta": true,
                                                "DetalhamentoMultas": true,
                                                "DetalhamentoDPVAT": true,
                                                "DetalhamentoIPVA": true,
                                                "DetalhamentoDebitos": true,
                                                "BloqueiosDetran": true,
                                                "InspecaoVeicular": true,
                                                "Restricoes": true,
                                                "IntencaoGravame": true,
                                                "ArrendarioFinanceira": true,
                                                "DadosProp": true,
                                                "DadosVeic": true
                                            };
                                    }
                                    else
                                    {
                                        $scope.ConsultaAvancadaData = result.data.CONSULTAS.CONSULTA.CONSULTAAVANCADAVEICULO;
                                    }
                                    $scope.objConsultaAvancadaPesquisa =
                                        {
                                            "PesquisaCompleta": $scope.ConsultaAvancadaData.PesquisaCompleta,
                                            "DetalhamentoMultas": $scope.ConsultaAvancadaData.DetalhamentoMultas,
                                            "DetalhamentoDPVAT": $scope.ConsultaAvancadaData.DetalhamentoDPVAT,
                                            "DetalhamentoIPVA": $scope.ConsultaAvancadaData.DetalhamentoIPVA,
                                            "DetalhamentoDebitos": $scope.ConsultaAvancadaData.DetalhamentoDebitos,
                                            "BloqueiosDetran": $scope.ConsultaAvancadaData.BloqueiosDetran,
                                            "InspecaoVeicular": $scope.ConsultaAvancadaData.InspecaoVeicular,
                                            "Restricoes": $scope.ConsultaAvancadaData.Restricoes,
                                            "IntencaoGravame": $scope.ConsultaAvancadaData.IntencaoGravame,
                                            "ArrendarioFinanceira": $scope.ConsultaAvancadaData.ArrendarioFinanceira,
                                            "DadosProp": $scope.ConsultaAvancadaData.DadosProp,
                                            "DadosVeic": $scope.ConsultaAvancadaData.DadosVeic
                                        };

                                    $scope.ShowOpcoesDaConsulta = true;
                                    $scope.TotalDeDebitos = parseFloat($scope.debitosData.IPVA) + parseFloat($scope.debitosData.LICENCIAMENTO) + parseFloat($scope.debitosData.MULTAS) + parseFloat($scope.debitosData.DPVAT);
                                }
                                else {
                                    $scope.FMsg = true;
                                    $scope.TMsg = false;
                                    $scope.ErrorMsg = result.data.CONSULTAS.CONSULTA.DESCRICAO;
                                }
                            }, function errorCallback(response) {
                                if (response.statusText == 'Unauthorized') {
                                }
                            });
                    }
                    else {
                        return false;
                    }
                }
            })
        }
        else
            return false;

    }

    //Save To Google Drive
    $scope.UploadFilePath = "";
    $scope.UploadFileName = "";

    //Save To Google Drive
    $scope.renderSaveZipToDrive = function (type) {
        gapi.savetodrive.render('savetodrive-div' + type, {
            src: $scope.PdfFullFilePath,
            filename: $scope.PdfFileName,
            sitename: 'JConsultans'
        });

        $scope.ShowSaveButton = true;
    }

   

    $scope.SetErroParamFalse = function () {
        $scope.ShowErroParam = false;
        $scope.ErroParam = "";
    }

    $scope.PesquisaCompleta = function (obj) {
        if (obj) {
            $scope.objConsultaAvancada.DetalhamentoMultas = true;
            $scope.objConsultaAvancada.DetalhamentoDPVAT = true;
            $scope.objConsultaAvancada.DetalhamentoIPVA = true;
            $scope.objConsultaAvancada.DetalhamentoDebitos = true;
            $scope.objConsultaAvancada.BloqueiosDetran = true;
            $scope.objConsultaAvancada.InspecaoVeicular = true;
            $scope.objConsultaAvancada.Restricoes = true;
            $scope.objConsultaAvancada.IntencaoGravame = true;
            $scope.objConsultaAvancada.ArrendarioFinanceira = true;
            $scope.objConsultaAvancada.DadosProp = true;
            $scope.objConsultaAvancada.DadosVeic = true;
            $scope.$apply();
        }
    }

    $scope.SalvarParametros = function (obj)
    {
        if (obj === null) {
            return false;
        }

        if (!obj.DetalhamentoMultas &&
            !obj.DetalhamentoDPVAT &&
            !obj.DetalhamentoIPVA &&
            !obj.DetalhamentoDebitos &&
            !obj.BloqueiosDetran &&
            !obj.InspecaoVeicular &&
            !obj.Restricoes &&
            !obj.IntencaoGravame &&
            !obj.ArrendarioFinanceira &&
            !obj.DadosProp &&
            !obj.DadosVeic)
        {
            $scope.ShowErroParam = true;
            $scope.ErroParam = "Selecione ao menos um tipo de dado";
            return false;
        }

        SingleSearchService.SalvarParametros(obj).then(
        function successCallback(result)
        {
            alert("Os parâmetros da consulta foram salvos.");
            $scope.ShowErroParam = false;
            $('.ConsultaAvancada').modal('hide');
        },
        function errorCallback(response)
        {
        });
    }

    $scope.BuscaParametros = function ()
    {
        SingleSearchService.BuscaParametros().then(
            function successCallback(result)
            {
                if (result.data == null || result.data == "")
                {
                    result.data =
                        {
                            "DetalhamentoMultas": true,
                            "DetalhamentoDPVAT": true,
                            "DetalhamentoIPVA": true,
                            "DetalhamentoDebitos": true,
                            "BloqueiosDetran": true,
                            "InspecaoVeicular": true,
                            "Restricoes": true,
                            "IntencaoGravame": true,
                            "ArrendarioFinanceira": true,
                            "DadosProp": true,
                            "DadosVeic": true
                        };
                }

                $scope.objConsultaAvancada = result.data;
                $scope.$apply();
            },
            function errorCallback(response) {
            });
    }

    $scope.renderSaveToDroupBox = function () {
        $scope.TMsg = false;
        $scope.FMsg = false;

        $scope.ShowSaveButton = true;
        $('#loading').fadeIn(400, "linear");
        UserService.UploadFileToDroupBox($scope.PdfFilePath, $scope.PdfFileName, null).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                if (result.data.Result) {
                    $scope.DTMsg = true;
                    $scope.DsuccessMsg = "Arquivo salvo com sucesso em Dropbox.";
                }
                else {
                    $scope.DFMsg = true;
                    $scope.DErrorMsg = result.data.Error;
                }
            }, function errorCallback(response) {
                $scope.DFMsg = true;
                $scope.DErrorMsg = "Ocorreu um erro não previsto";
            });
    }

    $scope.UploadToDrive = function () {
        $scope.ShowSaveButton = false;
        $scope.CreatePdf($scope.objSearch, false, true);
    }

    $scope.CreatePdf = function (obj, isdownload, isUpload) {
        //If file already created on server
        if ($scope.PdfFileName != "") {
            if (isdownload)
                window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + $scope.PdfFileName + "&filePath=" + $scope.PdfFilePath;

            if (isUpload) {
                if (getCookie("AccessToken") === undefined || getCookie("AccessToken") === null || getCookie("AccessToken") == "null" || getCookie("AccessToken") == "") {
                    $scope.renderSaveZipToDrive('S');
                }
                else {
                    $('#myModal').modal('show');
                }
            }
            return;
        }
        //If file not created on server
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            $('#loading').fadeIn(400, "linear");
            SingleSearchService.CreatePdf(obj.DocumentCode).then(
                function successCallback(result) {

                    $('#loading').fadeOut(400, "linear");
                    if (result.data.status == "1") {
                        //$scope.ShowDriveButton = true;
                        $scope.PdfFileName = result.data.filename;
                        $scope.PdfFilePath = result.data.filepath;
                        $scope.PdfFullFilePath = result.data.fullFilePath;

                        if (isdownload)
                            window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + result.data.filename + "&filePath=" + result.data.filepath;

                        if (isUpload) {
                            if (getCookie("AccessToken") === undefined || getCookie("AccessToken") === null || getCookie("AccessToken") == "null" || getCookie("AccessToken") == "") {
                                $scope.renderSaveZipToDrive('S');
                            }
                            else {
                                $('#myModal').modal('show');
                            }
                            //renderSaveToDrive($scope.PdfFilePath, $scope.PdfFileName);
                        }
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;
                        $scope.ErrorMsg = result.data.message;
                        $scope.PdfFileName = "";
                        $scope.PdfFilePath = "";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }
        return false;
    }

    //Upload To Drive Old Code
    //$scope.UploadToDrive = function () {
    //    $scope.searchsubmitted = true;
    //    if ($scope.fmSearch.$valid) {
    //        $('#loading').fadeIn(400, "linear");
    //        SingleSearchService.UploadPDfToDrive($scope.PdfFileName).then(
    //            function successCallback(result) {
    //                $('#loading').fadeOut(400, "linear");
    //                //Check for error and show message
    //                if (result.data.status == "0") {
    //                    $scope.FMsg = true;
    //                    $scope.TMsg = false;
    //                    $scope.ErrorMsg = result.data.message;
    //                    return;
    //                }
    //                if (result.data.status == "1") {
    //                    $scope.FMsg = false;
    //                    $scope.TMsg = true;
    //                    $scope.successMsg = 'Pdf file ' + result.data.filename + ' uploaded in google drive !';
    //                }
    //                else {
    //                    $scope.FMsg = true;
    //                    $scope.TMsg = false;
    //                    $scope.ErrorMsg = "Oops ! Internal error occurred , Please contact administrator !";;
    //                }
    //            }, function errorCallback(response) {
    //                if (response.statusText == 'Unauthorized') {
    //                }
    //            });
    //    }
    //    else {
    //        return false;
    //    }

    //    return false;
    //}

    $scope.CreateExcel = function (obj, isdownload) {

        //If file already created on server
        if ($scope.ExcelFileName != "") {
            if (isdownload)
                window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + $scope.ExcelFileName + "&filePath=" + $scope.ExcelFilePath;

            if (!isdownload)
                renderSaveToDrive($scope.PdfFullFilePath, $scope.PdfFileName);
            return;
        }

        //If file not created on server
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            $('#loading').fadeIn(400, "linear");
            SingleSearchService.CreateExcel(obj.DocumentCode).then(
                function successCallback(result) {

                    $('#loading').fadeOut(400, "linear");
                    if (result.data.status == "1") {
                        //$scope.ShowDriveButton = true;
                        $scope.ExcelFileName = result.data.filename;
                        //$scope.PdfFileName = result.data.filename;
                        $scope.ExcelFilePath = result.data.filepath;
                        $scope.ExcelFullFilePath = result.data.fullFilePath;

                        if (isdownload)
                            window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + result.data.filename + "&filePath=" + result.data.filepath;

                        if (!isdownload) {
                            renderSaveToDrive($scope.PdfFullFilePath, $scope.PdfFileName);
                        }
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;
                        $scope.ErrorMsg = result.data.message;
                        $scope.ExcelFileName = "";
                        $scope.ExcelFilePath = "";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }
        return false;
    }

}
]);

