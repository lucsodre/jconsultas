﻿angular.module('myApp.Controllers').controller('HomeCntrl', ['$scope', '$rootScope', '$http', 'HomeService', function ($scope, $rootScope, $http, HomeService) {

    $scope.User = { picture: null };
    $scope.Profile = [];
    $scope.GetCurrentUserDetails = function () {
        HomeService.GetCurrentUserDetails().then(
            function successCallback(result) {

                if (result.data.userdata.length > 0) {
                    var path = '/ProfilePhoto/' + result.data.userdata[0].Picture;
                    $('#ProfileImage').attr('src', path);
                    $('#ProfileImage2').attr('src', path);
                    $scope.User = result.data.userdata[0];
                    document.cookie = "AccessToken=" + $scope.User.AccessToken + ";path=/";
                }
                else {
                    $('#ProfileImage').attr('src', '../../assets/image-resources/noimage.png');
                    $('#ProfileImage2').attr('src', '../../assets/image-resources/noimage.png');
                }

                if (result.data.profiledata.length > 0) {
                    $scope.Profile = result.data.profiledata[0];
                    document.cookie = "ProfileName=" + $scope.Profile.ProfileName + ";path=/";

                    if ($scope.Profile.ProfileName == "Admin User") {
                        document.cookie = "AccountID=" + null + ";path=/";
                    }
                    else {

                        document.cookie = "AccountID=" + $scope.User.AccountId + ";path=/";
                    }
                }

            }, function errorCallback(response) {

            });
    }


    $scope.test = function () {

        return;
        HomeService.Test().then(
            function successCallback(result) {
                if (result.data.userdata.length > 0) {
                }
                else {
                    $('#ProfileImage').attr('src', '../../assets/image-resources/noimage.png');
                    $('#ProfileImage2').attr('src', '../../assets/image-resources/noimage.png');
                }

                if (result.data.profiledata.length > 0) {
                    $scope.Profile = result.data.profiledata[0];
                }

            }, function errorCallback(response) {

            });
    }

}
]);

