﻿angular.module('myApp.Controllers').controller('DetalheConsultaMotoristaCntrl', ['$scope', '$http', '$window', 'DetalheConsultaMotoristaService', 'UserService', function ($scope, $http, $window, DetalheConsultaMotoristaService, UserService) {

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    $scope.TMsg = false;
    $scope.FMsg = false;
    $scope.successMsg = '';
    $scope.ErrorMsg = '';
    $scope.ShowPdfButton = false;
    $scope.ShowDriveButton = false;
    $scope.ShowSaveButton = false;
    $scope.PdfFileName = "";
    $scope.parseFloat = parseFloat;
    $scope.ExcelFileName = "";
    $scope.ExcelFilePath = "";

    $scope.CondutoresData = [];

    $scope.Imprimir = false;

    $scope.objSearch = { QId: "", DocumentCode: "" }
    $scope.init = function (qId, docId) {
        $scope.objSearch = { QId: qId, DocumentCode: docId };
        $scope.SearchDocument($scope.objSearch);

        if (docId == "")
        {
            $scope.Imprimir = true;
            var innerContents = document.getElementById(printSectionId).innerHTML.replace('align = "right"', 'align = "left"');

            alert("AS: " + innerContents);
            
            //var doc = new jsPDF();
            //doc.text(document.getElementById('conteudo').innerHTML);
            //doc.save('teste.pdf');
        }   
    }

    $scope.documentData = [];
    $scope.SearchDocument = function (obj) {
        //if (obj.DocumentCode.length != 7 && obj.DocumentCode.length != 17) {
        //    $scope.FMsg = true;
        //    $scope.TMsg = false;
        //    $scope.ErrorMsg = "Please,Enter 7 or 17 character document code !";
        //    return;
        //}

        $scope.ShowPdfButton = false;
        $scope.ShowDriveButton = false;
        $scope.ShowSaveButton = false;

        $scope.PdfFileName = "";
        $scope.searchsubmitted = true;
        $scope.TMsg = false;
        $scope.FMsg = false;
        //if ($scope.fmSearch.$valid) {
        $('#loading').fadeIn(400, "linear");
        DetalheConsultaMotoristaService.SearchDocument(obj.QId).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                //Check for error
                if (result.data.status == "0") {
                    $scope.FMsg = true;
                    $scope.TMsg = false;
                    $scope.ErrorMsg = "Oops! Um erro interno ocorreu, por favor contacte o administrador!";
                    return;
                }

                //Check error-response in xml
                if (result.data.status == "-1") {
                    $scope.FMsg = true;
                    $scope.TMsg = false;
                    $scope.ErrorMsg = result.data.result.CONSULTAS.CONSULTA.DESCRICAO;
                    close();
                    return;
                }

                if (result.data.CONSULTAS.CONSULTA.RETORNOEXECUCAO == "000") {

                    $scope.ShowPdfButton = true;
                    $scope.ShowDriveButton = true;

                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.EXAMES.EXAME.length > 0) {

                        $scope.ShowExame = true;
                    }
                    else {
                        $scope.ShowExame = false;
                    }

                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.CURSOS.CURSO.length > 0) {

                        $scope.ShowCurso = true;
                    }
                    else {
                        $scope.ShowCurso = false;
                    }

                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.RECURSOS.RECURSO.length > 0) {

                        $scope.ShowRecurso = true;
                    }
                    else {
                        $scope.ShowRecurso = false;
                    }

                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.INFRACOES.INFRACAO.length > 0) {

                        $scope.ShowInfracao = true;
                    }
                    else {
                        $scope.ShowInfracao = false;
                    }

                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.BLOQUEIOS.BLOQUEIO.length > 0) {

                        $scope.ShowBloqueio = true;
                    }
                    else {
                        $scope.ShowBloqueio = false;
                    }

                    $scope.CondutorData = result.data.CONSULTAS.CONSULTA.RESULTADO.DADOSCONDUTOR;
                    $scope.CnhData = result.data.CONSULTAS.CONSULTA.RESULTADO.DADOSCNH;
                    $scope.ExamesData = result.data.CONSULTAS.CONSULTA.RESULTADO.EXAMES.EXAME;
                    $scope.CursosData = result.data.CONSULTAS.CONSULTA.RESULTADO.CURSOS.CURSO;
                    $scope.RecursosData = result.data.CONSULTAS.CONSULTA.RESULTADO.RECURSOS.RECURSO;
                    $scope.PontuacaoData = result.data.CONSULTAS.CONSULTA.RESULTADO.PONTUACAO;
                    $scope.InfracoesData = result.data.CONSULTAS.CONSULTA.RESULTADO.INFRACOES.INFRACAO;
                    $scope.BloqueiosData = result.data.CONSULTAS.CONSULTA.RESULTADO.BLOQUEIOS.BLOQUEIO;
                }
                else {
                    $scope.FMsg = true;
                    $scope.TMsg = false;
                    $scope.ErrorMsg = result.data.CONSULTAS.CONSULTA.DESCRICAO;
                }
            }, function errorCallback(response) {
                if (response.statusText == 'Unauthorized') {
                }
            });
    }

    $scope.printToCart = function (printSectionId) {

        var innerContents = document.getElementById(printSectionId).innerHTML.replace('align = "right"', 'align = "left"');

        var popupWinindow = window.open('', '_blank', 'width=900,height=1200,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.title = 'Impressão de consulta CNH';
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.css">' +
            '<link rel="stylesheet" type="text/css" href="/assets/helpers/colors.css"> ' +
            '<link rel="stylesheet" type="text/css" href="/assets/elements/responsive-tables.css"> ' +
            '<link href="/css/print.css" rel="stylesheet" media="print" /> ' +
            '</head > <body onload="window.print()" > <div><h2 style="margin:10px; float:left;">Consulta de CNH</h2> <img style="margin:10px; float:right;" src="../assets/image-resources/logosgd.png" />  ' + innerContents + '</div></html>');

        if (!$scope.ShowCurso) {
            var node = popupWinindow.document.getElementById('ShowCurso');
            if (node != null) {
                node.remove();
            }
        }

        if (!$scope.ShowBloqueio) {
            var node = popupWinindow.document.getElementById('ShowBloqueio');
            if (node != null) {
                node.remove();
            }
        }

        if (!$scope.ShowExame) {
            var node = popupWinindow.document.getElementById('ShowExame');
            if (node != null) {
                node.remove();
            }
        }

        if (!$scope.ShowInfracao) {
            var node = popupWinindow.document.getElementById('ShowInfracao');
            if (node != null) {
                node.remove();
            }
        }

        if (!$scope.ShowRecurso) {
            var node = popupWinindow.document.getElementById('ShowRecurso');
            if (node != null) {
                node.remove();
            }
        }       

        popupWinindow.document.close();
    }

    //Save To Google Drive
    $scope.renderSaveZipToDrive = function (type) {
        gapi.savetodrive.render('savetodrive-div' + type, {
            src: $scope.PdfFullFilePath,
            filename: $scope.PdfFileName,
            sitename: 'JConsultans'
        });

        $scope.ShowSaveButton = true;
    }

    //Save To Google Drive
    $scope.renderSaveToDroupBox = function () {
        $scope.DTMsg = false;
        $scope.DFMsg = false;

        $scope.ShowSaveButton = true;
        $('#loading').fadeIn(400, "linear");
        UserService.UploadFileToDroupBox($scope.PdfFilePath, $scope.PdfFileName, null).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                if (result.data.Result) {
                    $scope.DTMsg = true;
                    $scope.DsuccessMsg = "Arquivo salvo com sucesso em Dropbox.";
                }
                else {
                    $scope.DFMsg = true;
                    $scope.DErrorMsg = result.data.Error;
                }
            }, function errorCallback(response) {
                $scope.DFMsg = true;
                $scope.DErrorMsg = "Ocorreu um erro.";
            });
    }


    $scope.UploadToDrive = function () {
        $scope.ShowSaveButton = false;
        $scope.CreatePdf($scope.objSearch, false, true);
    }

    $scope.CreatePdf = function (obj, isdownload, isUpload) {
        //If file already created on server
        if ($scope.PdfFileName != "") {
            if (isdownload)
                window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + $scope.PdfFileName + "&filePath=" + $scope.PdfFilePath;

            if (isUpload) {

                if (getCookie("AccessToken") === undefined || getCookie("AccessToken") === null || getCookie("AccessToken") == "null" || getCookie("AccessToken") == "") {
                    $scope.renderSaveZipToDrive('S');
                }
                else {
                    $('#myModal').modal('show');
                }
                //renderSaveToDrive($scope.PdfFilePath, $scope.PdfFileName);
            }
            return;
        }
        //If file not created on server
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            $('#loading').fadeIn(400, "linear");
            DetalheConsultaMotoristaService.CreatePdf(obj.QId).then(
                function successCallback(result) {

                    $('#loading').fadeOut(400, "linear");

                    //Check error-response in xml
                    if (result.data.status == "-1") {
                        $scope.FMsg = true;
                        $scope.TMsg = false;
                        $scope.ErrorMsg = result.data.result.descricao;
                        return;
                    }

                    if (result.data.status == "1") {
                        //$scope.ShowDriveButton = true;
                        $scope.PdfFileName = result.data.filename;
                        $scope.PdfFilePath = result.data.filepath;
                        $scope.PdfFullFilePath = result.data.fullFilePath;

                        if (isdownload)
                            window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + result.data.filename + "&filePath=" + result.data.filepath;

                        if (isUpload) {
                            if (getCookie("AccessToken") === undefined || getCookie("AccessToken") === null || getCookie("AccessToken") == "null" || getCookie("AccessToken") == "") {
                                $scope.renderSaveZipToDrive('S');
                            }
                            else {
                                $('#myModal').modal('show');
                            }
                            //renderSaveToDrive($scope.PdfFilePath, $scope.PdfFileName);
                        }
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;
                        $scope.ErrorMsg = result.data.message;
                        $scope.PdfFileName = "";
                        $scope.PdfFilePath = "";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }
        return false;
    }


    $scope.CreateExcel = function (obj, isdownload) {
        //If file already created on server
        if ($scope.ExcelFileName != "") {
            if (isdownload)
                window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + $scope.ExcelFileName + "&filePath=" + $scope.ExcelFilePath;

            if (!isdownload)
                renderSaveToDrive($scope.PdfFullFilePath, $scope.PdfFileName);
            return;
        }

        //If file not created on server
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            $('#loading').fadeIn(400, "linear");
            DetalheConsultaMotoristaService.CreateExcel(obj.QId).then(
                function successCallback(result) {
                    $('#loading').fadeOut(400, "linear");
                    if (result.data.status == "1") {
                        //$scope.ShowDriveButton = true;
                        $scope.ExcelFileName = result.data.filename;
                        //$scope.PdfFileName = result.data.filename;
                        $scope.ExcelFilePath = result.data.filepath;
                        $scope.ExcelFullFilePath = result.data.fullFilePath;

                        if (isdownload)
                            window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + result.data.filename + "&filePath=" + result.data.filepath;

                        if (!isdownload) {
                            renderSaveToDrive($scope.PdfFullFilePath, $scope.PdfFileName);
                        }
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;
                        $scope.ErrorMsg = result.Msg;
                        $scope.ExcelFileName = "";
                        $scope.ExcelFilePath = "";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }
        return false;
    }

}
]);

