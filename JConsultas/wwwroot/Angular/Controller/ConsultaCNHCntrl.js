﻿angular.module('myApp.Controllers').controller('ConsultaCNHCntrl',
    ['$scope', '$http', '$filter', '$window', 'ConsultaCNHService', 'UserService',
    function ($scope, $http, $filter, $window, ConsultaCNHService, UserService) {

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

    $scope.UFs = ["AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG",
        "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR",
        "SC", "SP", "SE", "TO"];

    $scope.TMsg = false;
    $scope.FMsg = false;
    $scope.successMsg = '';
    $scope.ErrorMsg = '';
    $scope.ShowPdfButton = false;
    $scope.ShowDriveButton = false;
    $scope.ShowSaveButton = false;
    $scope.PdfFileName = "";
    $scope.ExcelFileName = "";
    $scope.ExcelFilePath = "";

    $scope.parseFloat = parseFloat;
    $scope.parseInt = parseInt;
    $scope.getFormattedDate = function (dt) {
        if (dt)
            return dt.replace(/^(\d{2})(\d{2})(\d{4})$/, '$1/$2/$3');
    }
    $scope.getFormattedValue = function (valor) {
        if (valor)
            return parseFloat(valor) / 100;
    }

    $scope.documentData = [];
    $scope.SearchDocument = function (obj) {
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            $scope.ShowPdfButton = false;
            $scope.ShowDriveButton = false;
            $scope.ShowSaveButton = false;
            $scope.PdfFileName = "";
            $scope.searchsubmitted = true;
            $scope.TMsg = false;
            $scope.FMsg = false;

            ConsultaCNHService.VerificaExisteBuscaBD(obj.UF, obj.Nome, obj.CPF, obj.NumRegistro, obj.NumRenach, obj.ValidadeCNH, obj.RG, obj.DataNascimento,
                obj.DataHabilitacao, obj.MunicipioNascimento, obj.NumCedulaEspelho)
                .then(function successCallback(r1) {

                    $scope.ExcelFileName = "";
                    $scope.ExcelFilePath = "";

                    var userConfirmedOrNot = true;
                    if (r1.data.status == true) {
                        bootbox.dialog({
                            closeButton: false,
                            message: "Existe uma consulta realizada para esta CNH em " + ($filter('date')(new Date(r1.data.date), 'dd/MM/yyyy hh:mm:ss a')) + " O que deseja fazer?", buttons: [{
                                "label": "Realizar nova consulta",
                                "class": "success",
                                "callback": function () {
                                    if ($scope.fmSearch.$valid) {
                                        $('#loading').fadeIn(400, "linear");
                                        ConsultaCNHService.BuscarCNH(true, obj.UF, obj.Nome, obj.CPF, obj.NumRegistro, obj.NumRenach, obj.ValidadeCNH, obj.RG, obj.DataNascimento,
                                            obj.DataHabilitacao, obj.MunicipioNascimento, obj.NumCedulaEspelho).then(
                                            function successCallback(result) {
                                                $('#loading').fadeOut(400, "linear");
                                                //Check for error
                                                if (result.data.status == "0") {
                                                    $scope.FMsg = true;
                                                    $scope.TMsg = false;
                                                    $scope.ErrorMsg = "Oops! Um erro interno ocorreu, por favor contacte o administrador!";
                                                    return;
                                                }

                                                //Check error-response in xml
                                                if (result.data.status == "-1") {
                                                    $scope.FMsg = true;
                                                    $scope.TMsg = false;
                                                    $scope.ErrorMsg = result.data.result.CONSULTAS.CONSULTA.DESCRICAO;
                                                    return;
                                                }

                                                if (result.data.CONSULTAS.CONSULTA.RETORNOEXECUCAO == "000") {
                                                    $scope.ShowPdfButton = true;
                                                    $scope.ShowDriveButton = true;

                                                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.EXAMES.EXAME.length > 0) {

                                                        $scope.ShowExame = true;
                                                    }
                                                    else {
                                                        $scope.ShowExame = false;
                                                    }

                                                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.CURSOS.CURSO.length > 0) {

                                                        $scope.ShowCurso = true;
                                                    }
                                                    else {
                                                        $scope.ShowCurso = false;
                                                    }

                                                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.RECURSOS.RECURSO.length > 0) {

                                                        $scope.ShowRecurso = true;
                                                    }
                                                    else {
                                                        $scope.ShowRecurso = false;
                                                    }

                                                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.INFRACOES.INFRACAO.length > 0) {

                                                        $scope.ShowInfracao = true;
                                                    }
                                                    else {
                                                        $scope.ShowInfracao = false;
                                                    }

                                                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.BLOQUEIOS.BLOQUEIO.length > 0) {

                                                        $scope.ShowBloqueio = true;
                                                    }
                                                    else {
                                                        $scope.ShowBloqueio = false;
                                                    }

                                                    $scope.CondutorData = result.data.CONSULTAS.CONSULTA.RESULTADO.DADOSCONDUTOR;
                                                    $scope.CnhData = result.data.CONSULTAS.CONSULTA.RESULTADO.DADOSCNH;
                                                    $scope.ExamesData = result.data.CONSULTAS.CONSULTA.RESULTADO.EXAMES.EXAME;
                                                    $scope.CursosData = result.data.CONSULTAS.CONSULTA.RESULTADO.CURSOS.CURSO;
                                                    $scope.RecursosData = result.data.CONSULTAS.CONSULTA.RESULTADO.RECURSOS.RECURSO;
                                                    $scope.PontuacaoData = result.data.CONSULTAS.CONSULTA.RESULTADO.PONTUACAO;
                                                    $scope.InfracoesData = result.data.CONSULTAS.CONSULTA.RESULTADO.INFRACOES.INFRACAO;
                                                    $scope.BloqueiosData = result.data.CONSULTAS.CONSULTA.RESULTADO.BLOQUEIOS.BLOQUEIO;
                                                }
                                                else {
                                                    $scope.FMsg = true;
                                                    $scope.TMsg = false;
                                                    $scope.ErrorMsg = result.data.CONSULTAS.CONSULTA.DESCRICAO;
                                                }
                                            }, function errorCallback(response) {
                                                if (response.statusText == 'Unauthorized') {
                                                }
                                            });
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            }, {
                                "label": "Exibir mesma consulta",
                                "class": "danger",
                                "callback": function () {
                                    if ($scope.fmSearch.$valid) {
                                        $('#loading').fadeIn(400, "linear");
                                        ConsultaCNHService.BuscarCNH(false, obj.UF, obj.Nome, obj.CPF, obj.NumRegistro, obj.NumRenach, obj.ValidadeCNH, obj.RG, obj.DataNascimento,
                                            obj.DataHabilitacao, obj.MunicipioNascimento, obj.NumCedulaEspelho).then(
                                            function successCallback(result) {
                                                $('#loading').fadeOut(400, "linear");
                                                //Check for error
                                                if (result.data.status == "0") {
                                                    $scope.FMsg = true;
                                                    $scope.TMsg = false;
                                                    $scope.ErrorMsg = "Oops! Um erro interno ocorreu, por favor contacte o administrador!";
                                                    return;
                                                }

                                                if (result.data.status == "-1") {
                                                    $scope.FMsg = true;
                                                    $scope.TMsg = false;
                                                    $scope.ErrorMsg = result.data.result.CONSULTAS.CONSULTA.DESCRICAO;
                                                    return;
                                                }
                                                //Check success-response in xml
                                                if (result.data.CONSULTAS.CONSULTA.RETORNOEXECUCAO == "000") {

                                                    $scope.ShowPdfButton = true;
                                                    $scope.ShowDriveButton = true;

                                                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.EXAMES.EXAME.length > 0) {

                                                        $scope.ShowExame = true;
                                                    }
                                                    else {
                                                        $scope.ShowExame = false;
                                                    }

                                                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.CURSOS.CURSO.length > 0) {

                                                        $scope.ShowCurso = true;
                                                    }
                                                    else {
                                                        $scope.ShowCurso = false;
                                                    }

                                                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.RECURSOS.RECURSO.length > 0) {

                                                        $scope.ShowRecurso = true;
                                                    }
                                                    else {
                                                        $scope.ShowRecurso = false;
                                                    }

                                                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.INFRACOES.INFRACAO.length > 0) {

                                                        $scope.ShowInfracao = true;
                                                    }
                                                    else {
                                                        $scope.ShowInfracao = false;
                                                    }

                                                    if (result.data.CONSULTAS.CONSULTA.RESULTADO.BLOQUEIOS.BLOQUEIO.length > 0) {

                                                        $scope.ShowBloqueio = true;
                                                    }
                                                    else {
                                                        $scope.ShowBloqueio = false;
                                                    }

                                                    $scope.CondutorData = result.data.CONSULTAS.CONSULTA.RESULTADO.DADOSCONDUTOR;
                                                    $scope.CnhData = result.data.CONSULTAS.CONSULTA.RESULTADO.DADOSCNH;
                                                    $scope.ExamesData = result.data.CONSULTAS.CONSULTA.RESULTADO.EXAMES.EXAME;
                                                    $scope.CursosData = result.data.CONSULTAS.CONSULTA.RESULTADO.CURSOS.CURSO;
                                                    $scope.RecursosData = result.data.CONSULTAS.CONSULTA.RESULTADO.RECURSOS.RECURSO;
                                                    $scope.PontuacaoData = result.data.CONSULTAS.CONSULTA.RESULTADO.PONTUACAO;
                                                    $scope.InfracoesData = result.data.CONSULTAS.CONSULTA.RESULTADO.INFRACOES.INFRACAO;
                                                    $scope.BloqueiosData = result.data.CONSULTAS.CONSULTA.RESULTADO.BLOQUEIOS.BLOQUEIO;
                                                }
                                                else {
                                                    $scope.FMsg = true;
                                                    $scope.TMsg = false;
                                                    $scope.ErrorMsg = result.CONSULTAS.CONSULTA.DESCRICAO;
                                                }
                                            }, function errorCallback(response) {
                                                if (response.statusText == 'Unauthorized') {
                                                }
                                            });
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            }, {
                                "label": "Cancelar",
                                "class": "primary",
                                "callback": function () {

                                }
                            }]
                        });
                    }
                    else {
                        userConfirmedOrNot = true;
                        if ($scope.fmSearch.$valid) {
                            $('#loading').fadeIn(400, "linear");
                            ConsultaCNHService.BuscarCNH(userConfirmedOrNot, obj.UF, obj.Nome, obj.CPF, obj.NumRegistro, obj.NumRenach, obj.ValidadeCNH, obj.RG, obj.DataNascimento,
                                obj.DataHabilitacao, obj.MunicipioNascimento, obj.NumCedulaEspelho).then(
                                function successCallback(result) {
                                    $('#loading').fadeOut(400, "linear");
                                    //Check for error
                                    if (result.data.status == "0") {
                                        $scope.FMsg = true;
                                        $scope.TMsg = false;
                                        $scope.ErrorMsg = "Oops! Um erro interno ocorreu, por favor contacte o administrador!";
                                        return;
                                    }

                                    if (result.data.status == "-1") {
                                        $scope.FMsg = true;
                                        $scope.TMsg = false;
                                        $scope.ErrorMsg = result.data.result.CONSULTAS.CONSULTA.DESCRICAO;
                                        return;
                                    }

                                    //Check success-response in xml
                                    if (result.data.CONSULTAS.CONSULTA.RETORNOEXECUCAO == "000") {

                                        $scope.ShowPdfButton = true;
                                        $scope.ShowDriveButton = true;

                                        if (result.data.CONSULTAS.CONSULTA.RESULTADO.EXAMES.EXAME.length > 0) {

                                            $scope.ShowExame = true;
                                        }
                                        else {
                                            $scope.ShowExame = false;
                                        }

                                        if (result.data.CONSULTAS.CONSULTA.RESULTADO.CURSOS.CURSO.length > 0) {

                                            $scope.ShowCurso = true;
                                        }
                                        else {
                                            $scope.ShowCurso = false;
                                        }

                                        if (result.data.CONSULTAS.CONSULTA.RESULTADO.RECURSOS.RECURSO.length > 0) {

                                            $scope.ShowRecurso = true;
                                        }
                                        else {
                                            $scope.ShowRecurso = false;
                                        }

                                        if (result.data.CONSULTAS.CONSULTA.RESULTADO.INFRACOES.INFRACAO.length > 0) {

                                            $scope.ShowInfracao = true;
                                        }
                                        else {
                                            $scope.ShowInfracao = false;
                                        }

                                        if (result.data.CONSULTAS.CONSULTA.RESULTADO.BLOQUEIOS.BLOQUEIO.length > 0) {

                                            $scope.ShowBloqueio = true;
                                        }
                                        else {
                                            $scope.ShowBloqueio = false;
                                        }

                                        $scope.CondutorData = result.data.CONSULTAS.CONSULTA.RESULTADO.DADOSCONDUTOR;
                                        $scope.CnhData = result.data.CONSULTAS.CONSULTA.RESULTADO.DADOSCNH;
                                        $scope.ExamesData = result.data.CONSULTAS.CONSULTA.RESULTADO.EXAMES.EXAME;
                                        $scope.CursosData = result.data.CONSULTAS.CONSULTA.RESULTADO.CURSOS.CURSO;
                                        $scope.RecursosData = result.data.CONSULTAS.CONSULTA.RESULTADO.RECURSOS.RECURSO;
                                        $scope.PontuacaoData = result.data.CONSULTAS.CONSULTA.RESULTADO.PONTUACAO;
                                        $scope.InfracoesData = result.data.CONSULTAS.CONSULTA.RESULTADO.INFRACOES.INFRACAO;
                                        $scope.BloqueiosData = result.data.CONSULTAS.CONSULTA.RESULTADO.BLOQUEIOS.BLOQUEIO;
                                    }
                                    else {
                                        $scope.FMsg = true;
                                        $scope.TMsg = false;
                                        $scope.ErrorMsg = result.data.CONSULTAS.CONSULTA.DESCRICAO;
                                    }
                                }, function errorCallback(response) {
                                    if (response.statusText == 'Unauthorized') {
                                    }
                                });
                        }
                        else {
                            return false;
                        }
                    }
                })
        }
        else
            return false;

    }

    //Save To Google Drive
    $scope.UploadFilePath = "";
    $scope.UploadFileName = "";

    $scope.printToCart = function (printSectionId)
    {
        var innerContents = document.getElementById(printSectionId).innerHTML.replace('align = "right"', 'align = "left"');
        var popupWinindow = window.open('', '_blank', 'width=900,height=1200,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.title = 'Impressão de consulta CNH';
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.css">' +
            '<link rel="stylesheet" type="text/css" href="/assets/helpers/colors.css"> ' +
            '<link rel="stylesheet" type="text/css" href="/assets/elements/responsive-tables.css"> ' +
            '<link href="/css/print.css" rel="stylesheet" media="print" /> ' +
            '</head > <body onload="window.print()" > <div><h2 style="margin:10px; float:left;">Consulta de CNH</h2> <img style="margin:10px; float:right;" src="../assets/image-resources/logosgd.png" />  ' + innerContents + '</div></html>');

        if (!$scope.ShowCurso)
        {
            var node = popupWinindow.document.getElementById('ShowCurso');
            if (node != null) {
                node.remove();
            }
        }

        if (!$scope.ShowBloqueio)
        {
            var node = popupWinindow.document.getElementById('ShowBloqueio');
            if (node != null) {
                node.remove();
            }
        }

        if (!$scope.ShowExame)
        {
            var node = popupWinindow.document.getElementById('ShowExame');
            if (node != null) {
                node.remove();
            }
        }

        if (!$scope.ShowInfracao)
        {
            var node = popupWinindow.document.getElementById('ShowInfracao');
            if (node != null) {
                node.remove();
            }
        }

        if (!$scope.ShowRecurso)
        {
            var node = popupWinindow.document.getElementById('ShowRecurso');
            if (node != null) {
                node.remove();
            }
        }

        popupWinindow.document.close();
    }

    //Save To Google Drive
    $scope.renderSaveZipToDrive = function (type) {
        gapi.savetodrive.render('savetodrive-div' + type, {
            src: $scope.PdfFullFilePath,
            filename: $scope.PdfFileName,
            sitename: 'JConsultans'
        });

        $scope.ShowSaveButton = true;
    }

    $scope.renderSaveToDroupBox = function () {
        $scope.TMsg = false;
        $scope.FMsg = false;

        $scope.ShowSaveButton = true;
        $('#loading').fadeIn(400, "linear");
        UserService.UploadFileToDroupBox($scope.PdfFilePath, $scope.PdfFileName, null).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                if (result.data.Result) {
                    $scope.DTMsg = true;
                    $scope.DsuccessMsg = "Arquivo salvo com sucesso em Dropbox.";
                }
                else {
                    $scope.DFMsg = true;
                    $scope.DErrorMsg = result.data.Error;
                }
            }, function errorCallback(response) {
                $scope.DFMsg = true;
                $scope.DErrorMsg = "Ocorreu um erro não previsto";
            });
    }

    $scope.UploadToDrive = function () {
        $scope.ShowSaveButton = false;
        $scope.CreatePdf($scope.objSearch, false, true);
    }

    $scope.CreatePdf = function (obj, isdownload, isUpload) {
        if ($scope.PdfFileName != "")
        {
            if (isdownload)
                window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + $scope.PdfFileName
                                    + "&filePath=" + $scope.PdfFilePath; 

            if (isUpload) {
                if (getCookie("AccessToken") === undefined || getCookie("AccessToken") === null || getCookie("AccessToken") == "null" || getCookie("AccessToken") == "") {
                    $scope.renderSaveZipToDrive('S');
                }
                else {
                    $('#myModal').modal('show');
                }
            }
            return;
        }
        //If file not created on server
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            $('#loading').fadeIn(400, "linear");
            ConsultaCNHService.CreatePdf(obj.CPF).then(
                function successCallback(result) {

                    $('#loading').fadeOut(400, "linear");
                    if (result.data.status == "1") {
                        //$scope.ShowDriveButton = true;
                        $scope.PdfFileName = result.data.filename;
                        $scope.PdfFilePath = result.data.filepath;
                        $scope.PdfFullFilePath = result.data.fullFilePath;

                        if (isdownload)
                            window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + $scope.PdfFileName
                                + "&filePath=" + $scope.PdfFilePath; 

                        if (isUpload) {
                            if (getCookie("AccessToken") === undefined || getCookie("AccessToken") === null || getCookie("AccessToken") == "null" || getCookie("AccessToken") == "") {
                                $scope.renderSaveZipToDrive('S');
                            }
                            else {
                                $('#myModal').modal('show');
                            }
                            //renderSaveToDrive($scope.PdfFilePath, $scope.PdfFileName);
                        }
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;
                        $scope.ErrorMsg = result.data.message;
                        $scope.PdfFileName = "";
                        $scope.PdfFilePath = "";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }
        return false;
    }

    $scope.CreateExcel = function (obj, isdownload) {

        //If file already created on server
        if ($scope.ExcelFileName != "") {
            if (isdownload)
                window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + $scope.ExcelFileName + "&filePath=" + $scope.ExcelFilePath;

            if (!isdownload)
                renderSaveToDrive($scope.PdfFullFilePath, $scope.PdfFileName);
            return;
        }

        //If file not created on server
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            $('#loading').fadeIn(400, "linear");
            ConsultaCNHService.CreateExcel(obj.CPF).then(
                function successCallback(result) {

                    $('#loading').fadeOut(400, "linear");
                    if (result.data.status == "1") {
                        //$scope.ShowDriveButton = true;
                        $scope.ExcelFileName = result.data.filename;
                        //$scope.PdfFileName = result.data.filename;
                        $scope.ExcelFilePath = result.data.filepath;
                        $scope.ExcelFullFilePath = result.data.fullFilePath;

                        if (isdownload)
                            window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + result.data.filename + "&filePath=" + result.data.filepath;

                        if (!isdownload) {
                            renderSaveToDrive($scope.PdfFullFilePath, $scope.PdfFileName);
                        }
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;
                        $scope.ErrorMsg = result.data.Msg;
                        $scope.ExcelFileName = "";
                        $scope.ExcelFilePath = "";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }
        return false;
    }
}
]);

