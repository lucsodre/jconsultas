﻿angular.module('myApp.Controllers').controller('DocDetailPageCntrl', ['$scope', '$http', '$window', 'DocDetailPageService', 'UserService', function ($scope, $http, $window, DocDetailPageService, UserService) {

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    $scope.TMsg = false;
    $scope.FMsg = false;
    $scope.successMsg = '';
    $scope.ErrorMsg = '';
    $scope.ShowPdfButton = false;
    $scope.ShowDriveButton = false;
    $scope.ShowSaveButton = false;
    $scope.PdfFileName = "";
    $scope.parseFloat = parseFloat;
    $scope.ExcelFileName = "";
    $scope.ExcelFilePath = "";

    $scope.objSearch = { QId: "", DocumentCode: "" }
    $scope.init = function (qId, docId) {
        $scope.objSearch = { QId: qId, DocumentCode: docId };
        $scope.SearchDocument($scope.objSearch);
    }

    $scope.documentData = [];
    $scope.SearchDocument = function (obj) {
        //if (obj.DocumentCode.length != 7 && obj.DocumentCode.length != 17) {
        //    $scope.FMsg = true;
        //    $scope.TMsg = false;
        //    $scope.ErrorMsg = "Please,Enter 7 or 17 character document code !";
        //    return;
        //}

        $scope.ShowPdfButton = false;
        $scope.ShowDriveButton = false;
        $scope.ShowSaveButton = false;

        $scope.PdfFileName = "";
        $scope.searchsubmitted = true;
        $scope.TMsg = false;
        $scope.FMsg = false;
        //if ($scope.fmSearch.$valid) {
        $('#loading').fadeIn(400, "linear");
        DocDetailPageService.SearchDocument(obj.QId).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                //Check for error
                if (result.data.status == "0") {
                    $scope.FMsg = true;
                    $scope.TMsg = false;
                    $scope.ErrorMsg = "Ocorreu um erro interno, por favor contacte o suporte!";
                    return;
                }

                //Check error-response in xml
                if (result.data.status == "-1") {
                    $scope.FMsg = true;
                    $scope.TMsg = false;
                    $scope.ErrorMsg = result.data.result.CONSULTAS.CONSULTA.DESCRICAO;
                    return;
                }

                //Check success-response in xml
                if (result.data.CONSULTAS.CONSULTA.RETORNOEXECUCAO == "000") {

                    $scope.ShowPdfButton = true;
                    $scope.ShowDriveButton = true;

                    $scope.veiculoData = result.data.CONSULTAS.CONSULTA.VEICULO;
                    $scope.proprietarioData = result.data.CONSULTAS.CONSULTA.PROPRIETARIO;
                    $scope.gravameData = result.data.CONSULTAS.CONSULTA.GRAVAME;
                    $scope.gravamE_INTENCAOData = result.data.CONSULTAS.CONSULTA.GRAVAME_INTENCAO;
                    $scope.debitosData = result.data.CONSULTAS.CONSULTA.TOTAL_DEBITOS;
                    $scope.restricoesData = result.data.CONSULTAS.CONSULTA.RESTRICOES;
                    $scope.multasData = result.data.CONSULTAS.CONSULTA.MULTAS;
                    $scope.bloqueiosDetranData = result.data.CONSULTAS.CONSULTA.BLOQUEIOSDETRAN;
                    $scope.inspecaoVeicularData = result.data.CONSULTAS.CONSULTA.INSPECAO_VEICULAR;
                    $scope.comunicacaoVendaData = result.data.CONSULTAS.CONSULTA.COMUNICACAO_VENDA;
                    $scope.debitosIPVAData = result.data.CONSULTAS.CONSULTA.IPVAS;
                    $scope.debitosDPVATData = result.data.CONSULTAS.CONSULTA.DPVATS;

                    if (result.data.CONSULTAS.CONSULTA.CONSULTAAVANCADAVEICULO == null) {
                        $scope.ConsultaAvancadaData =
                            {
                                "PesquisaCompleta": true,
                                "DetalhamentoMultas": true,
                                "DetalhamentoDPVAT": true,
                                "DetalhamentoIPVA": true,
                                "DetalhamentoDebitos": true,
                                "BloqueiosDetran": true,
                                "InspecaoVeicular": true,
                                "Restricoes": true,
                                "IntencaoGravame": true,
                                "ArrendarioFinanceira": true,
                                "DadosProp": true,
                                "DadosVeic": true
                            };
                    }
                    else {
                        $scope.ConsultaAvancadaData = result.data.CONSULTAS.CONSULTA.CONSULTAAVANCADAVEICULO;
                    }

                    $scope.objConsultaAvancadaPesquisa =
                        {
                            "PesquisaCompleta": $scope.ConsultaAvancadaData.PesquisaCompleta,
                            "DetalhamentoMultas": $scope.ConsultaAvancadaData.DetalhamentoMultas,
                            "DetalhamentoDPVAT": $scope.ConsultaAvancadaData.DetalhamentoDPVAT,
                            "DetalhamentoIPVA": $scope.ConsultaAvancadaData.DetalhamentoIPVA,
                            "DetalhamentoDebitos": $scope.ConsultaAvancadaData.DetalhamentoDebitos,
                            "BloqueiosDetran": $scope.ConsultaAvancadaData.BloqueiosDetran,
                            "InspecaoVeicular": $scope.ConsultaAvancadaData.InspecaoVeicular,
                            "Restricoes": $scope.ConsultaAvancadaData.Restricoes,
                            "IntencaoGravame": $scope.ConsultaAvancadaData.IntencaoGravame,
                            "ArrendarioFinanceira": $scope.ConsultaAvancadaData.ArrendarioFinanceira,
                            "DadosProp": $scope.ConsultaAvancadaData.DadosProp,
                            "DadosVeic": $scope.ConsultaAvancadaData.DadosVeic
                        };

                    $scope.ShowOpcoesDaConsulta = true;
                    $scope.TotalDeDebitos = parseFloat($scope.debitosData.IPVA) + parseFloat($scope.debitosData.LICENCIAMENTO) + parseFloat($scope.debitosData.MULTAS) + parseFloat($scope.debitosData.DPVAT);
                }
                else {
                    $scope.FMsg = true;
                    $scope.TMsg = false;
                    $scope.ErrorMsg = "Oops ! Ocorreu um erro interno. Contacte o administrador!";
                }
            }, function errorCallback(response) {
                if (response.statusText == 'Unauthorized') {
                }
            });
        //}
        //else {
        //    return false;
        //}

        //return false;
    }

    //Save To Google Drive
    $scope.renderSaveZipToDrive = function (type) {
        gapi.savetodrive.render('savetodrive-div' + type, {
            src: $scope.PdfFullFilePath,
            filename: $scope.PdfFileName,
            sitename: 'JConsultans'
        });

        $scope.ShowSaveButton = true;
    }

    //Save To Google Drive
    $scope.renderSaveToDroupBox = function () {
        $scope.DTMsg = false;
        $scope.DFMsg = false;

        $scope.ShowSaveButton = true;
        $('#loading').fadeIn(400, "linear");
        UserService.UploadFileToDroupBox($scope.PdfFilePath, $scope.PdfFileName, null).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");
                if (result.data.Result) {
                    $scope.DTMsg = true;
                    $scope.DsuccessMsg = "Arquivo salvo com sucesso em Dropbox.";
                }
                else {
                    $scope.DFMsg = true;
                    $scope.DErrorMsg = result.data.Error;
                }
            }, function errorCallback(response) {
                $scope.DFMsg = true;
                $scope.DErrorMsg = "Ocorreu um erro.";
            });
    }

    
    $scope.UploadToDrive = function () {
        $scope.ShowSaveButton = false;
        $scope.CreatePdf($scope.objSearch, false, true);
    }

    $scope.CreatePdf = function (obj, isdownload, isUpload) {
        //If file already created on server
        if ($scope.PdfFileName != "") {
            if (isdownload)
                window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + $scope.PdfFileName + "&filePath=" + $scope.PdfFilePath;

            if (isUpload) {

                if (getCookie("AccessToken") === undefined || getCookie("AccessToken") === null || getCookie("AccessToken") == "null" || getCookie("AccessToken") == "") {
                    $scope.renderSaveZipToDrive('S');
                }
                else {
                    $('#myModal').modal('show');
                }
                //renderSaveToDrive($scope.PdfFilePath, $scope.PdfFileName);
            }
            return;
        }
        //If file not created on server
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            $('#loading').fadeIn(400, "linear");
            DocDetailPageService.CreatePdf(obj.QId).then(
                function successCallback(result) {

                    $('#loading').fadeOut(400, "linear");

                    //Check error-response in xml
                    if (result.data.status == "-1") {
                        $scope.FMsg = true;
                        $scope.TMsg = false;
                        $scope.ErrorMsg = result.data.result.descricao;
                        return;
                    }

                    if (result.data.status == "1") {
                        //$scope.ShowDriveButton = true;
                        $scope.PdfFileName = result.data.filename;
                        $scope.PdfFilePath = result.data.filepath;
                        $scope.PdfFullFilePath = result.data.fullFilePath;

                        if (isdownload)
                            window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + result.data.filename + "&filePath=" + result.data.filepath;

                        if (isUpload) {
                            if (getCookie("AccessToken") === undefined || getCookie("AccessToken") === null || getCookie("AccessToken") == "null" || getCookie("AccessToken") == "") {
                                $scope.renderSaveZipToDrive('S');
                            }
                            else {
                                $('#myModal').modal('show');
                            }
                            //renderSaveToDrive($scope.PdfFilePath, $scope.PdfFileName);
                        }
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;
                        $scope.ErrorMsg = result.data.message;
                        $scope.PdfFileName = "";
                        $scope.PdfFilePath = "";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }
        return false;
    }


    $scope.CreateExcel = function (obj, isdownload) {
        //If file already created on server
        if ($scope.ExcelFileName != "") {
            if (isdownload)
                window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + $scope.ExcelFileName + "&filePath=" + $scope.ExcelFilePath;

            if (!isdownload)
                renderSaveToDrive($scope.PdfFullFilePath, $scope.PdfFileName);
            return;
        }

        //If file not created on server
        $scope.searchsubmitted = true;
        if ($scope.fmSearch.$valid) {
            $('#loading').fadeIn(400, "linear");
            DocDetailPageService.CreateExcel(obj.QId).then(
                function successCallback(result) {
                    $('#loading').fadeOut(400, "linear");
                    if (result.data.status == "1") {
                        //$scope.ShowDriveButton = true;
                        $scope.ExcelFileName = result.data.filename;
                        //$scope.PdfFileName = result.data.filename;
                        $scope.ExcelFilePath = result.data.filepath;
                        $scope.ExcelFullFilePath = result.data.fullFilePath;

                        if (isdownload)
                            window.location.href = "/home/SingleSearchPdfDownload?fileDownloadName=" + result.data.filename + "&filePath=" + result.data.filepath;

                        if (!isdownload) {
                            renderSaveToDrive($scope.PdfFullFilePath, $scope.PdfFileName);
                        }
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;
                        $scope.ErrorMsg = result.data.message;
                        $scope.ExcelFileName = "";
                        $scope.ExcelFilePath = "";
                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }
        return false;
    }

}
]);

