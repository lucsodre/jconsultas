﻿angular.module('myApp.Controllers').controller('MultipleDocSearchCntrl', ['$scope', '$filter', '$http', 'MultipleDocSearchService', function ($scope, $filter, $http, MultipleDocSearchService) {

    $scope.objSearch = {
        DocumentCodes: ""
    };

    //$scope.DocumentCodes = [];
    //$scope.$watch('objSearch.DocumentCode', function (newValue) {
    //    var str = newValue.replace(/(?:\r\n|\r|\n)/g, ',');
    //    var codeArr = str.split(',');
    //    var dif = _.difference(codeArr, $scope.DocumentCodes);
    //    $scope.DocumentCodes = $scope.DocumentCodes.concat(dif);
    //})

    $scope.objConsultaAvancada = null;

    $scope.SearchDocument = function (obj) {
      
        $scope.TMsg = false;
        $scope.FMsg = false;
        $scope.IMsg = false;

        $scope.searchsubmitted = true;
        if (obj.DocumentCodes) {
            // var objSend = { DocumentCodes: obj.DocumentCodes.replace(/(?:\r\n|\r|\n)/g, ',').split(',') };
            var objSend = { DocumentCodes: obj.DocumentCodes };

            if ($scope.fmSearch.$valid) {
                $('#loading').fadeIn(400, "linear");
                MultipleDocSearchService.GetMultipleDocSearch(objSend).then(
                    function successCallback(result) {
                        $('#loading').fadeOut(400, "linear");
                        if (result.data) {

                            $scope.FMsg = false;
                            $scope.TMsg = true;

                            $scope.successMsg = "Salvo com sucesso.";

                            window.location.href = '/Home/ListOfMultipleSearch';
                            return;
                            $scope.FMsg = false;
                            $scope.TMsg = true;

                            $scope.successMsg = "Salvo com sucesso.";
                            $scope.objSearch.DocumentCodes = "";


                            $scope.searchsubmitted = false;
                            $scope.fmSearch.$setPristine();

                            $scope.$apply();


                        }
                        else {
                            $scope.FMsg = true;
                            $scope.TMsg = false;

                            $scope.ErrorMsg = "Ocorreu um erro";
                        }
                    }, function errorCallback(response) {
                        if (response.statusText == 'Unauthorized') {
                        }
                    });
            }
            else {
                return false;
            }

            return false;
        }
    }

    $scope.PesquisaCompleta = function (obj) {
        if (obj) {
            $scope.objConsultaAvancada.DetalhamentoMultas = true;
            $scope.objConsultaAvancada.DetalhamentoDPVAT = true;
            $scope.objConsultaAvancada.DetalhamentoIPVA = true;
            $scope.objConsultaAvancada.DetalhamentoDebitos = true;
            $scope.objConsultaAvancada.BloqueiosDetran = true;
            $scope.objConsultaAvancada.InspecaoVeicular = true;
            $scope.objConsultaAvancada.Restricoes = true;
            $scope.objConsultaAvancada.IntencaoGravame = true;
            $scope.objConsultaAvancada.ArrendarioFinanceira = true;
            $scope.objConsultaAvancada.DadosProp = true;
            $scope.objConsultaAvancada.DadosVeic = true;
            $scope.$apply();
        }
    }

    $scope.SalvarParametros = function (obj) {
        if (obj === null) {
            return false;
        }

        MultipleDocSearchService.SalvarParametros(obj).then(
            function successCallback(result) {
                alert("Os parâmetros da consulta foram salvos.");
                $('.ConsultaAvancada').modal('hide');
            },
            function errorCallback(response) {
            });
    }

    $scope.BuscaParametros = function () {
        MultipleDocSearchService.BuscaParametros().then(
            function successCallback(result) {

                if (result.data == null || result.data == "") {
                    result.data =
                        {
                            "DetalhamentoMultas": true,
                            "DetalhamentoDPVAT": true,
                            "DetalhamentoIPVA": true,
                            "DetalhamentoDebitos": true,
                            "BloqueiosDetran": true,
                            "InspecaoVeicular": true,
                            "Restricoes": true,
                            "IntencaoGravame": true,
                            "ArrendarioFinanceira": true,
                            "DadosProp": true,
                            "DadosVeic": true
                        };
                }

                $scope.objConsultaAvancada = result.data;
                $scope.$apply();
            },
            function errorCallback(response) {
            });
    }

    $scope.objImport = { ExcelFile: '' };
    $('INPUT[type="file"]').change(function (e) {

        var fileExtension = ['xlsx', 'xls'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {

            $scope.TMsg = false;
            $scope.FMsg = false;

            $scope.IMsg = true;
            $scope.InfoMsg = "Extensão de arquivos permitidas: " + fileExtension.join(', ')

            $scope.Excelsubmitted = false;
            $scope.fmImportExcel.$setPristine();

            $('.form-control').val('');
            $scope.objImport.ExcelFile = '';

            $("html, body").animate({ scrollTop: 100 }, "slow");

            $scope.$apply();
        }
        else {

            $scope.IMsg = false;
            $scope.InfoMsg = null;

            $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
            $scope.objImport.ExcelFile = $("#ExcelFile").val();
        }
    });

    $scope.ImportData = null;
    $scope.ImpoerExcel = null;
    $scope.ImportExcelFile = function (obj) {

        $scope.TMsg = false;
        $scope.FMsg = false;
        $scope.IMsg = false;

        $scope.Excelsubmitted = true;

        if ($scope.objImport.ExcelFile != '') {
            //if ($scope.fmImportExcel.$valid) {

            var fileUpload = $("#ExcelFile").get(0);
            var files = fileUpload.files;
            var data = new FormData();
            data.append(fileUpload, files[0]);

            $('#loading').fadeIn(400, "linear");
            $.ajax({
                type: "POST",
                url: "/home/ImportExcel",
                contentType: false,
                processData: false,
                data: data,
                success: function (message) {
                    $('#loading').fadeOut(400, "linear");

                    try {
                        $("#dtImportData").dataTable().fnDestroy();
                    } catch (e) { }

                    if (message.Status) {
                        $scope.ImpoerExcel = message;
                        $scope.ImportData = message.ImportChildData;

                        var table = $('#dtImportData').DataTable({
                            destroy: true,
                            "aaData": $scope.ImportData,
                            responsive: true,
                            "aoColumns": [
                                { "data": "placa", "sWidth": "100px", "responsivePriority": 1, "bSortable": false },
                                { "data": "chassi", "sWidth": "100px", "responsivePriority": 2, "bSortable": false },
                            ]
                        });

                        $scope.FMsg = false;
                        $scope.ErrorMsg = '';

                        $scope.$apply();
                    }
                    else {
                        try {
                            var table = $('#dtImportData').DataTable();
                            table.clear().draw();
                        } catch (e) { }

                        $("html, body").animate({ scrollTop: 100 }, "slow");
                        $scope.FMsg = true;
                        $scope.ErrorMsg = message.Msg;
                        $scope.$apply();
                    }
                },
                error: function () {
                    alert("Ocorreu um erro ao enviar arquivos!");
                }
            });

        }

    }

    $scope.SaveExcelData = function () {
        $('#loading').fadeIn(400, "linear");
        MultipleDocSearchService.SaveExceldata($scope.ImpoerExcel).then(
            function successCallback(result) {
                $('#loading').fadeOut(400, "linear");

                if (result.data) {

                    $scope.TMsg = true;
                    $scope.successMsg = "Registro salvo com sucesso";

                    window.location.href = '/Home/ListOfMultipleSearch';
                    return;
                    $("html, body").animate({ scrollTop: 100 }, "slow");

                    $scope.TMsg = true;
                    $scope.successMsg = "Registro salvo com sucesso";

                    $('.form-control').val('');

                    $scope.objImport = { ExcelFile: '' };
                    $("#ExcelFile").val('');
                    $scope.ImportData = null;
                    $scope.ImpoerExcel = null;


                    $scope.Excelsubmitted = false;
                    $scope.fmImportExcel.$setPristine();

                    $scope.$apply();
                }

            }, function errorCallback(response) {
                if (response.statusText == 'Unauthorized') {
                }
            });

    }

}
]);

