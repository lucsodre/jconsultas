﻿angular.module('myApp.Controllers').controller('AccountCntrl', ['$scope', '$rootScope', '$http', 'AccountService', function ($scope, $rootScope, $http, AccountService) {

    $scope.TMsg = false;
    $scope.FMsg = false;
    $scope.successMsg = '';
    $scope.ErrorMsg = '';
    $scope.ShowAuditoria = true;

    $scope.objAccount = { AccountID: 0 };
    $scope.GetAccountList = function () {

        $('#loading').fadeIn(400, "linear");
        AccountService.GetAccountList().then(
            function successCallback(result) {
                if (result.data.length > 0) {

                    $('#loading').fadeOut(400, "linear");
                    try {
                        $("#dtAccount").dataTable().fnDestroy();
                    } catch (e) {

                    }
                    var table = $('#dtAccount').DataTable({
                        "aaData": result.data,
                        responsive: true,
                        "aoColumns": [
                            { "data": "AccountName", "sWidth": "100px", "responsivePriority": 1 },
                            { "data": "ApiCode", "sWidth": "100px", "responsivePriority": 1 },
                            {
                                "data": "AccountID",
                                "mRender": function (data, type, full) {
                                    var d = "<div class='btn-group btn-group-xs'><a data-toggle='tooltip' title='' data-original-title='Edit' class='btn btn-default' href='javascript:void(0)' onclick='EditAccount(" + JSON.stringify(full) + ")' ><i class='fa fa-edit'> </i> &nbsp; Editar</a> </div>";
                                    return d;
                                },
                                "sWidth": "10px"
                                , "responsivePriority": 2
                                , "bSortable": false
                            }
                        ]
                    });
                }
                else {
                    var table = $('#dtAccount').DataTable();
                    table.clear().draw();
                }

            }, function errorCallback(response) {
            });
    }

    $scope.SaveAccount = function (obj) {
        //$.ajax({
        //    url: 'http://localhost:51506/api/ConsultaVeiculo/BXM2860',
        //    type: 'GET',
        //    dataType: 'json',
        //    headers: {
        //        'Usuario': 'jconsultasadmin@gmail.com11',
        //        'Senha': 'jconsultas@123'
        //    },
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (result) {
        //        console.log(result);
        //    }, error: function (error) {
        //        console.log(error);
        //    }
        //});

        $scope.accountsubmitted = true;

        if ($scope.fmaccount.$valid) {
            $('#loading').fadeIn(400, "linear");
            AccountService.AddUpdateAccount(obj).then(
                function successCallback(result) {
                    $('#loading').fadeOut(400, "linear");
                    if (result.data) {
                        $scope.TMsg = true;
                        $scope.FMsg = false;

                        if (obj.AccountID == 0) {
                            $scope.successMsg = 'Conta salva com sucesso';
                        }
                        else
                            $scope.successMsg = 'Conta atualizada com sucesso';

                        $scope.ClearAccount();
                        $('.AccountModel').modal('hide');
                        $scope.GetAccountList();
                    }
                    else {
                        $scope.FMsg = true;
                        $scope.TMsg = false;

                        if (obj.AccountID == 0) {
                            $scope.successMsg = 'Erro ao salvar conta';
                        }
                        else
                            $scope.successMsg = 'Erro ao atualizar conta';

                    }
                }, function errorCallback(response) {
                    if (response.statusText == 'Unauthorized') {
                    }
                });
        }
        else {
            return false;
        }

        return false;
    }

    $scope.EditAccount = function (obj) {
        $('.AccountModel').modal('show');
        $scope.objAccount = obj;// { AccountID: obj.accountId, AccountName: obj.accountName, IsActive: obj.isActive, ApiCode: obj.apiCode, ModifiedBy: obj.modifiedBy, ModifiedAt: obj.modifiedAt, CreatedBy: obj.createdBy, CreatedAt: obj.createdAt }
        $scope.AuditoriaData = obj.Auditorias;
        $scope.$apply();
    }

    $('.AccountModel').on('hidden.bs.modal', function () {
        $scope.ClearAccount();
    })

    $scope.ClearAccount = function () {
        $scope.objAccount = { AccountID: 0 };
        $scope.AuditoriaData = null;
        $scope.accountsubmitted = false;
        $scope.fmaccount.$setPristine();
        $scope.$apply();
    }

}
]);

