﻿angular.module('myApp.Controllers').controller('UploadDocumentCntrl', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {

    $scope.TMsg = false;
    $scope.FMsg = false;
    $scope.ErrorMsg = '';

    /******************** GLOBAL VARIABLES ********************/
    var SCOPES = ['https://www.googleapis.com/auth/drive', 'profile'];
    var CLIENT_ID = '60237038968-9i6j8p9bjau33scdu5i2fp1k0hgc892p.apps.googleusercontent.com';
    var FOLDER_NAME = "";
    var FOLDER_ID = "root";
    var FOLDER_PERMISSION = true;
    var FOLDER_LEVEL = 0;
    var NO_OF_FILES = 1000;
    var DRIVE_FILES = [];
    var FILE_COUNTER = 0;
    var FOLDER_ARRAY = [];

    $scope.checkAuth = function () {
        gapi.auth.authorize({
            'client_id': CLIENT_ID,
            'scope': SCOPES.join(' '),
            'immediate': true
        }, $scope.handleAuthClick());
    }

    //authorize apps
    $scope.handleAuthClick = function (event) {
        gapi.auth.authorize(
            { client_id: CLIENT_ID, scope: SCOPES, immediate: false },
            $scope.handleAuthResult);
        return false;
    }

    //check the return authentication of the login is successful, we display the drive box and hide the login box.
    $scope.handleAuthResult = function (authResult) {
        if (authResult && !authResult.error) {
            $('#loading').fadeIn(400, "linear");

            var uploadObj = $("[id$=fUpload]");
            var file = uploadObj.prop("files")[0];
            var metadata = {
                'title': file.name,
                'description': "File Upload",
                'mimeType': file.type || 'application/octet-stream',
                "parents": [{
                    "kind": "drive#file",
                    "id": FOLDER_ID
                }]
            };
            var arrayBufferView = new Uint8Array(file);
            var uploadData = new Blob(arrayBufferView, { type: file.type || 'application/octet-stream' });

            try {
                var uploader = new MediaUploader({
                    file: file,
                    token: gapi.auth.getToken().access_token,
                    metadata: metadata,
                    onError: function (response) {
                        $('#loading').fadeOut(400, "linear");

                        var errorResponse = JSON.parse(response);
                        $scope.FMsg = true;
                        $scope.TMsg = false;

                        $scope.ErrorMsg = errorResponse.error.message;
                        $("#fUpload").val("");
                    },
                    onComplete: function (response) {
                        $('#loading').fadeOut(400, "linear");

                        var errorResponse = JSON.parse(response);
                        if (errorResponse.message != null) {
                            $scope.FMsg = true;
                            $scope.TMsg = false;

                            $scope.ErrorMsg = errorResponse.error.message;
                            $("#fUpload").val("");
                        } else {

                            $scope.TMsg = true;
                            $scope.FMsg = false;

                            $scope.successMsg = 'Arquivo enviado com sucesso';
                        }
                    },
                    onProgress: function (event) {
                        //showProgressPercentage(Math.round(((event.loaded / event.total) * 100), 0));
                    },
                    params: {
                        convert: false,
                        ocr: false
                    }
                });
                uploader.upload();
            } catch (exc) {
                $('#loading').fadeOut(400, "linear");
                $scope.FMsg = true;
                $scope.TMsg = false;

                $scope.ErrorMsg = exc;
                $("#fUpload").val("");
            }
        } else {
            $scope.handleAuthClick();
        }
    }

    $scope.UploadToDrive = function () {
        if ($("#fUpload").val() == "") {
            $scope.TMsg = true;
            $scope.FMsg = false;

            $scope.successMsg = 'É necessário selecionar um arquivo.';
            return;
        }

        gapi.auth.authorize({
            'client_id': CLIENT_ID,
            'scope': SCOPES.join(' '),
            'immediate': true
        }, $scope.handleAuthResult());
    }

    $("#oad").bind("change", function () {
        var uploadObj = $("[id$=fUpload]");

        var file = uploadObj.prop("files")[0];
        var metadata = {
            'title': file.name,
            'description': "bytutorial.com File Upload",
            'mimeType': file.type || 'application/octet-stream',
            "parents": [{
                "kind": "drive#file",
                "id": FOLDER_ID
            }]
        };
        var arrayBufferView = new Uint8Array(file);
        var uploadData = new Blob(arrayBufferView, { type: file.type || 'application/octet-stream' });
       
        try {
            var uploader = new MediaUploader({
                file: file,
                token: gapi.auth.getToken().access_token,
                metadata: metadata,
                onError: function (response) {
                    var errorResponse = JSON.parse(response);
                    showErrorMessage("Error: " + errorResponse.error.message);
                    $("#fUpload").val("");
                    $("#upload-percentage").hide(1000);
                    getDriveFiles();
                },
                onComplete: function (response) {
                    
                    hideStatus();
                    $("#upload-percentage").hide(1000);
                    var errorResponse = JSON.parse(response);
                    if (errorResponse.message != null) {
                        showErrorMessage("Error: " + errorResponse.error.message);
                        $("#fUpload").val("");
                        getDriveFiles();
                    } else {
                        showStatus("Carregando arquivos do Google Drive...");
                        getDriveFiles();
                    }
                },
                onProgress: function (event) {
                    //showProgressPercentage(Math.round(((event.loaded / event.total) * 100), 0));
                },
                params: {
                    convert: false,
                    ocr: false
                }
            });
            uploader.upload();
        } catch (exc) {
            ssshowErrorMessage("Error: " + exc);
            $("#fUpload").val("");
            getDriveFiles();
        }

    });

}
]);

