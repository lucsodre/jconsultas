﻿angular.module("myApp.Services").service('DetalheConsultaMotoristaService', ["$http", function ($http) {
    var list = {};

    list.SearchDocument = function (QId) {
        return $http({
            method: "GET",
            url: "/DetalheConsultaMotorista/GetSigleSearch?QId=" + QId,
            contentType: "application/json"
        });
    };

    list.CreatePdf = function (QId) {
        return $http({
            method: "GET",
            url: "/DetalheConsultaMotorista/CreatePdf?QId=" + QId,
            contentType: "application/json"
        });
    };

    list.DeletePdf = function (filepath) {
        return $http({
            method: "GET",
            url: "/DetalheConsultaMotorista/DeletePdf?filepath=" + filepath,
            contentType: "application/json"
        });
    };

    list.CreateExcel = function (QId) {
        return $http({
            method: "GET",
            url: "/DetalheConsultaMotorista/CreateExcel?QId=" + QId,
            contentType: "application/json"
        });
    };

    return list;
}]);
