﻿angular.module("myApp.Services").service('SingleSearchService', ["$http", function ($http) {
    var list = {};

    list.CheckExistsInDatabase = function (code) {
        return $http({
            method: "GET",
            url: "/SingleSearch/CheckExistsInDatabase?sDocumentCode=" + code,
            contentType: "application/json"
        });
    };

    list.SearchDocument = function (code, userConfirm) {
        return $http({
            method: "GET",
            url: "/SingleSearch/GetSigleSearch?sDocumentCode=" + code + "&userConfirm=" + userConfirm,
            contentType: "application/json"
        });
    };

    list.SalvarParametros = function (data) {
        return $http({
            method: "POST",
            url: "/SingleSearch/SalvarParametros",
            data: data
        });
    };


    list.BuscaParametros = function () {
        return $http({
            method: "GET",
            url: "/SingleSearch/ObterParametros",
            contentType: "application/json"
        });
    };    


    list.CreatePdf = function (code) {
        return $http({
            method: "GET",
            url: "/SingleSearch/CreatePdf?sDocumentCode=" + code,
            contentType: "application/json"
        });
    };

    list.UploadPDfToDrive = function (pdfname) {
        return $http({
            method: "GET",
            url: "/SingleSearch/UploadPDfToDrive?pdfname=" + pdfname,
            contentType: "application/json"
        });
    };

    list.CreateExcel = function (code) {
        return $http({
            method: "GET",
            url: "/SingleSearch/CreateExcel?sDocumentCode=" + code,
            contentType: "application/json"
        });
    };



    return list;
}]);
