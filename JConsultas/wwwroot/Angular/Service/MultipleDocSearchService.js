﻿angular.module("myApp.Services").service('MultipleDocSearchService', ["$http", function ($http) {
    var list = {};

    list.GetMultipleDocSearch = function (data) {
        return $http({
            method: "POST",
            url: "/MultipleDocSearch/GetMultipleDocSearch",
            data: data
        });
    };

    list.SaveExceldata = function (data) {
        return $http({
            method: "POST",
            url: "/MultipleDocSearch/SaveExcelData",
            data: data
        });
    };

    list.SalvarParametros = function (data) {
        return $http({
            method: "POST",
            url: "/SingleSearch/SalvarParametros",
            data: data
        });
    };


    list.BuscaParametros = function () {
        return $http({
            method: "GET",
            url: "/SingleSearch/ObterParametros",
            contentType: "application/json"
        });
    };   

    return list;
}]);
