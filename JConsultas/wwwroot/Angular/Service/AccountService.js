﻿angular.module("myApp.Services").service('AccountService', ["$http", function ($http) {
    var list = {};
    
    list.GetAccountList = function () {
        return $http({
            method: "GET",
            url: "/AccountMaster/GetAccountList",
            contentType: "application/json"
        });
    };

    list.GetActiveAccounts = function () {
        return $http({
            method: "GET",
            url: "/AccountMaster/GetActiveAccountList",
            contentType: "application/json"
        });
    };

    list.AddUpdateAccount = function (data) {
        return $http({
            method: "POST",
            url: "/AccountMaster/AddUpdateAccount",
            data: data
        });
    };

    list.DeleteAccount = function (id) {
        return $http({
            method: "DELETE",
            url: "/AccountMaster/DeleteAccount?Id=" + id,
            contentType: "application/json"
        });
    };

    return list;

}]);
