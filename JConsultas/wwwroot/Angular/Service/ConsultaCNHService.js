﻿angular.module("myApp.Services").service('ConsultaCNHService', ["$http", function ($http) {
    var list = {};

    list.VerificaExisteBuscaBD = function (UF, Nome, CPF, NumRegistro, NumRenach, ValidadeCNH,
        RG, DataNascimento, DataHabilitacao, MunicipioNascimento, NumCedulaEspelho) {
        return $http({
            method: "GET",
            url: "/ConsultaCNH/VerificaConsultaCNH?UF=" + UF
            + "&Nome=" + Nome
            + "&CPF=" + CPF
            + "&NumRegistro=" + NumRegistro
            + "&NumRenach=" + NumRenach
            + "&ValidadeCNH=" + ValidadeCNH
            + "&RG=" + RG
            + "&DataNascimento=" + DataNascimento
            + "&DataHabilitacao=" + DataHabilitacao
            + "&MunicipioNascimento=" + MunicipioNascimento
            + "&NumCedulaEspelho=" + NumCedulaEspelho,
            contentType: "application/json"
        });
    };

    list.BuscarCNH = function (userConfirm, UF, Nome, CPF, NumRegistro, NumRenach, ValidadeCNH,
        RG, DataNascimento, DataHabilitacao, MunicipioNascimento, NumCedulaEspelho) {

        return $http({
            method: "GET",
            url: "/ConsultaCNH/BuscaUnicaCNH?userConfirm=" + userConfirm 
            + "&UF=" + UF
            + "&Nome=" + Nome
            + "&CPF=" + CPF
            + "&NumRegistro=" + NumRegistro
            + "&NumRenach=" + NumRenach
            + "&ValidadeCNH=" + ValidadeCNH
            + "&RG=" + RG
            + "&DataNascimento=" + DataNascimento
            + "&DataHabilitacao=" + DataHabilitacao
            + "&MunicipioNascimento=" + MunicipioNascimento
            + "&NumCedulaEspelho=" + NumCedulaEspelho,
            contentType: "application/json"
        });
    };

    list.CreatePdf = function (CPF) {
        return $http({
            method: "GET",
            url: "/ConsultaCNH/CreatePdf?CPF=" + CPF,
            contentType: "application/json"
        });
    };

    list.UploadPDfToDrive = function (pdfname) {
        return $http({
            method: "GET",
            url: "/ConsultaCNH/UploadPDfToDrive?pdfname=" + pdfname,
            contentType: "application/json"
        });
    };

    list.CreateExcel = function (CPF) {
        return $http({
            method: "GET",
            url: "/ConsultaCNH/CreateExcel?CPF=" + CPF,
            contentType: "application/json"
        });
    };



    return list;
}]);