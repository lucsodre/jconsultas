﻿angular.module("myApp.Services").service('DocDetailPageService', ["$http", function ($http) {
    var list = {};

    list.SearchDocument = function (QId) {
        return $http({
            method: "GET",
            url: "/DocDetailPage/GetSigleSearch?QId=" + QId,
            contentType: "application/json"
        });
    };

    list.CreatePdf = function (QId) {
        return $http({
            method: "GET",
            url: "/DocDetailPage/CreatePdf?QId=" + QId,
            contentType: "application/json"
        });
    };

    list.DeletePdf = function (filepath) {
        return $http({
            method: "GET",
            url: "/SingleSearch/DeletePdf?filepath=" + filepath,
            contentType: "application/json"
        });
    };

    list.CreateExcel = function (QId) {
        return $http({
            method: "GET",
            url: "/DocDetailPage/CreateExcel?QId=" + QId,
            contentType: "application/json"
        });
    };

    return list;
}]);
