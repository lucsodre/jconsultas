﻿angular.module("myApp.Services").service('BuscaEmLoteCNHService', ["$http", function ($http) {
    var list = {};

    list.SaveExceldata = function (data) {
        return $http({
            method: "POST",
            url: "/BuscaEmLoteCNH/SaveExcelData",
            data: data
        });
    };

    return list;
}]);
