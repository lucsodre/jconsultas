﻿angular.module("myApp.Services").service('HomeService', ["$http", function ($http) {
    var list = {};

    list.GetCurrentUserDetails = function () {
        return $http({
            method: "GET",
            url: "/User/GetUserLoginUserDetails",
            contentType: "application/json"
        });
    };

    list.Test = function () {
        //url: "http://localhost:51506/api/ConsultaVeiculo",
        
        return $http({
            method: "GET",
            headers: {
                'id': 'a3b0352e-24b5-460e-b251-7126478149be'
            },
            url: "/api/ConsultaVeiculo/BXM2860",
            contentType: "application/json"
        });
    }


    return list;

}]);
