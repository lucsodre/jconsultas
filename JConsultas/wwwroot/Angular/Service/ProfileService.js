﻿angular.module("myApp.Services").service('ProfileService', ["$http", function ($http) {
    var list = {};

    list.GetUserProfileList = function () {
        return $http({
            method: "GET",
            url: "/Profile/GetUserProfileList",
            contentType: "application/json"
        });
    };

    list.AddUpdateUserProfile = function (data) {
        return $http({
            method: "POST",
            url: "/Profile/AddUpdateUserProfile",
            data: data
        });
    };

    return list;

}]);
