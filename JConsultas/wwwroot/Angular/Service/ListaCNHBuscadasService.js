﻿angular.module("myApp.Services").service('ListaCNHBuscadasService', ["$http", function ($http) {
    var list = {};

    list.GetUserList = function (code) {
        return $http({
            method: "GET",
            url: "/ListaCNHConsultadas/GetUserList",
            contentType: "application/json"
        });
    };

    list.GetListOfSearchedDocs = function (data) {
        return $http({
            method: "POST",
            url: "/ListaCNHConsultadas/GetListOfSearchedDocs",
            data: data
        });
    };

    list.GetListOfMultipleSearchedDocs = function (data) {
        return $http({
            method: "POST",
            url: "/ListaCNHConsultadas/GetListOfMultipleSearchedDocs",
            data: data
        });
    };


    list.GetListOfMultipleSearchedPendingDocs = function (data) {
        return $http({
            method: "POST",
            url: "/ListaCNHConsultadas/GetListOfMultipleSearchedPendingDocs",
            data: data
        });
    };

    list.GetLastSearchedDocs = function () {
        return $http({
            method: "POST",
            url: "/ListaCNHConsultadas/GetLastSearchedDocs"
        });
    };

    list.GetDataOnMultipleData = function (data) {
        return $http({
            method: "POST",
            url: "/ListaCNHConsultadas/GetDataOnMultipleData",
            data: data
        });
    };

    list.CreatePdf = function (QId) {
        return $http({
            method: "GET",
            url: "/DetalheConsultaMotorista/CreatePdf?QId=" + QId,
            contentType: "application/json"
        });
    };

    list.CreateExcel = function (QId) {
        return $http({
            method: "GET",
            url: "/DetalheConsultaMotorista/CreateExcel?QId=" + QId,
            contentType: "application/json"
        });
    };

    list.GetMultipleFile = function (data) {
        return $http({
            method: "POST",
            url: "/ListaCNHConsultadas/GetMultipleFile",
            data: data
        });
    };

    list.GetMultipleExcelFile = function (data) {
        return $http({
            method: "POST",
            url: "/ListaCNHConsultadas/GetMultipleExcelFile",
            data: data
        });
    };

    list.Retry = function (id) {
        return $http({
            method: "POST",
            url: "/BuscaEmLoteCNH/RetryProcess?Id=" + id,
            contentType: "application/json"
        });
    };


    list.ExportExcel = function (QId) {
        return $http({
            method: "POST",
            url: "/BuscaEmLoteCNH/ExportExcel?QId=" + QId,
            contentType: "application/json"
        });
    };



    return list;
}]);
