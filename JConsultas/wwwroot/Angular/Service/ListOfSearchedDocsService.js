﻿angular.module("myApp.Services").service('ListOfSearchedDocsService', ["$http", function ($http) {
    var list = {};

    list.GetUserList = function (code) {
        return $http({
            method: "GET",
            url: "/ListOfSearchedDocs/GetUserList",
            contentType: "application/json"
        });
    };

    list.GetListOfSearchedDocs = function (data) {
        return $http({
            method: "POST",
            url: "/ListOfSearchedDocs/GetListOfSearchedDocs",
            data: data
        });
    };

    list.GetListOfMultipleSearchedDocs = function (data) {
        return $http({
            method: "POST",
            url: "/ListOfSearchedDocs/GetListOfMultipleSearchedDocs",
            data: data
        });
    };


    list.GetListOfMultipleSearchedPendingDocs = function (data) {
        return $http({
            method: "POST",
            url: "/ListOfSearchedDocs/GetListOfMultipleSearchedPendingDocs",
            data: data
        });
    };

    list.GetLastSearchedDocs = function () {
        return $http({
            method: "POST",
            url: "/ListOfSearchedDocs/GetLastSearchedDocs"
        });
    };

    list.GetDataOnMultipleData = function (data) {
        return $http({
            method: "POST",
            url: "/ListOfSearchedDocs/GetDataOnMultipleData",
            data: data
        });
    };

    list.CreatePdf = function (QId) {
        return $http({
            method: "GET",
            url: "/DocDetailPage/CreatePdf?QId=" + QId,
            contentType: "application/json"
        });
    };

    list.CreateExcel = function (QId) {
        return $http({
            method: "GET",
            url: "/DocDetailPage/CreateExcel?QId=" + QId,
            contentType: "application/json"
        });
    };

    list.GetMultipleFile = function (data) {
        return $http({
            method: "POST",
            url: "/ListOfSearchedDocs/GetMultipleFile",
            data: data
        });
    };

    list.GetMultipleExcelFile = function (data) {
        return $http({
            method: "POST",
            url: "/ListOfSearchedDocs/GetMultipleExcelFile",
            data: data
        });
    };

    list.Retry = function (id) {
        return $http({
            method: "POST",
            url: "/MultipleDocSearch/RetryProcess?Id=" + id,
            contentType: "application/json"
        });
    };


    list.ExportExcel = function (QId) {
        return $http({
            method: "POST",
            url: "/MultipleDocSearch/ExportExcel?QId=" + QId,
            contentType: "application/json"
        });
    };



    return list;
}]);
