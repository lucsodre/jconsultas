﻿angular.module("myApp.Services").service('LoginService', ["$http", function ($http) {
    var list = {};


    list.UserLogin = function () {
        return $http({
            method: "GET",
            url: "/Account/Login?UserName=''&UserPassword=''",
            contentType: "application/json"
        });
    };


    list.ForgetPassword = function (emailid) {
        return $http({
            method: "GET",
            url: "/Account/ForgetPasswordRequest?emailid=" + emailid,
            contentType: "application/json"
        });
    };

    list.ResetPassword = function (id, Password) {
        return $http({
            method: "GET",
            url: "/Account/ResetPassword?id=" + id + "&Password=" + Password,
            contentType: "application/json"
        });
    };



    return list;

}]);
