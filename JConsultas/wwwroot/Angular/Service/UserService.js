﻿angular.module("myApp.Services").service('UserService', ["$http", function ($http) {
    var list = {};

    list.UploadFileToDroupBox = function (fpath, fname, token) {
        return $http({
            method: "GET",
            url: "/SingleSearch/SaveFileOnDroupBox?fpath=" + fpath + "&fname=" + fname + "&token=" + token,
            contentType: "application/json"
        });
    };

    list.GetUserList = function () {
        return $http({
            method: "GET",
            url: "/User/GetUserList",
            contentType: "application/json"
        });
    };

    list.GetActiveUserList = function () {
        return $http({
            method: "GET",
            url: "/User/GetActiveUserList",
            contentType: "application/json"
        });
    };

    list.GetProfile = function () {
        return $http({
            method: "GET",
            url: "/User/GetProfile",
            contentType: "application/json"
        });
    };

    list.SendAuthenticationEmail = function (id, Email, UserName) {
        return $http({
            method: "GET",
            url: "/User/AuthenticationEmail?id=" + id + "&Email=" + Email + "&UserName=" + UserName,
            contentType: "application/json"
        });
    };

    list.SaveUser = function (data) {
        return $http({
            method: "POST",
            url: "/User/SaveUser",
            data: data
        });
    };

    //list.SaveUser = function (formData, data) {
    //    return $http({
    //        method: "POST",
    //        url: "/User/SaveUser2",
    //        contentType: false,
    //        processData: false,
    //        data: data,
    //        //headers: { 'Content-Type': undefined },
    //        transformRequest: function (data, headersGetterFunction) {
    //            return data;
    //        }
    //    });
    //};

    list.SendEmailCode = function (id) {
        return $http({
            method: "POST",
            url: "/User/SendEmailVarification?id=" + id
        });
    };

    list.CheckEmailVerficationCode = function (id, code) {
        return $http({
            method: "POST",
            url: "/User/CheckEmailVerficationCode?id=" + id + "&code=" + code
        });
    };


    list.SendPhoneCode = function (id) {
        return $http({
            method: "POST",
            url: "/User/SendPhoneVarification?id=" + id
        });
    };

    list.CheckPhoneVerficationCode = function (id, code) {
        return $http({
            method: "POST",
            url: "/User/CheckPhoneVerficationCode?id=" + id + "&code=" + code
        });
    };


    return list;

}]);
