﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using EntityLayer.Models;
using BusinessLogicLayer;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using JConsultas.Controllers;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using JConsultas.Models;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Http;

namespace JConsultas
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();


            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
           
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string stringConexaoBanco = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<JConsultasContext>(options =>
               options.UseSqlServer(stringConexaoBanco));
            //services.AddDbContext<JConsultasContext>(options =>
            //   options.UseSqlServer("Server=WIN-TE4IMG7L17Q;Initial Catalog=JConsultas;User ID=lsodre;Password=Lucas100;Trusted_Connection=True;"));
            services.AddDbContext<SGDExpressContext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("SGDConnection")));
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<JConsultasContext>()
                .AddDefaultTokenProviders();

            
            //services.AddDbContext<JConsultasContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            //services.AddIdentity<ApplicationUser, IdentityRole>()
            //   .AddEntityFrameworkStores<JConsultasContext>()
            //   .AddDefaultTokenProviders();


            //services.AddMvc();
            services.AddMvc().AddJsonOptions(options =>
               {
                   options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
               });

            services.AddScoped<IPasswordHasher<ApplicationUser>, SQLPasswordHasher>();
            services.AddScoped<RoleManager<IdentityRole>>();
            services.AddSingleton(_ => Configuration);

            services.AddOptions();
            services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 0;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;

                // Cookie settings
                options.Cookies.ApplicationCookie.ExpireTimeSpan = TimeSpan.FromDays(150);
                options.Cookies.ApplicationCookie.LoginPath = "/Account/Index";
                options.Cookies.ApplicationCookie.LogoutPath = "/Account/LogOff";

                // User settings
                options.User.RequireUniqueEmail = true;
            });

            services.Configure<RequestLocalizationOptions>(
            opts =>
            {
                var supportedCultures = new List<CultureInfo>
                {
                    new CultureInfo("pt-BR"),
                    new CultureInfo("pt"),
                    new CultureInfo("en-GB"),
                    new CultureInfo("en-US"),
                    new CultureInfo("en"),
                    new CultureInfo("fr-FR"),
                    new CultureInfo("fr"),
                };

                //opts.DefaultRequestCulture = new RequestCulture("en-GB");
                opts.DefaultRequestCulture = new RequestCulture("pt-BR");
                // Formatting numbers, dates, etc.
                opts.SupportedCultures = supportedCultures;
                // UI strings that we have localized.
                opts.SupportedUICultures = supportedCultures;
            });


            services.AddSingleton<SMSHelper>(new SMSHelper("AKIAIODORAD4CT3TH6HA", "bO3KEO0psA+KzdBRv7kzMByKiJwYhhQRwV9nc2pW"));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseIdentity();

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);

            app.UseGoogleAuthentication(new GoogleOptions()
            {
                ClientId = "60237038968-9i6j8p9bjau33scdu5i2fp1k0hgc892p.apps.googleusercontent.com",
                ClientSecret = "ciw4vgI7D7Kl7YyuXvVC4nMB"
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Index}/{id?}");
                routes.MapRoute(name: "api", template: "api/{controller}/{action}/{id}");




            });
            //routes.MapRoute(name: "api", template: "api/{controller=API}");

            //app.UseFileServer(new FileServerOptions()
            //{
            //    FileProvider = new PhysicalFileProvider("/Test"),
            //    RequestPath = new PathString("/V1"),
            //    EnableDirectoryBrowsing = true // you make this true or false.
            //});

            //   var ci = new CultureInfo("en-US");
            //ci.DateTimeFormat.ShortDatePattern = "dd-MM-yyyy";

            // Configure the Localization middleware
            //app.UseRequestLocalization(new RequestLocalizationOptions
            //{
            //    DefaultRequestCulture = new RequestCulture(ci),
            //    SupportedCultures = new List<CultureInfo>
            //{
            //    new CultureInfo("en-GB"),
            //},
            //    SupportedUICultures = new List<CultureInfo>
            //{
            //    new CultureInfo("en-GB"),
            //}
            //});

        }



    }
}
