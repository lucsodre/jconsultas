﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JConsultas.Models
{
    public class DeserializarXML
    {
        public static CONSULTA2300 DeserializarXMLRetorno(string xml)
        {
            try
            {
                CONSULTA2300 retorno = null;
                XmlSerializer serializer = new XmlSerializer(typeof(CONSULTA2300));

                MemoryStream stream = new MemoryStream();
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                retorno = (CONSULTA2300)serializer.Deserialize(reader);
                //reader.Close();
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao deserializar XML. Detalhe: " + ex.ToString());
            }
        }
    }
}
