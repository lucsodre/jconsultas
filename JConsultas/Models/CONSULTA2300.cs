﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using EntityLayer.Models;

namespace JConsultas.Models
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class CONSULTA2300ERR
    {

        private string rETORNOEXECUCAOField;

        private string dESCRICAOField;
        public string RETORNOEXECUCAO
        {
            get
            {
                return this.rETORNOEXECUCAOField;
            }
            set
            {
                this.rETORNOEXECUCAOField = value;
            }
        }

        public string DESCRICAO
        {
            get
            {
                return this.dESCRICAOField;
            }
            set
            {
                this.dESCRICAOField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class CONSULTA2300
    {
        public static System.Globalization.CultureInfo minhaCultura
        {
            get
            {
                return new System.Globalization.CultureInfo("pt-BR"); //pt-BR usada como base
            }
        }

        private CONSULTA2300LOGIN lOGINField;

        private CONSULTA2300CONSULTAS cONSULTASField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public CONSULTA2300LOGIN LOGIN
        {
            get
            {
                return this.lOGINField;
            }
            set
            {
                this.lOGINField = value;
            }
        }

        public CONSULTA2300CONSULTAS CONSULTAS
        {
            get
            {
                return this.cONSULTASField;
            }
            set
            {
                this.cONSULTASField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300 object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300 object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300 object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300 obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300 obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300 Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300 object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300 object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300 object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300 obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300 obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300 LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300 object
        /// </summary>
        public virtual CONSULTA2300 Clone()
        {
            return ((CONSULTA2300)(this.MemberwiseClone()));
        }
        #endregion
    }

    [XmlRoot(ElementName = "REQUISICAO")]
    public class REQUISICAO
    {
        [XmlElement(ElementName = "AGREGADO")]
        public string AGREGADO { get; set; }
        [XmlElement(ElementName = "VEICULAR")]
        public string VEICULAR { get; set; }
        [XmlElement(ElementName = "DEBITOS_360")]
        public string DEBITOS_360 { get; set; }
        [XmlElement(ElementName = "NOTIFICACAO_MULTA")]
        public string NOTIFICACAO_MULTA { get; set; }
        [XmlElement(ElementName = "HISTORICO_MULTA")]
        public string HISTORICO_MULTA { get; set; }
        [XmlElement(ElementName = "BLOQUEIO_RENAJUD")]
        public string BLOQUEIO_RENAJUD { get; set; }
        [XmlElement(ElementName = "BLOQUEIO_DETRAN")]
        public string BLOQUEIO_DETRAN { get; set; }
        [XmlElement(ElementName = "GRAVAME")]
        public string GRAVAME { get; set; }
        [XmlElement(ElementName = "DADOS_PROPRIETARIO")]
        public string DADOS_PROPRIETARIO { get; set; }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300LOGIN
    {

        private string rETORNOField;

        private string dESCRICAOField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RETORNO
        {
            get
            {
                return this.rETORNOField;
            }
            set
            {
                this.rETORNOField = value;
            }
        }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DESCRICAO
        {
            get
            {
                return this.dESCRICAOField;
            }
            set
            {
                this.dESCRICAOField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300LOGIN));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300LOGIN object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300LOGIN object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300LOGIN object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300LOGIN obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300LOGIN);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300LOGIN obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300LOGIN Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300LOGIN)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300LOGIN object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300LOGIN object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300LOGIN object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300LOGIN obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300LOGIN);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300LOGIN obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300LOGIN LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300LOGIN object
        /// </summary>
        public virtual CONSULTA2300LOGIN Clone()
        {
            return ((CONSULTA2300LOGIN)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTAS
    {

        private CONSULTA2300CONSULTASCONSULTA cONSULTAField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public CONSULTA2300CONSULTASCONSULTA CONSULTA
        {
            get
            {
                return this.cONSULTAField;
            }
            set
            {
                this.cONSULTAField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTAS));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTAS object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTAS object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTAS object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTAS obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTAS);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTAS obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTAS Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTAS)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTAS object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTAS object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTAS object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTAS obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTAS);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTAS obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTAS LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTAS object
        /// </summary>
        public virtual CONSULTA2300CONSULTAS Clone()
        {
            return ((CONSULTA2300CONSULTAS)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTA
    {

        private string rETORNOEXECUCAOField;

        private string dESCRICAOField;

        private CONSULTA2300CONSULTASCONSULTAVEICULO vEICULOField;

        private CONSULTA2300CONSULTASCONSULTAPROPRIETARIO pROPRIETARIOField;

        private CONSULTA2300CONSULTASCONSULTAGRAVAME gRAVAMEField;

        private CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO gRAVAME_INTENCAOField;

        private string[] rESTRICOESField;

        private CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS tOTAL_DEBITOSField;

        private CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA cOMUNICACAO_VENDAField;

        private CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR iNSPECAO_VEICULARField;

        private CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO[] tAXAS_LICENCIAMENTOField;

        private CONSULTA2300CONSULTASCONSULTADPVAT[] dPVATSField;

        private CONSULTA2300CONSULTASCONSULTAIPVA[] iPVASField;

        private CONSULTA2300CONSULTASCONSULTANOTIFICACAO[] nOTIFICACOESField;

        private CONSULTA2300CONSULTASCONSULTAMULTA[] mULTASField;

        private CONSULTA2300CONSULTASCONSULTAMULTA1[] mULTASRENAINFField;

        private CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD[] bLOQUEIOSRENAJUDField;

        private CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN bLOQUEIOSDETRANField;

        private string dATAULTIMAATUALIZACAOField;

        private string iDLOGWSField;

        private string numField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string RETORNOEXECUCAO
        {
            get
            {
                return this.rETORNOEXECUCAOField;
            }
            set
            {
                this.rETORNOEXECUCAOField = value;
            }
        }

        public string DESCRICAO
        {
            get
            {
                return this.dESCRICAOField;
            }
            set
            {
                this.dESCRICAOField = value;
            }
        }

        public CONSULTA2300CONSULTASCONSULTAVEICULO VEICULO
        {
            get
            {
                return this.vEICULOField;
            }
            set
            {
                this.vEICULOField = value;
            }
        }

        public CONSULTA2300CONSULTASCONSULTAPROPRIETARIO PROPRIETARIO
        {
            get
            {
                return this.pROPRIETARIOField;
            }
            set
            {
                this.pROPRIETARIOField = value;
            }
        }

        public CONSULTA2300CONSULTASCONSULTAGRAVAME GRAVAME
        {
            get
            {
                return this.gRAVAMEField;
            }
            set
            {
                this.gRAVAMEField = value;
            }
        }

        public CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO GRAVAME_INTENCAO
        {
            get
            {
                return this.gRAVAME_INTENCAOField;
            }
            set
            {
                this.gRAVAME_INTENCAOField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("RESTRICAO", IsNullable = false)]
        public string[] RESTRICOES
        {
            get
            {
                return this.rESTRICOESField;
            }
            set
            {
                this.rESTRICOESField = value;
            }
        }

        public CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS TOTAL_DEBITOS
        {
            get
            {
                return this.tOTAL_DEBITOSField;
            }
            set
            {
                this.tOTAL_DEBITOSField = value;
            }
        }

        public CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA COMUNICACAO_VENDA
        {
            get
            {
                return this.cOMUNICACAO_VENDAField;
            }
            set
            {
                this.cOMUNICACAO_VENDAField = value;
            }
        }

        public CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR INSPECAO_VEICULAR
        {
            get
            {
                return this.iNSPECAO_VEICULARField;
            }
            set
            {
                this.iNSPECAO_VEICULARField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("TAXA_LICENCIAMENTO", IsNullable = false)]
        public CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO[] TAXAS_LICENCIAMENTO
        {
            get
            {
                return this.tAXAS_LICENCIAMENTOField;
            }
            set
            {
                this.tAXAS_LICENCIAMENTOField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("DPVAT", IsNullable = false)]
        public CONSULTA2300CONSULTASCONSULTADPVAT[] DPVATS
        {
            get
            {
                return this.dPVATSField;
            }
            set
            {
                this.dPVATSField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("IPVA", IsNullable = false)]
        public CONSULTA2300CONSULTASCONSULTAIPVA[] IPVAS
        {
            get
            {
                return this.iPVASField;
            }
            set
            {
                this.iPVASField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("NOTIFICACAO", IsNullable = false)]
        public CONSULTA2300CONSULTASCONSULTANOTIFICACAO[] NOTIFICACOES
        {
            get
            {
                return this.nOTIFICACOESField;
            }
            set
            {
                this.nOTIFICACOESField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("MULTA", IsNullable = false)]
        public CONSULTA2300CONSULTASCONSULTAMULTA[] MULTAS
        {
            get
            {
                return this.mULTASField;
            }
            set
            {
                this.mULTASField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("MULTA", IsNullable = false)]
        public CONSULTA2300CONSULTASCONSULTAMULTA1[] MULTASRENAINF
        {
            get
            {
                return this.mULTASRENAINFField;
            }
            set
            {
                this.mULTASRENAINFField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("BLOQUEIORENAJUD", IsNullable = false)]
        public CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD[] BLOQUEIOSRENAJUD
        {
            get
            {
                return this.bLOQUEIOSRENAJUDField;
            }
            set
            {
                this.bLOQUEIOSRENAJUDField = value;
            }
        }

        public CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN BLOQUEIOSDETRAN
        {
            get
            {
                return this.bLOQUEIOSDETRANField;
            }
            set
            {
                this.bLOQUEIOSDETRANField = value;
            }
        }

        [XmlIgnore]
        public ConsultaAvancadaVeiculo CONSULTAAVANCADAVEICULO { get; set; }

        [XmlElement(ElementName = "REQUISICAO")]
        public REQUISICAO REQUISICAO { get; set; }        

        public string DATAULTIMAATUALIZACAO
        {
            get
            {
                return this.dATAULTIMAATUALIZACAOField;
            }
            set
            {
                this.dATAULTIMAATUALIZACAOField = value;
            }
        }

        public string IDLOGWS
        {
            get
            {
                return this.iDLOGWSField;
            }
            set
            {
                this.iDLOGWSField = value;
            }
        }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Num
        {
            get
            {
                return this.numField;
            }
            set
            {
                this.numField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTA));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTA object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTA object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTA object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTA obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTA);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTA obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTA Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTA)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTA object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTA object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTA object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTA obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTA);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTA obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTA LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTA object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTA Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTA)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTAVEICULO
    {

        private string pLACAField;

        private string cHASSIField;

        private string rENAVAMField;

        private string mUNICIPIOField;

        private string mUNICIPIODESCField;

        private string ufField;

        private string pROCEDENCIAField;

        private string pROCEDENCIADESCField;

        private string mARCAField;

        private string mARCADESCField;

        private string aNOFABRICACAOField;

        private string aNOMODELOField;

        private string tIPOField;

        private string tIPODESCField;

        private string cARROCERIAField;

        private string cARROCERIADESCField;

        private string nR_CARROCERIAField;

        private string cORField;

        private string cORDESCField;

        private string cATEGORIAField;

        private string cATEGORIADESCField;

        private string cOMBUSTIVELField;

        private string cOMBUSTIVELDESCField;

        private string eSPECIEField;

        private string eSPECIEDESCField;

        private string cAPACIDADEPASSAGEIROField;

        private string cAPACIDADECARGAField;

        private string pOTENCIAField;

        private string cILINDRADAField;

        private string cMTField;

        private string pBTField;

        private string eIXOSField;

        private string nUMEROMOTORField;

        private string dATAALTERACAOField;

        private string nUMEROCAMBIOField;

        private string tIPOMONTAGEMField;

        private string sITUACAOVEICULOField;

        private string tIPOREMARCACAOCHASSIField;

        private string dATAEMISSAOCRVField;

        private string dATALICENCIAMENTOField;

        private string eXERCICIOLICENCIAMENTOField;

        private string vALORNFField;

        private string nUMERONFField;

        private string dATANFField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string PLACA
        {
            get
            {
                return this.pLACAField;
            }
            set
            {
                this.pLACAField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string CHASSI
        {
            get
            {
                return this.cHASSIField;
            }
            set
            {
                this.cHASSIField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string RENAVAM
        {
            get
            {
                return this.rENAVAMField;
            }
            set
            {
                this.rENAVAMField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string MUNICIPIO
        {
            get
            {
                return this.mUNICIPIOField;
            }
            set
            {
                this.mUNICIPIOField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string MUNICIPIODESC
        {
            get
            {
                return this.mUNICIPIODESCField;
            }
            set
            {
                this.mUNICIPIODESCField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string UF
        {
            get
            {
                return this.ufField;
            }
            set
            {
                this.ufField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string PROCEDENCIA
        {
            get
            {
                return this.pROCEDENCIAField;
            }
            set
            {
                this.pROCEDENCIAField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string PROCEDENCIADESC
        {
            get
            {
                return this.pROCEDENCIADESCField;
            }
            set
            {
                this.pROCEDENCIADESCField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string MARCA
        {
            get
            {
                return this.mARCAField;
            }
            set
            {
                this.mARCAField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string MARCADESC
        {
            get
            {
                return this.mARCADESCField;
            }
            set
            {
                this.mARCADESCField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string ANOFABRICACAO
        {
            get
            {
                return this.aNOFABRICACAOField;
            }
            set
            {
                this.aNOFABRICACAOField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string ANOMODELO
        {
            get
            {
                return this.aNOMODELOField;
            }
            set
            {
                this.aNOMODELOField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string TIPO
        {
            get
            {
                return this.tIPOField;
            }
            set
            {
                this.tIPOField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string TIPODESC
        {
            get
            {
                return this.tIPODESCField;
            }
            set
            {
                this.tIPODESCField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string CARROCERIA
        {
            get
            {
                return this.cARROCERIAField;
            }
            set
            {
                this.cARROCERIAField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string CARROCERIADESC
        {
            get
            {
                return this.cARROCERIADESCField;
            }
            set
            {
                this.cARROCERIADESCField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string NR_CARROCERIA
        {
            get
            {
                return this.nR_CARROCERIAField;
            }
            set
            {
                this.nR_CARROCERIAField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string COR
        {
            get
            {
                return this.cORField;
            }
            set
            {
                this.cORField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string CORDESC
        {
            get
            {
                return this.cORDESCField;
            }
            set
            {
                this.cORDESCField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string CATEGORIA
        {
            get
            {
                return this.cATEGORIAField;
            }
            set
            {
                this.cATEGORIAField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string CATEGORIADESC
        {
            get
            {
                return this.cATEGORIADESCField;
            }
            set
            {
                this.cATEGORIADESCField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string COMBUSTIVEL
        {
            get
            {
                return this.cOMBUSTIVELField;
            }
            set
            {
                this.cOMBUSTIVELField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string COMBUSTIVELDESC
        {
            get
            {
                return this.cOMBUSTIVELDESCField;
            }
            set
            {
                this.cOMBUSTIVELDESCField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string ESPECIE
        {
            get
            {
                return this.eSPECIEField;
            }
            set
            {
                this.eSPECIEField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string ESPECIEDESC
        {
            get
            {
                return this.eSPECIEDESCField;
            }
            set
            {
                this.eSPECIEDESCField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 25)]
        public string CAPACIDADEPASSAGEIRO
        {
            get
            {
                return this.cAPACIDADEPASSAGEIROField;
            }
            set
            {
                this.cAPACIDADEPASSAGEIROField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 26)]
        public string CAPACIDADECARGA
        {
            get
            {
                return this.cAPACIDADECARGAField;
            }
            set
            {
                this.cAPACIDADECARGAField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 27)]
        public string POTENCIA
        {
            get
            {
                return this.pOTENCIAField;
            }
            set
            {
                this.pOTENCIAField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 28)]
        public string CILINDRADA
        {
            get
            {
                return this.cILINDRADAField;
            }
            set
            {
                this.cILINDRADAField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 29)]
        public string CMT
        {
            get
            {
                return this.cMTField;
            }
            set
            {
                this.cMTField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 30)]
        public string PBT
        {
            get
            {
                int auxPbt= 0;
                //decimal vDec= 0;

                if (!string.IsNullOrEmpty(this.pBTField) && Int32.TryParse(this.pBTField, out auxPbt))
                {
                    double d = (double)auxPbt / 100;
                    return d.ToString();
                }
                else
                    return this.pBTField;
            }
            set
            {
                this.pBTField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 31)]
        public string EIXOS
        {
            get
            {
                return this.eIXOSField;
            }
            set
            {
                this.eIXOSField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 32)]
        public string NUMEROMOTOR
        {
            get
            {
                return this.nUMEROMOTORField;
            }
            set
            {
                this.nUMEROMOTORField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 33)]
        public string DATAALTERACAO
        {
            get
            {
                return this.dATAALTERACAOField;
            }
            set
            {
                this.dATAALTERACAOField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 34)]
        public string NUMEROCAMBIO
        {
            get
            {
                return this.nUMEROCAMBIOField;
            }
            set
            {
                this.nUMEROCAMBIOField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 35)]
        public string TIPOMONTAGEM
        {
            get
            {
                return this.tIPOMONTAGEMField;
            }
            set
            {
                this.tIPOMONTAGEMField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 36)]
        public string SITUACAOVEICULO
        {
            get
            {
                return this.sITUACAOVEICULOField;
            }
            set
            {
                this.sITUACAOVEICULOField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 37)]
        public string TIPOREMARCACAOCHASSI
        {
            get
            {
                return this.tIPOREMARCACAOCHASSIField;
            }
            set
            {
                this.tIPOREMARCACAOCHASSIField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 38)]
        public string DATAEMISSAOCRV
        {
            get
            {
                return this.dATAEMISSAOCRVField;
            }
            set
            {
                this.dATAEMISSAOCRVField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 39)]
        public string DATALICENCIAMENTO
        {
            get
            {
                return this.dATALICENCIAMENTOField;
            }
            set
            {
                this.dATALICENCIAMENTOField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 40)]
        public string EXERCICIOLICENCIAMENTO
        {
            get
            {
                return this.eXERCICIOLICENCIAMENTOField;
            }
            set
            {
                this.eXERCICIOLICENCIAMENTOField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 41)]
        public string VALORNF
        {
            get
            {
                return this.vALORNFField;
            }
            set
            {
                this.vALORNFField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 42)]
        public string NUMERONF
        {
            get
            {
                return this.nUMERONFField;
            }
            set
            {
                this.nUMERONFField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 43)]
        public string DATANF
        {
            get
            {
                return this.dATANFField;
            }
            set
            {
                this.dATANFField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTAVEICULO));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAVEICULO object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTAVEICULO object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAVEICULO object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAVEICULO obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAVEICULO);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAVEICULO obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAVEICULO Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTAVEICULO)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAVEICULO object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                // streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTAVEICULO object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAVEICULO object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAVEICULO obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAVEICULO);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAVEICULO obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAVEICULO LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                // sr.Close();
                // file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTAVEICULO object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTAVEICULO Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTAVEICULO)(this.MemberwiseClone()));
        }
        #endregion
    }


    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    // [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTAPROPRIETARIO
    {

        private string tIPOCPFPROPRIETARIOField;

        private string cPFPROPRIETARIOField;

        private string nOMEPROPRIETARIOField;

        private string rgField;

        private string oRGEXPField;

        private string eNDERECOField;

        private string cOMPLEMENTOField;

        private string bAIRROField;

        private string cIDADEField;

        private string cEPField;

        private string nUMEROField;

        private string dDDField;

        private string tELEFONEField;

        private string nOMEPROPRIETARIOANTERIORField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string TIPOCPFPROPRIETARIO
        {
            get
            {
                return this.tIPOCPFPROPRIETARIOField;
            }
            set
            {
                this.tIPOCPFPROPRIETARIOField = value;
            }
        }

        public string CPFPROPRIETARIO
        {
            get
            {
                return this.cPFPROPRIETARIOField;
            }
            set
            {
                this.cPFPROPRIETARIOField = value;
            }
        }

        public string NOMEPROPRIETARIO
        {
            get
            {
                return this.nOMEPROPRIETARIOField;
            }
            set
            {
                this.nOMEPROPRIETARIOField = value;
            }
        }

        public string RG
        {
            get
            {
                return this.rgField;
            }
            set
            {
                this.rgField = value;
            }
        }

        public string ORGEXP
        {
            get
            {
                return this.oRGEXPField;
            }
            set
            {
                this.oRGEXPField = value;
            }
        }

        public string ENDERECO
        {
            get
            {
                return this.eNDERECOField;
            }
            set
            {
                this.eNDERECOField = value;
            }
        }

        public string COMPLEMENTO
        {
            get
            {
                return this.cOMPLEMENTOField;
            }
            set
            {
                this.cOMPLEMENTOField = value;
            }
        }

        public string BAIRRO
        {
            get
            {
                return this.bAIRROField;
            }
            set
            {
                this.bAIRROField = value;
            }
        }

        public string CIDADE
        {
            get
            {
                return this.cIDADEField;
            }
            set
            {
                this.cIDADEField = value;
            }
        }

        public string CEP
        {
            get
            {
                return this.cEPField;
            }
            set
            {
                this.cEPField = value;
            }
        }

        public string NUMERO
        {
            get
            {
                return this.nUMEROField;
            }
            set
            {
                this.nUMEROField = value;
            }
        }

        public string DDD
        {
            get
            {
                return this.dDDField;
            }
            set
            {
                this.dDDField = value;
            }
        }

        public string TELEFONE
        {
            get
            {
                return this.tELEFONEField;
            }
            set
            {
                this.tELEFONEField = value;
            }
        }

        public string NOMEPROPRIETARIOANTERIOR
        {
            get
            {
                return this.nOMEPROPRIETARIOANTERIORField;
            }
            set
            {
                this.nOMEPROPRIETARIOANTERIORField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTAPROPRIETARIO));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAPROPRIETARIO object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTAPROPRIETARIO object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAPROPRIETARIO object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAPROPRIETARIO obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAPROPRIETARIO);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAPROPRIETARIO obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAPROPRIETARIO Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTAPROPRIETARIO)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAPROPRIETARIO object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTAPROPRIETARIO object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAPROPRIETARIO object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAPROPRIETARIO obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAPROPRIETARIO);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAPROPRIETARIO obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAPROPRIETARIO LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTAPROPRIETARIO object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTAPROPRIETARIO Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTAPROPRIETARIO)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    // [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTAGRAVAME
    {

        private string cNPJFINANCIADOField;

        private string aRRENDATARIOFINANCIADOField;

        private string tIPOTRANSACAOField;

        private string tIPOTRANSACAODESCField;

        private string tIPORESTRICAOFINANCEIRAField;

        private string tIPORESTRICAOFINANCEIRADESCField;

        private string cNPJFINANCEIRAField;

        private string aGENTEFINANCEIROField;

        private string aGENTEFINANCEIRODESCField;

        private string nUMEROCONTRATOField;

        private string dATAVIGENCIACONTRATOField;

        private string dATAINCLUSAOFINANCIAMENTOField;

        private string dATACONTRATOField;

        private string uF_GRAVAMEField;

        private string nUMERORESTRICAOField;

        private string sITUACAOField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string CNPJFINANCIADO
        {
            get
            {
                return this.cNPJFINANCIADOField;
            }
            set
            {
                this.cNPJFINANCIADOField = value;
            }
        }

        public string ARRENDATARIOFINANCIADO
        {
            get
            {
                return this.aRRENDATARIOFINANCIADOField;
            }
            set
            {
                this.aRRENDATARIOFINANCIADOField = value;
            }
        }

        public string TIPOTRANSACAO
        {
            get
            {
                return this.tIPOTRANSACAOField;
            }
            set
            {
                this.tIPOTRANSACAOField = value;
            }
        }

        public string TIPOTRANSACAODESC
        {
            get
            {
                return this.tIPOTRANSACAODESCField;
            }
            set
            {
                this.tIPOTRANSACAODESCField = value;
            }
        }

        public string TIPORESTRICAOFINANCEIRA
        {
            get
            {
                return this.tIPORESTRICAOFINANCEIRAField;
            }
            set
            {
                this.tIPORESTRICAOFINANCEIRAField = value;
            }
        }

        public string TIPORESTRICAOFINANCEIRADESC
        {
            get
            {
                return this.tIPORESTRICAOFINANCEIRADESCField;
            }
            set
            {
                this.tIPORESTRICAOFINANCEIRADESCField = value;
            }
        }

        public string CNPJFINANCEIRA
        {
            get
            {
                return this.cNPJFINANCEIRAField;
            }
            set
            {
                this.cNPJFINANCEIRAField = value;
            }
        }

        public string AGENTEFINANCEIRO
        {
            get
            {
                return this.aGENTEFINANCEIROField;
            }
            set
            {
                this.aGENTEFINANCEIROField = value;
            }
        }

        public string AGENTEFINANCEIRODESC
        {
            get
            {
                return this.aGENTEFINANCEIRODESCField;
            }
            set
            {
                this.aGENTEFINANCEIRODESCField = value;
            }
        }

        public string NUMEROCONTRATO
        {
            get
            {
                return this.nUMEROCONTRATOField;
            }
            set
            {
                this.nUMEROCONTRATOField = value;
            }
        }

        public string DATAVIGENCIACONTRATO
        {
            get
            {
                return this.dATAVIGENCIACONTRATOField;
            }
            set
            {
                this.dATAVIGENCIACONTRATOField = value;
            }
        }

        public string DATAINCLUSAOFINANCIAMENTO
        {
            get
            {
                return this.dATAINCLUSAOFINANCIAMENTOField;
            }
            set
            {
                this.dATAINCLUSAOFINANCIAMENTOField = value;
            }
        }

        public string DATACONTRATO
        {
            get
            {
                return this.dATACONTRATOField;
            }
            set
            {
                this.dATACONTRATOField = value;
            }
        }

        public string UF_GRAVAME
        {
            get
            {
                return this.uF_GRAVAMEField;
            }
            set
            {
                this.uF_GRAVAMEField = value;
            }
        }

        public string NUMERORESTRICAO
        {
            get
            {
                return this.nUMERORESTRICAOField;
            }
            set
            {
                this.nUMERORESTRICAOField = value;
            }
        }

        public string SITUACAO
        {
            get
            {
                return this.sITUACAOField;
            }
            set
            {
                this.sITUACAOField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTAGRAVAME));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAGRAVAME object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTAGRAVAME object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAGRAVAME object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAGRAVAME obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAGRAVAME);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAGRAVAME obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAGRAVAME Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTAGRAVAME)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAGRAVAME object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTAGRAVAME object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAGRAVAME object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAGRAVAME obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAGRAVAME);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAGRAVAME obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAGRAVAME LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTAGRAVAME object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTAGRAVAME Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTAGRAVAME)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO
    {

        private string cNPJFINANCEIRAField;

        private string iNFORMANTEFINANCIAMENTOField;

        private string tIPOTRANSACAOField;

        private string tIPOTRANSACAODESCField;

        private string tIPORESTRICAOFINANCEIRAField;

        private string tIPORESTRICAOFINANCEIRADESCField;

        private string aGENTEFINANCEIROField;

        private string aGENTEFINANCEIRODESCField;

        private string cNPJFINANCIADOField;

        private string nOMEFINANCIADOField;

        private string dATAINCLUSAOINTENCAOTROCAFINANCField;

        private string nUMEROCONTRATOFINANCField;

        private string dATAVIGENCIACONTRATOFINANCField;

        private string uF_GRAVAMEField;

        private string nUMERORESTRICAOField;

        private string sITUACAOField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string CNPJFINANCEIRA
        {
            get
            {
                return this.cNPJFINANCEIRAField;
            }
            set
            {
                this.cNPJFINANCEIRAField = value;
            }
        }

        public string INFORMANTEFINANCIAMENTO
        {
            get
            {
                return this.iNFORMANTEFINANCIAMENTOField;
            }
            set
            {
                this.iNFORMANTEFINANCIAMENTOField = value;
            }
        }

        public string TIPOTRANSACAO
        {
            get
            {
                return this.tIPOTRANSACAOField;
            }
            set
            {
                this.tIPOTRANSACAOField = value;
            }
        }

        public string TIPOTRANSACAODESC
        {
            get
            {
                return this.tIPOTRANSACAODESCField;
            }
            set
            {
                this.tIPOTRANSACAODESCField = value;
            }
        }

        public string TIPORESTRICAOFINANCEIRA
        {
            get
            {
                return this.tIPORESTRICAOFINANCEIRAField;
            }
            set
            {
                this.tIPORESTRICAOFINANCEIRAField = value;
            }
        }

        public string TIPORESTRICAOFINANCEIRADESC
        {
            get
            {
                return this.tIPORESTRICAOFINANCEIRADESCField;
            }
            set
            {
                this.tIPORESTRICAOFINANCEIRADESCField = value;
            }
        }

        public string AGENTEFINANCEIRO
        {
            get
            {
                return this.aGENTEFINANCEIROField;
            }
            set
            {
                this.aGENTEFINANCEIROField = value;
            }
        }

        public string AGENTEFINANCEIRODESC
        {
            get
            {
                return this.aGENTEFINANCEIRODESCField;
            }
            set
            {
                this.aGENTEFINANCEIRODESCField = value;
            }
        }

        public string CNPJFINANCIADO
        {
            get
            {
                return this.cNPJFINANCIADOField;
            }
            set
            {
                this.cNPJFINANCIADOField = value;
            }
        }

        public string NOMEFINANCIADO
        {
            get
            {
                return this.nOMEFINANCIADOField;
            }
            set
            {
                this.nOMEFINANCIADOField = value;
            }
        }

        public string DATAINCLUSAOINTENCAOTROCAFINANC
        {
            get
            {
                return this.dATAINCLUSAOINTENCAOTROCAFINANCField;
            }
            set
            {
                this.dATAINCLUSAOINTENCAOTROCAFINANCField = value;
            }
        }

        public string NUMEROCONTRATOFINANC
        {
            get
            {
                return this.nUMEROCONTRATOFINANCField;
            }
            set
            {
                this.nUMEROCONTRATOFINANCField = value;
            }
        }

        public string DATAVIGENCIACONTRATOFINANC
        {
            get
            {
                return this.dATAVIGENCIACONTRATOFINANCField;
            }
            set
            {
                this.dATAVIGENCIACONTRATOFINANCField = value;
            }
        }

        public string UF_GRAVAME
        {
            get
            {
                return this.uF_GRAVAMEField;
            }
            set
            {
                this.uF_GRAVAMEField = value;
            }
        }

        public string NUMERORESTRICAO
        {
            get
            {
                return this.nUMERORESTRICAOField;
            }
            set
            {
                this.nUMERORESTRICAOField = value;
            }
        }

        public string SITUACAO
        {
            get
            {
                return this.sITUACAOField;
            }
            set
            {
                this.sITUACAOField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                // file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTAGRAVAME_INTENCAO)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    // [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS
    {

        private string dEBITOIPVALICENCIAMENTO_DESCField;

        private string dEBITOSMULTAS_DESCField;

        private object dEBITOSDIVIDAATIVA_DESCField;

        private string iPVAField;

        private string lICENCIAMENTOField;

        private string mULTASField;

        private string dPVATField;

        private string iPVA_DIVIDAATIVAField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string DEBITOIPVALICENCIAMENTO_DESC
        {
            get
            {
                return this.dEBITOIPVALICENCIAMENTO_DESCField;
            }
            set
            {
                this.dEBITOIPVALICENCIAMENTO_DESCField = value;
            }
        }

        public string DEBITOSMULTAS_DESC
        {
            get
            {
                return this.dEBITOSMULTAS_DESCField;
            }
            set
            {
                this.dEBITOSMULTAS_DESCField = value;
            }
        }

        public object DEBITOSDIVIDAATIVA_DESC
        {
            get
            {
                return this.dEBITOSDIVIDAATIVA_DESCField;
            }
            set
            {
                this.dEBITOSDIVIDAATIVA_DESCField = value;
            }
        }

        public string IPVA
        {
            get
            {
                decimal vDec = 0;
                if (!string.IsNullOrEmpty(this.iPVAField) && Decimal.TryParse(this.iPVAField, out vDec))
                {
                    vDec = vDec / 100;

                    return string.Format(CONSULTA2300.minhaCultura, "{0:C}", vDec);
                    //return vDec.ToString("C");
                }
                else
                {
                    return this.iPVAField;
                }
            }
            set
            {
                this.iPVAField = value;
            }
        }

        public string LICENCIAMENTO
        {
            get
            {
                decimal vDec = 0;
                if (!string.IsNullOrEmpty(this.lICENCIAMENTOField) && Decimal.TryParse(this.lICENCIAMENTOField, out vDec))
                {
                    vDec = vDec / 100;

                    return string.Format(CONSULTA2300.minhaCultura, "{0:C}", vDec);
                    //return vDec.ToString("C");
                }
                else
                {
                    return this.lICENCIAMENTOField;
                }
            }
            set
            {
                this.lICENCIAMENTOField = value;
            }
        }

        public string MULTAS
        {
            get
            {
                decimal vDec = 0;
                if (!string.IsNullOrEmpty(this.mULTASField) && Decimal.TryParse(this.mULTASField, out vDec))
                {
                    vDec = vDec / 100;
                    return string.Format(CONSULTA2300.minhaCultura, "{0:C}", vDec);
                    //return vDec.ToString("C");
                }
                else
                {
                    return this.mULTASField;
                }
                
            }
            set
            {
                this.mULTASField = value;
            }
        }

        public string DPVAT
        {
            get
            {
                decimal vDec = 0;
                if (!string.IsNullOrEmpty(this.dPVATField) && Decimal.TryParse(this.dPVATField, out vDec)) { 
                    vDec = vDec / 100;

                    return string.Format(CONSULTA2300.minhaCultura, "{0:C}", vDec);
                    //return vDec.ToString("C");
                }
                else
                {
                    return this.dPVATField;
                }
            }
            set
            {
                this.dPVATField = value;
            }
        }

        public string IPVA_DIVIDAATIVA
        {
            get
            {
                decimal vDec = 0;
                if (!string.IsNullOrEmpty(this.iPVA_DIVIDAATIVAField) && Decimal.TryParse(this.iPVA_DIVIDAATIVAField, out vDec))
                {
                    vDec = vDec / 100;

                    return string.Format(CONSULTA2300.minhaCultura, "{0:C}", vDec);
                    //return vDec.ToString("C");
                }
                else
                {
                    return this.iPVA_DIVIDAATIVAField;
                }
            }
            set
            {
                this.iPVA_DIVIDAATIVAField = value;
            }
        }

        private string fieldTotalDebitos;
        public string TotalDebitos
        {
            get
            {

                decimal debitosIPVA = 0;
                decimal debitosLICENCIAMENTO = 0;
                decimal debitosMULTAS = 0;
                decimal debitosDPVAT = 0;

                Decimal.TryParse(this.iPVAField, out debitosIPVA);
                Decimal.TryParse(this.lICENCIAMENTOField, out debitosLICENCIAMENTO);
                Decimal.TryParse(this.mULTASField, out debitosMULTAS);
                
                Decimal.TryParse(this.dPVATField, out debitosDPVAT);

                decimal vDec = (debitosIPVA + debitosMULTAS + debitosDPVAT + debitosLICENCIAMENTO) / 100;
                return string.Format(CONSULTA2300.minhaCultura, "{0:C}", vDec);
                //return ((debitosIPVA + debitosLICENCIAMENTO + debitosMULTAS + debitosDPVAT) / 100).ToString("C");
            }
            set
            {
                this.fieldTotalDebitos = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                // sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTATOTAL_DEBITOS)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    // [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA
    {

        private string cOMUNICACAOVENDAField;

        private string cOMUNICACAOVENDADESCField;

        private string dATAINCLUSAOCOMUNICACAOVENDASField;

        private string dATAVENDAField;

        private string tIPODOCTOCOMPRADORField;

        private string cNPJCPFCOMPRADORField;

        private string dATANOTAFISCALField;

        private string dATAPROTOCOLODETRANField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string COMUNICACAOVENDA
        {
            get
            {
                return this.cOMUNICACAOVENDAField;
            }
            set
            {
                this.cOMUNICACAOVENDAField = value;
            }
        }

        public string COMUNICACAOVENDADESC
        {
            get
            {
                return this.cOMUNICACAOVENDADESCField;
            }
            set
            {
                this.cOMUNICACAOVENDADESCField = value;
            }
        }

        public string DATAINCLUSAOCOMUNICACAOVENDAS
        {
            get
            {
                return this.dATAINCLUSAOCOMUNICACAOVENDASField;
            }
            set
            {
                this.dATAINCLUSAOCOMUNICACAOVENDASField = value;
            }
        }

        public string DATAVENDA
        {
            get
            {
                return this.dATAVENDAField;
            }
            set
            {
                this.dATAVENDAField = value;
            }
        }

        public string TIPODOCTOCOMPRADOR
        {
            get
            {
                return this.tIPODOCTOCOMPRADORField;
            }
            set
            {
                this.tIPODOCTOCOMPRADORField = value;
            }
        }

        public string CNPJCPFCOMPRADOR
        {
            get
            {
                return this.cNPJCPFCOMPRADORField;
            }
            set
            {
                this.cNPJCPFCOMPRADORField = value;
            }
        }

        public string DATANOTAFISCAL
        {
            get
            {
                return this.dATANOTAFISCALField;
            }
            set
            {
                this.dATANOTAFISCALField = value;
            }
        }

        public string DATAPROTOCOLODETRAN
        {
            get
            {
                return this.dATAPROTOCOLODETRANField;
            }
            set
            {
                this.dATAPROTOCOLODETRANField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTACOMUNICACAO_VENDA)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    // [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR
    {

        private string iNSPECAOVEICULARField;

        private string iNSPECAOVEICULARDESCField;

        private string dATAINSPECAOVEICULARField;

        private string dATAINCLUSACAOINSPECAOVEICULARField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string INSPECAOVEICULAR
        {
            get
            {
                return this.iNSPECAOVEICULARField;
            }
            set
            {
                this.iNSPECAOVEICULARField = value;
            }
        }

        public string INSPECAOVEICULARDESC
        {
            get
            {
                return this.iNSPECAOVEICULARDESCField;
            }
            set
            {
                this.iNSPECAOVEICULARDESCField = value;
            }
        }

        public string DATAINSPECAOVEICULAR
        {
            get
            {
                return this.dATAINSPECAOVEICULARField;
            }
            set
            {
                this.dATAINSPECAOVEICULARField = value;
            }
        }

        public string DATAINCLUSACAOINSPECAOVEICULAR
        {
            get
            {
                return this.dATAINCLUSACAOINSPECAOVEICULARField;
            }
            set
            {
                this.dATAINCLUSACAOINSPECAOVEICULARField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTAINSPECAO_VEICULAR)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    // [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO
    {

        private string oRIGEMField;

        private string aNOREFField;

        private string vALORField;

        private string dESCRICAOField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string ORIGEM
        {
            get
            {
                return this.oRIGEMField;
            }
            set
            {
                this.oRIGEMField = value;
            }
        }

        public string ANOREF
        {
            get
            {
                return this.aNOREFField;
            }
            set
            {
                this.aNOREFField = value;
            }
        }

        public string VALOR
        {
            get
            {
                return this.vALORField;
            }
            set
            {
                this.vALORField = value;
            }
        }

        public string DESCRICAO
        {
            get
            {
                return this.dESCRICAOField;
            }
            set
            {
                this.dESCRICAOField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                // streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                // sr.Close();
                // file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTATAXA_LICENCIAMENTO)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    // [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTADPVAT
    {

        private string oRIGEMField;

        private string aNOREFField;

        private string vALDPVATField;

        private string nUMPARCELAField;

        private string dTVENCTODPVATField;

        private string cATEGORIAField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string ORIGEM
        {
            get
            {
                return this.oRIGEMField;
            }
            set
            {
                this.oRIGEMField = value;
            }
        }

        public string ANOREF
        {
            get
            {
                return this.aNOREFField;
            }
            set
            {
                this.aNOREFField = value;
            }
        }

        public string VALDPVAT
        {
            get
            {
                decimal vDec = 0;
                if (!string.IsNullOrEmpty(this.vALDPVATField) && Decimal.TryParse(this.vALDPVATField, out vDec))
                {
                    vDec = vDec / 100;

                    return string.Format(CONSULTA2300.minhaCultura, "{0:C}", vDec);
                }
                else
                {
                    return this.vALDPVATField;
                }
            }
            set
            {
                this.vALDPVATField = value;
            }
        }

        public string NUMPARCELA
        {
            get
            {
                return this.nUMPARCELAField;
            }
            set
            {
                this.nUMPARCELAField = value;
            }
        }

        public string DTVENCTODPVAT
        {
            get
            {
                return this.dTVENCTODPVATField;
            }
            set
            {
                this.dTVENCTODPVATField = value;
            }
        }

        public string CATEGORIA
        {
            get
            {
                return this.cATEGORIAField;
            }
            set
            {
                this.cATEGORIAField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTADPVAT));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTADPVAT object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTADPVAT object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTADPVAT object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTADPVAT obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTADPVAT);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTADPVAT obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTADPVAT Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTADPVAT)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTADPVAT object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTADPVAT object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTADPVAT object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTADPVAT obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTADPVAT);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTADPVAT obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTADPVAT LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTADPVAT object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTADPVAT Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTADPVAT)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTAIPVA
    {

        private string oRIGEMField;

        private string aNOREFField;

        private string vALIPVAField;

        private string cOTAIPVAField;

        private string cOTAIPVADESField;

        private string dTVENCTOIPVAField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string ORIGEM
        {
            get
            {
                return this.oRIGEMField;
            }
            set
            {
                this.oRIGEMField = value;
            }
        }

        public string ANOREF
        {
            get
            {
                return this.aNOREFField;
            }
            set
            {
                this.aNOREFField = value;
            }
        }

        public string VALIPVA
        {
            get
            {
                decimal vDec = 0;
                if (!string.IsNullOrEmpty(this.vALIPVAField) && Decimal.TryParse(this.vALIPVAField, out vDec))
                {
                    vDec = vDec / 100;

                    return string.Format(CONSULTA2300.minhaCultura, "{0:C}", vDec);
                }
                else
                {
                    return this.vALIPVAField;
                }
            }
            set
            {
                this.vALIPVAField = value;
            }
        }

        public string COTAIPVA
        {
            get
            {
                return this.cOTAIPVAField;
            }
            set
            {
                this.cOTAIPVAField = value;
            }
        }

        public string COTAIPVADES
        {
            get
            {
                return this.cOTAIPVADESField;
            }
            set
            {
                this.cOTAIPVADESField = value;
            }
        }

        public string DTVENCTOIPVA
        {
            get
            {
                return this.dTVENCTOIPVAField;
            }
            set
            {
                this.dTVENCTOIPVAField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTAIPVA));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAIPVA object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTAIPVA object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAIPVA object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAIPVA obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAIPVA);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAIPVA obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAIPVA Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTAIPVA)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAIPVA object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTAIPVA object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAIPVA object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAIPVA obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAIPVA);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAIPVA obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAIPVA LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTAIPVA object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTAIPVA Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTAIPVA)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    // [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTANOTIFICACAO
    {

        private string nUMAIIPField;

        private string lOCALINFRAField;

        private string eNQUADRAMField;

        private string eNQUADRAMDESCField;

        private string dTINFRAField;

        private string hORAINFRAField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string NUMAIIP
        {
            get
            {
                return this.nUMAIIPField;
            }
            set
            {
                this.nUMAIIPField = value;
            }
        }

        public string LOCALINFRA
        {
            get
            {
                return this.lOCALINFRAField;
            }
            set
            {
                this.lOCALINFRAField = value;
            }
        }

        public string ENQUADRAM
        {
            get
            {
                return this.eNQUADRAMField;
            }
            set
            {
                this.eNQUADRAMField = value;
            }
        }

        public string ENQUADRAMDESC
        {
            get
            {
                return this.eNQUADRAMDESCField;
            }
            set
            {
                this.eNQUADRAMDESCField = value;
            }
        }

        public string DTINFRA
        {
            get
            {
                if (this.dTINFRAField.Length != 8)
                    return this.dTINFRAField;

                return string.Format("{0}/{1}/{2}", this.dTINFRAField.Substring(0, 2), this.dTINFRAField.Substring(2, 2), this.dTINFRAField.Substring(4, 4));
            }
            set
            {
                this.dTINFRAField = value;
            }
        }

        public string HORAINFRA
        {
            get
            {
                return this.hORAINFRAField;
            }
            set
            {
                this.hORAINFRAField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTANOTIFICACAO));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTANOTIFICACAO object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTANOTIFICACAO object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTANOTIFICACAO object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTANOTIFICACAO obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTANOTIFICACAO);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTANOTIFICACAO obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTANOTIFICACAO Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTANOTIFICACAO)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTANOTIFICACAO object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                // streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTANOTIFICACAO object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTANOTIFICACAO object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTANOTIFICACAO obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTANOTIFICACAO);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTANOTIFICACAO obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTANOTIFICACAO LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTANOTIFICACAO object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTANOTIFICACAO Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTANOTIFICACAO)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTAMULTA
    {

        private string oRIGEMField;

        private string nUMGUIAField;

        private string nUMAIIPField;

        private string cODRECField;

        private string dTINFRAField;

        private string lOCALINFRAField;

        private string hORAINFRAField;

        private string mUNINFRAField;

        private string eNQUADRAMField;

        private string eNQUADRAMDESCField;

        private string eNQUADRAMLEGField;

        private string dTVENCTOINFRAField;

        private string vALMULTAField;

        private string eNTTRANSITANTField;

        private string nOMEENTTRANSITANTField;

        private string mUNINFRADESCField;

        private string eMPRESAField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string ORIGEM
        {
            get
            {
                return this.oRIGEMField;
            }
            set
            {
                this.oRIGEMField = value;
            }
        }

        public string NUMGUIA
        {
            get
            {
                return this.nUMGUIAField;
            }
            set
            {
                this.nUMGUIAField = value;
            }
        }

        public string NUMAIIP
        {
            get
            {
                return this.nUMAIIPField;
            }
            set
            {
                this.nUMAIIPField = value;
            }
        }

        public string CODREC
        {
            get
            {
                return this.cODRECField;
            }
            set
            {
                this.cODRECField = value;
            }
        }

        public string DTINFRA
        {
            get
            {
                if (this.dTINFRAField.Length != 8)
                    return this.dTINFRAField;

                return string.Format("{0}/{1}/{2}", this.dTINFRAField.Substring(0, 2), this.dTINFRAField.Substring(2, 2), this.dTINFRAField.Substring(4, 4));
            }
            set
            {
                this.dTINFRAField = value;
            }
        }

        public string LOCALINFRA
        {
            get
            {
                return this.lOCALINFRAField;
            }
            set
            {
                this.lOCALINFRAField = value;
            }
        }

        public string HORAINFRA
        {
            get
            {
                return this.hORAINFRAField;
            }
            set
            {
                this.hORAINFRAField = value;
            }
        }

        public string MUNINFRA
        {
            get
            {
                return this.mUNINFRAField;
            }
            set
            {
                this.mUNINFRAField = value;
            }
        }

        public string ENQUADRAM
        {
            get
            {
                return this.eNQUADRAMField;
            }
            set
            {
                this.eNQUADRAMField = value;
            }
        }

        public string ENQUADRAMDESC
        {
            get
            {
                return this.eNQUADRAMDESCField;
            }
            set
            {
                this.eNQUADRAMDESCField = value;
            }
        }

        public string ENQUADRAMLEG
        {
            get
            {
                return this.eNQUADRAMLEGField;
            }
            set
            {
                this.eNQUADRAMLEGField = value;
            }
        }

        public string DTVENCTOINFRA
        {
            get
            {

                if (this.dTVENCTOINFRAField.Length != 8)
                    return this.dTVENCTOINFRAField;

                return string.Format("{0}/{1}/{2}", this.dTVENCTOINFRAField.Substring(0, 2), this.dTVENCTOINFRAField.Substring(2, 2), this.dTVENCTOINFRAField.Substring(4, 4));
            }
            set
            {
                this.dTVENCTOINFRAField = value;
            }
        }

        public string VALMULTA
        {
            get
            {

                decimal vDec = 0;
                if (!string.IsNullOrEmpty(this.vALMULTAField) && Decimal.TryParse(this.vALMULTAField, out vDec))
                {
                    vDec = vDec / 100;

                    return string.Format(CONSULTA2300.minhaCultura, "{0:C}", vDec);
                    //return vDec.ToString("C");
                }
                else
                {
                    return this.vALMULTAField;
                }
            }
            set
            {
                this.vALMULTAField = value;
            }
        }

        public string ENTTRANSITANT
        {
            get
            {
                return this.eNTTRANSITANTField;
            }
            set
            {
                this.eNTTRANSITANTField = value;
            }
        }

        public string NOMEENTTRANSITANT
        {
            get
            {
                return this.nOMEENTTRANSITANTField;
            }
            set
            {
                this.nOMEENTTRANSITANTField = value;
            }
        }

        public string MUNINFRADESC
        {
            get
            {
                return this.mUNINFRADESCField;
            }
            set
            {
                this.mUNINFRADESCField = value;
            }
        }

        public string EMPRESA
        {
            get
            {
                return this.eMPRESAField;
            }
            set
            {
                this.eMPRESAField = value;
            }
        }

        public string NOTIFICACAO { get; set; }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTAMULTA));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAMULTA object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTAMULTA object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAMULTA object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAMULTA obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAMULTA);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAMULTA obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAMULTA Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTAMULTA)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAMULTA object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTAMULTA object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAMULTA object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAMULTA obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAMULTA);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAMULTA obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAMULTA LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTAMULTA object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTAMULTA Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTAMULTA)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTAMULTA1
    {

        private string oRIGEMField;

        private string nUMGUIAField;

        private string nUMAIIPField;

        private string cODRECField;

        private string dTINFRAField;

        private string lOCALINFRAField;

        private string hORAINFRAField;

        private string mUNINFRAField;

        private string eNQUADRAMField;

        private string eNQUADRAMDESCField;

        private string eNQUADRAMLEGField;

        private string dTVENCTOINFRAField;

        private string vALMULTAField;

        private string eNTTRANSITANTField;

        private string nOMEENTTRANSITANTField;

        private string mUNINFRADESCField;

        private string eMPRESAField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string ORIGEM
        {
            get
            {
                return this.oRIGEMField;
            }
            set
            {
                this.oRIGEMField = value;
            }
        }

        public string NUMGUIA
        {
            get
            {
                return this.nUMGUIAField;
            }
            set
            {
                this.nUMGUIAField = value;
            }
        }

        public string NUMAIIP
        {
            get
            {
                return this.nUMAIIPField;
            }
            set
            {
                this.nUMAIIPField = value;
            }
        }

        public string CODREC
        {
            get
            {
                return this.cODRECField;
            }
            set
            {
                this.cODRECField = value;
            }
        }

        public string DTINFRA
        {
            get
            {
                return this.dTINFRAField;
            }
            set
            {
                this.dTINFRAField = value;
            }
        }

        public string LOCALINFRA
        {
            get
            {
                return this.lOCALINFRAField;
            }
            set
            {
                this.lOCALINFRAField = value;
            }
        }

        public string HORAINFRA
        {
            get
            {
                return this.hORAINFRAField;
            }
            set
            {
                this.hORAINFRAField = value;
            }
        }

        public string MUNINFRA
        {
            get
            {
                return this.mUNINFRAField;
            }
            set
            {
                this.mUNINFRAField = value;
            }
        }

        public string ENQUADRAM
        {
            get
            {
                return this.eNQUADRAMField;
            }
            set
            {
                this.eNQUADRAMField = value;
            }
        }

        public string ENQUADRAMDESC
        {
            get
            {
                return this.eNQUADRAMDESCField;
            }
            set
            {
                this.eNQUADRAMDESCField = value;
            }
        }

        public string ENQUADRAMLEG
        {
            get
            {
                return this.eNQUADRAMLEGField;
            }
            set
            {
                this.eNQUADRAMLEGField = value;
            }
        }

        public string DTVENCTOINFRA
        {
            get
            {
                return this.dTVENCTOINFRAField;
            }
            set
            {
                this.dTVENCTOINFRAField = value;
            }
        }

        public string VALMULTA
        {
            get
            {
                return this.vALMULTAField;
            }
            set
            {
                this.vALMULTAField = value;
            }
        }

        public string ENTTRANSITANT
        {
            get
            {
                return this.eNTTRANSITANTField;
            }
            set
            {
                this.eNTTRANSITANTField = value;
            }
        }

        public string NOMEENTTRANSITANT
        {
            get
            {
                return this.nOMEENTTRANSITANTField;
            }
            set
            {
                this.nOMEENTTRANSITANTField = value;
            }
        }

        public string MUNINFRADESC
        {
            get
            {
                return this.mUNINFRADESCField;
            }
            set
            {
                this.mUNINFRADESCField = value;
            }
        }

        public string EMPRESA
        {
            get
            {
                return this.eMPRESAField;
            }
            set
            {
                this.eMPRESAField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTAMULTA1));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAMULTA1 object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTAMULTA1 object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAMULTA1 object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAMULTA1 obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAMULTA1);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTAMULTA1 obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAMULTA1 Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTAMULTA1)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTAMULTA1 object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTAMULTA1 object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTAMULTA1 object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAMULTA1 obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTAMULTA1);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTAMULTA1 obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTAMULTA1 LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                // sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTAMULTA1 object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTAMULTA1 Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTAMULTA1)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD
    {

        private string cODIGOJUDICIALField;

        private string cODIGOTRIBUNALField;

        private string dATAINCLUSAOField;

        private string hORAINCLUSAOField;

        private string nUMEROPROCESSOField;

        private string oRGAO_JUDICIALField;

        private string tIPO_RESTRICField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string CODIGOJUDICIAL
        {
            get
            {
                return this.cODIGOJUDICIALField;
            }
            set
            {
                this.cODIGOJUDICIALField = value;
            }
        }

        public string CODIGOTRIBUNAL
        {
            get
            {
                return this.cODIGOTRIBUNALField;
            }
            set
            {
                this.cODIGOTRIBUNALField = value;
            }
        }

        public string DATAINCLUSAO
        {
            get
            {
                return this.dATAINCLUSAOField;
            }
            set
            {
                this.dATAINCLUSAOField = value;
            }
        }

        public string HORAINCLUSAO
        {
            get
            {
                return this.hORAINCLUSAOField;
            }
            set
            {
                this.hORAINCLUSAOField = value;
            }
        }

        public string NUMEROPROCESSO
        {
            get
            {
                return this.nUMEROPROCESSOField;
            }
            set
            {
                this.nUMEROPROCESSOField = value;
            }
        }

        public string ORGAO_JUDICIAL
        {
            get
            {
                return this.oRGAO_JUDICIALField;
            }
            set
            {
                this.oRGAO_JUDICIALField = value;
            }
        }

        public string TIPO_RESTRIC
        {
            get
            {
                return this.tIPO_RESTRICField;
            }
            set
            {
                this.tIPO_RESTRICField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                //sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTABLOQUEIORENAJUD)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN
    {

        private CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN bLOQUEIODETRANField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN BLOQUEIODETRAN
        {
            get
            {
                return this.bLOQUEIODETRANField;
            }
            set
            {
                this.bLOQUEIODETRANField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                //streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                // sr.Close();
                // file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRAN)(this.MemberwiseClone()));
        }
        #endregion
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.8745")]
    //[System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN
    {

        private string tIPOField;

        private string mOTIVOField;

        private string dATAINCLUSAOField;

        private string pROTOCOLO_NUMEROField;

        private string pROTOCOLO_ANOField;

        private string pROCESSO_NUMEROField;

        private string pROCESSO_ANOField;

        private string oFICIO_NUMEROField;

        private string oFICIO_ANOField;

        private string iDCIDADEField;

        private string mUNICIPIOField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public string TIPO
        {
            get
            {
                return this.tIPOField;
            }
            set
            {
                this.tIPOField = value;
            }
        }

        public string MOTIVO
        {
            get
            {
                return this.mOTIVOField;
            }
            set
            {
                this.mOTIVOField = value;
            }
        }

        public string DATAINCLUSAO
        {
            get
            {
                if (this.dATAINCLUSAOField.Length != 8)
                    return this.dATAINCLUSAOField;

                return string.Format("{0}/{1}/{2}", this.dATAINCLUSAOField.Substring(0, 2), this.dATAINCLUSAOField.Substring(2, 2), this.dATAINCLUSAOField.Substring(4, 4));
            }
            set
            {
                this.dATAINCLUSAOField = value;
            }
        }

        public string PROTOCOLO_NUMERO
        {
            get
            {
                return this.pROTOCOLO_NUMEROField;
            }
            set
            {
                this.pROTOCOLO_NUMEROField = value;
            }
        }

        public string PROTOCOLO_ANO
        {
            get
            {
                return this.pROTOCOLO_ANOField;
            }
            set
            {
                this.pROTOCOLO_ANOField = value;
            }
        }

        public string PROCESSO_NUMERO
        {
            get
            {
                return this.pROCESSO_NUMEROField;
            }
            set
            {
                this.pROCESSO_NUMEROField = value;
            }
        }

        public string PROCESSO_ANO
        {
            get
            {
                return this.pROCESSO_ANOField;
            }
            set
            {
                this.pROCESSO_ANOField = value;
            }
        }

        public string OFICIO_NUMERO
        {
            get
            {
                return this.oFICIO_NUMEROField;
            }
            set
            {
                this.oFICIO_NUMEROField = value;
            }
        }

        public string OFICIO_ANO
        {
            get
            {
                return this.oFICIO_ANOField;
            }
            set
            {
                this.oFICIO_ANOField = value;
            }
        }

        public string IDCIDADE
        {
            get
            {
                return this.iDCIDADEField;
            }
            set
            {
                this.iDCIDADEField = value;
            }
        }

        public string MUNICIPIO
        {
            get
            {
                return this.mUNICIPIOField;
            }
            set
            {
                this.mUNICIPIOField = value;
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN));
                }
                return serializer;
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                // streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                // sr.Close();
                //file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN object
        /// </summary>
        public virtual CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN Clone()
        {
            return ((CONSULTA2300CONSULTASCONSULTABLOQUEIOSDETRANBLOQUEIODETRAN)(this.MemberwiseClone()));
        }
        #endregion
    }
}
