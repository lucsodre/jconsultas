﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using EntityLayer.Models;
using Microsoft.AspNetCore.Hosting;

namespace JConsultas.Models
{
    public static class Helper
    {
        #region LogError
        public static void LogError(string strHeader, string strError, IHostingEnvironment _env)
        {
            string strPath;
            string strActualError;
            DateTime objDt = DateTime.Now;

            string strDate;
            //strDate = objDt.ToString("ddMMyyyy");
            strDate = objDt.Ticks.ToString();
            strPath = _env.WebRootPath + $@"\ErrorLog\\";

            //Generates Path & LogFile Name of  ErrorLog                
            EnsureDirectory(new System.IO.DirectoryInfo(strPath));
            strPath = strPath + strDate + ".log";

            // Generates Error Message
            strActualError = DateTime.Now + " : " + strHeader + " : " + strError;

            // Creation of File.
            var logFile = System.IO.File.Create(strPath);
            var logWriter = new System.IO.StreamWriter(logFile);
            logWriter.WriteLine(strActualError);
            logWriter.Dispose();
        }
        public static void GravarLog(string Log, string strCaminhoLog)
        {
            try
            {
                DateTime dtAtual = DateTime.Now;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(string.Format("{0}/{1}/{2} {3}:{4}:{5} {6} >> {7}{8}", dtAtual.Day.ToString(), dtAtual.Month.ToString(), dtAtual.Year.ToString(), dtAtual.Hour.ToString(), dtAtual.Minute.ToString(), dtAtual.Second.ToString(), dtAtual.Millisecond.ToString(), Log, Environment.NewLine));

                string caminhoLogParametro = string.Format(@"{0}\", strCaminhoLog);

                if (!Directory.Exists(caminhoLogParametro))
                    Directory.CreateDirectory(caminhoLogParametro);

                caminhoLogParametro = string.Format(@"{0}\Log_{1}{2}{3}.txt", strCaminhoLog, DateTime.Today.Day.ToString("00"), DateTime.Today.Month.ToString("00"), DateTime.Today.Year.ToString("0000"));

                File.AppendAllText(caminhoLogParametro, sb.ToString(), System.Text.Encoding.UTF8);

            }
            catch (Exception ex)
            {
               
            }
        }
        #endregion

        #region EnsureDirectory
        public static void EnsureDirectory(System.IO.DirectoryInfo oDirInfo)
        {
            if (!oDirInfo.Exists)
            {
                oDirInfo.Create();
            }
        }
        #endregion EnsureDirectory
    }
}
