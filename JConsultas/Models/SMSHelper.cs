﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Amazon;

namespace JConsultas.Models
{
    public class SMSHelper
    {
        public AmazonSimpleNotificationServiceClient smsClient;

        public SMSHelper(string awsKey, string awsToken)
        {
            smsClient = new AmazonSimpleNotificationServiceClient(awsKey, awsToken, RegionEndpoint.USEast1);
        }

        public async Task<PublishResponse> Send(string Message, string PhoneNumber)
        {
            PublishResponse _PublishResponse = new PublishResponse();
            try
            {
                PublishRequest publishRequest = new PublishRequest();

                publishRequest.Message = Message;
                publishRequest.PhoneNumber = "+55" + PhoneNumber.Replace("(","").Replace(")","").Replace("-","").Replace(" ","");

                _PublishResponse = await smsClient.PublishAsync(publishRequest);
                return _PublishResponse;
            }
            catch (Exception ex)
            {
                _PublishResponse.MessageId = null;
                return _PublishResponse;
            }
        }

    }
}
