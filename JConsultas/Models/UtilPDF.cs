﻿using EntityLayer.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JConsultas.Models
{
    public class UtilPDF
    {
        public static string GenerateHtmlViewPDF(CONSULTA2300 objCONSULTA2300, string urlSite)
        {
            var veiculoData = objCONSULTA2300.CONSULTAS.CONSULTA.VEICULO;
            var proprietarioData = objCONSULTA2300.CONSULTAS.CONSULTA.PROPRIETARIO;
            var gravameData = objCONSULTA2300.CONSULTAS.CONSULTA.GRAVAME;
            var gravamE_INTENCAOData = objCONSULTA2300.CONSULTAS.CONSULTA.GRAVAME_INTENCAO;
            var debitosData = objCONSULTA2300.CONSULTAS.CONSULTA.TOTAL_DEBITOS;
            var restricoesData = objCONSULTA2300.CONSULTAS.CONSULTA.RESTRICOES;
            var multasData = objCONSULTA2300.CONSULTAS.CONSULTA.MULTAS;

            var comunicacaoVendaData = objCONSULTA2300.CONSULTAS.CONSULTA.COMUNICACAO_VENDA;
            var debitosIPVAData = objCONSULTA2300.CONSULTAS.CONSULTA.IPVAS;
            var debitosDPVATData = objCONSULTA2300.CONSULTAS.CONSULTA.DPVATS;
            var bloqueiosDetranData = objCONSULTA2300.CONSULTAS.CONSULTA.BLOQUEIOSDETRAN;
            var inspecaoVeicularData = objCONSULTA2300.CONSULTAS.CONSULTA.INSPECAO_VEICULAR;

            var consultaAvancadaVeic = objCONSULTA2300.CONSULTAS.CONSULTA.CONSULTAAVANCADAVEICULO;

            List<string> parametrosConsulta = new List<string>();
            bool pesquisaCompleta = true;

            #region Condições da tela
            if (consultaAvancadaVeic != null)
            {
                if (!consultaAvancadaVeic.DadosVeic)
                {
                    veiculoData = null;
                    pesquisaCompleta = false;
                }
                else
                {
                    parametrosConsulta.Add("Dados do veículo");
                }

                if (!consultaAvancadaVeic.ArrendarioFinanceira)
                {
                    gravameData = null;
                    pesquisaCompleta = false;
                }
                else
                {
                    parametrosConsulta.Add("Gravame");
                }

                if (!consultaAvancadaVeic.BloqueiosDetran)
                {
                    bloqueiosDetranData = null;
                    pesquisaCompleta = false;
                }
                else
                {
                    parametrosConsulta.Add("Bloqueios Detran");
                }

                if (!consultaAvancadaVeic.DadosProp)
                {
                    proprietarioData = null;
                    pesquisaCompleta = false;
                }
                else
                {
                    parametrosConsulta.Add("Dados do proprietário");
                }

                if (!consultaAvancadaVeic.DetalhamentoDebitos)
                {
                    debitosData = null;
                    pesquisaCompleta = false;
                }
                else
                {
                    parametrosConsulta.Add("Detalhamento dos débitos");
                }

                if (!consultaAvancadaVeic.DetalhamentoDPVAT)
                {
                    debitosDPVATData = null;
                    pesquisaCompleta = false;
                }
                else
                {
                    parametrosConsulta.Add("Detalhamento DPVAT");
                }

                if (!consultaAvancadaVeic.DetalhamentoIPVA)
                {
                    debitosIPVAData = null;
                    pesquisaCompleta = false;
                }
                else
                {
                    parametrosConsulta.Add("Detalhamento IPVA");
                }

                if (!consultaAvancadaVeic.DetalhamentoMultas)
                {
                    multasData = null;
                    pesquisaCompleta = false;
                }
                else
                {
                    parametrosConsulta.Add("Detalhamento das multas");
                }

                if (!consultaAvancadaVeic.InspecaoVeicular)
                {
                    inspecaoVeicularData = null;
                    pesquisaCompleta = false;
                }
                else
                {
                    parametrosConsulta.Add("Inspeção veicular");
                }

                if (!consultaAvancadaVeic.IntencaoGravame)
                {
                    gravamE_INTENCAOData = null;
                    pesquisaCompleta = false;
                }
                else
                {
                    parametrosConsulta.Add("Intenção de gravame");
                }

                if (!consultaAvancadaVeic.Restricoes)
                {
                    restricoesData = null;
                    pesquisaCompleta = false;
                }
                else
                {
                    parametrosConsulta.Add("Restrições");
                }
            }
            else
            {
                consultaAvancadaVeic = new ConsultaAvancadaVeiculo();
                consultaAvancadaVeic.ArrendarioFinanceira = true;
                consultaAvancadaVeic.BloqueiosDetran = true;
                consultaAvancadaVeic.DadosProp = true;
                consultaAvancadaVeic.DadosVeic = true;
                consultaAvancadaVeic.DetalhamentoDebitos = true;
                consultaAvancadaVeic.DetalhamentoDPVAT = true;
                consultaAvancadaVeic.DetalhamentoIPVA = true;
                consultaAvancadaVeic.DetalhamentoMultas = true;
                consultaAvancadaVeic.InspecaoVeicular = true;
                consultaAvancadaVeic.IntencaoGravame = true;
                consultaAvancadaVeic.Restricoes = true;
            }
            #endregion

            string sData = "";

            #region "Generate html for pdf"
        
            sData += "<table>";
            sData += "<tr>";
            sData += "<td width = '50%' align='left' colspan='2'><h2>Consulta veículo</h2></td>";
            sData += "<td width = '50%' align='right'><img width='120' src='" + urlSite + "//assets//image-resources//logosgd.png' /></td>";
            sData += "</tr>";
            sData += "</table>";

            if (veiculoData != null)
            {
                #region "Dados veículo"
                sData += "<table>";
                sData += "<tr bgcolor='#ACDBF4'>";
                sData += "<th><b>Dados do veiculo</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Placa</b> </td>";
                sData += "<td width = '20%' align='left'>" + (veiculoData.PLACA ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Estado</b> </td>";
                sData += "<td width = '30%' align='left'>" + (veiculoData.UF ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Chassi</b> </td>";
                sData += "<td width = '20%' align='left'>" + (veiculoData.CHASSI ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Munícipio</b> </td>";
                sData += "<td width = '30%' align='left'>" + (veiculoData.MUNICIPIODESC ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Procedência</b> </td>";
                sData += "<td width = '20%' align='left'>" + ((veiculoData.PROCEDENCIA ?? "") + "" + (veiculoData.PROCEDENCIADESC ?? "")) ?? "-" + "</td>";
                sData += "<td width = '20%' align='right'><b>Renavam</b> </td>";
                sData += "<td width = '30%' align='left'>" + (veiculoData.RENAVAM ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Ano modelo</b> </td>";
                sData += "<td width = '20%' align='left'>" + (veiculoData.ANOMODELO ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Carroceria</b> </td>";
                sData += "<td width = '30%' align='left'>" + (veiculoData.CARROCERIADESC ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Ano de fabricação</b> </td>";
                sData += "<td width = '20%' align='left'>" + (veiculoData.ANOFABRICACAO ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Nr. carroceria</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (veiculoData.NR_CARROCERIA ?? "-") + " </td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Tipo</b> </td>";
                sData += "<td width = '20%' align='left'>" + (veiculoData.TIPODESC ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Cilindrada</b> </td>";
                sData += "<td width = '30%' align='left'>" + (veiculoData.CILINDRADA ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Cor</b> </td>";
                sData += "<td width = '20%' align='left'>" + ((veiculoData.COR ?? "") + "" + (veiculoData.CORDESC ?? "")) ?? "-" + "</td>";
                sData += "<td width = '20%' align='right'><b>PBT</b> </td>";
                sData += "<td width = '30%' align='left'>" + (veiculoData.PBT ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Combustível</b> </td>";
                sData += "<td width = '20%' align='left'>" + ((veiculoData.COMBUSTIVEL ?? "") + "" + (veiculoData.COMBUSTIVELDESC ?? "")) ?? "-" + " </td>";
                sData += "<td width = '20%' align='right'><b>Cap. passageiro</b> </td>";
                sData += "<td width = '30%' align='left'>" + (veiculoData.CAPACIDADEPASSAGEIRO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Marca</b> </td>";
                sData += "<td width = '20%' align='left'>" + (veiculoData.MARCA ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Potência</b> </td>";
                sData += "<td width = '30%' align='left'>" + (veiculoData.POTENCIA ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Espécie</b> </td>";
                sData += "<td width = '20%' align='left'>" + ((veiculoData.ESPECIE ?? "") + "" + (veiculoData.ESPECIEDESC ?? "")) ?? "-" + " </td>";
                sData += "<td width = '20%' align='right'><b>CMT</b> </td>";
                sData += "<td width = '30%' align='left'>" + (veiculoData.CMT ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Cap. carga</b> </td>";
                sData += "<td width = '20%' align='left'>" + (veiculoData.CAPACIDADECARGA ?? "-") + " </td>";
                sData += "<td width = '20%' align='right'><b>Eixos</b> </td>";
                sData += "<td width = '30%' align='left'>" + (veiculoData.EIXOS ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Categoria</b> </td>";
                sData += "<td width = '20%' align='left'>" + (string.Join(" ", veiculoData.CATEGORIA, veiculoData.CATEGORIADESC) ?? "-") + " </td>";
                sData += "<td width = '20%' align='right'><b>Data alteração</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (veiculoData.DATAALTERACAO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Número do motor</b> </td>";
                sData += "<td width = '20%' align='left'>" + (veiculoData.NUMEROMOTOR ?? "-") + " </td>";
                sData += "<td width = '20%' align='right'><b>Tipo remarcação do chassi</b> </td>";
                sData += "<td width = '30%' align='left'>" + (veiculoData.TIPOREMARCACAOCHASSI ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Situação veículo</b> </td>";
                sData += "<td width = '20%' align='left' colspan='3'>" + (veiculoData.SITUACAOVEICULO ?? "-") + " </td>";
                sData += "</tr>";
                sData += "</table>";
                #endregion
            }

            if (proprietarioData != null)
            {
                #region "Dados do proprietario"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; ' >";
                sData += "<tr  bgcolor='#ACDBF4' >";
                sData += "<th><b>Dados do proprietário</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";

                sData += "<tr>";
                sData += "<td width='25%' align='right'><b>Nome</b> </td>";
                sData += "<td width='100%' align='left' colspan='3'" + (proprietarioData.NOMEPROPRIETARIO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width='25%' align='right'><b>Proprietário anterior</b> </td>";
                sData += "<td width='100%' align='left' colspan='3'>" + (proprietarioData.NOMEPROPRIETARIOANTERIOR ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width='25%' align='right'><b>CPF/CNPJ</b> </td>";
                sData += "<td width='20%' align='left'>" + (proprietarioData.CPFPROPRIETARIO ?? "-") + "</td>";
                sData += "<td width='20%' align='right'><b>RG</b> </td>";
                sData += "<td width='30%' align='left'>" + (proprietarioData.RG ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width='25%' align='right'><b>Orgão de expedição</b> </td>";
                sData += "<td width='20%' align='left'>" + (proprietarioData.ORGEXP ?? "-") + "</td>";
                sData += "<td width='20%' align='right'><b>UF do orgão</b> </td>";
                sData += "<td width='30%' align='left'>" + "-" + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width='25%' align='right'><b>Endereço do proprietário</b> </td>";
                sData += "<td width='20%' align='left'>" + (proprietarioData.ENDERECO ?? "-") + "</td>";
                sData += "<td width='20%' align='right'><b>Número</b> </td>";
                sData += "<td width='30%' align='left'>" + (proprietarioData.NUMERO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width='25%' align='right'><b>Complemento</b> </td>";
                sData += "<td width='20%' align='left'>" + (proprietarioData.COMPLEMENTO ?? "-") + "</td>";
                sData += "<td width='20%' align='right'><b>Bairro</b> </td>";
                sData += "<td width='30%' align='left'>" + (proprietarioData.BAIRRO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width='25%' align='right'><b>Cidade</b> </td>";
                sData += "<td width='20%' align='left'>" + (proprietarioData.DDD ?? "-") + "</td>";
                sData += "<td width='20%' align='right'><b>Cep</b> </td>";
                sData += "<td width='30%' align='left'>" + (proprietarioData.CEP ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width='25%' align='right'><b>DDD</b> </td>";
                sData += "<td width='20%' align='left'>" + (proprietarioData.DDD ?? "-") + "</td>";
                sData += "<td width='20%' align='right'><b>Telefone</b> </td>";
                sData += "<td width='30%' align='left'>" + (proprietarioData.TELEFONE ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width='25%' align='right'><b>CPF/CNPJ faturado</b> </td>";
                sData += "<td width='20%' align='left'>" + "-" + "</td>";
                sData += "<td width='20%' align='right'><b></b> </td>";
                sData += "<td width='30%' align='left'>" + "</td>";
                sData += "</tr>";
                sData += "</table>";
                #endregion
            }

            if (gravameData != null)
            {
                #region "Arrendario/Financeira"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; ' >";
                sData += "<tr  bgcolor='#ACDBF4' >";
                sData += "<th><b>Arrendatário / Financeira</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width='25%' align='right'><b>Situação</b></td>";
                sData += "<td width='20%' align='left'>" + (gravameData.SITUACAO ?? "-") + "</td>";
                sData += "<td width='20%' align='right'><b></b> </td> ";
                sData += "<td width='30%' align='left'>" + "</td>";
                sData += "</tr> ";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Restrição financeira</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (gravameData.TIPORESTRICAOFINANCEIRADESC ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Nº contrato</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (gravameData.NUMEROCONTRATO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Financeira</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (gravameData.AGENTEFINANCEIRODESC ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>CNPJ financeira</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (gravameData.CNPJFINANCEIRA ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Arrendatário/Financiado</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (gravameData.ARRENDATARIOFINANCIADO ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>CNPJ/CPF do financiado</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (gravameData.CNPJFINANCIADO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Data vigência contrato</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (gravameData.DATAVIGENCIACONTRATO ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Data do contrato</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (gravameData.DATACONTRATO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Nome agente</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + "-" + "</td>";
                sData += "<td width = '20%' align='right'><b></b> </td>";
                sData += "<td width = '30%' align= 'left' >" + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Data da inclusão</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (gravameData.DATAINCLUSAOFINANCIAMENTO ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b></b> </td>";
                sData += "<td width = '30%' align= 'left' >" + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>UF gravame</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (gravameData.UF_GRAVAME ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Número restrição</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (gravameData.NUMERORESTRICAO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "</table>";
                #endregion
            }

            if (gravamE_INTENCAOData != null)
            {
                #region "Intecao de gravame"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; ' >";
                sData += "<tr  bgcolor='#ACDBF4' >";
                sData += "<th><b>Intenção de gravame</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Situação</b> </td>";
                sData += "<td width = '75%' align='left' colspan='3'>" + (gravameData.SITUACAO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Tipo de transacao</b> </td>";
                sData += "<td width = '20%' align='left'>" + (gravamE_INTENCAOData.TIPOTRANSACAODESC ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Tipo de restricao financeira</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (gravamE_INTENCAOData.TIPORESTRICAOFINANCEIRADESC ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Nome do agente financeiro</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (gravamE_INTENCAOData.AGENTEFINANCEIRODESC ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>CNPJ financeira</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (gravamE_INTENCAOData.CNPJFINANCEIRA ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Nome do agente financiado</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (gravamE_INTENCAOData.NOMEFINANCIADO ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>CNPJ do financiado</b> </td>";
                sData += "<td width = '30%' align='left'>" + (gravamE_INTENCAOData.CNPJFINANCIADO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Código agente financeira</b> </td>";
                sData += "<td width = '20%' align='left'>" + (gravamE_INTENCAOData.AGENTEFINANCEIRO ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Número do contrato</b> </td>";
                sData += "<td width = '30%' align='left'>" + (gravamE_INTENCAOData.NUMEROCONTRATOFINANC ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Data vigência contrato</b> </td>";
                sData += "<td width = '20%' align='left'>" + (gravameData.DATAVIGENCIACONTRATO ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b></b> </td>";
                sData += "<td width = '30%' align='left'></td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Data da inclusão<br /> intenção/troca financ.</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (gravamE_INTENCAOData.DATAINCLUSAOINTENCAOTROCAFINANC ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Informante do <br /> financiamento</b> </td>";
                sData += "<td width = '30%' align='left'>" + "-" + "</td>";
                sData += "</tr>";
                sData += "</table>";

                #region "Licenciamento"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; ' >";
                sData += "<tr  bgcolor='#ACDBF4' >";
                sData += "<th><b>Licenciamento</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Exercício licenciamento</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (veiculoData.EXERCICIOLICENCIAMENTO ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Data licenciamento</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (FormataData(veiculoData.DATALICENCIAMENTO) ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Data de emissão CRV</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (FormataData(veiculoData.DATAEMISSAOCRV) ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Última atualização <br />despachante</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + "-" + "</td>";
                sData += "</tr>";
                sData += "</table>";
                #endregion

                #endregion
            }

            if (restricoesData != null)
            {
                #region "Restrições"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; ' >";
                sData += "<tr  bgcolor='#ACDBF4' >";
                sData += "<th><b>Restrições</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Restrição 1</b> </td>";
                sData += "<td width = '75%' align= 'left' >" + ((restricoesData.Count() > 1 ? restricoesData[0] : "") ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Restrição 2</b> </td>";
                sData += "<td width = '75%' align= 'left' >" + ((restricoesData.Count() > 2 ? restricoesData[1] : "") ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Restrição 3</b> </td>";
                sData += "<td width = '75%' align= 'left' >" + ((restricoesData.Count() > 3 ? restricoesData[2] : "") ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Restrição 4</b> </td>";
                sData += "<td width = '75%' align= 'left' >" + ((restricoesData.Count() > 4 ? restricoesData[3] : "") ?? "-") + "</td>";
                sData += "</tr>";
                sData += "</table>";
                #endregion

                #region "Comunicação de Vendas"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; ' >";
                sData += "<tr  bgcolor='#ACDBF4' >";
                sData += "<th><b>Comunicação de vendas</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Comunicação de vendas</b> </td>";
                sData += "<td width = '75%' align= 'left' colspan='3' >" + (comunicacaoVendaData.COMUNICACAOVENDADESC ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Data protocolo Detran</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (FormataData(comunicacaoVendaData.DATAPROTOCOLODETRAN) ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Data nota fiscal</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (FormataData(comunicacaoVendaData.DATANOTAFISCAL) ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Data inclusão</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (FormataData(comunicacaoVendaData.DATAINCLUSAOCOMUNICACAOVENDAS) ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Data venda</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (FormataData(comunicacaoVendaData.DATAVENDA) ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Tipo documento comprador</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (comunicacaoVendaData.TIPODOCTOCOMPRADOR ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>CNPJ/CPF comprador</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + (comunicacaoVendaData.CNPJCPFCOMPRADOR ?? "-") + "</td>";
                sData += "</tr>";
                sData += "</table>";
                #endregion
            }

            if (inspecaoVeicularData != null)
            {
                #region "Inspeção veicular"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; ' >";
                sData += "<tr  bgcolor='#ACDBF4' >";
                sData += "<th><b>Inspeção veicular</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Data da inspeção veicular</b> </td>";
                sData += "<td width = '20%' align= 'left' >" + (FormataData(inspecaoVeicularData.DATAINSPECAOVEICULAR) ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b>Tipo de restrição financeira</b> </td>";
                sData += "<td width = '30%' align= 'left' >" + "-" + "</td>";
                sData += "</tr>";
                sData += "</table>";
                #endregion
            }

            if (bloqueiosDetranData != null && bloqueiosDetranData.BLOQUEIODETRAN != null)
            {
                #region "Bloqueios Detran"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; ' >";
                sData += "<tr  bgcolor='#ACDBF4' >";
                sData += "<th><b>Bloqueios Detran</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";

                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Situação</b> </td>";
                sData += "<td width = '20%' align='left' >" + (bloqueiosDetranData.BLOQUEIODETRAN.TIPO ?? "-") + "</td>";
                sData += "<td width = '20%' align='right'><b></b> </td>";
                sData += "<td width = '30%' align= 'left' >" + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Data de inclusão</b> </td>";
                sData += "<td width = '75%' align='left' colspan='3'>" + (FormataData(bloqueiosDetranData.BLOQUEIODETRAN.DATAINCLUSAO) ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Número protocolo</b> </td>";
                sData += "<td width = '75%' align='left' colspan='3'>" + (bloqueiosDetranData.BLOQUEIODETRAN.PROTOCOLO_NUMERO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Ano protocolo</b> </td>";
                sData += "<td width = '75%' align='left' colspan='3'>" + (bloqueiosDetranData.BLOQUEIODETRAN.PROTOCOLO_ANO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Número processo</b> </td>";
                sData += "<td width = '75%' align='left' colspan='3'>" + (bloqueiosDetranData.BLOQUEIODETRAN.PROCESSO_NUMERO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Ano do processo</b> </td>";
                sData += "<td width = '75%' align='left' colspan='3'>" + (bloqueiosDetranData.BLOQUEIODETRAN.PROCESSO_ANO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Número ofício</b> </td>";
                sData += "<td width = '75%' align='left' colspan='3'>" + (bloqueiosDetranData.BLOQUEIODETRAN.OFICIO_NUMERO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Ano do ofício</b> </td>";
                sData += "<td width = '75%' align='left' colspan='3'>" + (bloqueiosDetranData.BLOQUEIODETRAN.OFICIO_ANO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Munícipio do bloqueio</b> </td>";
                sData += "<td width = '75%' align='left' colspan='3'>" + (bloqueiosDetranData.BLOQUEIODETRAN.MUNICIPIO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width = '25%' align='right'><b>Motivo</b> </td>";
                sData += "<td width = '75%' align='left' colspan='3'>" + (bloqueiosDetranData.BLOQUEIODETRAN.MOTIVO ?? "-") + "</td>";
                sData += "</tr>";
                sData += "</table>";
                #endregion

                sData += "<br />";
            }
            
            if (debitosData != null)
            {
                #region "Detalhamento dos debitos"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; ' >";
                sData += "<tr bgcolor='#ACDBF4' >";
                sData += "<th width='40%'><b>Detalhamento dos débitos</b></th>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<th width='10%'></th>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td width='40%'><b>IPVA</b></td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='10%'><b>Total IPVA</b></td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td></td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td>" + debitosData.IPVA + " </td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td><b>IPVA divída ativa</b></td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td><b>Total IPVA divída ativa</b></td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td></td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td>" + debitosData.IPVA_DIVIDAATIVA + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td><b>DPVAT</b></td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td><b>Total DPVAT</b></td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td></td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td>" + debitosData.DPVAT + "</td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td><b>Multas</b>  </td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td><b>Total multas</b></td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td></td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td>" + debitosData.MULTAS + "</td>";
                sData += "</tr>";
                sData += "<td><b>Licenciamento</b>  </td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td><b>Total multas</b></td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td></td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td>" + debitosData.LICENCIAMENTO + "</td>";
                sData += "</tr>";
                sData += "<tr class='font-bold font-black'>";
                sData += "<td></td>";
                sData += "<td width='25%'>&nbsp;</td>";
                sData += "<td width='25%' align='right' style='color:red'>Total geral de débitos</td>";
                sData += "<td>" + debitosData.TotalDebitos + "</td>";
                sData += "</tr>";
                sData += "</table>";

                sData += "<br />";
                #endregion
            }

            if (debitosIPVAData != null && debitosIPVAData.Count() > 0)
            {

                #region "Detalhamento dos débitos - IPVA"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; '>";
                sData += "<tr bgcolor='#ACDBF4'>";
                sData += "<td colspan='5'><b>Detalhamento dos débitos - IPVA</b> </td>";
                sData += "</tr>";

                sData += "<tr bgcolor='#CDCCCB'>";
                sData += "<td width ='20%' ><b> Exercicio </ b ></td>";
                sData += "<td width ='15%' ><b> Cota </ b ></td>";
                sData += "<td width ='15%' ><b> Valor </ b ></td>";
                sData += "<td width ='20%' ><b> Data Vencimento </ b ></td>";
                sData += "<td width ='15%' ><b> Origem </ b ></td>";
                sData += "</tr>";

                foreach (var item in debitosIPVAData)
                {
                    sData += "<tr>";
                    sData += "<td width ='20%' ><b> " + item.ANOREF + " </ b ></td>";
                    sData += "<td width ='20%' ><b> " + item.COTAIPVADES + " </ b ></td>";
                    sData += "<td width ='20%' ><b> " + item.VALIPVA + " </ b ></td>";
                    sData += "<td width ='20%' ><b> " + FormataData(item.DTVENCTOIPVA) + "</ b ></td>";
                    sData += "<td width ='20%' ><b> " + item.ORIGEM + " </ b ></td>";
                    sData += "</tr>";
                }
                sData += "</table>";
                #endregion

                sData += "<br />";
            }
            else if (consultaAvancadaVeic.DetalhamentoIPVA)
            {
                #region "Detalhamento dos débitos - IPVA"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; '>";
                sData += "<tr bgcolor='#ACDBF4'>";
                sData += "<td colspan='5'><b>Detalhamento dos débitos - IPVA</b> </td>";
                sData += "</tr>";

                sData += "<tr>";
                sData += "<td colspan='5'>Não há débitos de IPVA para exibir</td>";
                sData += "</tr>";

                sData += "</table>";

                sData += "<br />";
                #endregion
            }

            if (debitosDPVATData != null && debitosDPVATData.Count() > 0)
            {
                #region "Detalhamento dos débitos - DPVAT"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; '>";
                sData += "<tr bgcolor='#ACDBF4'>";
                sData += "<td colspan='5'><b>Detalhamento dos débitos - DPVAT</b> </td>";
                sData += "</tr>";

                sData += "<tr bgcolor='#CDCCCB'>";
                sData += "<td width ='20%' ><b> Categoria </ b ></td>";
                sData += "<td width ='15%' ><b> Exercício </ b ></td>";
                sData += "<td width ='15%' ><b> Nro parcela </ b ></td>";
                sData += "<td width ='20%' ><b> Valor</ b ></td>";
                sData += "<td width ='15%' ><b> Data vencimento  </ b ></td>";
                sData += "</tr>";

                foreach (var item in debitosDPVATData)
                {
                    sData += "<tr>";
                    sData += "<td width ='20%' ><b> " + item.CATEGORIA + " </ b ></td>";
                    sData += "<td width ='20%' ><b> " + item.ANOREF + " </ b ></td>";
                    sData += "<td width ='20%' ><b> " + item.NUMPARCELA + " </ b ></td>";
                    sData += "<td width ='20%' ><b> " + item.VALDPVAT + "</ b ></td>";
                    sData += "<td width ='20%' ><b> " + FormataData(item.DTVENCTODPVAT) + " </ b ></td>";
                    sData += "</tr>";
                }
                sData += "</table>";
                #endregion

                sData += "<br />";
            }
            else if (consultaAvancadaVeic.DetalhamentoDPVAT)
            {
                #region "Detalhamento dos débitos - DPVAT"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; '>";
                sData += "<tr bgcolor='#ACDBF4'>";
                sData += "<td colspan='5'><b>Detalhamento dos débitos - DPVAT</b> </td>";
                sData += "</tr>";

                sData += "<tr>";
                sData += "<td colspan='5'>Não há débitos de DPVAT para exibir</td>";
                sData += "</tr>";

                sData += "</table>";

                sData += "<br />";
                #endregion
            }

            if (multasData != null && multasData.Count() > 0)
            {
                #region "Detalhamento das multas"
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; '>";
                sData += "<tr bgcolor='#ACDBF4'>";
                sData += "<td width= '20%'><b> Detalhamento das multas</b> </td>";
                sData += "<td width= '40%'><b></b></td>";
                sData += "<td width= '30%'><b></b></td>";
                sData += "<td width= '10%'><b></b></td>";
                sData += "</tr>";
                sData += "<tr bgcolor='#CDCCCB'>";
                sData += "<td width ='20%' ><b> AIIP </ b ></td>";
                sData += "<td width= '40%' ><b> Guia </ b ></td>";
                sData += "<td width= '30%' ><b> Dt.Hr. infracao </ b ></td>";
                sData += "<td width= '10%' ><b> Data vencimento</b></td>";
                sData += "</tr>";
                sData += "<tr bgcolor='#CDCCCB'>";
                sData += "<td width= '20%'><b>Valor</b></td>";
                sData += "<td width= '40%'><b>Enquadramento </ b ></td>";
                sData += "<td width= '30%'><b></ b ></td>";
                sData += "<td width= '10%'><b></ b ></td>";
                sData += "</tr>";
                sData += "<tr bgcolor='#CDCCCB'>";
                sData += "<td width= '20%'><b> Munícipio </ b ></td>";
                sData += "<td width= '40%'><b> Local </ b ></td>";
                sData += "<td width= '30%'><b> Orgão autuador</b></td>";
                sData += "<td width= '10%'><b>Tipo</b></td>";
                sData += "</tr>";
                sData += "</table>";

                for (int i = 0; i < multasData.Length; i++)
                {
                    sData += "<table>";
                    sData += "<tr >";
                    sData += "<td width= '20%'>" + multasData[i].NUMAIIP + "<br>" + multasData[i].VALMULTA + "<br>" + multasData[i].MUNINFRADESC + " </td>";
                    sData += "<td width= '50%'>" + multasData[i].NUMGUIA + "<br>" + multasData[i].ENQUADRAMDESC + "<br>" + multasData[i].LOCALINFRA + "</td>";
                    sData += "<td width= '20%'>" + multasData[i].DTINFRA + " " + multasData[i].HORAINFRA + "<br> <br> " + multasData[i].NOMEENTTRANSITANT + "</td>";
                    sData += "<td width= '20%'>" + multasData[i].DTVENCTOINFRA + "<br>" + multasData[i].NOTIFICACAO + "</td>";
                    sData += "</tr>";
                    sData += "</table>";
                    sData += "<hr width='100%'>";
                }
                #endregion
            }
            else if (consultaAvancadaVeic.DetalhamentoMultas)
            {
                sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; '>";
                sData += "<tr bgcolor='#ACDBF4'>";
                sData += "<td colspan='5'><b> Detalhamento das multas</b> </td>";
                sData += "</tr>";
                sData += "<tr>";
                sData += "<td colspan='5'><b>Nenhuma multa encontrada</b></td>";
                sData += "</tr>";
                sData += "</table>";
            }

            sData += "<br/>";
            sData += "<br/>";
            sData += "<br/>";
            sData += "<br/>";
            sData += "<br/>";

            #region "Dados requisitados na consulta"          
            sData += "<font size='1px'><b>Dados requisitados na consulta</b></font>";
            sData += "<hr width='100%'>";
            sData += "<table width='100%'>";

            if (!pesquisaCompleta)
            {
                sData += "<tr>";
                for (int i = 0; i < parametrosConsulta.Count; i++)
                {
                    if (i == 3 || i == 6 || i == 9)
                    {
                        sData += "</tr>";
                        sData += "<tr>";
                    }

                    sData += "<td><font size='1px'>" + parametrosConsulta[i] + "</font></td>";
                }

                var resto = parametrosConsulta.Count % 3;
                if (resto == 1)
                {
                    resto++;
                    var linhas = resto;
                    for (int i = 0; i < linhas; i++)
                    {
                        sData += "<td><font size='1px'></font></td>";
                    }
                }
                else if(resto == 2)
                {
                    resto--;
                    var linhas = resto;
                    for (int i = 0; i < linhas; i++)
                    {
                        sData += "<td><font size='1px'></font></td>";
                    }
                }
                sData += "</tr>";
            }
            else
            {
                sData += "<tr>";
                sData += "<td><font size='1'>" + "Pesquisa completa" + " </font></td>";
                sData += "</tr>";
            }
                      
            sData += "</table>";
            sData += "<hr width='100%'>";
            #endregion

            #endregion

            return sData;
        }

        public static string GenerateHtmlViewPDFCNH(CONSULTA1650 objCONSULTA1650)
        {

            var CondutorData = objCONSULTA1650.CONSULTAS.CONSULTA.RESULTADO.DADOSCONDUTOR;
            var CnhData = objCONSULTA1650.CONSULTAS.CONSULTA.RESULTADO.DADOSCNH;
            var ExamesData = objCONSULTA1650.CONSULTAS.CONSULTA.RESULTADO.EXAMES.EXAME;
            var CursosData = objCONSULTA1650.CONSULTAS.CONSULTA.RESULTADO.CURSOS.CURSO;
            var RecursosData = objCONSULTA1650.CONSULTAS.CONSULTA.RESULTADO.RECURSOS.RECURSO;
            var PontuacaoData = objCONSULTA1650.CONSULTAS.CONSULTA.RESULTADO.PONTUACAO;
            var InfracoesData = objCONSULTA1650.CONSULTAS.CONSULTA.RESULTADO.INFRACOES.INFRACAO;
            var BloqueiosData = objCONSULTA1650.CONSULTAS.CONSULTA.RESULTADO.BLOQUEIOS.BLOQUEIO;

            string sData = "";

            #region "Dados do motorista"


            //sData += "<table style='font-family:'Helvetica Neue', Helvetica, Arial, sans-serif; box-sizing: border-box; margin: 0;' >";
            sData += "<table>";
            sData += "<tr>";
            sData += "<td width = '25%' align='left'><h2>Consulta CNH</h2></td>";
            sData += "<td width = '20%' align='left'></td>";
            sData += "<td width = '20%' align='right'></td>";
            sData += "<td width = '30%' align='left'><img width='120' src='http://localhost:51506/assets/image-resources/logosgd.png' /></td>";
            sData += "</tr>";
            sData += "<tr bgcolor='#ACDBF4'>";
            sData += "<th><b>Dados do motorista</b></th>";
            sData += "<th><b></b></th>";
            sData += "<th><b></b></th>";
            sData += "<th><b></b></th>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "<td width = '25%' align='right'><b>Nome</b> </td>";
            sData += "<td width = '20%' align='left'>" + (CondutorData.NOME ?? "-") + "</td>";
            sData += "<td width = '20%' align='right'><b>CPF</b> </td>";
            sData += "<td width = '30%' align='left'>" + (CondutorData.CPF ?? "-") + "</td>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "<td width = '25%' align='right'><b>Data de nascimento</b> </td>";
            sData += "<td width = '20%' align='left'>" + (CondutorData.DATA_NASCIMENTO ?? "-") + "</td>";
            sData += "<td width = '20%' align='right'><b>UF</b> </td>";
            sData += "<td width = '30%' align='left'>" + (CondutorData.UF ?? "-") + "</td>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "<td width = '25%' align='right'><b>Nome do pai</b> </td>";
            sData += "<td width = '20%' align='left'>" + (CondutorData.NOME_PAI ?? "-") + "</td>";
            sData += "<td width = '20%' align='right'><b>Nome da mãe</b> </td>";
            sData += "<td width = '30%' align='left'>" + (CondutorData.NOME_MAE ?? "-") + "</td>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "</table>";
            #endregion


            #region "Dados da CNH"
            sData += "<table style='font - family: 'Helvetica Neue', Helvetica, Arial, sans - serif; box - sizing: border - box; margin: 0; ' >";

            sData += "<tr  bgcolor='#ACDBF4' >";
            sData += "<th><b>Dados da CNH</b></th>";
            sData += "<th><b></b></th>";
            sData += "<th><b></b></th>";
            sData += "<th><b></b></th>";
            sData += "</tr>";

            sData += "<tr>";
            sData += "<td width='25%' align='right'><b>Renach</b> </td>";
            sData += "<td width='20%' align='left'>" + (CnhData.NUMERO_RENACH ?? "-") + "</td>";
            sData += "<td width='20%' align='right'><b>Número do registro</b> </td>";
            sData += "<td width='30%' align='left'>" + (CnhData.NUMERO_REGISTRO ?? "-") + "</td>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "<td width='25%' align='right'><b>Categoria</b> </td>";
            sData += "<td width='20%' align='left'>" + (CnhData.CATEGORIA ?? "-") + "</td>";
            sData += "<td width='20%' align='right'><b>Data da emissão</b> </td>";
            sData += "<td width='30%' align='left'>" + (CnhData.DATA_EMISSAO ?? "-") + "</td>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "<td width='25%' align='right'><b>Data da emissão</b> </td>";
            sData += "<td width='20%' align='left'>" + (CnhData.DATA_EMISSAO ?? "-") + "</td>";
            sData += "<td width='20%' align='right'><b>Data de validade</b> </td>";
            sData += "<td width='30%' align='left'>" + (CnhData.DATA_VALIDADE ?? "-") + "</td>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "<td width='25%' align='right'><b>Data de validade</b> </td>";
            sData += "<td width='20%' align='left'>" + (CnhData.DATA_VALIDADE ?? "-") + "</td>";
            sData += "<td width='20%' align='right'><b>Data da primeira habilitação</b> </td>";
            sData += "<td width='30%' align='left'>" + (CnhData.DATA_1_HABILITACACAO ?? "-") + "</td>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "<td width='25%' align='right'><b>Impedimento</b> </td>";
            sData += "<td width='20%' align='left'>" + (CnhData.IMPEDIMENTO ?? "-") + "</td>";
            sData += "<td width='20%' align='right'><b>Portaria</b> </td>";
            sData += "<td width='30%' align='left'>" + (CnhData.PORTARIA ?? "-") + "</td>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "<td width='25%' align='right'><b>Observação</b> </td>";
            sData += "<td width = '75%' align='left' colspan='3'>" + (CnhData.OBSERVACAO ?? "-") + "</td>";
            sData += "</tr>";
            #endregion


            #region "Dados da pontuação"
            sData += "<tr bgcolor='#ACDBF4'>";
            sData += "<th><b>Dados da pontuação</b></th>";
            sData += "<th><b></b></th>";
            sData += "<th><b></b></th>";
            sData += "<th><b></b></th>";
            sData += "</tr>";

            sData += "<tr>";
            sData += "<td width='25%' align='right'><b>Efeito suspensivo</b> </td>";
            sData += "<td width='20%' align='left'>" + (PontuacaoData.EFEITO_SUSPENSIVO ?? "-") + "</td>";
            sData += "<td width='20%' align='right'><b>Efeito suspensivo judicial</b> </td>";
            sData += "<td width='30%' align='left'>" + (PontuacaoData.EFEITO_SUSPENSIVO_JUD ?? "-") + "</td>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "<td width='25%' align='right'><b>Recurso penal</b> </td>";
            sData += "<td width='20%' align='left'>" + (PontuacaoData.RECURSO_PENAL ?? "-") + "</td>";
            sData += "<td width='20%' align='right'><b>Prazo de defeza</b> </td>";
            sData += "<td width='30%' align='left'>" + (PontuacaoData.PRAZO_DEFESA ?? "-") + "</td>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "<td width='25%' align='right'><b>Informações pontuáveis últimos 5 anos</b> </td>";
            sData += "<td width='20%' align='left'>" + (PontuacaoData.INFRACOES_PONTUAVEIS_ULTIMOS_5_ANOS ?? "-") + "</td>";
            sData += "<td width='20%' align='right'><b>Informações pontuáveis últimos 12 meses</b> </td>";
            sData += "<td width='30%' align='left'>" + (PontuacaoData.INFRACOES_PONTUAVEIS_ULTIMOS_12_MESES ?? "-") + "</td>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "<td width='25%' align='right'><b>Informações mandatorias últimos 12 meses</b> </td>";
            sData += "<td width='20%' align='left'>" + (PontuacaoData.INFRACOES_MANDATORIA_ULTIMOS_12_MESES ?? "-") + "</td>";
            sData += "<td width='20%' align='right'><b>Informações mandatorias últimos 5 anos</b> </td>";
            sData += "<td width='30%' align='left'>" + (PontuacaoData.INFRACOES_MANDATORIA_ULTIMOS_5_ANOS ?? "-") + "</td>";
            sData += "</tr>";
            sData += "<tr>";
            sData += "<td width='25%' align='right'><b>Total de pontos</b> </td>";
            sData += "<td width = '75%' align='left' colspan='3'>" + (PontuacaoData.TOTAL_PONTO ?? "-") + "</td>";
            sData += "</tr>";
            #endregion

            #region "Dados dos Exames"
            if (ExamesData != null && ExamesData.Count > 0)
            {
                sData += "<tr bgcolor='#ACDBF4'>";
                sData += "<th><b>Exames</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";
                foreach (var x in ExamesData)
                {
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Descrição</b> </td>";
                    sData += "<td width='75%' align='left' colspan='3'>" + (x.DESCEXAME ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Catergoria pretendida</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.CATEGORIA_PRETENDIDA ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Catergoria permitida</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.CATEGORIA_PERMITIDA ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>CNPJ Entidade credênciada</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.CNPJ_ENTIDADE_CREDENCIADA ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>CPF profissional 1</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.CPF_PROFISSIONAL_1 ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>CPF profissional 2</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.CPF_PROFISSIONAL_2 ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Data do exame</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.DATA_EXAME ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Data de validade</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.DATA_VALIDADE ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Munícipio do exame</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.MUNICIPIO_EXAME ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Observação</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.OBSERVACAO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Restrições</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.RESTRICOES_X_TEXTO ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Resultado</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.RESULTADO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Tipo de atualização</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.TIPO_DE_ATUALIZACAO ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width = '25%' align='left'></td>";
                    sData += "<td width = '20%' align='left'></td>";
                    sData += "<td width = '20%' align='right'></td>";
                    sData += "<td width = '30%' align='left'></td>";
                    sData += "</tr>";
                }
            }
            #endregion

            #region "Dados dos Cursos"
            if (CursosData != null && CursosData.Count > 0)
            {
                sData += "<tr bgcolor='#ACDBF4'>";
                sData += "<th><b>Cursos</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";
                foreach (var x in CursosData)
                {
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Descrição</b> </td>";
                    sData += "<td width='75%' align='left' colspan='3'>" + (x.CURSODESC ?? "-") + " </td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Carga horária</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.CARGA_HORARIA ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Catergoria</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.CATEGORIA ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>CNPJ Entidade credênciada</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.CNPJ_ENTIDADE_CREDENCIADA ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>CPF profissional</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.CPF_PROFISSIONAL ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Data do inicio do curso</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.DATA_INICIO_CURSO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Data fim do curso</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.DATA_FIM_CURSO ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Data de validade</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.DATA_VALIDADE ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Modalidade</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.MODALIDADE ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Munícipio do curso</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.MUNICIPIO_CURSO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Número do certificado</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.NUMERO_CERTIFICADO ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>UF</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.UF_CURSO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Tipo de atualização</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.TIPO_DE_ATUALIZACAO ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width = '25%' align='left'></td>";
                    sData += "<td width = '20%' align='left'></td>";
                    sData += "<td width = '20%' align='right'></td>";
                    sData += "<td width = '30%' align='left'></td>";
                    sData += "</tr>";
                }
            }
            #endregion

            #region "Dados dos Cursos"
            if (InfracoesData != null && InfracoesData.Count > 0)
            {
                sData += "<tr bgcolor='#ACDBF4'>";
                sData += "<th><b>Infrações</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";
                foreach (var x in InfracoesData)
                {
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Descrição</b> </td>";
                    sData += "<td width='75%' align='left' colspan='3'>" + (x.DESCRICAO ?? "-") + " </td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Número do auto</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.NUMERO_AUTO ?? "-") + " </td>";
                    sData += "<td width='20%' align='right'><b>Orgão autuador</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.ORGAO_AUTUADOR ?? "-") + "</td>";
                    sData += "</tr>";

                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Valor</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.VALOR ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Data da infração</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.DATA_INFRACAO ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Placa</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.PLACA ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Local</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.LOCAL ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Pontuação</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.PONTUACAO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Numero da multa</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.NUMERO_MULTA ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Processo</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.PROCESSO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Situação</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.SITUACAO ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Responsável pelos pontos</b> </td>";
                    sData += "<td width='75%' align='left' colspan='3'>" + (x.RESPONSAVEL_PONTOS ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width = '25%' align='left'></td>";
                    sData += "<td width = '20%' align='left'></td>";
                    sData += "<td width = '20%' align='right'></td>";
                    sData += "<td width = '30%' align='left'></td>";
                    sData += "</tr>";
                }
            }
            #endregion

            #region "Dados dos bloqueios"
            if (BloqueiosData != null && BloqueiosData.Count > 0)
            {
                sData += "<tr bgcolor='#ACDBF4'>";
                sData += "<th><b>Bloqueios</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";
                foreach (var x in BloqueiosData)
                {
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Descrição</b> </td>";
                    sData += "<td width='75%' align='left' colspan='4'>" + (x.DESCRICAO ?? "-") + " </td>";
                    sData += "<tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Data do inicio do bloqueio</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.DATA_INI_PENALIDADE_BLOQ ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Data do fim do bloqueio</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.DATA_FIM_PENALIDADE_BLOQ ?? "-") + "</td>";
                    sData += "<tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Data do bloqueio</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.DATA_BLOQUEIO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Data da liberação do bloqueio</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.DATA_LIBERACAO_BLOQUEIO ?? "-") + "</td>";
                    sData += "<tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Documento gerador da liberação</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.DOC_GERADOR_LIBERACAO_BLOQ ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Documento gerador do bloqueio</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.DOCUMENTO_GERADOR_BLOQ ?? "-") + "</td>";
                    sData += "<tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Motivo do bloqueio</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.MOTIVO_BLOQUEIO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Motivo da liberação do bloqueio</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.MOTIVO_LIBERACAO_BLOQUEIO ?? "-") + "</td>";
                    sData += "<tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Orgão responsável pela liberação </b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.ORGAO_RESP_LIBERACAO_BLOQ ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Orgão responsável</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.ORGAO_RESPONSAVEL ?? "-") + "</td>";
                    sData += "<tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Prazo da penalidade</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.PRAZO_PENALIDADE ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Prazo total da penalidade</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.PRAZO_PENALIDADE_TOTAL ?? "-") + "</td>";
                    sData += "<tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Recolhimento de CNH</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.RECOLHIMENTO_CNH ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Requisitos de liberação</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.REQUISITOS_LIBERACAO ?? "-") + "</td>";
                    sData += "<tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Tipo de atualização</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.TIPO_ATUALIZACAO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Tipo de decisão de bloqueio</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.TIPO_DECISAO_BLOQUEIO ?? "-") + "</td>";
                    sData += "<tr>";
                    sData += "<tr>";
                    sData += "<td width = '25%' align='left'></td>";
                    sData += "<td width = '20%' align='left'></td>";
                    sData += "<td width = '20%' align='right'></td>";
                    sData += "<td width = '30%' align='left'></td>";
                    sData += "</tr>";
                }
            }
            #endregion

            #region "Dados dos recursos"
            if (RecursosData != null && RecursosData.Count > 0)
            {
                sData += "<tr  bgcolor='#ACDBF4' >";
                sData += "<th><b>Recursos</b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "<th><b></b></th>";
                sData += "</tr>";
                foreach (var x in RecursosData)
                {
                    sData += "<tr>";
                    sData += "<td width='25%' align='center' colspan='3'> <h4><font color='black'><b>Veiculo</b></font></h4></td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Marca</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.VEICULO.MARCA_DESC ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Tipo</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.VEICULO.TIPO_DESC ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Especie</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.VEICULO.ESPECIE_DESC ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Categoria</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.VEICULO.CATEGORIA_DESC ?? "-") + "</td>";
                    sData += "</tr>";

                    sData += "<tr>";
                    sData += "<td width='25%' align='center' colspan='4'><h4><font color='black'><b>Recursos da infração</b></font></h4></td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Orgão autuador</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.INFRACAO_RECURSO.ORGAO_AUTUADOR ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Data da infração</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.INFRACAO_RECURSO.DATA_INFRACAO ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Descrição</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.INFRACAO_RECURSO.DESCRICAO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Enquadramento</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.INFRACAO_RECURSO.ENQUADRAMENTO ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Responsável pelos pontos</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.INFRACAO_RECURSO.RESPONSAVEL_PONTOS ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Situação</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.INFRACAO_RECURSO.SITUACAO ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Vencimento</b> </td>";
                    sData += "<td width='75%' align='left' colspan='4'>" + (x.INFRACAO_RECURSO.VENCIMENTO ?? "-") + "</td>";
                    sData += "</tr>";

                    sData += "<tr>";
                    sData += "<td width='25%' align='center' colspan='4'><h4><font color='black'><b>Dados do processo</b></font></h4></td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Número do processo</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.DADOS_PROCESSO.NUME_ROPROCESSO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Data do processo</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.DADOS_PROCESSO.DATA_PROCESSO ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Situação</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.DADOS_PROCESSO.SITUACAO ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Prazo de suspensão</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.DADOS_PROCESSO.PRAZO_SUSPENSAO ?? "-") + "</td>";
                    sData += "</tr>";
                    sData += "<tr>";
                    sData += "<td width='25%' align='right'><b>Total de multas</b> </td>";
                    sData += "<td width='20%' align='left'>" + (x.DADOS_PROCESSO.TOTAL_MULTAS ?? "-") + "</td>";
                    sData += "<td width='20%' align='right'><b>Total de pontos</b> </td>";
                    sData += "<td width='30%' align='left'>" + (x.DADOS_PROCESSO.TOTAL_PONTOS ?? "-") + "</td>";
                    sData += "</tr>";

                    if (x.EVENTOS != null)
                    {
                        if (x.EVENTOS.EVENTO != null && x.EVENTOS.EVENTO.Count > 0)
                        {
                            sData += "<tr>";
                            sData += "<td width='25%' align='center' colspan='4'><h4><font color='black'><b>Eventos</b></font></h4></td>";
                            sData += "</tr>";
                            foreach (var e in x.EVENTOS.EVENTO)
                            {
                                sData += "<tr>";
                                sData += "<td width='25%' align='right'><b>Descrição</b> </td>";
                                sData += "<td width='75%' align='left' colspan='4'>" + (e.DESCRICAO ?? "-") + " </td>";
                                sData += "</tr>";
                                sData += "<tr>";
                                sData += "<td width='25%' align='right'><b>Data</b> </td>";
                                sData += "<td width='20%' align='left'>" + (e.DATA ?? "-") + "</td>";
                                sData += "<td width='20%' align='right'><b>Entregue</b> </td>";
                                sData += "<td width='30%' align='left'>" + (e.ENTREGUE ?? "-") + "</td>";
                                sData += "</tr>";
                                sData += "<tr>";
                                sData += "<td width='25%' align='right'><b>Motivo</b> </td>";
                                sData += "<td width='20%' align='left'>" + (e.MOTIVO ?? "-") + "</td>";
                                sData += "<td width='20%' align='right'><b>Penalidade</b> </td>";
                                sData += "<td width='30%' align='left'>" + (e.PENALIDADE ?? "-") + "</td>";
                                sData += "</tr>";
                                sData += "<tr>";
                                sData += "<td width='25%' align='right'><b>Resultado</b> </td>";
                                sData += "<td width='75%' align='left' colspan='4'>" + (e.RESULTADO ?? "-") + " </td>";
                                sData += "</tr>";
                                sData += "<tr>";
                                sData += "<td width = '25%' align='left'></td>";
                                sData += "<td width = '20%' align='left'></td>";
                                sData += "<td width = '20%' align='right'></td>";
                                sData += "<td width = '30%' align='left'></td>";
                                sData += "</tr>";
                            }
                        }
                    }
                    sData += "<tr>";
                    sData += "<td width = '25%' align='left'></td>";
                    sData += "<td width = '20%' align='left'></td>";
                    sData += "<td width = '20%' align='right'></td>";
                    sData += "<td width = '30%' align='left'></td>";
                    sData += "</tr>";
                }
            }
            #endregion

            sData += "</table>";

            return sData;
        }

        private static string FormataData(string data)
        {
            if (data == null)
            {
                return "";
            }
            if (data.Length != 8)
                return data;

            return string.Format("{0}/{1}/{2}", data.Substring(0, 2), data.Substring(2, 2), data.Substring(4, 4));
        }
    }

    public class PdfEventHandler : iTextSharp.text.pdf.PdfPageEventHelper
    {
        protected PdfTemplate total;
        protected BaseFont helv;

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            total = writer.DirectContent.CreateTemplate(100, 100);
            total.BoundingBox = new Rectangle(-20, -20, 100, 100);

            helv = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            PdfContentByte cb = writer.DirectContent;
            cb.SaveState();
            string text = writer.PageNumber.ToString().Trim() + "/"+ total.ToString().Trim();
            float textBase = document.Bottom - 15;
            float textSize = 10;
            cb.BeginText();
            cb.SetFontAndSize(helv, 10);
            if ((writer.PageNumber % 2) == 1)
            {
                cb.SetTextMatrix(document.Right, textBase);
                cb.ShowText(text);
                cb.EndText();
                cb.AddTemplate(total, document.Right + textSize, textBase);
            }
            else
            {
                float adjust = helv.GetWidthPoint("0", 10);
                cb.SetTextMatrix(document.Right - textSize - adjust, textBase);
                cb.ShowText(text);
                cb.EndText();
                cb.AddTemplate(total, document.Right - adjust, textBase);
            }
            cb.RestoreState();
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            total.BeginText();
            total.SetFontAndSize(helv, 10);
            total.SetTextMatrix(0, 0);
            int pageNumber = writer.PageNumber - 1;
            total.ShowText(Convert.ToString(pageNumber));
            total.EndText();
        }

    }
}
