﻿using EntityLayer.Models;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JConsultas.Models
{
    public class EmailHelper
    {
        private IOptions<ApplicationSettings> _settings;
        static string sSMTPName = "";
        public EmailHelper(IOptions<ApplicationSettings> settings)
        {
            _settings = settings;
            sSMTPName = settings.Value.SMTPName;
        }

        public bool SendEmail(string to, string subject, string content)
        {
            try
            {
                string snm = sSMTPName.ToString();
                string from = "";
                using (var client = new SmtpClient())
                {
                    MimeMessage mes = new MimeMessage();
                    mes.From.Add(new MailboxAddress(from));
                    mes.To.Add(new MailboxAddress(to));
                    mes.Subject = subject;
                    mes.Body = new TextPart("html")
                    {
                        Text = content
                    };

                    client.Connect("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
                    client.Authenticate("connectusdemo12@gmail.com ", "connectus@infoway");
                    client.Send(mes);
                    client.Disconnect(true);
                }

                //string from = "";
                ////string to = "vipulpithiya5619@gmail.com";
                ////string subject = "test";
                ////string content = "<h1> test </h1>";
                ////string from = "";


                //MimeMessage message = new MimeMessage();
                //message.Subject = subject;
                //message.Body = new TextPart("html") { Text = content };
                ////message.Body = content;
                //message.From.Add(new MailboxAddress(from));
                //message.To.Add(new MailboxAddress(to));


                //SmtpClient smtp = new SmtpClient();
                //smtp.Connect(
                //      "smtp.gmail.com"
                //    , 587
                //    , MailKit.Security.SecureSocketOptions.StartTls
                //);
                //smtp.Authenticate("kanjilala2016@gmail.com", "kanjilala2016@2016");
                //smtp.Send(message);
                //smtp.Disconnect(true);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
