﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace JConsultas.Models
{
    [XmlRoot(ElementName = "LOGIN")]
    public class LOGIN
    {
        [XmlAttribute(AttributeName = "RETORNO")]
        public string RETORNO { get; set; }
        [XmlAttribute(AttributeName = "DESCRICAO")]
        public string DESCRICAO { get; set; }
    }

    [XmlRoot(ElementName = "DADOSCONDUTOR")]
    public class DADOSCONDUTOR
    {
        [XmlElement(ElementName = "NOME")]
        public string NOME { get; set; }
        [XmlElement(ElementName = "CPF")]
        public string CPF { get; set; }
        [XmlElement(ElementName = "DATA_NASCIMENTO")]
        public string DATA_NASCIMENTO { get; set; }
        [XmlElement(ElementName = "UF")]
        public string UF { get; set; }
        [XmlElement(ElementName = "NOME_PAI")]
        public string NOME_PAI { get; set; }
        [XmlElement(ElementName = "NOME_MAE")]
        public string NOME_MAE { get; set; }
    }

    [XmlRoot(ElementName = "DADOSCNH")]
    public class DADOSCNH
    {
        [XmlElement(ElementName = "NUMERO_RENACH")]
        public string NUMERO_RENACH { get; set; }
        [XmlElement(ElementName = "NUMERO_REGISTRO")]
        public string NUMERO_REGISTRO { get; set; }
        [XmlElement(ElementName = "CATEGORIA")]
        public string CATEGORIA { get; set; }
        [XmlElement(ElementName = "DATA_EMISSAO")]
        public string DATA_EMISSAO { get; set; }
        [XmlElement(ElementName = "DATA_VALIDADE")]
        public string DATA_VALIDADE { get; set; }
        [XmlElement(ElementName = "DATA_1_HABILITACACAO")]
        public string DATA_1_HABILITACACAO { get; set; }
        [XmlElement(ElementName = "OBSERVACAO")]
        public string OBSERVACAO { get; set; }
        [XmlElement(ElementName = "PORTARIA")]
        public string PORTARIA { get; set; }
        [XmlElement(ElementName = "IMPEDIMENTO")]
        public string IMPEDIMENTO { get; set; }
    }

    [XmlRoot(ElementName = "PONTUACAO")]
    public class PONTUACAO
    {
        [XmlElement(ElementName = "EFEITO_SUSPENSIVO")]
        public string EFEITO_SUSPENSIVO { get; set; }
        [XmlElement(ElementName = "EFEITO_SUSPENSIVO_JUD")]
        public string EFEITO_SUSPENSIVO_JUD { get; set; }
        [XmlElement(ElementName = "RECURSO_PENAL")]
        public string RECURSO_PENAL { get; set; }
        [XmlElement(ElementName = "PRAZO_DEFESA")]
        public string PRAZO_DEFESA { get; set; }
        [XmlElement(ElementName = "INFRACOES_PONTUAVEIS_ULTIMOS_5_ANOS")]
        public string INFRACOES_PONTUAVEIS_ULTIMOS_5_ANOS { get; set; }
        [XmlElement(ElementName = "INFRACOES_PONTUAVEIS_ULTIMOS_12_MESES")]
        public string INFRACOES_PONTUAVEIS_ULTIMOS_12_MESES { get; set; }
        [XmlElement(ElementName = "INFRACOES_MANDATORIA_ULTIMOS_12_MESES")]
        public string INFRACOES_MANDATORIA_ULTIMOS_12_MESES { get; set; }
        [XmlElement(ElementName = "INFRACOES_MANDATORIA_ULTIMOS_5_ANOS")]
        public string INFRACOES_MANDATORIA_ULTIMOS_5_ANOS { get; set; }
        [XmlElement(ElementName = "TOTAL_PONTO")]
        public string TOTAL_PONTO { get; set; }
    }

    [XmlRoot(ElementName = "INFRACAO")]
    public class INFRACAO
    {
        [XmlElement(ElementName = "NUMERO_AUTO")]
        public string NUMERO_AUTO { get; set; }
        [XmlElement(ElementName = "ORGAO_AUTUADOR")]
        public string ORGAO_AUTUADOR { get; set; }
        [XmlElement(ElementName = "DATA_INFRACAO")]
        public string DATA_INFRACAO { get; set; }
        [XmlElement(ElementName = "DESCRICAO")]
        public string DESCRICAO { get; set; }
        [XmlElement(ElementName = "PLACA")]
        public string PLACA { get; set; }
        [XmlElement(ElementName = "LOCAL")]
        public string LOCAL { get; set; }
        [XmlElement(ElementName = "PONTUACAO")]
        public string PONTUACAO { get; set; }
        [XmlElement(ElementName = "NUMERO_MULTA")]
        public string NUMERO_MULTA { get; set; }
        [XmlElement(ElementName = "PROCESSO")]
        public string PROCESSO { get; set; }
        [XmlElement(ElementName = "SITUACAO")]
        public string SITUACAO { get; set; }
        [XmlElement(ElementName = "RESPONSAVEL_PONTOS")]
        public string RESPONSAVEL_PONTOS { get; set; }
        [XmlElement(ElementName = "VALOR")]
        public string VALOR { get; set; }
    }

    [XmlRoot(ElementName = "INFRACOES")]
    public class INFRACOES
    {
        [XmlElement(ElementName = "INFRACAO")]
        public List<INFRACAO> INFRACAO { get; set; }
    }

    [XmlRoot(ElementName = "EXAME")]
    public class EXAME
    {
        [XmlElement(ElementName = "DESCEXAME")]
        public string DESCEXAME { get; set; }
        [XmlElement(ElementName = "CATEGORIA_PERMITIDA")]
        public string CATEGORIA_PERMITIDA { get; set; }
        [XmlElement(ElementName = "CATEGORIA_PRETENDIDA")]
        public string CATEGORIA_PRETENDIDA { get; set; }
        [XmlElement(ElementName = "CNPJ_ENTIDADE_CREDENCIADA")]
        public string CNPJ_ENTIDADE_CREDENCIADA { get; set; }
        [XmlElement(ElementName = "CPF_PROFISSIONAL_1")]
        public string CPF_PROFISSIONAL_1 { get; set; }
        [XmlElement(ElementName = "CPF_PROFISSIONAL_2")]
        public string CPF_PROFISSIONAL_2 { get; set; }
        [XmlElement(ElementName = "DATA_EXAME")]
        public string DATA_EXAME { get; set; }
        [XmlElement(ElementName = "DATA_VALIDADE")]
        public string DATA_VALIDADE { get; set; }
        [XmlElement(ElementName = "MUNICIPIO_EXAME")]
        public string MUNICIPIO_EXAME { get; set; }
        [XmlElement(ElementName = "OBSERVACAO")]
        public string OBSERVACAO { get; set; }
        [XmlElement(ElementName = "RESTRICOES_X_TEXTO")]
        public string RESTRICOES_X_TEXTO { get; set; }
        [XmlElement(ElementName = "RESULTADO")]
        public string RESULTADO { get; set; }
        [XmlElement(ElementName = "TIPO_DE_ATUALIZACAO")]
        public string TIPO_DE_ATUALIZACAO { get; set; }
        [XmlElement(ElementName = "UF_EXAME")]
        public string UF_EXAME { get; set; }
    }

    [XmlRoot(ElementName = "EXAMES")]
    public class EXAMES
    {
        [XmlElement(ElementName = "EXAME")]
        public List<EXAME> EXAME { get; set; }
    }

    [XmlRoot(ElementName = "CURSO")]
    public class CURSO
    {
        [XmlElement(ElementName = "CURSODESC")]
        public string CURSODESC { get; set; }
        [XmlElement(ElementName = "CARGA_HORARIA")]
        public string CARGA_HORARIA { get; set; }
        [XmlElement(ElementName = "CATEGORIA")]
        public string CATEGORIA { get; set; }
        [XmlElement(ElementName = "CNPJ_ENTIDADE_CREDENCIADA")]
        public string CNPJ_ENTIDADE_CREDENCIADA { get; set; }
        [XmlElement(ElementName = "CPF_PROFISSIONAL")]
        public string CPF_PROFISSIONAL { get; set; }
        [XmlElement(ElementName = "DATA_INICIO_CURSO")]
        public string DATA_INICIO_CURSO { get; set; }
        [XmlElement(ElementName = "DATA_FIM_CURSO")]
        public string DATA_FIM_CURSO { get; set; }
        [XmlElement(ElementName = "DATA_VALIDADE")]
        public string DATA_VALIDADE { get; set; }
        [XmlElement(ElementName = "MODALIDADE")]
        public string MODALIDADE { get; set; }
        [XmlElement(ElementName = "MUNICIPIO_CURSO")]
        public string MUNICIPIO_CURSO { get; set; }
        [XmlElement(ElementName = "NUMERO_CERTIFICADO")]
        public string NUMERO_CERTIFICADO { get; set; }
        [XmlElement(ElementName = "TIPO_DE_ATUALIZACAO")]
        public string TIPO_DE_ATUALIZACAO { get; set; }
        [XmlElement(ElementName = "UF_CURSO")]
        public string UF_CURSO { get; set; }
    }

    [XmlRoot(ElementName = "CURSOS")]
    public class CURSOS
    {
        [XmlElement(ElementName = "CURSO")]
        public List<CURSO> CURSO { get; set; }
    }

    [XmlRoot(ElementName = "VEICULO")]
    public class VEICULO
    {
        [XmlElement(ElementName = "MARCA_DESC")]
        public string MARCA_DESC { get; set; }
        [XmlElement(ElementName = "TIPO_DESC")]
        public string TIPO_DESC { get; set; }
        [XmlElement(ElementName = "ESPECIE_DESC")]
        public string ESPECIE_DESC { get; set; }
        [XmlElement(ElementName = "CATEGORIA_DESC")]
        public string CATEGORIA_DESC { get; set; }
    }

    [XmlRoot(ElementName = "INFRACAO_RECURSO")]
    public class INFRACAO_RECURSO
    {
        [XmlElement(ElementName = "ORGAO_AUTUADOR")]
        public string ORGAO_AUTUADOR { get; set; }
        [XmlElement(ElementName = "DATA_INFRACAO")]
        public string DATA_INFRACAO { get; set; }
        [XmlElement(ElementName = "DESCRICAO")]
        public string DESCRICAO { get; set; }
        [XmlElement(ElementName = "ENQUADRAMENTO")]
        public string ENQUADRAMENTO { get; set; }
        [XmlElement(ElementName = "RESPONSAVEL_PONTOS")]
        public string RESPONSAVEL_PONTOS { get; set; }
        [XmlElement(ElementName = "SITUACAO")]
        public string SITUACAO { get; set; }
        [XmlElement(ElementName = "VENCIMENTO")]
        public string VENCIMENTO { get; set; }
    }

    [XmlRoot(ElementName = "DADOS_PROCESSO")]
    public class DADOS_PROCESSO
    {
        [XmlElement(ElementName = "NUME_ROPROCESSO")]
        public string NUME_ROPROCESSO { get; set; }
        [XmlElement(ElementName = "DATA_PROCESSO")]
        public string DATA_PROCESSO { get; set; }
        [XmlElement(ElementName = "SITUACAO")]
        public string SITUACAO { get; set; }
        [XmlElement(ElementName = "PRAZO_SUSPENSAO")]
        public string PRAZO_SUSPENSAO { get; set; }
        [XmlElement(ElementName = "TOTAL_MULTAS")]
        public string TOTAL_MULTAS { get; set; }
        [XmlElement(ElementName = "TOTAL_PONTOS")]
        public string TOTAL_PONTOS { get; set; }
    }

    [XmlRoot(ElementName = "EVENTO")]
    public class EVENTO
    {
        [XmlElement(ElementName = "DATA")]
        public string DATA { get; set; }
        [XmlElement(ElementName = "DESCRICAO")]
        public string DESCRICAO { get; set; }
        [XmlElement(ElementName = "ENTREGUE")]
        public string ENTREGUE { get; set; }
        [XmlElement(ElementName = "MOTIVO")]
        public string MOTIVO { get; set; }
        [XmlElement(ElementName = "PENALIDADE")]
        public string PENALIDADE { get; set; }
        [XmlElement(ElementName = "RESULTADO")]
        public string RESULTADO { get; set; }
    }

    [XmlRoot(ElementName = "EVENTOS")]
    public class EVENTOS
    {
        [XmlElement(ElementName = "EVENTO")]
        public List<EVENTO> EVENTO { get; set; }
    }

    [XmlRoot(ElementName = "RECURSO")]
    public class RECURSO
    {
        [XmlElement(ElementName = "VEICULO")]
        public VEICULO VEICULO { get; set; }
        [XmlElement(ElementName = "INFRACAO_RECURSO")]
        public INFRACAO_RECURSO INFRACAO_RECURSO { get; set; }
        [XmlElement(ElementName = "DADOS_PROCESSO")]
        public DADOS_PROCESSO DADOS_PROCESSO { get; set; }
        [XmlElement(ElementName = "EVENTOS")]
        public EVENTOS EVENTOS { get; set; }
    }

    [XmlRoot(ElementName = "RECURSOS")]
    public class RECURSOS
    {
        [XmlElement(ElementName = "RECURSO")]
        public List<RECURSO> RECURSO { get; set; }
    }

    [XmlRoot(ElementName = "BLOQUEIO")]
    public class BLOQUEIO
    {
        [XmlElement(ElementName = "DESCRICAO")]
        public string DESCRICAO { get; set; }
        [XmlElement(ElementName = "DATA_BLOQUEIO")]
        public string DATA_BLOQUEIO { get; set; }
        [XmlElement(ElementName = "DATA_FIM_PENALIDADE_BLOQ")]
        public string DATA_FIM_PENALIDADE_BLOQ { get; set; }
        [XmlElement(ElementName = "DATA_INI_PENALIDADE_BLOQ")]
        public string DATA_INI_PENALIDADE_BLOQ { get; set; }
        [XmlElement(ElementName = "DATA_LIBERACAO_BLOQUEIO")]
        public string DATA_LIBERACAO_BLOQUEIO { get; set; }
        [XmlElement(ElementName = "DOC_GERADOR_LIBERACAO_BLOQ")]
        public string DOC_GERADOR_LIBERACAO_BLOQ { get; set; }
        [XmlElement(ElementName = "DOCUMENTO_GERADOR_BLOQ")]
        public string DOCUMENTO_GERADOR_BLOQ { get; set; }
        [XmlElement(ElementName = "MOTIVO_BLOQUEIO")]
        public string MOTIVO_BLOQUEIO { get; set; }
        [XmlElement(ElementName = "MOTIVO_LIBERACAO_BLOQUEIO")]
        public string MOTIVO_LIBERACAO_BLOQUEIO { get; set; }
        [XmlElement(ElementName = "ORGAO_RESP_LIBERACAO_BLOQ")]
        public string ORGAO_RESP_LIBERACAO_BLOQ { get; set; }
        [XmlElement(ElementName = "ORGAO_RESPONSAVEL")]
        public string ORGAO_RESPONSAVEL { get; set; }
        [XmlElement(ElementName = "PRAZO_PENALIDADE")]
        public string PRAZO_PENALIDADE { get; set; }
        [XmlElement(ElementName = "PRAZO_PENALIDADE_TOTAL")]
        public string PRAZO_PENALIDADE_TOTAL { get; set; }
        [XmlElement(ElementName = "RECOLHIMENTO_CNH")]
        public string RECOLHIMENTO_CNH { get; set; }
        [XmlElement(ElementName = "REQUISITOS_LIBERACAO")]
        public string REQUISITOS_LIBERACAO { get; set; }
        [XmlElement(ElementName = "TIPO_ATUALIZACAO")]
        public string TIPO_ATUALIZACAO { get; set; }
        [XmlElement(ElementName = "TIPO_DECISAO_BLOQUEIO")]
        public string TIPO_DECISAO_BLOQUEIO { get; set; }
        [XmlElement(ElementName = "UF")]
        public string UF { get; set; }
        [XmlElement(ElementName = "UF_DETRAN_LIBERACAO_BLOQ")]
        public string UF_DETRAN_LIBERACAO_BLOQ { get; set; }
    }

    [XmlRoot(ElementName = "BLOQUEIOS")]
    public class BLOQUEIOS
    {
        [XmlElement(ElementName = "BLOQUEIO")]
        public List<BLOQUEIO> BLOQUEIO { get; set; }
    }

    [XmlRoot(ElementName = "RESULTADO")]
    public class RESULTADO
    {
        [XmlElement(ElementName = "DADOSCONDUTOR")]
        public DADOSCONDUTOR DADOSCONDUTOR { get; set; }
        [XmlElement(ElementName = "DADOSCNH")]
        public DADOSCNH DADOSCNH { get; set; }
        [XmlElement(ElementName = "PONTUACAO")]
        public PONTUACAO PONTUACAO { get; set; }
        [XmlElement(ElementName = "INFRACOES")]
        public INFRACOES INFRACOES { get; set; }
        [XmlElement(ElementName = "EXAMES")]
        public EXAMES EXAMES { get; set; }
        [XmlElement(ElementName = "CURSOS")]
        public CURSOS CURSOS { get; set; }
        [XmlElement(ElementName = "RECURSOS")]
        public RECURSOS RECURSOS { get; set; }
        [XmlElement(ElementName = "BLOQUEIOS")]
        public BLOQUEIOS BLOQUEIOS { get; set; }
    }

    [XmlRoot(ElementName = "CONSULTA")]
    public class CONSULTA
    {
        [XmlElement(ElementName = "RETORNOEXECUCAO")]
        public string RETORNOEXECUCAO { get; set; }
        [XmlElement(ElementName = "DESCRICAO")]
        public string DESCRICAO { get; set; }
        [XmlElement(ElementName = "RESULTADO")]
        public RESULTADO RESULTADO { get; set; }
        [XmlElement(ElementName = "IDLOGWS")]
        public string IDLOGWS { get; set; }
        [XmlAttribute(AttributeName = "Num")]
        public string Num { get; set; }
    }

    [XmlRoot(ElementName = "CONSULTAS")]
    public class CONSULTAS
    {
        [XmlElement(ElementName = "CONSULTA")]
        public CONSULTA CONSULTA { get; set; }
    }

    [XmlRoot(ElementName = "CONSULTA1650")]
    public class CONSULTA1650
    {
        [XmlElement(ElementName = "LOGIN")]
        public LOGIN LOGIN { get; set; }
        [XmlElement(ElementName = "CONSULTAS")]
        public CONSULTAS CONSULTAS { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }
}



